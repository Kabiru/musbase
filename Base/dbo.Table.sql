﻿CREATE TABLE [dbo].[Table]
(
	[Admin_No] INT NOT NULL PRIMARY KEY, 
    [FName] VARCHAR(50) NOT NULL, 
    [MName] VARCHAR(50) NULL, 
    [LName] VARCHAR(50) NOT NULL, 
    [Email] VARCHAR(50) NULL, 
    [Phone] INT NULL
)
