﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MusBase
{
    public partial class frmrptChoirGrade : Form
    {
        public frmrptChoirGrade()
        {
            InitializeComponent();
        }

        private void frmrptChoirGrade_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'DataSet2.Choir_Academic_View' table. You can move, or remove it, as needed.
            this.Choir_Academic_ViewTableAdapter.Fill(this.DataSet2.Choir_Academic_View);

            this.reportViewer1.RefreshReport();
        }
    }
}
