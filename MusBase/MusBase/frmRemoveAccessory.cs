﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace MusBase
{
    public partial class frmRemoveAccessory : Form
    {
        public frmRemoveAccessory()
        {
            InitializeComponent();
            fillcboAccCode();
            loadAccessoryDetails();

            btnRmvAccessory.Enabled = false;
        }

        private void loadAccessoryDetails() 
        {
            string connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;            
            string query = "Load_Accessory_Details_Procedure";

            SqlConnection conn = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            try 
            {
                SqlDataAdapter dAdapter = new SqlDataAdapter(cmd);
                DataTable AccessoryDetails = new DataTable();
                //fill datatable
                dAdapter.Fill(AccessoryDetails);
                //sync with dgv
                BindingSource bSource = new BindingSource();
                bSource.DataSource = AccessoryDetails;
                dgvRemoveAccessory.DataSource = bSource;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void loadAccessoryDetailsAccCode() 
        {
            string connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;
            string sqlQuery = "SELECT Accessory_Details.Accessory_Code AS [CODE], Accessory_Details.Name AS [NAME], Accessory_Details.Accessory_Type " +
            "AS [TYPE], Accessory_Details.Size AS [SIZE], Accessory_Details.Condition AS [CONDITION] FROM Accessory_Details WHERE Accessory_Details.Accessory_Code = @Accessorycode";

            SqlConnection conn = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(sqlQuery, conn);
            cmd.Parameters.AddWithValue("@AccessoryCode", cboRmvAccCode.Text);
            try 
            {
                SqlDataAdapter dAdapter = new SqlDataAdapter(cmd);
                DataTable filterAccessories = new DataTable();
                //fill
                dAdapter.Fill(filterAccessories);
                //sync
                BindingSource bSource = new BindingSource();
                bSource.DataSource = filterAccessories;
                dgvRemoveAccessory.DataSource = bSource;
            }
            catch (Exception ex) { MessageBox.Show("Error\n" + ex.Message); }
            
        }
        void filltxts() 
        {
            //FILL TEXTBOXES
            string connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;
            string sqlQuery = "SELECT * FROM [dbo].[Accessory_Details] WHERE [Accessory_Code] = @Accessorycode";

            SqlConnection conn = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(sqlQuery, conn);
            cmd.Parameters.AddWithValue("@Accessorycode", cboRmvAccCode.Text);
            try
            {
                conn.Open();
                //conection open
                SqlDataReader reader = cmd.ExecuteReader();

                int ordinalName = reader.GetOrdinal("Name");
                int ordinalcondition = reader.GetOrdinal("Condition");
                int ordinalSize = reader.GetOrdinal("Size");
                int ordinalType = reader.GetOrdinal("Accessory_Type");

                while (reader.Read())
                {
                    string tempName = reader.GetString(ordinalName);
                    string tempCondition = reader.GetString(ordinalcondition);
                    string tempSize = reader.GetString(ordinalSize);
                    string tempType = reader.GetString(ordinalType);

                    txtRmvAccName.Text = tempName;
                    txtRmvAccSize.Text = tempSize;
                    txtRmvAccCondition.Text = tempCondition;
                    txtRmvAccType.Text = tempType;
                   
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            } 
        }

        void fillcboAccCode() 
        {
            //ACCESSORY CODE
            string connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;
            string sqlQuery = "SELECT DISTINCT [Accessory_Code] FROM [dbo].[Accessory_Details]";

            SqlConnection conn = new SqlConnection(connectionString);

            try
            {

                SqlCommand cmd = new SqlCommand(sqlQuery, conn);
                SqlDataReader reader;
                //open connection
                conn.Open();
                reader = cmd.ExecuteReader();//executing command

                int ordinal = reader.GetOrdinal("Accessory_Code");
                while (reader.Read())
                {
                    string temp = reader.GetString(ordinal);
                    cboRmvAccCode.Items.Add(temp);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            finally
            {
                conn.Close();//closing connection
            }
        }

        private void btnRmvAccClear_Click(object sender, EventArgs e)
        {
            cboRmvAccCode.SelectedItem = null;
            cboRmvAccCode.Text = null;
            txtRmvAccCondition.Clear();
            txtRmvAccName.Clear();
            txtRmvAccSize.Clear();
            txtRmvAccType.Clear();
            loadAccessoryDetails();
            btnRmvAccessory.Enabled = false;
        }

        private void btnRmvAccDone_Click(object sender, EventArgs e)
        {
            this.Close();//closing form
        }


        private void frmRemoveAccessory_Load(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void btnRmvAccessory_Click_1(object sender, EventArgs e)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;
            string sqlQueryDelete = "DELETE  FROM [Accessory_Details] WHERE [Accessory_Code] = @Accessory_Code";

            SqlConnection conn = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(sqlQueryDelete, conn);
            cmd.Parameters.AddWithValue("@Accessory_Code", cboRmvAccCode.Text);
            try
            {
                conn.Open();
                int TotalRowsAffected = cmd.ExecuteNonQuery();

                if (TotalRowsAffected == 1)
                {
                    MessageBox.Show(cboRmvAccCode.Text + " has been deleted from the database.", "Record Deleted", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    btnRmvAccessory.Enabled = false;
                }
                else
                {
                    MessageBox.Show("Delete unsuccessful, please try again.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();//closing connecion
            }
        }

        private void cboRmvAccCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboRmvAccCode.Text != null)
            {
                filltxts();
                loadAccessoryDetailsAccCode();
                btnRmvAccessory.Enabled = true;
            }
        }
    }
}
