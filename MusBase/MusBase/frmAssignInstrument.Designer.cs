﻿namespace MusBase
{
    partial class frmAssignInstrument
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAssignInstrument));
            this.dgvAssignInstrument = new System.Windows.Forms.DataGridView();
            this.gbxAsgInstSelectInst = new System.Windows.Forms.GroupBox();
            this.txtAsgInstFamily = new System.Windows.Forms.TextBox();
            this.txtAsgInstType = new System.Windows.Forms.TextBox();
            this.txtAsgInstCondition = new System.Windows.Forms.TextBox();
            this.cboAsgInstSerialNo = new System.Windows.Forms.ComboBox();
            this.lblAsgInstFamily = new System.Windows.Forms.Label();
            this.lblInstCondition = new System.Windows.Forms.Label();
            this.lblAsgInstSerialNo = new System.Windows.Forms.Label();
            this.txtAsgInstMake = new System.Windows.Forms.TextBox();
            this.lblAsgInstMake = new System.Windows.Forms.Label();
            this.lblAsgInstType = new System.Windows.Forms.Label();
            this.gbxAsgInstSelectstd = new System.Windows.Forms.GroupBox();
            this.txtAsgInstForm = new System.Windows.Forms.TextBox();
            this.txtAsgInstHouse = new System.Windows.Forms.TextBox();
            this.cboAsgInstAdminNo = new System.Windows.Forms.ComboBox();
            this.lblAsgInstForm = new System.Windows.Forms.Label();
            this.lblAsgInstHouse = new System.Windows.Forms.Label();
            this.lblAsgInstAdminNo = new System.Windows.Forms.Label();
            this.txtAsgInstMName = new System.Windows.Forms.TextBox();
            this.lblAsgInstMName = new System.Windows.Forms.Label();
            this.txtAsgInstLName = new System.Windows.Forms.TextBox();
            this.txtAsgInstFName = new System.Windows.Forms.TextBox();
            this.lblAsgInstLName = new System.Windows.Forms.Label();
            this.lblAsgInstFName = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnAsgInstSubmitInst = new System.Windows.Forms.Button();
            this.btnAsgInstDone = new System.Windows.Forms.Button();
            this.btnAsgInstNext = new System.Windows.Forms.Button();
            this.btnNewAsgInst = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAssignInstrument)).BeginInit();
            this.gbxAsgInstSelectInst.SuspendLayout();
            this.gbxAsgInstSelectstd.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvAssignInstrument
            // 
            this.dgvAssignInstrument.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvAssignInstrument.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAssignInstrument.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAssignInstrument.Location = new System.Drawing.Point(0, 0);
            this.dgvAssignInstrument.Name = "dgvAssignInstrument";
            this.dgvAssignInstrument.ReadOnly = true;
            this.dgvAssignInstrument.Size = new System.Drawing.Size(781, 206);
            this.dgvAssignInstrument.TabIndex = 120;
            this.dgvAssignInstrument.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAssignInstrument_CellContentClick);
            // 
            // gbxAsgInstSelectInst
            // 
            this.gbxAsgInstSelectInst.BackColor = System.Drawing.Color.Transparent;
            this.gbxAsgInstSelectInst.Controls.Add(this.txtAsgInstFamily);
            this.gbxAsgInstSelectInst.Controls.Add(this.txtAsgInstType);
            this.gbxAsgInstSelectInst.Controls.Add(this.txtAsgInstCondition);
            this.gbxAsgInstSelectInst.Controls.Add(this.cboAsgInstSerialNo);
            this.gbxAsgInstSelectInst.Controls.Add(this.lblAsgInstFamily);
            this.gbxAsgInstSelectInst.Controls.Add(this.lblInstCondition);
            this.gbxAsgInstSelectInst.Controls.Add(this.lblAsgInstSerialNo);
            this.gbxAsgInstSelectInst.Controls.Add(this.txtAsgInstMake);
            this.gbxAsgInstSelectInst.Controls.Add(this.lblAsgInstMake);
            this.gbxAsgInstSelectInst.Controls.Add(this.lblAsgInstType);
            this.gbxAsgInstSelectInst.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxAsgInstSelectInst.ForeColor = System.Drawing.Color.RoyalBlue;
            this.gbxAsgInstSelectInst.Location = new System.Drawing.Point(500, 24);
            this.gbxAsgInstSelectInst.Name = "gbxAsgInstSelectInst";
            this.gbxAsgInstSelectInst.Size = new System.Drawing.Size(261, 275);
            this.gbxAsgInstSelectInst.TabIndex = 96;
            this.gbxAsgInstSelectInst.TabStop = false;
            this.gbxAsgInstSelectInst.Text = "Select Serial No";
            // 
            // txtAsgInstFamily
            // 
            this.txtAsgInstFamily.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAsgInstFamily.Location = new System.Drawing.Point(91, 100);
            this.txtAsgInstFamily.Name = "txtAsgInstFamily";
            this.txtAsgInstFamily.ReadOnly = true;
            this.txtAsgInstFamily.Size = new System.Drawing.Size(132, 28);
            this.txtAsgInstFamily.TabIndex = 119;
            // 
            // txtAsgInstType
            // 
            this.txtAsgInstType.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAsgInstType.Location = new System.Drawing.Point(91, 60);
            this.txtAsgInstType.Name = "txtAsgInstType";
            this.txtAsgInstType.ReadOnly = true;
            this.txtAsgInstType.Size = new System.Drawing.Size(132, 28);
            this.txtAsgInstType.TabIndex = 118;
            // 
            // txtAsgInstCondition
            // 
            this.txtAsgInstCondition.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAsgInstCondition.Location = new System.Drawing.Point(91, 176);
            this.txtAsgInstCondition.Name = "txtAsgInstCondition";
            this.txtAsgInstCondition.ReadOnly = true;
            this.txtAsgInstCondition.Size = new System.Drawing.Size(132, 28);
            this.txtAsgInstCondition.TabIndex = 117;
            // 
            // cboAsgInstSerialNo
            // 
            this.cboAsgInstSerialNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboAsgInstSerialNo.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cboAsgInstSerialNo.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboAsgInstSerialNo.FormattingEnabled = true;
            this.cboAsgInstSerialNo.Location = new System.Drawing.Point(91, 19);
            this.cboAsgInstSerialNo.Name = "cboAsgInstSerialNo";
            this.cboAsgInstSerialNo.Size = new System.Drawing.Size(132, 28);
            this.cboAsgInstSerialNo.TabIndex = 3;
            this.cboAsgInstSerialNo.SelectedIndexChanged += new System.EventHandler(this.cboAsgInstSerialNo_SelectedIndexChanged);
            // 
            // lblAsgInstFamily
            // 
            this.lblAsgInstFamily.AutoSize = true;
            this.lblAsgInstFamily.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAsgInstFamily.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lblAsgInstFamily.Location = new System.Drawing.Point(6, 100);
            this.lblAsgInstFamily.Name = "lblAsgInstFamily";
            this.lblAsgInstFamily.Size = new System.Drawing.Size(51, 17);
            this.lblAsgInstFamily.TabIndex = 112;
            this.lblAsgInstFamily.Text = "Family";
            // 
            // lblInstCondition
            // 
            this.lblInstCondition.AutoSize = true;
            this.lblInstCondition.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInstCondition.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lblInstCondition.Location = new System.Drawing.Point(6, 175);
            this.lblInstCondition.Name = "lblInstCondition";
            this.lblInstCondition.Size = new System.Drawing.Size(73, 17);
            this.lblInstCondition.TabIndex = 111;
            this.lblInstCondition.Text = "Condition";
            // 
            // lblAsgInstSerialNo
            // 
            this.lblAsgInstSerialNo.AutoSize = true;
            this.lblAsgInstSerialNo.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAsgInstSerialNo.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lblAsgInstSerialNo.Location = new System.Drawing.Point(6, 24);
            this.lblAsgInstSerialNo.Name = "lblAsgInstSerialNo";
            this.lblAsgInstSerialNo.Size = new System.Drawing.Size(66, 17);
            this.lblAsgInstSerialNo.TabIndex = 110;
            this.lblAsgInstSerialNo.Text = "Serial No";
            // 
            // txtAsgInstMake
            // 
            this.txtAsgInstMake.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAsgInstMake.Location = new System.Drawing.Point(91, 137);
            this.txtAsgInstMake.Name = "txtAsgInstMake";
            this.txtAsgInstMake.ReadOnly = true;
            this.txtAsgInstMake.Size = new System.Drawing.Size(132, 28);
            this.txtAsgInstMake.TabIndex = 109;
            // 
            // lblAsgInstMake
            // 
            this.lblAsgInstMake.AutoSize = true;
            this.lblAsgInstMake.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAsgInstMake.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lblAsgInstMake.Location = new System.Drawing.Point(6, 137);
            this.lblAsgInstMake.Name = "lblAsgInstMake";
            this.lblAsgInstMake.Size = new System.Drawing.Size(43, 17);
            this.lblAsgInstMake.TabIndex = 108;
            this.lblAsgInstMake.Text = "Make";
            // 
            // lblAsgInstType
            // 
            this.lblAsgInstType.AutoSize = true;
            this.lblAsgInstType.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAsgInstType.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lblAsgInstType.Location = new System.Drawing.Point(6, 65);
            this.lblAsgInstType.Name = "lblAsgInstType";
            this.lblAsgInstType.Size = new System.Drawing.Size(39, 17);
            this.lblAsgInstType.TabIndex = 107;
            this.lblAsgInstType.Text = "Type";
            // 
            // gbxAsgInstSelectstd
            // 
            this.gbxAsgInstSelectstd.BackColor = System.Drawing.Color.Transparent;
            this.gbxAsgInstSelectstd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.gbxAsgInstSelectstd.Controls.Add(this.txtAsgInstForm);
            this.gbxAsgInstSelectstd.Controls.Add(this.txtAsgInstHouse);
            this.gbxAsgInstSelectstd.Controls.Add(this.cboAsgInstAdminNo);
            this.gbxAsgInstSelectstd.Controls.Add(this.lblAsgInstForm);
            this.gbxAsgInstSelectstd.Controls.Add(this.lblAsgInstHouse);
            this.gbxAsgInstSelectstd.Controls.Add(this.lblAsgInstAdminNo);
            this.gbxAsgInstSelectstd.Controls.Add(this.txtAsgInstMName);
            this.gbxAsgInstSelectstd.Controls.Add(this.lblAsgInstMName);
            this.gbxAsgInstSelectstd.Controls.Add(this.txtAsgInstLName);
            this.gbxAsgInstSelectstd.Controls.Add(this.txtAsgInstFName);
            this.gbxAsgInstSelectstd.Controls.Add(this.lblAsgInstLName);
            this.gbxAsgInstSelectstd.Controls.Add(this.lblAsgInstFName);
            this.gbxAsgInstSelectstd.Font = new System.Drawing.Font("Tempus Sans ITC", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxAsgInstSelectstd.ForeColor = System.Drawing.Color.RoyalBlue;
            this.gbxAsgInstSelectstd.Location = new System.Drawing.Point(62, 24);
            this.gbxAsgInstSelectstd.Name = "gbxAsgInstSelectstd";
            this.gbxAsgInstSelectstd.Size = new System.Drawing.Size(261, 275);
            this.gbxAsgInstSelectstd.TabIndex = 0;
            this.gbxAsgInstSelectstd.TabStop = false;
            this.gbxAsgInstSelectstd.Text = "Select Admin No";
            // 
            // txtAsgInstForm
            // 
            this.txtAsgInstForm.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAsgInstForm.Location = new System.Drawing.Point(104, 60);
            this.txtAsgInstForm.Name = "txtAsgInstForm";
            this.txtAsgInstForm.ReadOnly = true;
            this.txtAsgInstForm.Size = new System.Drawing.Size(121, 24);
            this.txtAsgInstForm.TabIndex = 130;
            // 
            // txtAsgInstHouse
            // 
            this.txtAsgInstHouse.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAsgInstHouse.Location = new System.Drawing.Point(104, 97);
            this.txtAsgInstHouse.Name = "txtAsgInstHouse";
            this.txtAsgInstHouse.ReadOnly = true;
            this.txtAsgInstHouse.Size = new System.Drawing.Size(121, 24);
            this.txtAsgInstHouse.TabIndex = 129;
            // 
            // cboAsgInstAdminNo
            // 
            this.cboAsgInstAdminNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboAsgInstAdminNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboAsgInstAdminNo.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cboAsgInstAdminNo.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboAsgInstAdminNo.FormattingEnabled = true;
            this.cboAsgInstAdminNo.Location = new System.Drawing.Point(104, 21);
            this.cboAsgInstAdminNo.Name = "cboAsgInstAdminNo";
            this.cboAsgInstAdminNo.Size = new System.Drawing.Size(121, 25);
            this.cboAsgInstAdminNo.Sorted = true;
            this.cboAsgInstAdminNo.TabIndex = 1;
            this.cboAsgInstAdminNo.SelectedIndexChanged += new System.EventHandler(this.cboAsgInstAdminNo_SelectedIndexChanged);
            // 
            // lblAsgInstForm
            // 
            this.lblAsgInstForm.AutoSize = true;
            this.lblAsgInstForm.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAsgInstForm.Location = new System.Drawing.Point(6, 60);
            this.lblAsgInstForm.Name = "lblAsgInstForm";
            this.lblAsgInstForm.Size = new System.Drawing.Size(42, 17);
            this.lblAsgInstForm.TabIndex = 128;
            this.lblAsgInstForm.Text = "Form";
            // 
            // lblAsgInstHouse
            // 
            this.lblAsgInstHouse.AutoSize = true;
            this.lblAsgInstHouse.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAsgInstHouse.Location = new System.Drawing.Point(6, 100);
            this.lblAsgInstHouse.Name = "lblAsgInstHouse";
            this.lblAsgInstHouse.Size = new System.Drawing.Size(48, 17);
            this.lblAsgInstHouse.TabIndex = 127;
            this.lblAsgInstHouse.Text = "House";
            // 
            // lblAsgInstAdminNo
            // 
            this.lblAsgInstAdminNo.AutoSize = true;
            this.lblAsgInstAdminNo.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAsgInstAdminNo.Location = new System.Drawing.Point(6, 21);
            this.lblAsgInstAdminNo.Name = "lblAsgInstAdminNo";
            this.lblAsgInstAdminNo.Size = new System.Drawing.Size(76, 17);
            this.lblAsgInstAdminNo.TabIndex = 126;
            this.lblAsgInstAdminNo.Text = "Admin No";
            // 
            // txtAsgInstMName
            // 
            this.txtAsgInstMName.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAsgInstMName.Location = new System.Drawing.Point(104, 176);
            this.txtAsgInstMName.Name = "txtAsgInstMName";
            this.txtAsgInstMName.ReadOnly = true;
            this.txtAsgInstMName.Size = new System.Drawing.Size(121, 24);
            this.txtAsgInstMName.TabIndex = 125;
            // 
            // lblAsgInstMName
            // 
            this.lblAsgInstMName.AutoSize = true;
            this.lblAsgInstMName.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAsgInstMName.Location = new System.Drawing.Point(6, 176);
            this.lblAsgInstMName.Name = "lblAsgInstMName";
            this.lblAsgInstMName.Size = new System.Drawing.Size(94, 17);
            this.lblAsgInstMName.TabIndex = 124;
            this.lblAsgInstMName.Text = "Middle Name";
            // 
            // txtAsgInstLName
            // 
            this.txtAsgInstLName.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAsgInstLName.Location = new System.Drawing.Point(104, 215);
            this.txtAsgInstLName.Name = "txtAsgInstLName";
            this.txtAsgInstLName.ReadOnly = true;
            this.txtAsgInstLName.Size = new System.Drawing.Size(121, 24);
            this.txtAsgInstLName.TabIndex = 123;
            // 
            // txtAsgInstFName
            // 
            this.txtAsgInstFName.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAsgInstFName.Location = new System.Drawing.Point(104, 137);
            this.txtAsgInstFName.Name = "txtAsgInstFName";
            this.txtAsgInstFName.ReadOnly = true;
            this.txtAsgInstFName.Size = new System.Drawing.Size(121, 24);
            this.txtAsgInstFName.TabIndex = 122;
            // 
            // lblAsgInstLName
            // 
            this.lblAsgInstLName.AutoSize = true;
            this.lblAsgInstLName.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAsgInstLName.Location = new System.Drawing.Point(6, 215);
            this.lblAsgInstLName.Name = "lblAsgInstLName";
            this.lblAsgInstLName.Size = new System.Drawing.Size(75, 17);
            this.lblAsgInstLName.TabIndex = 121;
            this.lblAsgInstLName.Text = "Last Name";
            // 
            // lblAsgInstFName
            // 
            this.lblAsgInstFName.AutoSize = true;
            this.lblAsgInstFName.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAsgInstFName.Location = new System.Drawing.Point(6, 137);
            this.lblAsgInstFName.Name = "lblAsgInstFName";
            this.lblAsgInstFName.Size = new System.Drawing.Size(78, 17);
            this.lblAsgInstFName.TabIndex = 120;
            this.lblAsgInstFName.Text = "First Name";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dgvAssignInstrument);
            this.panel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(21, 348);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(781, 206);
            this.panel1.TabIndex = 121;
            // 
            // btnAsgInstSubmitInst
            // 
            this.btnAsgInstSubmitInst.BackColor = System.Drawing.SystemColors.Control;
            this.btnAsgInstSubmitInst.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAsgInstSubmitInst.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAsgInstSubmitInst.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnAsgInstSubmitInst.Image = global::MusBase.Properties.Resources.Violin_32;
            this.btnAsgInstSubmitInst.Location = new System.Drawing.Point(353, 101);
            this.btnAsgInstSubmitInst.Name = "btnAsgInstSubmitInst";
            this.btnAsgInstSubmitInst.Size = new System.Drawing.Size(104, 44);
            this.btnAsgInstSubmitInst.TabIndex = 4;
            this.btnAsgInstSubmitInst.Text = "&SUBMIT";
            this.btnAsgInstSubmitInst.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnAsgInstSubmitInst.UseVisualStyleBackColor = false;
            this.btnAsgInstSubmitInst.Click += new System.EventHandler(this.btnAsgIsntSubmitInst_Click);
            // 
            // btnAsgInstDone
            // 
            this.btnAsgInstDone.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAsgInstDone.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.btnAsgInstDone.Image = global::MusBase.Properties.Resources._1427990866_Done;
            this.btnAsgInstDone.Location = new System.Drawing.Point(353, 200);
            this.btnAsgInstDone.Name = "btnAsgInstDone";
            this.btnAsgInstDone.Size = new System.Drawing.Size(104, 44);
            this.btnAsgInstDone.TabIndex = 6;
            this.btnAsgInstDone.Text = "&Quit";
            this.btnAsgInstDone.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnAsgInstDone.UseVisualStyleBackColor = true;
            this.btnAsgInstDone.Click += new System.EventHandler(this.btnAsgInstDone_Click);
            // 
            // btnAsgInstNext
            // 
            this.btnAsgInstNext.BackColor = System.Drawing.SystemColors.Control;
            this.btnAsgInstNext.BackgroundImage = global::MusBase.Properties.Resources.next;
            this.btnAsgInstNext.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnAsgInstNext.Font = new System.Drawing.Font("Tempus Sans ITC", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAsgInstNext.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnAsgInstNext.Location = new System.Drawing.Point(365, 45);
            this.btnAsgInstNext.Name = "btnAsgInstNext";
            this.btnAsgInstNext.Size = new System.Drawing.Size(75, 36);
            this.btnAsgInstNext.TabIndex = 2;
            this.btnAsgInstNext.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnAsgInstNext.UseVisualStyleBackColor = false;
            this.btnAsgInstNext.Click += new System.EventHandler(this.btnAsgInstSubmit_Click);
            // 
            // btnNewAsgInst
            // 
            this.btnNewAsgInst.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNewAsgInst.Image = global::MusBase.Properties.Resources.Trombone_add_32;
            this.btnNewAsgInst.Location = new System.Drawing.Point(353, 257);
            this.btnNewAsgInst.Name = "btnNewAsgInst";
            this.btnNewAsgInst.Size = new System.Drawing.Size(104, 42);
            this.btnNewAsgInst.TabIndex = 5;
            this.btnNewAsgInst.Text = "&NEW";
            this.btnNewAsgInst.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnNewAsgInst.UseVisualStyleBackColor = true;
            this.btnNewAsgInst.Click += new System.EventHandler(this.btnNewAsgInst_Click);
            // 
            // frmAssignInstrument
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.MenuBar;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(814, 575);
            this.Controls.Add(this.btnAsgInstSubmitInst);
            this.Controls.Add(this.btnAsgInstDone);
            this.Controls.Add(this.btnAsgInstNext);
            this.Controls.Add(this.btnNewAsgInst);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.gbxAsgInstSelectstd);
            this.Controls.Add(this.gbxAsgInstSelectInst);
            this.ForeColor = System.Drawing.Color.RoyalBlue;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmAssignInstrument";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Assign  Instrument";
            this.Load += new System.EventHandler(this.frmAssignInstrument_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAssignInstrument)).EndInit();
            this.gbxAsgInstSelectInst.ResumeLayout(false);
            this.gbxAsgInstSelectInst.PerformLayout();
            this.gbxAsgInstSelectstd.ResumeLayout(false);
            this.gbxAsgInstSelectstd.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvAssignInstrument;
        private System.Windows.Forms.Button btnAsgInstDone;
        private System.Windows.Forms.GroupBox gbxAsgInstSelectInst;
        private System.Windows.Forms.TextBox txtAsgInstCondition;
        private System.Windows.Forms.ComboBox cboAsgInstSerialNo;
        private System.Windows.Forms.Label lblAsgInstFamily;
        private System.Windows.Forms.Label lblInstCondition;
        private System.Windows.Forms.Label lblAsgInstSerialNo;
        private System.Windows.Forms.TextBox txtAsgInstMake;
        private System.Windows.Forms.Label lblAsgInstMake;
        private System.Windows.Forms.Label lblAsgInstType;
        private System.Windows.Forms.GroupBox gbxAsgInstSelectstd;
        private System.Windows.Forms.Button btnAsgInstNext;
        private System.Windows.Forms.TextBox txtAsgInstHouse;
        private System.Windows.Forms.ComboBox cboAsgInstAdminNo;
        private System.Windows.Forms.Label lblAsgInstForm;
        private System.Windows.Forms.Label lblAsgInstHouse;
        private System.Windows.Forms.Label lblAsgInstAdminNo;
        private System.Windows.Forms.TextBox txtAsgInstMName;
        private System.Windows.Forms.Label lblAsgInstMName;
        private System.Windows.Forms.TextBox txtAsgInstLName;
        private System.Windows.Forms.TextBox txtAsgInstFName;
        private System.Windows.Forms.Label lblAsgInstLName;
        private System.Windows.Forms.Label lblAsgInstFName;
        private System.Windows.Forms.Button btnAsgInstSubmitInst;
        private System.Windows.Forms.TextBox txtAsgInstForm;
        private System.Windows.Forms.TextBox txtAsgInstFamily;
        private System.Windows.Forms.TextBox txtAsgInstType;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnNewAsgInst;
    }
}