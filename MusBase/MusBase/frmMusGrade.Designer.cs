﻿namespace MusBase
{
    partial class frmMusGrade
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.gbxStudent = new System.Windows.Forms.GroupBox();
            this.txtForm = new System.Windows.Forms.TextBox();
            this.cboAdmin = new System.Windows.Forms.ComboBox();
            this.lblAsgAccAdminNo = new System.Windows.Forms.Label();
            this.txtLName = new System.Windows.Forms.TextBox();
            this.lblAsgAccForm = new System.Windows.Forms.Label();
            this.txtFName = new System.Windows.Forms.TextBox();
            this.lblAsgAccLName = new System.Windows.Forms.Label();
            this.lblAsgAccFName = new System.Windows.Forms.Label();
            this.btnSubmitScore = new System.Windows.Forms.Button();
            this.gbxScore = new System.Windows.Forms.GroupBox();
            this.cboTerm = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.txtDesc = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPaper3 = new System.Windows.Forms.TextBox();
            this.txtPaper2 = new System.Windows.Forms.TextBox();
            this.txtPaper1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dgvGrading = new System.Windows.Forms.DataGridView();
            this.Paper1errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.Paper2errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.Paper3errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.gbxStudent.SuspendLayout();
            this.gbxScore.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGrading)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Paper1errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Paper2errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Paper3errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSubmit
            // 
            this.btnSubmit.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSubmit.Location = new System.Drawing.Point(325, 83);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(89, 45);
            this.btnSubmit.TabIndex = 229;
            this.btnSubmit.Text = "&Submit";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // gbxStudent
            // 
            this.gbxStudent.Controls.Add(this.txtForm);
            this.gbxStudent.Controls.Add(this.cboAdmin);
            this.gbxStudent.Controls.Add(this.lblAsgAccAdminNo);
            this.gbxStudent.Controls.Add(this.txtLName);
            this.gbxStudent.Controls.Add(this.lblAsgAccForm);
            this.gbxStudent.Controls.Add(this.txtFName);
            this.gbxStudent.Controls.Add(this.lblAsgAccLName);
            this.gbxStudent.Controls.Add(this.lblAsgAccFName);
            this.gbxStudent.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxStudent.Location = new System.Drawing.Point(441, 14);
            this.gbxStudent.Name = "gbxStudent";
            this.gbxStudent.Size = new System.Drawing.Size(293, 256);
            this.gbxStudent.TabIndex = 228;
            this.gbxStudent.TabStop = false;
            this.gbxStudent.Text = "Student";
            // 
            // txtForm
            // 
            this.txtForm.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtForm.Location = new System.Drawing.Point(121, 132);
            this.txtForm.Name = "txtForm";
            this.txtForm.ReadOnly = true;
            this.txtForm.Size = new System.Drawing.Size(77, 24);
            this.txtForm.TabIndex = 200;
            // 
            // cboAdmin
            // 
            this.cboAdmin.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboAdmin.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboAdmin.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cboAdmin.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboAdmin.FormattingEnabled = true;
            this.cboAdmin.Location = new System.Drawing.Point(121, 23);
            this.cboAdmin.Name = "cboAdmin";
            this.cboAdmin.Size = new System.Drawing.Size(121, 25);
            this.cboAdmin.TabIndex = 189;
            this.cboAdmin.SelectedIndexChanged += new System.EventHandler(this.cboAdmin_SelectedIndexChanged);
            // 
            // lblAsgAccAdminNo
            // 
            this.lblAsgAccAdminNo.AutoSize = true;
            this.lblAsgAccAdminNo.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAsgAccAdminNo.Location = new System.Drawing.Point(19, 23);
            this.lblAsgAccAdminNo.Name = "lblAsgAccAdminNo";
            this.lblAsgAccAdminNo.Size = new System.Drawing.Size(76, 17);
            this.lblAsgAccAdminNo.TabIndex = 196;
            this.lblAsgAccAdminNo.Text = "Admin No";
            // 
            // txtLName
            // 
            this.txtLName.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLName.Location = new System.Drawing.Point(121, 97);
            this.txtLName.Name = "txtLName";
            this.txtLName.ReadOnly = true;
            this.txtLName.Size = new System.Drawing.Size(121, 24);
            this.txtLName.TabIndex = 193;
            // 
            // lblAsgAccForm
            // 
            this.lblAsgAccForm.AutoSize = true;
            this.lblAsgAccForm.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAsgAccForm.Location = new System.Drawing.Point(55, 132);
            this.lblAsgAccForm.Name = "lblAsgAccForm";
            this.lblAsgAccForm.Size = new System.Drawing.Size(42, 17);
            this.lblAsgAccForm.TabIndex = 198;
            this.lblAsgAccForm.Text = "Form";
            // 
            // txtFName
            // 
            this.txtFName.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFName.Location = new System.Drawing.Point(121, 60);
            this.txtFName.Name = "txtFName";
            this.txtFName.ReadOnly = true;
            this.txtFName.Size = new System.Drawing.Size(121, 24);
            this.txtFName.TabIndex = 192;
            // 
            // lblAsgAccLName
            // 
            this.lblAsgAccLName.AutoSize = true;
            this.lblAsgAccLName.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAsgAccLName.Location = new System.Drawing.Point(20, 97);
            this.lblAsgAccLName.Name = "lblAsgAccLName";
            this.lblAsgAccLName.Size = new System.Drawing.Size(75, 17);
            this.lblAsgAccLName.TabIndex = 191;
            this.lblAsgAccLName.Text = "Last Name";
            // 
            // lblAsgAccFName
            // 
            this.lblAsgAccFName.AutoSize = true;
            this.lblAsgAccFName.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAsgAccFName.Location = new System.Drawing.Point(19, 61);
            this.lblAsgAccFName.Name = "lblAsgAccFName";
            this.lblAsgAccFName.Size = new System.Drawing.Size(78, 17);
            this.lblAsgAccFName.TabIndex = 190;
            this.lblAsgAccFName.Text = "First Name";
            // 
            // btnSubmitScore
            // 
            this.btnSubmitScore.Font = new System.Drawing.Font("Tempus Sans ITC", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSubmitScore.Location = new System.Drawing.Point(325, 23);
            this.btnSubmitScore.Name = "btnSubmitScore";
            this.btnSubmitScore.Size = new System.Drawing.Size(89, 45);
            this.btnSubmitScore.TabIndex = 227;
            this.btnSubmitScore.Text = " >>>";
            this.btnSubmitScore.UseVisualStyleBackColor = true;
            this.btnSubmitScore.Click += new System.EventHandler(this.btnSubmitScore_Click);
            // 
            // gbxScore
            // 
            this.gbxScore.Controls.Add(this.cboTerm);
            this.gbxScore.Controls.Add(this.label6);
            this.gbxScore.Controls.Add(this.dateTimePicker1);
            this.gbxScore.Controls.Add(this.label5);
            this.gbxScore.Controls.Add(this.txtDesc);
            this.gbxScore.Controls.Add(this.label4);
            this.gbxScore.Controls.Add(this.label3);
            this.gbxScore.Controls.Add(this.txtPaper3);
            this.gbxScore.Controls.Add(this.txtPaper2);
            this.gbxScore.Controls.Add(this.txtPaper1);
            this.gbxScore.Controls.Add(this.label2);
            this.gbxScore.Controls.Add(this.label1);
            this.gbxScore.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxScore.Location = new System.Drawing.Point(13, 13);
            this.gbxScore.Margin = new System.Windows.Forms.Padding(4);
            this.gbxScore.Name = "gbxScore";
            this.gbxScore.Padding = new System.Windows.Forms.Padding(4);
            this.gbxScore.Size = new System.Drawing.Size(292, 256);
            this.gbxScore.TabIndex = 226;
            this.gbxScore.TabStop = false;
            this.gbxScore.Text = "Score";
            // 
            // cboTerm
            // 
            this.cboTerm.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboTerm.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboTerm.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cboTerm.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboTerm.FormattingEnabled = true;
            this.cboTerm.Location = new System.Drawing.Point(155, 133);
            this.cboTerm.Name = "cboTerm";
            this.cboTerm.Size = new System.Drawing.Size(101, 25);
            this.cboTerm.TabIndex = 190;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(11, 132);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 17);
            this.label6.TabIndex = 10;
            this.label6.Text = "Term";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Font = new System.Drawing.Font("Tempus Sans ITC", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker1.Location = new System.Drawing.Point(69, 200);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(215, 23);
            this.dateTimePicker1.TabIndex = 9;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(7, 200);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 20);
            this.label5.TabIndex = 8;
            this.label5.Text = "Date";
            // 
            // txtDesc
            // 
            this.txtDesc.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDesc.Location = new System.Drawing.Point(154, 165);
            this.txtDesc.Margin = new System.Windows.Forms.Padding(4);
            this.txtDesc.Name = "txtDesc";
            this.txtDesc.Size = new System.Drawing.Size(129, 24);
            this.txtDesc.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(7, 165);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(119, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "Type/Description";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(8, 97);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(117, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "Paper 3 (Theory)";
            // 
            // txtPaper3
            // 
            this.txtPaper3.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPaper3.Location = new System.Drawing.Point(155, 97);
            this.txtPaper3.Margin = new System.Windows.Forms.Padding(4);
            this.txtPaper3.Name = "txtPaper3";
            this.txtPaper3.Size = new System.Drawing.Size(101, 24);
            this.txtPaper3.TabIndex = 4;
            this.txtPaper3.Validating += new System.ComponentModel.CancelEventHandler(this.txtPaper3_Validating);
            // 
            // txtPaper2
            // 
            this.txtPaper2.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPaper2.Location = new System.Drawing.Point(155, 61);
            this.txtPaper2.Margin = new System.Windows.Forms.Padding(4);
            this.txtPaper2.Name = "txtPaper2";
            this.txtPaper2.Size = new System.Drawing.Size(101, 24);
            this.txtPaper2.TabIndex = 3;
            this.txtPaper2.Validating += new System.ComponentModel.CancelEventHandler(this.txtPaper2_Validating);
            // 
            // txtPaper1
            // 
            this.txtPaper1.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPaper1.Location = new System.Drawing.Point(155, 23);
            this.txtPaper1.Margin = new System.Windows.Forms.Padding(4);
            this.txtPaper1.Name = "txtPaper1";
            this.txtPaper1.Size = new System.Drawing.Size(101, 24);
            this.txtPaper1.TabIndex = 2;
            this.txtPaper1.Validating += new System.ComponentModel.CancelEventHandler(this.txtPaper1_Validating);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 23);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(126, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Paper 1 (Practical)";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 60);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Paper 2 (Aural)";
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.ForeColor = System.Drawing.Color.Red;
            this.lblStatus.Location = new System.Drawing.Point(10, 304);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(40, 15);
            this.lblStatus.TabIndex = 225;
            this.lblStatus.Text = "label7";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.dgvGrading);
            this.panel1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(11, 324);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(723, 188);
            this.panel1.TabIndex = 224;
            // 
            // dgvGrading
            // 
            this.dgvGrading.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvGrading.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvGrading.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvGrading.Location = new System.Drawing.Point(0, 0);
            this.dgvGrading.Name = "dgvGrading";
            this.dgvGrading.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvGrading.Size = new System.Drawing.Size(723, 188);
            this.dgvGrading.TabIndex = 0;
            this.dgvGrading.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvGrading_CellContentClick);
            // 
            // Paper1errorProvider
            // 
            this.Paper1errorProvider.ContainerControl = this;
            // 
            // Paper2errorProvider
            // 
            this.Paper2errorProvider.ContainerControl = this;
            // 
            // Paper3errorProvider
            // 
            this.Paper3errorProvider.ContainerControl = this;
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(659, 286);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(75, 23);
            this.btnEdit.TabIndex = 232;
            this.btnEdit.Text = "Edit";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.btnClose.Image = global::MusBase.Properties.Resources._1427990866_Done;
            this.btnClose.Location = new System.Drawing.Point(325, 213);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(89, 45);
            this.btnClose.TabIndex = 231;
            this.btnClose.Text = "&Quit";
            this.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnReset
            // 
            this.btnReset.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.Image = global::MusBase.Properties.Resources._1429511309_131707;
            this.btnReset.Location = new System.Drawing.Point(325, 150);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(89, 45);
            this.btnReset.TabIndex = 230;
            this.btnReset.Text = "Reset";
            this.btnReset.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // frmMusGrade
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(750, 524);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.gbxStudent);
            this.Controls.Add(this.btnSubmitScore);
            this.Controls.Add(this.gbxScore);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.panel1);
            this.ForeColor = System.Drawing.Color.RoyalBlue;
            this.Name = "frmMusGrade";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmMusGrade";
            this.Load += new System.EventHandler(this.frmMusGrade_Load);
            this.gbxStudent.ResumeLayout(false);
            this.gbxStudent.PerformLayout();
            this.gbxScore.ResumeLayout(false);
            this.gbxScore.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvGrading)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Paper1errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Paper2errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Paper3errorProvider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.GroupBox gbxStudent;
        private System.Windows.Forms.TextBox txtForm;
        private System.Windows.Forms.ComboBox cboAdmin;
        private System.Windows.Forms.Label lblAsgAccAdminNo;
        private System.Windows.Forms.TextBox txtLName;
        private System.Windows.Forms.Label lblAsgAccForm;
        private System.Windows.Forms.TextBox txtFName;
        private System.Windows.Forms.Label lblAsgAccLName;
        private System.Windows.Forms.Label lblAsgAccFName;
        private System.Windows.Forms.Button btnSubmitScore;
        private System.Windows.Forms.GroupBox gbxScore;
        private System.Windows.Forms.ComboBox cboTerm;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtDesc;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtPaper3;
        private System.Windows.Forms.TextBox txtPaper2;
        private System.Windows.Forms.TextBox txtPaper1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dgvGrading;
        private System.Windows.Forms.ErrorProvider Paper1errorProvider;
        private System.Windows.Forms.ErrorProvider Paper2errorProvider;
        private System.Windows.Forms.ErrorProvider Paper3errorProvider;
        private System.Windows.Forms.Button btnEdit;

    }
}