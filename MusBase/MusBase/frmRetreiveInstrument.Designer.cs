﻿namespace MusBase
{
    partial class frmRetrieveInstrument
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxRtvInstrument = new System.Windows.Forms.GroupBox();
            this.cboRtvInstAdminNo = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cboRtvInstSerialNo = new System.Windows.Forms.ComboBox();
            this.cboRtvInstType = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblRtvInst = new System.Windows.Forms.Label();
            this.lblLink = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dgvRtvInstBearerDetails = new System.Windows.Forms.DataGridView();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnRtvInstrument = new System.Windows.Forms.Button();
            this.btnRtvInstSubmitInst = new System.Windows.Forms.Button();
            this.btnRtvInstDone = new System.Windows.Forms.Button();
            this.gbxRtvInstrument.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRtvInstBearerDetails)).BeginInit();
            this.SuspendLayout();
            // 
            // gbxRtvInstrument
            // 
            this.gbxRtvInstrument.Controls.Add(this.cboRtvInstAdminNo);
            this.gbxRtvInstrument.Controls.Add(this.label2);
            this.gbxRtvInstrument.Controls.Add(this.cboRtvInstSerialNo);
            this.gbxRtvInstrument.Controls.Add(this.cboRtvInstType);
            this.gbxRtvInstrument.Controls.Add(this.label1);
            this.gbxRtvInstrument.Controls.Add(this.lblRtvInst);
            this.gbxRtvInstrument.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxRtvInstrument.Location = new System.Drawing.Point(55, 12);
            this.gbxRtvInstrument.Name = "gbxRtvInstrument";
            this.gbxRtvInstrument.Size = new System.Drawing.Size(307, 149);
            this.gbxRtvInstrument.TabIndex = 0;
            this.gbxRtvInstrument.TabStop = false;
            this.gbxRtvInstrument.Text = "Enter the following details";
            // 
            // cboRtvInstAdminNo
            // 
            this.cboRtvInstAdminNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboRtvInstAdminNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboRtvInstAdminNo.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cboRtvInstAdminNo.FormattingEnabled = true;
            this.cboRtvInstAdminNo.Location = new System.Drawing.Point(149, 106);
            this.cboRtvInstAdminNo.Name = "cboRtvInstAdminNo";
            this.cboRtvInstAdminNo.Size = new System.Drawing.Size(121, 28);
            this.cboRtvInstAdminNo.TabIndex = 193;
            this.cboRtvInstAdminNo.SelectedIndexChanged += new System.EventHandler(this.cboRtvInstAdminNo_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(36, 106);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 17);
            this.label2.TabIndex = 192;
            this.label2.Text = "Admin No";
            // 
            // cboRtvInstSerialNo
            // 
            this.cboRtvInstSerialNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboRtvInstSerialNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboRtvInstSerialNo.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cboRtvInstSerialNo.FormattingEnabled = true;
            this.cboRtvInstSerialNo.Location = new System.Drawing.Point(149, 63);
            this.cboRtvInstSerialNo.Name = "cboRtvInstSerialNo";
            this.cboRtvInstSerialNo.Size = new System.Drawing.Size(121, 28);
            this.cboRtvInstSerialNo.TabIndex = 2;
            this.cboRtvInstSerialNo.SelectedIndexChanged += new System.EventHandler(this.cboRtvInstSerialNo_SelectedIndexChanged);
            // 
            // cboRtvInstType
            // 
            this.cboRtvInstType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboRtvInstType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboRtvInstType.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cboRtvInstType.FormattingEnabled = true;
            this.cboRtvInstType.Location = new System.Drawing.Point(149, 22);
            this.cboRtvInstType.Name = "cboRtvInstType";
            this.cboRtvInstType.Size = new System.Drawing.Size(121, 28);
            this.cboRtvInstType.TabIndex = 1;
            this.cboRtvInstType.SelectedIndexChanged += new System.EventHandler(this.cboRtvInstType_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(36, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 17);
            this.label1.TabIndex = 191;
            this.label1.Text = "Instrument Type";
            // 
            // lblRtvInst
            // 
            this.lblRtvInst.AutoSize = true;
            this.lblRtvInst.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRtvInst.Location = new System.Drawing.Point(34, 63);
            this.lblRtvInst.Name = "lblRtvInst";
            this.lblRtvInst.Size = new System.Drawing.Size(66, 17);
            this.lblRtvInst.TabIndex = 144;
            this.lblRtvInst.Text = "Serial No";
            // 
            // lblLink
            // 
            this.lblLink.AutoSize = true;
            this.lblLink.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLink.ForeColor = System.Drawing.Color.Red;
            this.lblLink.Location = new System.Drawing.Point(21, 247);
            this.lblLink.Name = "lblLink";
            this.lblLink.Size = new System.Drawing.Size(145, 20);
            this.lblLink.TabIndex = 203;
            this.lblLink.Text = "Registration link: ";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dgvRtvInstBearerDetails);
            this.panel1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(38, 288);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(651, 190);
            this.panel1.TabIndex = 204;
            // 
            // dgvRtvInstBearerDetails
            // 
            this.dgvRtvInstBearerDetails.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvRtvInstBearerDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRtvInstBearerDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvRtvInstBearerDetails.Location = new System.Drawing.Point(0, 0);
            this.dgvRtvInstBearerDetails.Name = "dgvRtvInstBearerDetails";
            this.dgvRtvInstBearerDetails.Size = new System.Drawing.Size(651, 190);
            this.dgvRtvInstBearerDetails.TabIndex = 203;
            // 
            // btnReset
            // 
            this.btnReset.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.Image = global::MusBase.Properties.Resources._1429511309_131707;
            this.btnReset.Location = new System.Drawing.Point(55, 187);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(95, 44);
            this.btnReset.TabIndex = 205;
            this.btnReset.Text = "Reset";
            this.btnReset.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnRtvInstrument
            // 
            this.btnRtvInstrument.Font = new System.Drawing.Font("Tempus Sans ITC", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRtvInstrument.Image = global::MusBase.Properties.Resources.violin_cancel_48;
            this.btnRtvInstrument.Location = new System.Drawing.Point(354, 187);
            this.btnRtvInstrument.Name = "btnRtvInstrument";
            this.btnRtvInstrument.Size = new System.Drawing.Size(139, 57);
            this.btnRtvInstrument.TabIndex = 4;
            this.btnRtvInstrument.Text = "Retrieve Instrument";
            this.btnRtvInstrument.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnRtvInstrument.UseVisualStyleBackColor = true;
            this.btnRtvInstrument.Click += new System.EventHandler(this.btnRtvInstrument_Click);
            // 
            // btnRtvInstSubmitInst
            // 
            this.btnRtvInstSubmitInst.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRtvInstSubmitInst.Image = global::MusBase.Properties.Resources.folder_32;
            this.btnRtvInstSubmitInst.Location = new System.Drawing.Point(389, 119);
            this.btnRtvInstSubmitInst.Name = "btnRtvInstSubmitInst";
            this.btnRtvInstSubmitInst.Size = new System.Drawing.Size(104, 42);
            this.btnRtvInstSubmitInst.TabIndex = 3;
            this.btnRtvInstSubmitInst.Text = "&Submit";
            this.btnRtvInstSubmitInst.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnRtvInstSubmitInst.UseVisualStyleBackColor = true;
            this.btnRtvInstSubmitInst.Click += new System.EventHandler(this.btnRtvInstSubmitInst_Click);
            // 
            // btnRtvInstDone
            // 
            this.btnRtvInstDone.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRtvInstDone.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.btnRtvInstDone.Image = global::MusBase.Properties.Resources._1427990866_Done;
            this.btnRtvInstDone.Location = new System.Drawing.Point(213, 187);
            this.btnRtvInstDone.Name = "btnRtvInstDone";
            this.btnRtvInstDone.Size = new System.Drawing.Size(95, 44);
            this.btnRtvInstDone.TabIndex = 5;
            this.btnRtvInstDone.Text = "&Quit";
            this.btnRtvInstDone.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnRtvInstDone.UseVisualStyleBackColor = true;
            this.btnRtvInstDone.Click += new System.EventHandler(this.btnAsgBkDone_Click);
            // 
            // frmRetrieveInstrument
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(717, 490);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnRtvInstrument);
            this.Controls.Add(this.gbxRtvInstrument);
            this.Controls.Add(this.btnRtvInstSubmitInst);
            this.Controls.Add(this.lblLink);
            this.Controls.Add(this.btnRtvInstDone);
            this.ForeColor = System.Drawing.Color.RoyalBlue;
            this.MaximizeBox = false;
            this.Name = "frmRetrieveInstrument";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Retrieve Instrument";
            this.Load += new System.EventHandler(this.frmRetreiveInstrument_Load);
            this.gbxRtvInstrument.ResumeLayout(false);
            this.gbxRtvInstrument.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRtvInstBearerDetails)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxRtvInstrument;
        private System.Windows.Forms.ComboBox cboRtvInstSerialNo;
        private System.Windows.Forms.ComboBox cboRtvInstType;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblRtvInst;
        private System.Windows.Forms.Button btnRtvInstSubmitInst;
        private System.Windows.Forms.Label lblLink;
        private System.Windows.Forms.Button btnRtvInstDone;
        private System.Windows.Forms.Button btnRtvInstrument;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dgvRtvInstBearerDetails;
        private System.Windows.Forms.ComboBox cboRtvInstAdminNo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnReset;
    }
}