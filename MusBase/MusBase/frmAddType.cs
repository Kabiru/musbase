﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace MusBase
{
    public partial class frmAddType : Form
    {
        string connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;
        SqlDataAdapter dAdapter;
        DataTable table;
        SqlCommandBuilder cmdbld;
        public frmAddType()
        {
            InitializeComponent();
            startState();

            lblTypeTitle.Text = "Waiting ...";
        }

        private void startState() 
        {
            txtType.Enabled = false;
            btnSubmit.Enabled = false;
            //btnReset.Enabled = false;
            btnUpdate.Enabled = false;
            gbxRdos.Enabled = false;
        }        

        private void LoadDgv(string query) 
        {
            //fill datagridview
            SqlConnection conn = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            try 
            {
                dAdapter = new SqlDataAdapter(cmd);
                table = new DataTable();
                //fill datatable
                dAdapter.Fill(table);
                //sync
                BindingSource bsource = new BindingSource();
                bsource.DataSource = table;
                dgvType.DataSource = bsource;

            }
            catch(Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message, "Error");
            }
        }

        private void frmAddType_Load(object sender, EventArgs e)
        {
           
        }      

        private void btnaddType_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (txtType.Text != null)
            {
                btnReset.Enabled = true;
                btnSubmit.Enabled = true;
            }
        }

        private void setFamilyInsert(string familyType, string family)
        {
            SqlConnection conn = new SqlConnection(connectionString);
            SqlCommand cmd;
            string query = "INSERT INTO dbo.Instrument_Type (Instrument_Type_Id, Name, FAMILY) VALUES (@id, @type, @family)";
            try
            {
                int counter = 100;
                string id = familyType+ Convert.ToString(counter);
                cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@type", txtType.Text);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@family", family);
                conn.Open();
                int totalrowsaffected = cmd.ExecuteNonQuery();
                if (totalrowsaffected == 1)
                {
                    MessageBox.Show("Insert Successfull.", "Status", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    counter++;//increment counter.
                    txtType.Clear();
                }

            }
            catch (SqlException sex)
            {
                if (sex.Number == 2627)
                {
                    MessageBox.Show("THE RECORD ALREADY EXISTS IN THE DATABASE.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                else
                {
                    MessageBox.Show(sex.Message);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        private void InsertRecord(string query) 
        {
            SqlConnection conn = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(query, conn);
            
            try 
            {                
                cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@type", txtType.Text);                
                conn.Open();
                int totalrowsaffected = cmd.ExecuteNonQuery();
                if (totalrowsaffected == 1)
                {
                    MessageBox.Show("Insert Successfull.", "Status", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtType.Clear();
                }
            }

            catch (SqlException sex)
            {
                if (sex.Number == 2627)
                {
                    MessageBox.Show("THE RECORD ALREADY EXISTS IN THE DATABASE.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                else
                {
                    MessageBox.Show(sex.Message);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
           
        }

        private void rdoInstrumentType_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoInstrumentType.Checked)
            {
                gbxRdos.Enabled = true;
                txtType.Clear();
                btnSubmit.Enabled = false;
            }
        }

        private void rdoAccessoryType_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoAccessoryType.Checked)
            {
                txtType.Enabled = true;
                gbxRdos.Enabled = false;
                txtType.Clear();
                btnSubmit.Enabled = false;
            }
        }

        private void rdoBookType_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoBookType.Checked)
            {
                txtType.Enabled = true;
                gbxRdos.Enabled = false;
                txtType.Clear();
                btnSubmit.Enabled = false;
            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoBrass.Checked)
            {
                txtType.Enabled = true;
            }
        }

        private void rdoStrings_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoStrings.Checked)
            {
                txtType.Enabled = true;
            }
        }

        private void rdoPercussion_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoPercussion.Checked)
                txtType.Enabled = true;
        }

        private void rdoWoodwinds_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoWoodwinds.Checked)
                txtType.Enabled = true;
        }

        private void btnBkClose_Click(object sender, EventArgs e)
        {
            this.Close();//closing form
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            txtType.Clear();
            startState();
        }

        private void tabPage3_Click(object sender, EventArgs e)
        {

        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            string fam = null;
            if (rdoInstrumentType.Checked)
            {
                if (rdoBrass.Checked)
                {
                    fam = "B";
                    setFamilyInsert(fam, "Brass");
                }
                else if (rdoPercussion.Checked)
                {
                    fam = "P";
                    setFamilyInsert(fam, "Percussion");
                }
                else if (rdoStrings.Checked)
                {
                    fam = "S";
                    setFamilyInsert(fam, "Strings");
                }
                else if (rdoWoodwinds.Checked)
                {
                    fam = "W";
                    setFamilyInsert(fam, "Woodwinds");
                }
            }
            else if (rdoAccessoryType.Checked)
            {
                string query = "INSERT INTO dbo.Accessories_Type (Acc_Type) VALUES (@type)";
                InsertRecord(query);
            }
            else if (rdoBookType.Checked)
            {
                string query = "INSERT INTO dbo.Book_Type (Bk_Type) VALUES (@type)";
                InsertRecord(query);
            }
        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void rdoAccTypedgv_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoAccTypedgv.Checked)
            {
                string query = "Load_Accessory_Type_Procedure";
                LoadDgv(query);
                lblTypeTitle.Text = "Accessory Type";
            }
        }

        private void btnBkClose_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void rdoInstTypedgv_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoInstTypedgv.Checked)
            {
                string query = "Load_Instrument_Type_Procedure";
                LoadDgv(query);
                lblTypeTitle.Text = "Instrument Type";
            }
        }

        private void rdoBkTypedgv_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoBkTypedgv.Checked)
            {
                string query = "Load_Book_Type_Procedure";
                LoadDgv(query);
                lblTypeTitle.Text = "Book Type";
            }
        }

        private void dgvType_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            try
            {
                cmdbld = new SqlCommandBuilder(dAdapter);
                //update data table
                dAdapter.Update(table);
                MessageBox.Show("Information Updated into the Database.", "Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvType_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            btnUpdate.Enabled = true;
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = tabPageView;
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = tabPageAdd;
        }

        private void rdoInstrumentType_CheckedChanged_1(object sender, EventArgs e)
        {
            if (rdoInstrumentType.Checked)
            {
                gbxRdos.Enabled = true;
                txtType.Clear();
                btnSubmit.Enabled = false;
            }
        }

        private void rdoAccessoryType_CheckedChanged_1(object sender, EventArgs e)
        {
            if (rdoAccessoryType.Checked)
            {
                txtType.Enabled = true;
                gbxRdos.Enabled = false;
                txtType.Clear();
                btnSubmit.Enabled = false;
            }
        }

        private void rdoBookType_CheckedChanged_1(object sender, EventArgs e)
        {
            if (rdoBookType.Checked)
            {
                txtType.Enabled = true;
                gbxRdos.Enabled = false;
                txtType.Clear();
                btnSubmit.Enabled = false;
            }
        }

        private void txtType_TextChanged(object sender, EventArgs e)
        {
            if (txtType.Text != null)
            {
                btnSubmit.Enabled = true;
            }
        }

        private void btnReset_Click_1(object sender, EventArgs e)
        {
            txtType.Clear();
            startState();
        }
    }
}
