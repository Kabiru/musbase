﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace MusBase
{
    public partial class frmMusChoir : Form
    {
        private string connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;

        SqlConnection conn;
        SqlCommand cmd;
        SqlDataAdapter dAdapter;
        DataTable muschoir = new DataTable();
        SqlCommandBuilder cmdbld;

        public frmMusChoir()
        {
            InitializeComponent();
            LoadMusChoir();
            fillAdminNo();
            startState();
        }

        private void fillvoice()
        {
            cboVoice.Items.Clear();
            cboVoice.Items.Add("Soprano");
            cboVoice.Items.Add("Alto");
            cboVoice.Items.Add("Tenor");
            cboVoice.Items.Add("Bass");
            cboVoice.SelectedItem = "Bass";
        }

        private void clear()
        {
            txtFName.Clear();
            txtForm.Clear();
            txtLName.Clear();
            txtPosition.Clear();
            cboAdmin.Text = null;
            cboAdmin.SelectedItem = null;
            cboChoirClass.Text = null;
            cboChoirClass.SelectedItem = null;
            cboVoice.Text = null;
            cboVoice.SelectedItem = null;
        }

        private void fillChoirClass()
        {
            cboChoirClass.Items.Clear();
            cboChoirClass.Items.Add("Junior Choir");
            cboChoirClass.Items.Add("Senior Choir");
            cboChoirClass.SelectedItem = "Senior Choir";
        }

        private void filltxts()
        {
            string query = "SELECT FName, LName, Form " +
                " FROM Student_Profile WHERE Admin_No = @adm";

            conn = new SqlConnection(connectionString);
            cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@adm", cboAdmin.Text);

            try
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                int FNameordinal = reader.GetOrdinal("FName");
                int LNameordinal = reader.GetOrdinal("LName");
                int Formordinal = reader.GetOrdinal("Form");

                while (reader.Read())
                {
                    string fname = reader.GetString(FNameordinal);
                    string lname = reader.GetString(LNameordinal);
                    string form = reader.GetString(Formordinal);

                    txtFName.Text = fname;
                    txtLName.Text = lname;
                    txtForm.Text = form;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                conn.Close();
            }
        }

        public void fillAdminNo()
        {
            cboAdmin.Items.Clear();
            conn = new SqlConnection(connectionString);
            string query = "Load_MusBand_Admin_Procedure";
            cmd = new SqlCommand(query, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                int ordinal = reader.GetOrdinal("Admin_No");
                while (reader.Read())
                {
                    int temp = reader.GetInt32(ordinal);
                    cboAdmin.Items.Add(temp);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void frmMusChoir_Load(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btnBandAcademic_Click(object sender, EventArgs e)
        {
            frmrptChoirGrade c = new frmrptChoirGrade();
            c.Show();
        }

        private void startState()
        {
            cboVoice.Enabled = false;
            cboChoirClass.Enabled = false;
            txtPosition.Enabled = false;
            btnAdd.Enabled = false;
            btnUpdate.Enabled = false;
            btnDelete.Enabled = false;
        }

        private void LoadMusChoir()
        {
            string query = "Load_MusChoir_Procedure";
            conn = new SqlConnection(connectionString);
            try
            {
                cmd = new SqlCommand(query, conn);
                cmd.CommandType = CommandType.StoredProcedure;
                dAdapter = new SqlDataAdapter(cmd);
                //fill
                dAdapter.Fill(muschoir);
                //sync
                BindingSource bsource = new BindingSource();
                bsource.DataSource = muschoir;
                dgvMusChoir.DataSource = bsource;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            string query = "INSERT INTO MusChoir_Table (Voice, AdminNo, Position, ChoirClass)" +
                " VALUES(@voice, @adm, @pos, @ChoirClass)";
            conn = new SqlConnection(connectionString);
            cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@voice", cboVoice.Text);
            cmd.Parameters.AddWithValue("@adm", cboAdmin.Text);
            cmd.Parameters.AddWithValue("@pos", txtPosition.Text);
            cmd.Parameters.AddWithValue("@ChoirClass", cboChoirClass.Text);
            try
            {
                conn.Open();
                int TotalRowsAffected = cmd.ExecuteNonQuery();

                if (TotalRowsAffected == 1)
                {
                    MessageBox.Show("Record Added Successfully.", "Add", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    LoadMusChoir();
                    clear();
                }
                else
                {
                    MessageBox.Show("Error\nRecord not added.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (SqlException sex)
            {
                if (sex.Number == 2627)
                {
                    MessageBox.Show("THE RECORD ALREADY EXISTS IN THE DATABASE.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                else
                {
                    MessageBox.Show(sex.Message);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            finally
            {
                conn.Close();
            }
        }

        private void cboAdmin_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboAdmin.Text != null)
            {
                filltxts();
                fillChoirClass();
                fillvoice();
                cboVoice.Enabled = true;
                btnDelete.Enabled = true;
            }
        }

        private void cboVoice_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboVoice.Text != null)
            {
                cboChoirClass.Enabled = true;
            }
        }

        private void cboChoirClass_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboChoirClass.Text != null)
            {
                txtPosition.Enabled = true;
            }
        }

        private void txtPosition_TextChanged(object sender, EventArgs e)
        {
            if (txtPosition.Text != null)
            {
                btnAdd.Enabled = true;
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            clear();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                cmdbld = new SqlCommandBuilder(dAdapter);
                dAdapter.Update(muschoir);
                MessageBox.Show("Information successfully updated into the database.", "Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (cboAdmin.Text != null)
            {
                string query = "DELETE FROM MusChoir_Table WHERE Admin_No = @adm";
                conn = new SqlConnection(connectionString);
                cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@adm", cboAdmin.Text);

                try
                {
                    DialogResult rs = MessageBox.Show("Are you sure you want to delete this record from MusChoir?", "Confirm Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (rs == System.Windows.Forms.DialogResult.Yes)
                    {
                        conn.Open();
                        int t = cmd.ExecuteNonQuery();
                        if (t >= 1)
                        {
                            MessageBox.Show("Record Deleted", "Delete.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                            MessageBox.Show("Error\nRecord not Deleted", "Delete Status", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else if (rs == System.Windows.Forms.DialogResult.No)
                        return;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        private void dgvMusChoir_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            btnUpdate.Enabled = true;
        }
    }
}
