﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MusBase
{
    public partial class frmrptAccessoryDetails : Form
    {
        public frmrptAccessoryDetails()
        {
            InitializeComponent();
        }

        private void frmrptAccessoryDetails_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'DataSet1.Accessory_Details' table. You can move, or remove it, as needed.
            this.Accessory_DetailsTableAdapter.Fill(this.DataSet1.Accessory_Details);

            this.reportViewer1.RefreshReport();
        }
    }
}
