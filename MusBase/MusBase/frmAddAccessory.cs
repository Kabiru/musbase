﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
namespace MusBase
{
    public partial class frmAddAccessory : Form
    {
        private string connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;
        private string SqlQuery;
        private SqlConnection conn;
        private SqlCommand cmd;
        SqlDataAdapter dAdapter;
        SqlCommandBuilder cmdbld;
        DataTable AccTable = new DataTable();

        public frmAddAccessory()
        {
            InitializeComponent();
            LoadAccessories();
            //filling accessory type
            fillcboAccType();

            btnAddNewAcc.Enabled = false;
            btnUpdate.Enabled = false;
            lblInsertStatus.Text = null;
        }

        private void LoadAccessories() 
        {
            try 
            {
                conn = new SqlConnection(connectionString);
                SqlQuery = "Load_Accessory_Details_Procedure";
                cmd = new SqlCommand(SqlQuery, conn);
                cmd.CommandType = CommandType.StoredProcedure;
                dAdapter = new SqlDataAdapter(cmd);
                //filling the datatable with data
                dAdapter.Fill(AccTable);
                //syncing by binding with data grid view
                BindingSource bSource = new BindingSource();
                bSource.DataSource = AccTable;
                dgvAccessoryDetails.DataSource = bSource;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }            
        }
        void fillcboAccType()
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString);
            SqlQuery = "SELECT * FROM [dbo].[Accessories_Type]";
            cmd = new SqlCommand(SqlQuery, conn);
            try
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    string AccType = reader.GetString(1);
                    cboAccType.Items.Add(AccType);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                conn.Close();
            }

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void btnAccClose_Click(object sender, EventArgs e)
        {
            this.Close();//closing form
        }

        private void frmAddAccessory_Load(object sender, EventArgs e)
        {

        }

        private void btnAccSave_Click(object sender, EventArgs e)
        {          
            SqlQuery = "INSERT INTO [dbo].[Accessory_Details] ([Accessory_Code], [Name], [Size], [Condition], [Accessory_Type]) VALUES ('"+txtAccCode.Text+"','"+txtAccName.Text+"', '"+txtAccSize.Text+"', '"+txtAccCondition.Text+"', '"+cboAccType.Text+"')";
            conn = new SqlConnection(connectionString);
            cmd = new SqlCommand(SqlQuery, conn);
            try
            {
                conn.Open();
                int TotalRowsAffected = cmd.ExecuteNonQuery();

                if (TotalRowsAffected == 1)
                {
                    lblInsertStatus.Text = "Record saved!";
                    LoadAccessories();
                    btnAddNewAcc.Enabled = true;                    
                    gbxAddAccAccessoryDetails.Enabled = false;
                }
                else if (TotalRowsAffected > 1)
                {
                    lblInsertStatus.Text = "Duplicate record";
                }
                else
                {
                    lblInsertStatus.Text = "Insert incomplete, please try again";
                }
            }
            catch (SqlException sex)
            {
                if (sex.Number == 2627)
                {
                    MessageBox.Show("THE RECORD ALREADY EXISTS IN THE DATABASE.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                else
                {
                    MessageBox.Show(sex.Message);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            finally
            {
                conn.Close();
            }
        }

        private void btnAddNewAcc_Click(object sender, EventArgs e)
        {
            
            //reactivate gbx
            gbxAddAccAccessoryDetails.Enabled = true;
            //clear
            txtAccName.Clear();
            txtAccSize.Clear();
            txtAccCode.Clear();
            txtAccCondition.Clear();

            lblInsertStatus.Text = "Waiting ...";
            cboAccType.Focus();
        }

        private void btnAccAssign_Click(object sender, EventArgs e)
        {
            frmAssignAccessory asgacc = new frmAssignAccessory();
            asgacc.ShowDialog();//modal call
        }

        private void btnAccClear_Click(object sender, EventArgs e)
        {
            txtAccName.Clear();
            txtAccSize.Clear();
            txtAccCode.Clear();
            txtAccCondition.Clear();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                cmdbld = new SqlCommandBuilder(dAdapter);
                dAdapter.Update(AccTable);
                MessageBox.Show("Information updated successfully into the database.", "Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvAccessoryDetails_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            btnUpdate.Enabled = true;
        }

        private void dgvAccessoryDetails_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            btnUpdate.Enabled = true;
        }
    }
}
