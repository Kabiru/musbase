﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace MusBase
{
    public partial class frmStudentViewProfile : Form
    {
        string connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;

        public frmStudentViewProfile()
        {
            InitializeComponent();
            fillcboAdminNo();
            startlbl();
            LoadAcademicChart();
            //panel1.Enabled = false;
            btnSearchStudent.Enabled = false;
        }

        private void LoadAcademicChart()
        {
           foreach (var series in StudentAcademicProgresschart.Series)
           {
               series.Points.Clear();
           }
            
            string query = "SELECT * FROM Grade_Table WHERE Admin_No = @adm ORDER BY DateDone ASC";
            SqlConnection conn = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@adm", cboAsgAccAdminNo.Text);

            try 
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                int avgordinal = reader.GetOrdinal("Average");
                int Descordinal = reader.GetOrdinal("Descprition");

                while(reader.Read())
                {
                    this.StudentAcademicProgresschart.Series["Test"].Points.AddXY(reader.GetString(Descordinal), reader.GetDouble(avgordinal));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }

        }

        private void llstbxAssignedBooks_SelectedIndexChanged(object sender, EventArgs e)
        {

        }        

        private void frmStudentViewProfile_Load(object sender, EventArgs e)
        {

            
        }

        private void pictureBoxProfile_Click(object sender, EventArgs e)
        {

        }

        void fillcboAdminNo()
        {
            SqlConnection conn = new SqlConnection(connectionString);
            string sqlQuery = "SELECT [Admin_No] FROM [dbo].[Student_Profile]";
            SqlCommand cmd = new SqlCommand(sqlQuery, conn);
            try
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    int temp = reader.GetInt32(0);
                    cboAsgAccAdminNo.Items.Add(temp);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        private void cboAsgAccAdminNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboAsgAccAdminNo.Text != null)
            {
                btnSearchStudent.Enabled = true;                
            }
        }

        private void startlbl()
        {
            lblBandMemebership.Text = null;
            lblChoirMembership.Text = null;
            lblForm.Text = null;
            lblHouse.Text = null;
            lblName.Text = null;
        }

        private void checkBand()
        {
            string query = "SELECT * FROM MusBand_Table WHERE AdminNo = @adm";
            SqlConnection conn = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@adm", cboAsgAccAdminNo.Text);

            try 
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                int Pos = reader.GetOrdinal("Position");
                int ty = reader.GetOrdinal("BandType");
                if (reader.Read() == true)
                {
                    string _pos = reader.GetString(Pos);
                    string _ty = reader.GetString(ty);
                    lblBandMemebership.Text = _pos + " " + _ty + ".";
                }
                else if (reader.Read() == false)
                {
                    lblBandMemebership.Text = "Non-Memeber";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                conn.Close();
            }
        }

        private void loadProfileDetails()
        {
            string query = "SELECT FName, LName, House, Form FROM Student_Profile WHERE Admin_No = @adm";
            SqlConnection conn = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@adm", cboAsgAccAdminNo.Text);

            try 
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                int f = reader.GetOrdinal("FName");
                int l = reader.GetOrdinal("LName");
                int h = reader.GetOrdinal("House");
                int fo = reader.GetOrdinal("Form");

                while (reader.Read())
                {
                    string _f = reader.GetString(f);
                    string _l = reader.GetString(l);
                    string _fo = reader.GetString(fo);
                    string _h = reader.GetString(h);
                    //fill
                    lblName.Text = _f + "  " + _l;
                    lblHouse.Text = _h;
                    lblForm.Text = _fo;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        private void loadAssignedBooks()
        {
            lstbxAssignedBooks.Items.Clear();

            string query = "SELECT Book_Details.Book_Title, Student_Book.Book_Code FROM Book_Details "
                + " INNER JOIN Student_Book ON Book_Details.Book_Code = Student_Book.Book_Code " +
                "  WHERE Admin_No = @adm ";
            SqlConnection conn = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@adm", cboAsgAccAdminNo.Text);
            try
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                int c = reader.GetOrdinal("Book_Code");
                int t = reader.GetOrdinal("Book_title");

                if (reader.HasRows == true)
                {
                    while (reader.Read())
                    {

                        string _c = reader.GetString(c);
                        string _t = reader.GetString(t);
                        string concat = _c + ": " + _t;
                        lstbxAssignedBooks.Items.Add(concat);
                    }
                }
                else
                    lstbxAssignedBooks.Items.Add("NULL");


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                conn.Close();
            }
        }

        private void loadAssignedInst()
        {
            lstbxAssignedInstruments.Items.Clear();

            string query = "SELECT Instrument_Details.Instrument_Type, Instrument_Details.Inst_Serial_No FROM Instrument_Details "
                + " INNER JOIN Student_Instrument ON Instrument_Details.Inst_Serial_No = Student_Instrument.Inst_Serial_No " +
                "  WHERE Admin_No = @adm ";
            SqlConnection conn = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@adm", cboAsgAccAdminNo.Text);
            try
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                int n = reader.GetOrdinal("Instrument_Type");
                int m = reader.GetOrdinal("Inst_Serial_No");
                if (reader.HasRows == true)
                {
                    while (reader.Read())
                    {
                        string _n = reader.GetString(n);
                        string _m = reader.GetString(m);
                        string concat = _n + ": " + _m;
                        lstbxAssignedInstruments.Items.Add(concat);
                    }
                }
                else
                {
                    lstbxAssignedInstruments.Items.Add("NULL");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                conn.Close();
            }
        }

        private void loadAssignedAcc()
        {
            lstbxAssignedAccessories.Items.Clear();

            string query = "SELECT Accessory_Details.Name, Student_Accessory.Accessory_Code, Accessory_Details.Size FROM Accessory_Details "
                +" INNER JOIN Student_Accessory ON Accessory_Details.Accessory_Code = Student_Accessory.Accessory_Code "+
                "  WHERE Admin_No = @adm ";
            SqlConnection conn = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@adm", cboAsgAccAdminNo.Text);
            try 
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                int n = reader.GetOrdinal("Name");
                int c = reader.GetOrdinal("Accessory_Code");
                int s = reader.GetOrdinal("Size");
                if (reader.HasRows == true)
                {
                    while (reader.Read())
                    {
                        string _n = reader.GetString(n);
                        string _c = reader.GetString(c);
                        string _s = reader.GetString(s);
                        string concat = _c + "  " + _n + " " + _s;

                        lstbxAssignedAccessories.Items.Add(concat);
                    }
                }
                else
                {
                    lstbxAssignedAccessories.Items.Add("NULL");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                conn.Close();
            }
        }

        private void btnSearchStudent_Click(object sender, EventArgs e)
        {
            //panel1.Enabled = true;
            loadProfileDetails();
            checkBand();
            LoadAcademicChart();
            loadAssignedAcc();
            loadAssignedInst();
            loadAssignedBooks();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void lblForm_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }
    }
}
