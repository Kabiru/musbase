﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace MusBase
{
    public partial class frmMusBand : Form
    {
        private string connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;

        SqlConnection conn;
        SqlCommand cmd;
        SqlDataAdapter dAdapter;
        DataTable musband = new DataTable();
        SqlCommandBuilder cmdbld;

        public frmMusBand()
        {
            InitializeComponent();
            LoadMusBand();
            fillAdminNo();
            startState();
        }

        private void enable()
        {
            cboInstrument.Enabled = true;
            cboBandClass.Enabled = true;
            txtPosition.Enabled = true;
        }

        private void startState()
        {
            cboInstrument.Enabled = false;
            cboBandClass.Enabled = false;
            txtPosition.Enabled = false;
            btnAdd.Enabled = false;
            btnUpdate.Enabled = false;
            btnDelete.Enabled = false;
        }

        private void filltxts()
        {
            string query = "SELECT FName, LName, Form " +
                " FROM Student_Profile WHERE Admin_No = @adm";
                
            conn = new SqlConnection(connectionString);
            cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@adm", cboAdmin.Text);

            try
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                int FNameordinal = reader.GetOrdinal("FName");
                int LNameordinal = reader.GetOrdinal("LName");
                int Formordinal = reader.GetOrdinal("Form");              

                while (reader.Read())
                {
                    string fname = reader.GetString(FNameordinal);
                    string lname = reader.GetString(LNameordinal);
                    string form = reader.GetString(Formordinal);

                    txtFName.Text = fname;
                    txtLName.Text = lname;
                    txtForm.Text = form;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                conn.Close();
            }
        }

        private void fillBandClass()
        {
            cboBandClass.Items.Clear();
            cboBandClass.Items.Add("Junior Band");
            cboBandClass.Items.Add("Senior Band");
            cboBandClass.SelectedItem = "Junior Band";
        }

        

        private void fillInstrument()
        {
            cboInstrument.Items.Clear();
            conn = new SqlConnection(connectionString);
            string query = "SELECT Name FROM Instrument_Type";
            cmd = new SqlCommand(query, conn);            
            try
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                int ordinal = reader.GetOrdinal("Name");
                while (reader.Read())
                {
                    string temp = reader.GetString(ordinal);
                    cboInstrument.Items.Add(temp);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        public void fillAdminNo()
        {
            cboAdmin.Items.Clear();
            conn = new SqlConnection(connectionString);
            string query = "Load_MusBand_Admin_Procedure";
            cmd = new SqlCommand(query, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                int ordinal = reader.GetOrdinal("Admin_No");
                while (reader.Read())
                {
                    int temp = reader.GetInt32(ordinal);
                    cboAdmin.Items.Add(temp);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }
        private void LoadMusBand()
        {
            //dgvMusBand.Refresh();
            string query = "Load_MusBand_Procedure";
            conn = new SqlConnection(connectionString);            
            try 
            {
                cmd = new SqlCommand(query, conn);
                cmd.CommandType = CommandType.StoredProcedure;            
                dAdapter = new SqlDataAdapter(cmd);                
                //fill
                dAdapter.Fill(musband);
                //sync
                BindingSource bsource = new BindingSource();
                bsource.DataSource = musband;
                dgvMusBand.DataSource = bsource;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }            
        }

        private void splitContainer1_Panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void splitContainer1_Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void frmMusBand_Load(object sender, EventArgs e)
        {

        }

        private void cboAdmin_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboAdmin.Text != null)
            {
                filltxts();
                fillBandClass();
                fillInstrument();
                cboInstrument.Enabled = true;
                btnDelete.Enabled = true;
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {

        }

        private void clear()
        {
            txtFName.Clear();
            txtForm.Clear();
            txtLName.Clear();
            txtPosition.Clear();
            cboAdmin.Text = null;
            cboAdmin.SelectedItem = null;
            cboBandClass.Text = null;
            cboBandClass.SelectedItem = null;
            cboInstrument.Text = null;
            cboInstrument.SelectedItem = null;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            string query = "INSERT INTO MusBand_Table (Instrument, AdminNo, Position, BandType)"+
                " VALUES(@inst, @adm, @pos, @bandClass)";            
            conn = new SqlConnection(connectionString);
            cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@inst", cboInstrument.Text);
            cmd.Parameters.AddWithValue("@adm", cboAdmin.Text);
            cmd.Parameters.AddWithValue("@pos", txtPosition.Text);
            cmd.Parameters.AddWithValue("@bandClass", cboBandClass.Text);
            try
            {
                conn.Open();
                int TotalRowsAffected = cmd.ExecuteNonQuery();

                if (TotalRowsAffected == 1)
                {
                    MessageBox.Show("Record Added Successfully.", "Add", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    LoadMusBand();
                    clear();
                }               
                else
                {
                    MessageBox.Show("Error\nRecord not added.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (SqlException sex)
            {
                if (sex.Number == 2627)
                {
                    MessageBox.Show("THE RECORD ALREADY EXISTS IN THE DATABASE.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                else
                {
                    MessageBox.Show(sex.Message);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            finally
            {
                conn.Close();
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            clear();
            startState();
        }

        private void cboInstrument_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboInstrument.Text != null)
            {
                cboBandClass.Enabled = true;
            }
        }

        private void cboBandClass_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboBandClass.Text != null)
            {
                txtPosition.Enabled = true;
            }
        }

        private void txtPosition_TextChanged(object sender, EventArgs e)
        {
            if (txtPosition.Text != null)
            {
                btnAdd.Enabled = true;
            }
        }

        private void dgvMusBand_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            btnUpdate.Enabled = true;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                cmdbld = new SqlCommandBuilder(dAdapter);
                dAdapter.Update(musband);
                MessageBox.Show("Information successfully updated into the database.", "Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dgvMusBand_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            btnUpdate.Enabled = true;
        }

        private void btnBandAcademic_Click(object sender, EventArgs e)
        {
            //frmrptBandAcademics a = new frmrptBandAcademics();
            //a.Show();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
          if (cboAdmin.Text != null)
          {
              string query = "DELETE FROM MusBand_Table WHERE Admin_No = @adm";
              conn = new SqlConnection(connectionString);
              cmd = new SqlCommand(query, conn);
              cmd.Parameters.AddWithValue("@adm", cboAdmin.Text);

              try 
              {                  
                  DialogResult rs = MessageBox.Show("Are you sure you want to delete this record from MusBand?", "Confirm Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                  if (rs == System.Windows.Forms.DialogResult.Yes)
                  {
                      conn.Open();
                      int t = cmd.ExecuteNonQuery();
                      if (t >= 1)
                      {
                          MessageBox.Show("Record Deleted", "Delete.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                      }
                      else
                          MessageBox.Show("Error\nRecord not Deleted", "Delete Status", MessageBoxButtons.OK, MessageBoxIcon.Error);
                  }
                  else if (rs == System.Windows.Forms.DialogResult.No)
                      return;
              }
              catch (Exception ex)
              {
                  MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
              }
              finally
              {
                  conn.Close();
              }
          }
        }
    }
}
