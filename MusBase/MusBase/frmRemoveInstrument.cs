﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;


namespace MusBase
{
    public partial class frmRemoveInstrument : Form
    {
        public frmRemoveInstrument()
        {
            InitializeComponent();
            fillcboFamily();
            //disabling cbos
            cboRmvInstType.Enabled = false;
            cboRmvInstSerialNo.Enabled = false;
            btnRmvInstrument.Enabled = false;

            LoadInstruments();
        }

        private void LoadInstruments() 
        {
            string connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;
            string sqlQuery = "Load_Instruments_Procedure";

            SqlConnection conn = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(sqlQuery, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            try 
            {
                SqlDataAdapter dAdapter = new SqlDataAdapter(cmd);
                DataTable Instruments = new DataTable();
                //fill
                dAdapter.Fill(Instruments);
                //sync
                BindingSource bsource = new BindingSource();
                bsource.DataSource = Instruments;
                dgvRemoveInstrument.DataSource = bsource;
            }
            catch (Exception ex)
            { MessageBox.Show("Error\n" + ex.Message); }
        }

        private void LoadInstrumentsFamily()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;
            string sqlQuery = "SELECT Instrument_Details.Inst_Serial_No AS [SERIAL NO], Instrument_Details.Instrument_Type AS [TYPE], Instrument_Details.Inst_Family "+
	            "AS [FAMILY], Instrument_Details.Inst_Make AS [MAKE], Instrument_Details.Inst_Condition AS [CONDITION] FROM Instrument_Details  WHERE Inst_Family = @family";

            SqlConnection conn = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(sqlQuery, conn);
            cmd.Parameters.AddWithValue("@family", cboRmvInstFamily.Text);
            try
            {
                SqlDataAdapter dAdapter = new SqlDataAdapter(cmd);
                DataTable InstrumentsFamily = new DataTable();
                //fill
                dAdapter.Fill(InstrumentsFamily);
                //sync
                BindingSource bsource = new BindingSource();
                bsource.DataSource = InstrumentsFamily;
                dgvRemoveInstrument.DataSource = bsource;
            }
            catch (Exception ex)
            { MessageBox.Show("Error\n" + ex.Message); }
        }

        private void LoadInstrumentsType() 
        {
            string connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;
            string sqlQuery = "SELECT Instrument_Details.Inst_Serial_No AS [SERIAL NO], Instrument_Details.Instrument_Type AS [TYPE], Instrument_Details.Inst_Family " +
                "AS [FAMILY], Instrument_Details.Inst_Make AS [MAKE], Instrument_Details.Inst_Condition AS [CONDITION] FROM Instrument_Details  WHERE Instrument_Type = @Type";

            SqlConnection conn = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(sqlQuery, conn);
            cmd.Parameters.AddWithValue("@Type", cboRmvInstType.Text);
            try
            {
                SqlDataAdapter dAdapter = new SqlDataAdapter(cmd);
                DataTable InstrumentsType = new DataTable();
                //fill
                dAdapter.Fill(InstrumentsType);
                //sync
                BindingSource bsource = new BindingSource();
                bsource.DataSource = InstrumentsType;
                dgvRemoveInstrument.DataSource = bsource;
            }
            catch (Exception ex)
            { MessageBox.Show("Error\n" + ex.Message); }
        }

        private void LoadInstrumentsSerial()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;
            string sqlQuery = "SELECT Instrument_Details.Inst_Serial_No AS [SERIAL NO], Instrument_Details.Instrument_Type AS [TYPE], Instrument_Details.Inst_Family " +
                "AS [FAMILY], Instrument_Details.Inst_Make AS [MAKE], Instrument_Details.Inst_Condition AS [CONDITION] FROM Instrument_Details  WHERE Inst_Serial_No = @serial";

            SqlConnection conn = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(sqlQuery, conn);
            cmd.Parameters.AddWithValue("@serial", cboRmvInstSerialNo.Text);
            try
            {
                SqlDataAdapter dAdapter = new SqlDataAdapter(cmd);
                DataTable InstrumentsSerial = new DataTable();
                //fill
                dAdapter.Fill(InstrumentsSerial);
                //sync
                BindingSource bsource = new BindingSource();
                bsource.DataSource = InstrumentsSerial;
                dgvRemoveInstrument.DataSource = bsource;
            }
            catch (Exception ex)
            { MessageBox.Show("Error\n" + ex.Message); }
        }

        void filltxt() 
        {
            //FILL TEXTBOXES
            string connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;
            string sqlQuery = "SELECT [Inst_Make], [Inst_Condition] FROM [dbo].[Instrument_Details] WHERE [Inst_Serial_No] = @Instrument_SerialNo";

            SqlConnection conn = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(sqlQuery, conn);
            cmd.Parameters.AddWithValue("@Instrument_SerialNo", cboRmvInstSerialNo.Text);
            try
            {
                conn.Open();
                //conection open
                SqlDataReader reader = cmd.ExecuteReader();

                int ordinalMake = reader.GetOrdinal("Inst_Make");
                int ordinalcondition = reader.GetOrdinal("Inst_Condition");
                while (reader.Read())
                {
                    string tempMake = reader.GetString(ordinalMake);
                    string tempCondition = reader.GetString(ordinalcondition);

                    txtRmvInstMake.Text = tempMake;
                    txtRmvInstCondition.Text = tempCondition;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }    
        }

        void fillcboSerialNo() 
        {
            //FILL SERIAL NUMBER
            string connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;
            string sqlQuery = "SELECT [Inst_Serial_No] FROM [dbo].[Instrument_Details] WHERE [Instrument_Type] = @Instrument_Type";

            SqlConnection conn = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(sqlQuery, conn);
            cmd.Parameters.AddWithValue("@Instrument_Type", cboRmvInstType.Text);
            try
            {
                conn.Open();
                //conection open
                SqlDataReader reader =  cmd.ExecuteReader();

                while (reader.Read())
                {
                    string temp = reader.GetString(0);
                    cboRmvInstSerialNo.Items.Add(temp);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }
        void fillcboType() 
        {
            //FAMILY >> TYPE
            string connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;
            string sqlQuery = "SELECT DISTINCT [Instrument_Type] FROM [dbo].[Instrument_Details]  WHERE [Inst_Family] = @Instrument_Family";

            SqlConnection conn = new SqlConnection(connectionString);

            try
            {

                SqlCommand cmd = new SqlCommand(sqlQuery, conn);
                cmd.Parameters.AddWithValue("@Instrument_Family", cboRmvInstFamily.Text);

                SqlDataReader reader;
                //open connection
                conn.Open();
                reader = cmd.ExecuteReader();//executing command

                int ordinal = reader.GetOrdinal("Instrument_Type");
                while (reader.Read())
                {
                    string instType = reader.GetString(ordinal);
                    cboRmvInstType.Items.Add(instType);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            finally
            {
                conn.Close();//closing connection
            }
        }

        void fillcboFamily() 
        {
            //FAMILY
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString);
            string sqlQuerySelect = "SELECT DISTINCT [Inst_Family] FROM [dbo].[Instrument_Details]";
            SqlCommand cmd = new SqlCommand(sqlQuerySelect, conn);

            try
            {
                conn.Open();//connection open

                SqlDataReader reader =  cmd.ExecuteReader();

                int ordinal = reader.GetOrdinal("Inst_Family");

                while (reader.Read())
                {
                    string temp = reader.GetString(ordinal);
                    cboRmvInstFamily.Items.Add(temp);//populating the combo box
                }
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }
            finally
            {
                conn.Close();//connection closed
            }
        }

        private void lblRmvInst_Click(object sender, EventArgs e)
        {

        }

        private void btnRmvInstDone_Click(object sender, EventArgs e)
        {
            this.Close();//closing form
        }

        private void frmRemoveInstrument_Load(object sender, EventArgs e)
        {

        }

        private void cboRmvInstFamily_SelectedIndexChanged(object sender, EventArgs e)
        {
            //FAMILY
            cboRmvInstType.Items.Clear();

            if (cboRmvInstFamily.Text != null)
            {
                fillcboType();
                LoadInstrumentsFamily();
                cboRmvInstType.Enabled = true; 
            }
        }

        private void cboRmvInstType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboRmvInstType.Text != null)
            {//INSTRUMENT TYPE
                cboRmvInstSerialNo.Items.Clear();
                fillcboSerialNo();
                cboRmvInstSerialNo.Enabled = true;
                cboRmvInstFamily.Enabled = false;
                LoadInstrumentsType();
            }
        }

        private void btnRmvInstrument_Click(object sender, EventArgs e)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;
            string sqlQueryDelete = "DELETE  FROM [Instrument_Details] WHERE [Inst_Serial_No] = @SerialNo";

            SqlConnection conn = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(sqlQueryDelete, conn);
            cmd.Parameters.AddWithValue("@SerialNo", cboRmvInstSerialNo.Text);
            try
            {
                conn.Open();
               int TotalRowsAffected =  cmd.ExecuteNonQuery();

               if (TotalRowsAffected == 1)
               {
                   MessageBox.Show(cboRmvInstSerialNo.Text+" has been deleted from the database.", "Record Deleted", MessageBoxButtons.OK, MessageBoxIcon.Information);
                   btnRmvInstrument.Enabled = false;
               }
               else
               {
                   MessageBox.Show("Delete unsuccessful, please try again.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
               }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();//closing connecion
            }
        }

        private void cboRmvInstSerialNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cboRmvInstSerialNo.Text != null)
            {//filling the textboxes
                filltxt();
                btnRmvInstrument.Enabled = true;
                cboRmvInstType.Enabled = false;
                LoadInstrumentsSerial();
            }
        }

        private void btnRmvInstClear_Click(object sender, EventArgs e)
        {
            cboRmvInstType.Enabled = true;
            cboRmvInstFamily.SelectedItem = null;            
            cboRmvInstType.Text = null;
            cboRmvInstSerialNo.SelectedItem = null;
            txtRmvInstCondition.Clear();
            txtRmvInstMake.Clear();

            LoadInstruments();

            cboRmvInstFamily.Enabled = true;
            btnRmvInstrument.Enabled = false;
            cboRmvInstType.Enabled = false;
            cboRmvInstSerialNo.Enabled = false;
        }
    }
}
