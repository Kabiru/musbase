﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace MusBase
{
    public partial class frmAssignBook : Form
    {
        string connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;
        SqlConnection conn;
        SqlCommand cmd;

        public frmAssignBook()
        {
            InitializeComponent();
            LoadAsgBk();
            fillcboAdmin();

            btnAsgBkSubmitBk.Enabled = false;
            btnAsgBkNext.Enabled = false;
            btnNewAsgBk.Enabled = false;
            //disabling book
            gbxAsgBkSelectBk.Enabled = false;
        }

        private void assignBook()
        {
            string query = "INSERT INTO [dbo].[Student_Book] ([Admin_No], [Book_Code])" +
                "VALUES(@AdminNo, @BookCode)";

            conn = new SqlConnection(connectionString);
            cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@AdminNo", Convert.ToInt32(cboAsgBkAdminNo.Text));
            cmd.Parameters.AddWithValue("@BookCode", cboAsgBkCode.Text);
            try
            {
                conn.Open();
                int TotalRowsAffected = cmd.ExecuteNonQuery();
                if (TotalRowsAffected == 1)
                {
                    MessageBox.Show("BOOK SUCCESSFULLY ASSIGNED.", "Assign Status", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    LoadAsgBk();
                    gbxAsgBkSelectBk.Enabled = false;
                    gbxAsgBkSelectStd.Enabled = false;
                    btnAsgBkNext.Enabled = false;
                    btnAsgBkSubmitBk.Enabled = false;
                    btnNewAsgBk.Enabled = true;
                    btnAsgBkSubmitBk.Enabled = false;
                }
                else
                {
                    MessageBox.Show("BOOK NOT ASSIGNED,\nPLEASE TRY AGAIN.", "Assign Status", MessageBoxButtons.RetryCancel, MessageBoxIcon.Exclamation);
                }
            }
            catch (SqlException sex)
            {
                if (sex.Number == 2627)
                {
                    MessageBox.Show("THE RECORD ALREADY EXISTS IN THE DATABASE.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    addnew();
                }
                else
                {
                    MessageBox.Show(sex.Message);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                conn.Close();
            }
        }

        private void LoadAsgBk() 
        {
            string query = "AssignedBook_Procedure";
            conn = new SqlConnection(connectionString);
            cmd = new SqlCommand(query, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter dAdapter = new SqlDataAdapter(cmd);
            DataTable AssignedBooks = new DataTable();
            //fill
            dAdapter.Fill(AssignedBooks);
            //sync
            BindingSource bSource = new BindingSource();
            bSource.DataSource = AssignedBooks;
            dgvAssignBooks.DataSource = bSource;
        }

        private void filltxtBook() 
        {
            string query = "SELECT * FROM [Book_Details] WHERE [Book_Code] = @BookCode";

            conn = new SqlConnection(connectionString);
            cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@BookCode", cboAsgBkCode.Text);
            try
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                int ordinalTitle = reader.GetOrdinal("Book_Title");
                int ordinalType = reader.GetOrdinal("Book_Type");                
                int ordinalCondition = reader.GetOrdinal("Book_Condition");

                while (reader.Read())
                {
                    string Title = reader.GetString(ordinalTitle);
                    string Type = reader.GetString(ordinalType);                    
                    string Condition = reader.GetString(ordinalCondition);
                                        
                    txtAsgBkType.Text = Type;
                    txtAsgBkTitle.Text = Title;
                    txtAsgBkCondition.Text = Condition;
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Error);
            }
            finally
            {
                conn.Close();
            } 
        }

        private void fillcboBkCode() 
        {
            
            string sqlQuery = "Select_Book_Code_Procedure";
            SqlConnection conn = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(sqlQuery, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                int ordinalAdmin = reader.GetOrdinal("Book_Code");
                while (reader.Read())
                {
                    string temp = reader.GetString(ordinalAdmin);
                    cboAsgBkCode.Items.Add(temp);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n"+ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); 
            }
            finally
            { 
                conn.Close(); 
            }
        }

        private void fillcboAdmin()
        {            
            string sqlQuery = "SELECT [Admin_No] FROM [dbo].[Student_Profile]";

            SqlConnection conn = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(sqlQuery, conn);
            try
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                int ordinalAdmin = reader.GetOrdinal("Admin_No");
                while (reader.Read())
                {
                    int temp = reader.GetInt32(ordinalAdmin);
                    cboAsgBkAdminNo.Items.Add(temp);
                }
                reader.Close();
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }
            finally
            { conn.Close(); }
        }

        private void fillStudentTxts() 
        {            
            string query = "SELECT * FROM [Student_Profile] WHERE [Admin_No] = @AdminNo";
            SqlDataReader reader;
            SqlConnection conn = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@AdminNo", Convert.ToInt32(cboAsgBkAdminNo.Text));
            try
            {
                conn.Open();
                reader = cmd.ExecuteReader();
                int ordinalFName = reader.GetOrdinal("FName");
                int ordinalMName = reader.GetOrdinal("MName");
                int ordinalLName = reader.GetOrdinal("LName");
                int ordinalForm = reader.GetOrdinal("Form");
                int ordinalHouse = reader.GetOrdinal("House");

                while (reader.Read())
                {
                    string FName = reader.GetString(ordinalFName);
                    string MName = reader.GetString(ordinalMName);
                    string LName = reader.GetString(ordinalLName);
                    string Form = reader.GetString(ordinalForm);
                    string House = reader.GetString(ordinalHouse);

                    txtAsgBkFName.Text = FName;
                    txtAsgBkMName.Text = MName;
                    txtAsgBkLName.Text = LName;
                    txtAsgBkForm.Text = Form;
                    txtAsgBkHouse.Text = House;
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Error);
            }
            finally
            {
                conn.Close();
            }
        }

        private void frmAssignBook_Load(object sender, EventArgs e)
        {

        }

        private void btnAsgBkSubmit_Click(object sender, EventArgs e)
        {
            //activate select book
            gbxAsgBkSelectBk.Enabled = true;
            fillcboBkCode();
            btnAsgBkSubmitBk.Enabled = false;
        }

        private void btnAsgBkDone_Click(object sender, EventArgs e)
        {
            this.Close();//closing form
        }

        private void lblAsgAccForm_Click(object sender, EventArgs e)
        {

        }

        private void cboAsgInstForm_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cboAsgBkAdminNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboAsgBkAdminNo.Text != null) 
            {
                fillStudentTxts();
                btnAsgBkNext.Enabled = true;    
            }
        }

        private void cboAsgBkCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboAsgBkCode.Text != null)
            {
                filltxtBook();
                btnAsgBkSubmitBk.Enabled = true;
            }            
        }

        private void btnAsgBkSubmitBk_Click(object sender, EventArgs e)
        {
            assignBook();                     
        }

        private void btnNewAsgBk_Click(object sender, EventArgs e)
        {
             gbxAsgBkSelectStd.Enabled = true;
            //clear student
            txtAsgBkForm.Clear();
            txtAsgBkHouse.Clear();
            txtAsgBkFName.Clear();
            txtAsgBkLName.Clear();
            txtAsgBkMName.Clear();
            //clear inst
            cboAsgBkCode.SelectedItem = null;
            cboAsgBkCode.Text = null;            
            txtAsgBkType.Clear();
            txtAsgBkCondition.Clear();
            txtAsgBkTitle.Clear();
            cboAsgBkAdminNo.Focus();
        }        

        private void frmAssignBook_FormClosing(object sender, FormClosingEventArgs e)
        {
           
        }

        private void cboAsgBkAdminNo_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            if (cboAsgBkAdminNo.Text != null)
            {
                fillStudentTxts();
                btnAsgBkNext.Enabled = true;
            }
        }

        private void btnAsgBkSubmitBk_Click_1(object sender, EventArgs e)
        {
            assignBook();
        }

        private void cboAsgBkCode_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            if (cboAsgBkCode.Text != null)
            {
                filltxtBook();
                btnAsgBkSubmitBk.Enabled = true;
            } 
        }

        private void btnAsgBkDone_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAsgBkNext_Click(object sender, EventArgs e)
        {
            gbxAsgBkSelectBk.Enabled = true;
            fillcboBkCode();
        }

        private void addnew()
        {
            gbxAsgBkSelectStd.Enabled = true;
            //clear student
            txtAsgBkForm.Clear();
            txtAsgBkHouse.Clear();
            txtAsgBkFName.Clear();
            txtAsgBkLName.Clear();
            txtAsgBkMName.Clear();
            //clear inst
            cboAsgBkCode.SelectedItem = null;
            cboAsgBkCode.Text = null;
            txtAsgBkType.Clear();
            txtAsgBkCondition.Clear();
            txtAsgBkTitle.Clear();
            cboAsgBkAdminNo.Focus();
            
            //disable                     
            btnAsgBkSubmitBk.Enabled = false;
            btnAsgBkNext.Enabled = false;
            btnNewAsgBk.Enabled = false;
            //disabling book
            gbxAsgBkSelectBk.Enabled = false;
        }

        private void btnNewAsgBk_Click_1(object sender, EventArgs e)
        {
            addnew();
        }
      
    }
}
