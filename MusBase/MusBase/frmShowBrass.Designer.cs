﻿namespace MusBase
{
    partial class frmShowBrass
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea6 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend6 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dgvBrass = new System.Windows.Forms.DataGridView();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cboRmvInstSerialNo = new System.Windows.Forms.ComboBox();
            this.cboRmvInstType = new System.Windows.Forms.ComboBox();
            this.lblRmvInstSerialNo = new System.Windows.Forms.Label();
            this.lblRmvInstType = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBrass)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.dgvBrass);
            this.panel1.Location = new System.Drawing.Point(12, 195);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(685, 221);
            this.panel1.TabIndex = 0;
            // 
            // dgvBrass
            // 
            this.dgvBrass.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBrass.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvBrass.Location = new System.Drawing.Point(0, 0);
            this.dgvBrass.Name = "dgvBrass";
            this.dgvBrass.Size = new System.Drawing.Size(685, 221);
            this.dgvBrass.TabIndex = 0;
            // 
            // chart1
            // 
            chartArea6.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea6);
            legend6.Name = "Legend1";
            this.chart1.Legends.Add(legend6);
            this.chart1.Location = new System.Drawing.Point(391, 12);
            this.chart1.Name = "chart1";
            series6.ChartArea = "ChartArea1";
            series6.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Doughnut;
            series6.Legend = "Legend1";
            series6.Name = "Series1";
            this.chart1.Series.Add(series6);
            this.chart1.Size = new System.Drawing.Size(300, 159);
            this.chart1.TabIndex = 1;
            this.chart1.Text = "chart1";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cboRmvInstSerialNo);
            this.groupBox1.Controls.Add(this.cboRmvInstType);
            this.groupBox1.Controls.Add(this.lblRmvInstSerialNo);
            this.groupBox1.Controls.Add(this.lblRmvInstType);
            this.groupBox1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(274, 159);
            this.groupBox1.TabIndex = 204;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Search";
            // 
            // cboRmvInstSerialNo
            // 
            this.cboRmvInstSerialNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboRmvInstSerialNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboRmvInstSerialNo.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboRmvInstSerialNo.ForeColor = System.Drawing.Color.RoyalBlue;
            this.cboRmvInstSerialNo.FormattingEnabled = true;
            this.cboRmvInstSerialNo.Location = new System.Drawing.Point(109, 76);
            this.cboRmvInstSerialNo.Name = "cboRmvInstSerialNo";
            this.cboRmvInstSerialNo.Size = new System.Drawing.Size(150, 25);
            this.cboRmvInstSerialNo.TabIndex = 205;
            this.cboRmvInstSerialNo.SelectedIndexChanged += new System.EventHandler(this.cboRmvInstSerialNo_SelectedIndexChanged);
            // 
            // cboRmvInstType
            // 
            this.cboRmvInstType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboRmvInstType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboRmvInstType.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboRmvInstType.ForeColor = System.Drawing.Color.RoyalBlue;
            this.cboRmvInstType.FormattingEnabled = true;
            this.cboRmvInstType.Location = new System.Drawing.Point(109, 31);
            this.cboRmvInstType.Name = "cboRmvInstType";
            this.cboRmvInstType.Size = new System.Drawing.Size(150, 25);
            this.cboRmvInstType.TabIndex = 204;
            this.cboRmvInstType.SelectedIndexChanged += new System.EventHandler(this.cboRmvInstType_SelectedIndexChanged);
            // 
            // lblRmvInstSerialNo
            // 
            this.lblRmvInstSerialNo.AutoSize = true;
            this.lblRmvInstSerialNo.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRmvInstSerialNo.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lblRmvInstSerialNo.Location = new System.Drawing.Point(6, 79);
            this.lblRmvInstSerialNo.Name = "lblRmvInstSerialNo";
            this.lblRmvInstSerialNo.Size = new System.Drawing.Size(97, 17);
            this.lblRmvInstSerialNo.TabIndex = 207;
            this.lblRmvInstSerialNo.Text = "Serial Number";
            // 
            // lblRmvInstType
            // 
            this.lblRmvInstType.AutoSize = true;
            this.lblRmvInstType.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRmvInstType.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lblRmvInstType.Location = new System.Drawing.Point(6, 31);
            this.lblRmvInstType.Name = "lblRmvInstType";
            this.lblRmvInstType.Size = new System.Drawing.Size(39, 17);
            this.lblRmvInstType.TabIndex = 206;
            this.lblRmvInstType.Text = "Type";
            // 
            // frmShowBrass
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(703, 428);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.panel1);
            this.Name = "frmShowBrass";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Brass";
            this.Load += new System.EventHandler(this.frmShowBrass_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBrass)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dgvBrass;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cboRmvInstSerialNo;
        private System.Windows.Forms.ComboBox cboRmvInstType;
        private System.Windows.Forms.Label lblRmvInstSerialNo;
        private System.Windows.Forms.Label lblRmvInstType;
    }
}