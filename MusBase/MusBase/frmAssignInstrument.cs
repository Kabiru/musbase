﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace MusBase
{
    public partial class frmAssignInstrument : Form
    {
        string connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;
        SqlConnection conn;
        SqlCommand cmd;
        public frmAssignInstrument()
        {
            InitializeComponent();
            LoadAsgInstruments();
            fillcboAdmin();

            btnAsgInstNext.Enabled = false;
            btnAsgInstSubmitInst.Enabled = false;
            btnNewAsgInst.Enabled = false;
            //disabling instrument groupbox
            gbxAsgInstSelectInst.Enabled = false;
        }

        private void LoadAsgInstruments() 
        {
            string query = "AssignedInst_Procedure";
            conn = new SqlConnection(connectionString);
            SqlDataAdapter dAdapter;
            cmd = new SqlCommand(query, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            dAdapter = new SqlDataAdapter(cmd);
            DataTable AsgInstTable = new DataTable();
            //fill data table with data
            dAdapter.Fill(AsgInstTable);
            //sync data with dgv 
            BindingSource bSource = new BindingSource();
            bSource.DataSource = AsgInstTable;
            dgvAssignInstrument.DataSource = bSource;

        }

        private void fillcboInstSerialNumber() 
        {

            string sqlQuery = "SELECT [Inst_Serial_No] FROM [dbo].[Instrument_Details]";

            SqlConnection conn = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(sqlQuery, conn);
            try
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                int ordinalAdmin = reader.GetOrdinal("Inst_Serial_No");
                while (reader.Read())
                {
                    string temp = reader.GetString(ordinalAdmin);
                    cboAsgInstSerialNo.Items.Add(temp);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n"+ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); 
            }
            finally
            { 
                conn.Close(); 
            }
        }

        private void fillcboAdmin() 
        {
            string connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;
            string sqlQuery = "SELECT [Admin_No] FROM [dbo].[Student_Profile]";

            SqlConnection conn = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(sqlQuery, conn);
            try
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                int ordinalAdmin = reader.GetOrdinal("Admin_No");
                while (reader.Read())
                {
                    int  temp = reader.GetInt32(ordinalAdmin);
                    cboAsgInstAdminNo.Items.Add(temp);
                }
                reader.Close();
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }
            finally
            { conn.Close(); }
        }

        private void fillstdtxtSerial() 
        {
            string query = "SELECT * FROM [Instrument_Details] WHERE [Inst_Serial_No] = @SerialNo";

            conn = new SqlConnection(connectionString);
            cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@SerialNo", cboAsgInstSerialNo.Text);

            try
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();                
                int ordinalMake = reader.GetOrdinal("Inst_Make");
                int ordinalType = reader.GetOrdinal("Instrument_Type");
                int ordinalFamily = reader.GetOrdinal("Inst_Family");
                int ordinalCondition = reader.GetOrdinal("Inst_Condition");

                while (reader.Read())
                {
                    string Make = reader.GetString(ordinalMake);
                    string Type = reader.GetString(ordinalType);
                    string Family = reader.GetString(ordinalFamily);
                    string Condition = reader.GetString(ordinalCondition);

                    txtAsgInstLName.Text = Make;
                    txtAsgInstType.Text = Type;
                    txtAsgInstFamily.Text = Family;
                    txtAsgInstCondition.Text = Condition;
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Error);
            }
            finally
            {
                conn.Close();
            } 
        }

        private void fillStdtxt() 
        {
            string connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;
            string query = "SELECT * FROM [Student_Profile] WHERE [Admin_No] = @AdminNo";
            SqlDataReader reader;
            SqlConnection conn = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@AdminNo", Convert.ToInt32(cboAsgInstAdminNo.Text));            
            try
            {
                conn.Open();
                reader = cmd.ExecuteReader();
                int ordinalFName = reader.GetOrdinal("FName");
                int ordinalMName = reader.GetOrdinal("MName");
                int ordinalLName = reader.GetOrdinal("LName");
                int ordinalForm = reader.GetOrdinal("Form");
                int ordinalHouse = reader.GetOrdinal("House");

                while (reader.Read())
                {
                    string FName = reader.GetString(ordinalFName);
                    string MName = reader.GetString(ordinalMName);
                    string LName = reader.GetString(ordinalLName);
                    string Form = reader.GetString(ordinalForm);
                    string House = reader.GetString(ordinalHouse);

                    txtAsgInstFName.Text = FName;
                    txtAsgInstMName.Text = MName;
                    txtAsgInstLName.Text = LName;
                    txtAsgInstForm.Text = Form;
                    txtAsgInstHouse.Text = House;
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n"+ex.Message,"Error", MessageBoxButtons.AbortRetryIgnore,MessageBoxIcon.Error);
            }
            finally
            {                
                conn.Close();
            }
        }

        private void frmAssignInstrument_Load(object sender, EventArgs e)
        {


        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dgvAssignInstrument_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnAsgInstAddInstrument_Click(object sender, EventArgs e)
        {
            frmAddInstrument AddIsnt = new frmAddInstrument();
            AddIsnt.Show();//add instrument
        }

        private void btnAsgInstClear_Click(object sender, EventArgs e)
        {
            
        }

        private void btnAsgInstClearStudent_Click(object sender, EventArgs e)
        {
          
        }

        private void btnAsgInstAddStudent_Click(object sender, EventArgs e)
        {
            frmAddStudent AddStd = new frmAddStudent();
            AddStd.ShowDialog();// add student modal call
        }

        private void btnAsgInstDone_Click(object sender, EventArgs e)
        {
            this.Close();//closing form
        }

        private void btnAsgInstSubmit_Click(object sender, EventArgs e)
        {
            gbxAsgInstSelectInst.Enabled = true;
            btnAsgInstSubmitInst.Enabled = false;
            fillcboInstSerialNumber();
            btnAsgInstSubmitInst.Enabled = false;
        }

        private void cboAsgInstForm_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnAsgInstClear_Click_1(object sender, EventArgs e)
        {

        }

        private void cboAsgInstAdminNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboAsgInstAdminNo.Text != null)
            {
                fillStdtxt();
                btnAsgInstNext.Enabled = true;
            }
        }

        private void btnAsgInstClearStudent_Click_1(object sender, EventArgs e)
        {

        }
        private void disableSubmit() { btnAsgInstSubmitInst.Enabled = false; }
        private void btnAsgIsntSubmitInst_Click(object sender, EventArgs e)
        {            
            string query = "INSERT INTO [dbo].[Student_Instrument] ([Admin_No], [Inst_Serial_No])"+
                "VALUES(@AdminNo, @InstSerialNo)";

            conn = new SqlConnection(connectionString);
            cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@AdminNo", Convert.ToInt32(cboAsgInstAdminNo.Text));
            cmd.Parameters.AddWithValue("@InstSerialNo", cboAsgInstSerialNo.Text);
            try
            {
                conn.Open();
                int TotalRowsAffected = cmd.ExecuteNonQuery();
                if (TotalRowsAffected == 1)
                {
                    MessageBox.Show("INSTRUMENT SUCCESSFULLY ASSIGNED.", "Assign Status", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    LoadAsgInstruments();                    
                    gbxAsgInstSelectInst.Enabled = false;
                    gbxAsgInstSelectstd.Enabled = false;
                    btnAsgInstNext.Enabled = false;
                    btnAsgInstSubmitInst.Enabled = false;
                    btnNewAsgInst.Enabled = true;
                    disableSubmit();
                }
                else
                {
                    MessageBox.Show("INSTRUMENT NOT ASSIGNED,\nPLEASE TRY AGAIN.", "Assign Status", MessageBoxButtons.RetryCancel, MessageBoxIcon.Exclamation);
                }
            }
            catch (SqlException sex)
            {
                if (sex.Number == 2627)
                {
                    MessageBox.Show("THE RECORD ALREADY EXISTS IN THE DATABASE.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    addnew();
                }
                else 
                {
                    MessageBox.Show(sex.Message);
                    addnew();
                }
            }
            catch (Exception ex) 
            {
                MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            { 
                conn.Close();
            }
        }

        private void cboAsgInstSerialNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboAsgInstSerialNo.Text != null)
            {
                fillstdtxtSerial();
                btnAsgInstSubmitInst.Enabled = true;
            }
        }

        private void addnew()
        {
            gbxAsgInstSelectstd.Enabled = true;
            //clear student
            txtAsgInstForm.Clear();
            txtAsgInstHouse.Clear();
            txtAsgInstFName.Clear();
            txtAsgInstLName.Clear();
            txtAsgInstMName.Clear();
            //clear inst
            cboAsgInstSerialNo.SelectedItem = null;
            cboAsgInstSerialNo.Text = null;
            txtAsgInstFamily.Clear();
            txtAsgInstType.Clear();
            txtAsgInstCondition.Clear();
            txtAsgInstMake.Clear();
            //
            btnAsgInstNext.Enabled = false;
            btnAsgInstSubmitInst.Enabled = false;
            btnNewAsgInst.Enabled = false;
            //disabling instrument groupbox
            gbxAsgInstSelectInst.Enabled = false;
        }
        private void btnNewAsgInst_Click(object sender, EventArgs e)
        {
            addnew();
        }
    }
}
