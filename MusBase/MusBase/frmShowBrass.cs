﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace MusBase
{
    public partial class frmShowBrass : Form
    {
        private string connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;
        SqlConnection conn;
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter dAdapter;
        DataTable InstTable = new DataTable();

        public frmShowBrass()
        {
            InitializeComponent();
            loadAll();
            fillcboType();
            cboRmvInstSerialNo.Enabled = false; 
        }

        private void frmShowBrass_Load(object sender, EventArgs e)
        {

        }

        private void LoadType()
        {
            string query = "SELECT Instrument_Details.Inst_Serial_No AS [SERIAL NO], Instrument_Details.Instrument_Type AS [TYPE], "+ 
	                " Instrument_Details.Inst_Make AS [MAKE], Instrument_Details.Inst_Condition AS [CONDITION] " +
	                "FROM Instrument_Details WHERE Instrument_Type = @type AND Inst_Family = 'Brass'";

            
            LoadInstruments(query);
        }

        private void loadSer()
        {
            string query = "SELECT Instrument_Details.Inst_Serial_No AS [SERIAL NO], Instrument_Details.Instrument_Type AS [TYPE], "+ 
	                " Instrument_Details.Inst_Make AS [MAKE], Instrument_Details.Inst_Condition AS [CONDITION] " +
	                "FROM Instrument_Details WHERE Inst_Serial_No = @ser AND [Inst_Family] = 'Brass'";            
            LoadInstruments(query);
        }

        private void loadAll()
        {
            string query = "SELECT Instrument_Details.Inst_Serial_No AS [SERIAL NO], Instrument_Details.Instrument_Type AS [TYPE], Instrument_Details.Inst_Family " +
	                "AS [FAMILY], Instrument_Details.Inst_Make AS [MAKE], Instrument_Details.Inst_Condition AS [CONDITION] "+
	                "FROM Instrument_Details WHERE [Inst_Family] = 'Brass'";

            LoadInstruments(query);
        }

        private void LoadInstruments(string query)
        {
            conn = new SqlConnection(connectionString);            
            cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@ser", cboRmvInstSerialNo.Text);
            cmd.Parameters.AddWithValue("@type", cboRmvInstType.Text);
           try
            {
                dAdapter = new SqlDataAdapter(cmd);                
                //fill datatable with data
                dAdapter.Fill(InstTable);
                //binding to datagridview
                BindingSource bSource = new BindingSource();
                bSource.DataSource = InstTable;
                dgvBrass.DataSource = bSource;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }            
        }

        void fillcboType()
        {
            //FAMILY >> TYPE
            string connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;
            string sqlQuery = "SELECT DISTINCT [Instrument_Type] FROM [dbo].[Instrument_Details]  WHERE [Inst_Family] = 'Brass'";

            SqlConnection conn = new SqlConnection(connectionString);

            try
            {

                SqlCommand cmd = new SqlCommand(sqlQuery, conn);               

                SqlDataReader reader;
                //open connection
                conn.Open();
                reader = cmd.ExecuteReader();//executing command

                int ordinal = reader.GetOrdinal("Instrument_Type");
                while (reader.Read())
                {
                    string instType = reader.GetString(ordinal);
                    cboRmvInstType.Items.Add(instType);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            finally
            {
                conn.Close();//closing connection
            }
        }
        void fillcboSerialNo()
        {
            //FILL SERIAL NUMBER
            string connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;
            string sqlQuery = "SELECT [Inst_Serial_No] FROM [dbo].[Instrument_Details] WHERE [Instrument_Type] = @Instrument_Type";

            SqlConnection conn = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(sqlQuery, conn);
            cmd.Parameters.AddWithValue("@Instrument_Type", cboRmvInstType.Text);
            try
            {
                conn.Open();
                //conection open
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    string temp = reader.GetString(0);
                    cboRmvInstSerialNo.Items.Add(temp);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }
        private void cboRmvInstType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboRmvInstType.Text != null)
            {
                LoadType();
                fillcboSerialNo();
                cboRmvInstSerialNo.Enabled = true;
            }
        }

        private void cboRmvInstSerialNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboRmvInstSerialNo.Text != null)
            {
                loadSer();
            }
        }
    }
}
