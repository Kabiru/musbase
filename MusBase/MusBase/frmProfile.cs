﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Configuration;

namespace MusBase
{
    public partial class frmProfile : Form
    {
        private string connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;
        private SqlConnection conn;
        private SqlCommand cmd = new SqlCommand();

        public frmProfile()
        {
            InitializeComponent();
        }

        private void frmProfile_Load(object sender, EventArgs e)
        {
            conn = new SqlConnection(connectionString);
            cmd.Connection = conn;
            SqlParameter picture = new SqlParameter("@pic", SqlDbType.Image);
        }

        private void open()
        {
            try
            {
                OpenFileDialog opendlg = new OpenFileDialog();
                opendlg.InitialDirectory = @"C:\Users\Public\Pictures";
                opendlg.Filter = "All Files|*.*|JPEGs|*.jpg|Bitmaps|*.bmp|GIFs|*.gif";
                opendlg.FilterIndex = 2;//setting filter index to .jpg as default

                if (opendlg.ShowDialog() == DialogResult.OK)
                {
                    //getting the image
                    pictureBoxStudent.Image = Image.FromFile(opendlg.FileName);

                    //setting picture box prrroperties                
                    pictureBoxStudent.SizeMode = PictureBoxSizeMode.StretchImage;//autofit image
                    pictureBoxStudent.BorderStyle = BorderStyle.Fixed3D;

                }
            }
            catch (Exception ex)
            { 
                MessageBox.Show("Error\n"+ex.Message); 
            }
        }

        private void uploadImage() 
        {
            try 
            {
                if (pictureBoxStudent.Image != null)
                {
                    MemoryStream ms = new MemoryStream();
                    pictureBoxStudent.Image.Save(ms, pictureBoxStudent.Image.RawFormat);                    
                    byte[] a = ms.GetBuffer();
                    ms.Close();
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@pic", a);
                    cmd.CommandText = "INSERT INTO Image_Table (pic) VALUES (@pic)";

                    conn.Open();
                    int TotalrowsAffected =  cmd.ExecuteNonQuery();
                    if (TotalrowsAffected == 1)
                    {
                        MessageBox.Show("Image Saved.", "Upload", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    pictureBoxStudent.Image = null;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            open();
        }

    }
}
