﻿namespace MusBase
{
    partial class frmRemoveInstrument
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRemoveInstrument));
            this.lblFName = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnRmvInstDone = new System.Windows.Forms.Button();
            this.txtRmvInstCondition = new System.Windows.Forms.TextBox();
            this.cboRmvInstSerialNo = new System.Windows.Forms.ComboBox();
            this.cboRmvInstType = new System.Windows.Forms.ComboBox();
            this.cboRmvInstFamily = new System.Windows.Forms.ComboBox();
            this.lblRmvInstFamily = new System.Windows.Forms.Label();
            this.lblRmvInstCondition = new System.Windows.Forms.Label();
            this.lblRmvInstSerialNo = new System.Windows.Forms.Label();
            this.txtRmvInstMake = new System.Windows.Forms.TextBox();
            this.lblRmvInstMake = new System.Windows.Forms.Label();
            this.lblRmvInstType = new System.Windows.Forms.Label();
            this.btnRmvInstrument = new System.Windows.Forms.Button();
            this.btnRmvInstClear = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dgvRemoveInstrument = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRemoveInstrument)).BeginInit();
            this.SuspendLayout();
            // 
            // lblFName
            // 
            this.lblFName.AutoSize = true;
            this.lblFName.Location = new System.Drawing.Point(8, 83);
            this.lblFName.Name = "lblFName";
            this.lblFName.Size = new System.Drawing.Size(0, 13);
            this.lblFName.TabIndex = 182;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnRmvInstDone);
            this.groupBox1.Controls.Add(this.txtRmvInstCondition);
            this.groupBox1.Controls.Add(this.cboRmvInstSerialNo);
            this.groupBox1.Controls.Add(this.cboRmvInstType);
            this.groupBox1.Controls.Add(this.cboRmvInstFamily);
            this.groupBox1.Controls.Add(this.lblRmvInstFamily);
            this.groupBox1.Controls.Add(this.lblRmvInstCondition);
            this.groupBox1.Controls.Add(this.lblRmvInstSerialNo);
            this.groupBox1.Controls.Add(this.txtRmvInstMake);
            this.groupBox1.Controls.Add(this.lblRmvInstMake);
            this.groupBox1.Controls.Add(this.lblRmvInstType);
            this.groupBox1.Controls.Add(this.btnRmvInstrument);
            this.groupBox1.Controls.Add(this.btnRmvInstClear);
            this.groupBox1.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(14, 29);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(678, 194);
            this.groupBox1.TabIndex = 183;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Select an instrument ";
            // 
            // btnRmvInstDone
            // 
            this.btnRmvInstDone.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRmvInstDone.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.btnRmvInstDone.Image = global::MusBase.Properties.Resources._1427990866_Done;
            this.btnRmvInstDone.Location = new System.Drawing.Point(522, 120);
            this.btnRmvInstDone.Name = "btnRmvInstDone";
            this.btnRmvInstDone.Size = new System.Drawing.Size(95, 44);
            this.btnRmvInstDone.TabIndex = 184;
            this.btnRmvInstDone.Text = "&Quit";
            this.btnRmvInstDone.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnRmvInstDone.UseVisualStyleBackColor = true;
            this.btnRmvInstDone.Click += new System.EventHandler(this.btnRmvInstDone_Click);
            // 
            // txtRmvInstCondition
            // 
            this.txtRmvInstCondition.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRmvInstCondition.ForeColor = System.Drawing.Color.RoyalBlue;
            this.txtRmvInstCondition.Location = new System.Drawing.Point(298, 77);
            this.txtRmvInstCondition.Name = "txtRmvInstCondition";
            this.txtRmvInstCondition.ReadOnly = true;
            this.txtRmvInstCondition.Size = new System.Drawing.Size(151, 24);
            this.txtRmvInstCondition.TabIndex = 205;
            // 
            // cboRmvInstSerialNo
            // 
            this.cboRmvInstSerialNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboRmvInstSerialNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboRmvInstSerialNo.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboRmvInstSerialNo.ForeColor = System.Drawing.Color.RoyalBlue;
            this.cboRmvInstSerialNo.FormattingEnabled = true;
            this.cboRmvInstSerialNo.Location = new System.Drawing.Point(522, 36);
            this.cboRmvInstSerialNo.Name = "cboRmvInstSerialNo";
            this.cboRmvInstSerialNo.Size = new System.Drawing.Size(150, 25);
            this.cboRmvInstSerialNo.TabIndex = 3;
            this.cboRmvInstSerialNo.SelectedIndexChanged += new System.EventHandler(this.cboRmvInstSerialNo_SelectedIndexChanged);
            // 
            // cboRmvInstType
            // 
            this.cboRmvInstType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboRmvInstType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboRmvInstType.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboRmvInstType.ForeColor = System.Drawing.Color.RoyalBlue;
            this.cboRmvInstType.FormattingEnabled = true;
            this.cboRmvInstType.Location = new System.Drawing.Point(264, 36);
            this.cboRmvInstType.Name = "cboRmvInstType";
            this.cboRmvInstType.Size = new System.Drawing.Size(142, 25);
            this.cboRmvInstType.TabIndex = 2;
            this.cboRmvInstType.SelectedIndexChanged += new System.EventHandler(this.cboRmvInstType_SelectedIndexChanged);
            // 
            // cboRmvInstFamily
            // 
            this.cboRmvInstFamily.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboRmvInstFamily.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboRmvInstFamily.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboRmvInstFamily.ForeColor = System.Drawing.Color.RoyalBlue;
            this.cboRmvInstFamily.FormattingEnabled = true;
            this.cboRmvInstFamily.Location = new System.Drawing.Point(63, 36);
            this.cboRmvInstFamily.Name = "cboRmvInstFamily";
            this.cboRmvInstFamily.Size = new System.Drawing.Size(121, 25);
            this.cboRmvInstFamily.TabIndex = 1;
            this.cboRmvInstFamily.SelectedIndexChanged += new System.EventHandler(this.cboRmvInstFamily_SelectedIndexChanged);
            // 
            // lblRmvInstFamily
            // 
            this.lblRmvInstFamily.AutoSize = true;
            this.lblRmvInstFamily.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRmvInstFamily.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lblRmvInstFamily.Location = new System.Drawing.Point(6, 36);
            this.lblRmvInstFamily.Name = "lblRmvInstFamily";
            this.lblRmvInstFamily.Size = new System.Drawing.Size(51, 17);
            this.lblRmvInstFamily.TabIndex = 201;
            this.lblRmvInstFamily.Text = "Family";
            // 
            // lblRmvInstCondition
            // 
            this.lblRmvInstCondition.AutoSize = true;
            this.lblRmvInstCondition.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRmvInstCondition.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lblRmvInstCondition.Location = new System.Drawing.Point(219, 80);
            this.lblRmvInstCondition.Name = "lblRmvInstCondition";
            this.lblRmvInstCondition.Size = new System.Drawing.Size(73, 17);
            this.lblRmvInstCondition.TabIndex = 200;
            this.lblRmvInstCondition.Text = "Condition";
            // 
            // lblRmvInstSerialNo
            // 
            this.lblRmvInstSerialNo.AutoSize = true;
            this.lblRmvInstSerialNo.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRmvInstSerialNo.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lblRmvInstSerialNo.Location = new System.Drawing.Point(419, 39);
            this.lblRmvInstSerialNo.Name = "lblRmvInstSerialNo";
            this.lblRmvInstSerialNo.Size = new System.Drawing.Size(97, 17);
            this.lblRmvInstSerialNo.TabIndex = 199;
            this.lblRmvInstSerialNo.Text = "Serial Number";
            // 
            // txtRmvInstMake
            // 
            this.txtRmvInstMake.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRmvInstMake.ForeColor = System.Drawing.Color.RoyalBlue;
            this.txtRmvInstMake.Location = new System.Drawing.Point(62, 77);
            this.txtRmvInstMake.Name = "txtRmvInstMake";
            this.txtRmvInstMake.ReadOnly = true;
            this.txtRmvInstMake.Size = new System.Drawing.Size(151, 24);
            this.txtRmvInstMake.TabIndex = 198;
            // 
            // lblRmvInstMake
            // 
            this.lblRmvInstMake.AutoSize = true;
            this.lblRmvInstMake.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRmvInstMake.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lblRmvInstMake.Location = new System.Drawing.Point(6, 80);
            this.lblRmvInstMake.Name = "lblRmvInstMake";
            this.lblRmvInstMake.Size = new System.Drawing.Size(43, 17);
            this.lblRmvInstMake.TabIndex = 197;
            this.lblRmvInstMake.Text = "Make";
            // 
            // lblRmvInstType
            // 
            this.lblRmvInstType.AutoSize = true;
            this.lblRmvInstType.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRmvInstType.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lblRmvInstType.Location = new System.Drawing.Point(219, 36);
            this.lblRmvInstType.Name = "lblRmvInstType";
            this.lblRmvInstType.Size = new System.Drawing.Size(39, 17);
            this.lblRmvInstType.TabIndex = 196;
            this.lblRmvInstType.Text = "Type";
            // 
            // btnRmvInstrument
            // 
            this.btnRmvInstrument.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRmvInstrument.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnRmvInstrument.Image = global::MusBase.Properties.Resources.Trumpet_delete_32;
            this.btnRmvInstrument.Location = new System.Drawing.Point(62, 120);
            this.btnRmvInstrument.Name = "btnRmvInstrument";
            this.btnRmvInstrument.Size = new System.Drawing.Size(104, 42);
            this.btnRmvInstrument.TabIndex = 194;
            this.btnRmvInstrument.Text = "&Remove";
            this.btnRmvInstrument.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnRmvInstrument.UseVisualStyleBackColor = true;
            this.btnRmvInstrument.Click += new System.EventHandler(this.btnRmvInstrument_Click);
            // 
            // btnRmvInstClear
            // 
            this.btnRmvInstClear.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRmvInstClear.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnRmvInstClear.Image = global::MusBase.Properties.Resources.Oboe_write_32;
            this.btnRmvInstClear.Location = new System.Drawing.Point(264, 122);
            this.btnRmvInstClear.Name = "btnRmvInstClear";
            this.btnRmvInstClear.Size = new System.Drawing.Size(104, 42);
            this.btnRmvInstClear.TabIndex = 193;
            this.btnRmvInstClear.Text = "&Clear";
            this.btnRmvInstClear.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnRmvInstClear.UseVisualStyleBackColor = true;
            this.btnRmvInstClear.Click += new System.EventHandler(this.btnRmvInstClear_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dgvRemoveInstrument);
            this.panel1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.ForeColor = System.Drawing.Color.RoyalBlue;
            this.panel1.Location = new System.Drawing.Point(11, 260);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(681, 169);
            this.panel1.TabIndex = 184;
            // 
            // dgvRemoveInstrument
            // 
            this.dgvRemoveInstrument.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvRemoveInstrument.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRemoveInstrument.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvRemoveInstrument.Location = new System.Drawing.Point(0, 0);
            this.dgvRemoveInstrument.Name = "dgvRemoveInstrument";
            this.dgvRemoveInstrument.ReadOnly = true;
            this.dgvRemoveInstrument.Size = new System.Drawing.Size(681, 169);
            this.dgvRemoveInstrument.TabIndex = 0;
            // 
            // frmRemoveInstrument
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(704, 454);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lblFName);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmRemoveInstrument";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Remove Instrument";
            this.Load += new System.EventHandler(this.frmRemoveInstrument_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRemoveInstrument)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblFName;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtRmvInstCondition;
        private System.Windows.Forms.ComboBox cboRmvInstSerialNo;
        private System.Windows.Forms.ComboBox cboRmvInstType;
        private System.Windows.Forms.ComboBox cboRmvInstFamily;
        private System.Windows.Forms.Label lblRmvInstFamily;
        private System.Windows.Forms.Label lblRmvInstCondition;
        private System.Windows.Forms.Label lblRmvInstSerialNo;
        private System.Windows.Forms.TextBox txtRmvInstMake;
        private System.Windows.Forms.Label lblRmvInstMake;
        private System.Windows.Forms.Label lblRmvInstType;
        private System.Windows.Forms.Button btnRmvInstrument;
        private System.Windows.Forms.Button btnRmvInstClear;
        private System.Windows.Forms.Button btnRmvInstDone;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dgvRemoveInstrument;
    }
}