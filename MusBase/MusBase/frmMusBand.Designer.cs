﻿namespace MusBase
{
    partial class frmMusBand
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.dgvMusBand = new System.Windows.Forms.DataGridView();
            this.gbxStudent = new System.Windows.Forms.GroupBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.txtPosition = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cboBandClass = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cboInstrument = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtForm = new System.Windows.Forms.TextBox();
            this.cboAdmin = new System.Windows.Forms.ComboBox();
            this.lblAsgAccAdminNo = new System.Windows.Forms.Label();
            this.txtLName = new System.Windows.Forms.TextBox();
            this.lblAsgAccForm = new System.Windows.Forms.Label();
            this.txtFName = new System.Windows.Forms.TextBox();
            this.lblAsgAccLName = new System.Windows.Forms.Label();
            this.lblAsgAccFName = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnBandAcademic = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMusBand)).BeginInit();
            this.gbxStudent.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.dgvMusBand);
            this.panel1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(12, 265);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(769, 177);
            this.panel1.TabIndex = 0;
            // 
            // dgvMusBand
            // 
            this.dgvMusBand.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvMusBand.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMusBand.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMusBand.Location = new System.Drawing.Point(0, 0);
            this.dgvMusBand.Name = "dgvMusBand";
            this.dgvMusBand.Size = new System.Drawing.Size(769, 177);
            this.dgvMusBand.TabIndex = 0;
            this.dgvMusBand.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMusBand_CellClick);
            this.dgvMusBand.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMusBand_CellContentClick);
            // 
            // gbxStudent
            // 
            this.gbxStudent.Controls.Add(this.btnClear);
            this.gbxStudent.Controls.Add(this.btnAdd);
            this.gbxStudent.Controls.Add(this.txtPosition);
            this.gbxStudent.Controls.Add(this.label3);
            this.gbxStudent.Controls.Add(this.cboBandClass);
            this.gbxStudent.Controls.Add(this.label2);
            this.gbxStudent.Controls.Add(this.cboInstrument);
            this.gbxStudent.Controls.Add(this.label1);
            this.gbxStudent.Controls.Add(this.txtForm);
            this.gbxStudent.Controls.Add(this.cboAdmin);
            this.gbxStudent.Controls.Add(this.lblAsgAccAdminNo);
            this.gbxStudent.Controls.Add(this.txtLName);
            this.gbxStudent.Controls.Add(this.lblAsgAccForm);
            this.gbxStudent.Controls.Add(this.txtFName);
            this.gbxStudent.Controls.Add(this.lblAsgAccLName);
            this.gbxStudent.Controls.Add(this.lblAsgAccFName);
            this.gbxStudent.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxStudent.Location = new System.Drawing.Point(12, 43);
            this.gbxStudent.Name = "gbxStudent";
            this.gbxStudent.Size = new System.Drawing.Size(567, 186);
            this.gbxStudent.TabIndex = 229;
            this.gbxStudent.TabStop = false;
            // 
            // btnClear
            // 
            this.btnClear.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.Location = new System.Drawing.Point(406, 131);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 23);
            this.btnClear.TabIndex = 208;
            this.btnClear.Text = "&CLEAR";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Location = new System.Drawing.Point(296, 132);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 207;
            this.btnAdd.Text = "&ADD";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // txtPosition
            // 
            this.txtPosition.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPosition.Location = new System.Drawing.Point(406, 97);
            this.txtPosition.Name = "txtPosition";
            this.txtPosition.Size = new System.Drawing.Size(121, 24);
            this.txtPosition.TabIndex = 206;
            this.txtPosition.TextChanged += new System.EventHandler(this.txtPosition_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(294, 97);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 17);
            this.label3.TabIndex = 205;
            this.label3.Text = "Position";
            // 
            // cboBandClass
            // 
            this.cboBandClass.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboBandClass.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboBandClass.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cboBandClass.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboBandClass.FormattingEnabled = true;
            this.cboBandClass.Location = new System.Drawing.Point(406, 61);
            this.cboBandClass.Name = "cboBandClass";
            this.cboBandClass.Size = new System.Drawing.Size(121, 25);
            this.cboBandClass.TabIndex = 204;
            this.cboBandClass.SelectedIndexChanged += new System.EventHandler(this.cboBandClass_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(294, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 17);
            this.label2.TabIndex = 203;
            this.label2.Text = "Band Class";
            // 
            // cboInstrument
            // 
            this.cboInstrument.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboInstrument.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboInstrument.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cboInstrument.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboInstrument.FormattingEnabled = true;
            this.cboInstrument.Location = new System.Drawing.Point(406, 23);
            this.cboInstrument.Name = "cboInstrument";
            this.cboInstrument.Size = new System.Drawing.Size(121, 25);
            this.cboInstrument.TabIndex = 202;
            this.cboInstrument.SelectedIndexChanged += new System.EventHandler(this.cboInstrument_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(293, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 17);
            this.label1.TabIndex = 201;
            this.label1.Text = "Instrument";
            // 
            // txtForm
            // 
            this.txtForm.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtForm.Location = new System.Drawing.Point(121, 132);
            this.txtForm.Name = "txtForm";
            this.txtForm.ReadOnly = true;
            this.txtForm.Size = new System.Drawing.Size(77, 24);
            this.txtForm.TabIndex = 200;
            // 
            // cboAdmin
            // 
            this.cboAdmin.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboAdmin.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboAdmin.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cboAdmin.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboAdmin.FormattingEnabled = true;
            this.cboAdmin.Location = new System.Drawing.Point(121, 23);
            this.cboAdmin.Name = "cboAdmin";
            this.cboAdmin.Size = new System.Drawing.Size(121, 25);
            this.cboAdmin.TabIndex = 189;
            this.cboAdmin.SelectedIndexChanged += new System.EventHandler(this.cboAdmin_SelectedIndexChanged);
            // 
            // lblAsgAccAdminNo
            // 
            this.lblAsgAccAdminNo.AutoSize = true;
            this.lblAsgAccAdminNo.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAsgAccAdminNo.Location = new System.Drawing.Point(19, 23);
            this.lblAsgAccAdminNo.Name = "lblAsgAccAdminNo";
            this.lblAsgAccAdminNo.Size = new System.Drawing.Size(76, 17);
            this.lblAsgAccAdminNo.TabIndex = 196;
            this.lblAsgAccAdminNo.Text = "Admin No";
            // 
            // txtLName
            // 
            this.txtLName.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLName.Location = new System.Drawing.Point(121, 97);
            this.txtLName.Name = "txtLName";
            this.txtLName.ReadOnly = true;
            this.txtLName.Size = new System.Drawing.Size(121, 24);
            this.txtLName.TabIndex = 193;
            // 
            // lblAsgAccForm
            // 
            this.lblAsgAccForm.AutoSize = true;
            this.lblAsgAccForm.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAsgAccForm.Location = new System.Drawing.Point(55, 132);
            this.lblAsgAccForm.Name = "lblAsgAccForm";
            this.lblAsgAccForm.Size = new System.Drawing.Size(42, 17);
            this.lblAsgAccForm.TabIndex = 198;
            this.lblAsgAccForm.Text = "Form";
            // 
            // txtFName
            // 
            this.txtFName.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFName.Location = new System.Drawing.Point(121, 60);
            this.txtFName.Name = "txtFName";
            this.txtFName.ReadOnly = true;
            this.txtFName.Size = new System.Drawing.Size(121, 24);
            this.txtFName.TabIndex = 192;
            // 
            // lblAsgAccLName
            // 
            this.lblAsgAccLName.AutoSize = true;
            this.lblAsgAccLName.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAsgAccLName.Location = new System.Drawing.Point(20, 97);
            this.lblAsgAccLName.Name = "lblAsgAccLName";
            this.lblAsgAccLName.Size = new System.Drawing.Size(75, 17);
            this.lblAsgAccLName.TabIndex = 191;
            this.lblAsgAccLName.Text = "Last Name";
            // 
            // lblAsgAccFName
            // 
            this.lblAsgAccFName.AutoSize = true;
            this.lblAsgAccFName.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAsgAccFName.Location = new System.Drawing.Point(19, 61);
            this.lblAsgAccFName.Name = "lblAsgAccFName";
            this.lblAsgAccFName.Size = new System.Drawing.Size(78, 17);
            this.lblAsgAccFName.TabIndex = 190;
            this.lblAsgAccFName.Text = "First Name";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(12, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(336, 19);
            this.label4.TabIndex = 230;
            this.label4.Text = "Enter the following details to add band members.";
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Location = new System.Drawing.Point(418, 236);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 23);
            this.btnUpdate.TabIndex = 231;
            this.btnUpdate.Text = "&UPDATE";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnBandAcademic
            // 
            this.btnBandAcademic.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBandAcademic.Location = new System.Drawing.Point(606, 43);
            this.btnBandAcademic.Name = "btnBandAcademic";
            this.btnBandAcademic.Size = new System.Drawing.Size(148, 23);
            this.btnBandAcademic.TabIndex = 232;
            this.btnBandAcademic.Text = "Band Academic Report";
            this.btnBandAcademic.UseVisualStyleBackColor = true;
            this.btnBandAcademic.Click += new System.EventHandler(this.btnBandAcademic_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Location = new System.Drawing.Point(309, 236);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 233;
            this.btnDelete.Text = "&DELETE";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // frmMusBand
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(793, 454);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnBandAcademic);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.gbxStudent);
            this.Controls.Add(this.panel1);
            this.ForeColor = System.Drawing.Color.RoyalBlue;
            this.Name = "frmMusBand";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MusBand";
            this.Load += new System.EventHandler(this.frmMusBand_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMusBand)).EndInit();
            this.gbxStudent.ResumeLayout(false);
            this.gbxStudent.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dgvMusBand;
        private System.Windows.Forms.GroupBox gbxStudent;
        private System.Windows.Forms.TextBox txtForm;
        private System.Windows.Forms.ComboBox cboAdmin;
        private System.Windows.Forms.Label lblAsgAccAdminNo;
        private System.Windows.Forms.TextBox txtLName;
        private System.Windows.Forms.Label lblAsgAccForm;
        private System.Windows.Forms.TextBox txtFName;
        private System.Windows.Forms.Label lblAsgAccLName;
        private System.Windows.Forms.Label lblAsgAccFName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtPosition;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cboBandClass;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cboInstrument;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnBandAcademic;
        private System.Windows.Forms.Button btnDelete;

    }
}