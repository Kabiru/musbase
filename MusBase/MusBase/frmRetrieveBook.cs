﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace MusBase
{
    public partial class frmRetrieveBook : Form
    {
        string connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;
        SqlConnection conn;
        SqlCommand cmd; 

        public frmRetrieveBook()
        {
            InitializeComponent();
            fillcboBookType();
            //deactivate constrols
            btnRtvBook.Enabled = false;
            btnRtvBkSubmitBk.Enabled = false;
            cboRtvBkBookCode.Enabled = false;
            cboRtvBkAdminNo.Enabled = false;
            lblLink.Text = "Waiting ...";
        }

        private void fillcboBookType() 
        {   cboRtvBkBookType.Items.Clear();
            cboRtvBkBookType.Text = null;
            string query = "Fill_cbo_BkType_Procedure";
            conn = new SqlConnection(connectionString);
            cmd = new SqlCommand(query, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                int ordinal = reader.GetOrdinal("Book_Type");
                while (reader.Read())
                {
                    string temp = reader.GetString(ordinal);
                    cboRtvBkBookType.Items.Add(temp);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnRtvInstCancel_Click(object sender, EventArgs e)
        {

        }

        private void frmRetrieveBook_Load(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void btnAsgBkSubmitBk_Click(object sender, EventArgs e)
        {
            LoadAssignedBooksAdmin();
            btnRtvBook.Enabled = true;
        }

        private void btnAsgBkDone_Click(object sender, EventArgs e)
        {
            this.Close();//closing form
        }

        private void fillcboBkCode()
        {
            cboRtvBkBookCode.Items.Clear();
            cboRtvBkBookCode.Text = null;
            string query = "SELECT DISTINCT Student_Book.Book_Code FROM Student_Book " +
                "INNER JOIN Book_Details ON Student_Book.Book_Code = Book_Details.Book_Code " +
                "WHERE Book_Details.Book_Type = @BookType";
            conn = new SqlConnection(connectionString);
            cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@BookType", cboRtvBkBookType.Text);
            try
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                int ordinalSerial = reader.GetOrdinal("Book_Code");
                while (reader.Read())
                {
                    string temp = reader.GetString(ordinalSerial);
                    cboRtvBkBookCode.Items.Add(temp);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            { conn.Close(); }
        }

        private void cboRtvBkBookType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboRtvBkBookType.SelectedItem != null)
            {
                cboRtvBkBookCode.Enabled = true;
                fillcboBkCode();
            }
        }
        private void LoadAssigedBooks()
        {
            try
            {
                lblLink.Text = "Registration link:";
                string query = "SELECT Student_Book.Admin_No AS [ADMIN NO], Student_Profile.LName AS [LAST NAME], Student_Profile.FName AS [FIRST NAME]," +
                    " Book_Details.Book_Title As [TITLE], Book_Details.Book_Condition AS [CONDITION], Student_Book.Assign_Date [ISSUE DATE] FROM Student_Book INNER JOIN Student_Profile ON " +
                    "Student_Book.Admin_No = Student_Profile.Admin_No " +
                    "INNER JOIN Book_Details ON Student_Book.Book_Code = Book_Details.Book_Code " +
                    "WHERE Student_Book.Book_Code = @BookCode";
                conn = new SqlConnection(connectionString);
                SqlDataAdapter dAdapter;
                cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@BookCode", cboRtvBkBookCode.Text);
                dAdapter = new SqlDataAdapter(cmd);
                DataTable AsgAccTable = new DataTable();
                //fill data table with data
                dAdapter.Fill(AsgAccTable);
                //sync data with dgv 
                BindingSource bSource = new BindingSource();
                bSource.DataSource = AsgAccTable;
                dgvRtvBkBearerDetails.DataSource = bSource;
                btnRtvBkSubmitBk.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void fillcboAdmin()
        {
            cboRtvBkAdminNo.Items.Clear();
            cboRtvBkAdminNo.Text = null;
            string query = "SELECT Student_Book.Admin_No FROM Student_Book " +
                "INNER JOIN Book_Details ON Student_Book.Book_Code = Book_Details.Book_Code " +
                "WHERE Book_Details.Book_Code = @BookCode";
            conn = new SqlConnection(connectionString);
            cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@BookCode", cboRtvBkBookCode.Text);
            try
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                int ordinal = reader.GetOrdinal("Admin_No");
                while (reader.Read())
                {
                    int temp = reader.GetInt32(ordinal);
                    cboRtvBkAdminNo.Items.Add(temp);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            { conn.Close(); }
        }
        private void cboRtvBkBookCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboRtvBkBookCode.SelectedItem != null)
            {
                cboRtvBkAdminNo.Enabled = true;
                fillcboAdmin();
                LoadAssigedBooks();
            }
        }

        private void LoadAssignedBooksAdmin()
        {
            try
            {
                string query = "SELECT Student_Book.Admin_No AS [ADMIN NO], Student_Profile.LName AS [LAST NAME], Student_Profile.FName AS [FIRST NAME]," +
                    " Book_Details.Book_Title As [TITLE],Student_Book.Assign_Date [ISSUE DATE] FROM Student_Book INNER JOIN Student_Profile ON " +
                    "Student_Book.Admin_No = Student_Profile.Admin_No " +
                    "INNER JOIN Book_Details ON Student_Book.Book_Code = Book_Details.Book_Code " +
                    "WHERE Student_Book.Admin_No = @AdminNo AND Student_Book.Book_Code = @BookCode";
                conn = new SqlConnection(connectionString);
                SqlDataAdapter dAdapter;
                cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@AdminNo", Convert.ToInt32(cboRtvBkAdminNo.Text));
                cmd.Parameters.AddWithValue("@BookCode", cboRtvBkBookCode.Text);
                dAdapter = new SqlDataAdapter(cmd);
                DataTable AsgInstTable = new DataTable();
                //fill data table with data
                dAdapter.Fill(AsgInstTable);
                //sync data with dgv 
                BindingSource bSource = new BindingSource();
                bSource.DataSource = AsgInstTable;
                dgvRtvBkBearerDetails.DataSource = bSource;
                btnRtvBkSubmitBk.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cboRtvBkAdminNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboRtvBkAdminNo.SelectedItem != null)
            {
                btnRtvBkSubmitBk.Enabled = true;                
            }
        }

        private void RetrieveBook()
        {
            string query = "DELETE FROM Student_Book WHERE Book_Code = @BookCode AND Admin_No = @AdminNo ";
            conn = new SqlConnection(connectionString);
            cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@BookCode", cboRtvBkBookCode.Text);
            cmd.Parameters.AddWithValue("@AdminNo", Convert.ToInt32(cboRtvBkAdminNo.Text));
            try
            {
                conn.Open();
                int TotalRowsAffected = cmd.ExecuteNonQuery();
                if (TotalRowsAffected == 1)
                {
                    MessageBox.Show("BOOK SUCCESSFULLY RETRIEVED.", "RETRIEVAL STATUS", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    gbxRtvBook.Enabled = true;
                    cboRtvBkBookType.Enabled = true;
                }
                else
                {
                    MessageBox.Show("ERROR,\nBOOK NOT RETRIEVED,\nPLEASE TRY AGAIN.", "RETRIEVAL STATUS", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            { conn.Close(); }
        }

        private void btnRtvBook_Click(object sender, EventArgs e)
        {
            RetrieveBook();
            cboRtvBkBookType.Enabled = true;
            dgvRtvBkBearerDetails.DataSource = null;
            reset();
        }
        private void reset()
        {
            lblLink.Text = "Waiting ...";
            //enable status
            gbxRtvBook.Enabled = true;
            cboRtvBkBookType.Enabled = true;
            cboRtvBkBookCode.Enabled = false;
            cboRtvBkAdminNo.Enabled = false;
            btnRtvBook.Enabled = false;
            btnRtvBkSubmitBk.Enabled = false;
            //clear
            dgvRtvBkBearerDetails.DataSource = null;
            cboRtvBkAdminNo.Items.Clear();
            cboRtvBkAdminNo.Text = null;
            cboRtvBkBookCode.Items.Clear();
            cboRtvBkBookCode.Text = null;
            fillcboBookType();
        }
        private void btnReset_Click(object sender, EventArgs e)
        {
            reset();
        }
    }
}
