﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace MusBase
{
    public partial class frmMusGrade : Form
    {
        private string connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;               
        SqlConnection conn;
        SqlCommand cmd;

        public frmMusGrade()
        {
            InitializeComponent();            
            startState();
            LoadStudentGrades();
        }

        private string AssignGrade(double avg, string grade)
        {
            //assign grade
            if (avg >= 85 && avg <= 100)
            {
                grade = "A";
                return grade;
            }
            else if (avg >= 84 && avg >= 75)
            {
                grade = "A-";
                return grade;
            }
            else if (avg >=74 && avg <= 70)
            {
                grade = "B+";
                return grade;
            }
            else if (avg <= 69 && avg >= 65)
            {
                grade = "B";
                return grade;
            }
            else if (avg <= 64 && avg >= 60)
            {
                grade = "B-";
                return grade;
            }
            else if (avg <= 59 && avg >= 55)
            {
                grade = "C+";
                return grade;
            }
            else if (avg <= 54 && avg >= 50)
            {
                grade = "C";
                return grade;
            }
            else if (avg <= 49 && avg >= 45)
            {
                grade = "C-";
                return grade;
            }
            else if (avg <= 44 && avg >= 40)
            {
                grade = "D+";
                return grade;
            }
            else if (avg <= 39 && avg >= 35)
            {
                grade = "D";
                return grade;
            }
            else if (avg <= 34 && avg >= 30)
            {
                grade = "D-";
                return grade;
            }
            else if (avg >= 29 && avg > 0)
            {
                grade = "E";
                return grade;
            }
            else
            {
                grade = "Incomplete";
                return grade;
            }
        }

        private void LoadScore()
        {
            string query = "INSERT INTO dbo.Grade_Table (Paper1, Paper2, Paper3, Average, GradeAttained, DateDone, Descprition, Admin_No, Term) " +
                "VALUES (@pp1, @pp2, @pp3, @avg, @grd, @date, @desc, @adm, @term)";

            string grade = " ";
            int pp1 = Convert.ToInt32(txtPaper1.Text);
            int pp2 = Convert.ToInt32(txtPaper2.Text);
            int pp3 = Convert.ToInt32(txtPaper3.Text);

            //calculate average
            double avg = (pp1 + pp2 + pp3) / 2;
            string mygrade = AssignGrade(avg, grade);

            conn = new SqlConnection(connectionString);
            cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@pp1", pp1);
            cmd.Parameters.AddWithValue("@pp2", pp2);
            cmd.Parameters.AddWithValue("@pp3", pp3);
            cmd.Parameters.AddWithValue("@avg", avg);
            cmd.Parameters.AddWithValue("@grd", mygrade);
            cmd.Parameters.AddWithValue("@date", dateTimePicker1.Value.ToShortDateString());
            cmd.Parameters.AddWithValue("@desc", txtDesc.Text);
            cmd.Parameters.AddWithValue("@adm", cboAdmin.Text);
            cmd.Parameters.AddWithValue("@term", cboTerm.Text);


            try
            {

                conn.Open();
                int TotalRowsAffected = cmd.ExecuteNonQuery();
                if (TotalRowsAffected == 1)
                {
                    lblStatus.Text = "Record Added!";
                    LoadStudentGrades();
                    startState();
                }
                else
                {
                    lblStatus.Text = "Insert Unsuccessful.";
                }
            }
            catch (SqlException sex)
            {
                if (sex.Number == 2627)
                {
                    MessageBox.Show("THE RECORD ALREADY EXISTS IN THE DATABASE.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    //addnew();
                }
                else
                {
                    MessageBox.Show(sex.Message);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                conn.Close();
            }
        }

        private void reset()
        {
            //clear score
            txtPaper1.Clear();
            txtPaper2.Clear();
            txtPaper3.Clear();
            txtDesc.Clear();
            //clear student
            cboAdmin.Text = null;
            cboAdmin.SelectedItem = null;
            txtFName.Clear();
            txtLName.Clear();
            txtForm.Clear();
        }

        private void fillTerm()
        {
            cboTerm.Items.Clear();
            cboTerm.Items.Add("1");
            cboTerm.Items.Add("2");
            cboTerm.Items.Add("3");
            cboTerm.SelectedItem = "1";
        }

        private void fillStdtxts()
        {
            conn = new SqlConnection(connectionString);
            string query = "SELECT * FROM [Student_Profile] WHERE [Admin_No] = @adm";
            SqlCommand commandSelect = new SqlCommand(query, conn);
            commandSelect.Parameters.AddWithValue("@adm", cboAdmin.Text);

            try
            {
                conn.Open();
                SqlDataReader reader = commandSelect.ExecuteReader();
                int ordinalFName = reader.GetOrdinal("FName");
                int ordinalLName = reader.GetOrdinal("LName");
                int ordianlForm = reader.GetOrdinal("Form");
                while (reader.Read())
                {
                    string tempFName = reader.GetString(ordinalFName);
                    string tempLName = reader.GetString(ordinalLName);
                    string tempForm = reader.GetString(ordianlForm);

                    txtFName.Text = tempFName;
                    txtLName.Text = tempLName;
                    txtForm.Text = tempForm;
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Error);
            }
            finally
            {
                conn.Close();
            }
        }

        public void LoadStudentGrades()
        {
            string query = "Load_Grades_Procedure";
            conn = new SqlConnection(connectionString);
            cmd = new SqlCommand(query, conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                SqlDataAdapter dAdapter = new SqlDataAdapter(cmd);
                DataTable grade = new DataTable();
                //fill
                dAdapter.Fill(grade);
                //sync
                BindingSource bsource = new BindingSource();
                bsource.DataSource = grade;
                dgvGrading.DataSource = bsource;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Error);
            }
        }

        private void startState()
        {
            btnSubmit.Enabled = false;
            btnSubmitScore.Enabled = false;
            gbxStudent.Enabled = false;
            lblStatus.Text = "Waiting ...";
            fillTerm();
        }

        private void frmMusGrade_Load(object sender, EventArgs e)
        {

            
        }

        private void dgvGrading_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void cboAsgAccAdminNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboAdmin.Text != null)
            {
                fillStdtxts();
                btnSubmit.Enabled = true;

            }
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            if (dateTimePicker1.Text != null)
            {
                btnSubmitScore.Enabled = true;
            }
        }

        public void fillAdminNo()
        {
            cboAdmin.Items.Clear();
            conn = new SqlConnection(connectionString);
            string query = "Load_Admin_Procedure";
            cmd = new SqlCommand(query, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                int ordinal = reader.GetOrdinal("Admin_No");
                while (reader.Read())
                {
                    int temp = reader.GetInt32(ordinal);
                    cboAdmin.Items.Add(temp);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        private void btnSubmitScore_Click(object sender, EventArgs e)
        {
            fillAdminNo();
            gbxStudent.Enabled = true;
            gbxScore.Enabled = false;
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            LoadScore();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            gbxScore.Enabled = true;
            reset();
            startState();
            LoadStudentGrades();
        }

        //txt validation
        private void txtPaper1_Validating(object sender, CancelEventArgs e)
        {
            int PaperOneMark;
            if (int.TryParse(txtPaper1.Text, out PaperOneMark))
            {
                //marks are numeric
                if (PaperOneMark < 0 || PaperOneMark > 50)
                {
                    Paper1errorProvider.SetError(txtPaper1, "Paper 1 marks cannot be greater than 50 or less than 0.");
                    txtPaper1.Clear();
                    return;
                }
                else
                {
                    Paper1errorProvider.SetError(txtPaper1, null);
                }
            }
            else
            {
                //marks are not numeric
                Paper1errorProvider.SetError(txtPaper1, "Invalid Marks");
                txtPaper1.Clear();
                return;
            }
        }

        private void txtPaper2_Validating(object sender, CancelEventArgs e)
        {
            int PapertwoMark;
            if (int.TryParse(txtPaper2.Text, out PapertwoMark))
            {
                //marks are numeric
                if (PapertwoMark < 0 || PapertwoMark > 50)
                {
                    Paper2errorProvider.SetError(txtPaper2, "Paper 2 marks cannot be greater than 50 or less than 0.");
                    txtPaper2.Clear();
                    return;
                }
                else
                {
                    Paper2errorProvider.SetError(txtPaper2, null);
                }
            }
            else
            {
                //marks are not numeric
                Paper2errorProvider.SetError(txtPaper2, "Invalid Marks");
                txtPaper2.Clear();
                return;
            }
        }

        private void txtPaper3_Validating(object sender, CancelEventArgs e)
        {
            int PaperThreeMark;
            if (int.TryParse(txtPaper3.Text, out PaperThreeMark))
            {
                //marks are numeric
                if (PaperThreeMark < 0 || PaperThreeMark > 100)
                {
                    Paper3errorProvider.SetError(txtPaper3, "Paper 3 marks cannot be greater than 100 or less than 0.");
                    txtPaper3.Clear();
                    return;
                }
                else
                {
                    Paper3errorProvider.SetError(txtPaper3, null);
                }
            }
            else
            {
                //marks are not numeric
                Paper3errorProvider.SetError(txtPaper3, "Invalid Marks");
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cboAdmin_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboAdmin.Text != null)
            {
                fillStdtxts();
                btnSubmit.Enabled = true;

            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            frmEditMusGrade edit = new frmEditMusGrade();
            edit.ShowDialog();//modal call
        }
    }
}
