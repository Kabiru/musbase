﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;

namespace MusBase
{
    public partial class frmRemoveBook : Form
    {
        public frmRemoveBook()
        {
            InitializeComponent();
            fillcboType();
            LoadBooks();
            cboRmvBkCode.Enabled = false;
            btnRmvBook.Enabled = false;
        }

        private void LoadBooks() 
        {
            string connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;
            string sqlQuery = "Load_Book_Details_Procedure";

            SqlConnection conn = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(sqlQuery, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            try 
            {
                SqlDataAdapter dAdapter = new SqlDataAdapter(cmd);
                DataTable books = new DataTable();
                //fill
                dAdapter.Fill(books);
                //sync
                BindingSource bsource = new BindingSource();
                bsource.DataSource = books;
                dgvRemoveBook.DataSource = bsource;
            }
            catch (Exception ex) { MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        private void LoadBooksBookType() 
        {
            string connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;
            string sqlQuery = "SELECT Book_Details.Book_Code AS [BOOK CODE], Book_Details.Book_Type AS [TYPE], Book_Details.Book_Title AS [TITLE], Book_Details.Book_Condition " +
                "AS [CONDITION] FROM [dbo].[Book_Details] WHERE [Book_Type] = @BookType";

            SqlConnection conn = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(sqlQuery, conn);
            cmd.Parameters.AddWithValue("@BookType", cboRmvBkType.Text);
            try
            {
                SqlDataAdapter dAdapter = new SqlDataAdapter(cmd);
                DataTable filterBooksType = new DataTable();
                //fill
                dAdapter.Fill(filterBooksType);
                //sync
                BindingSource bsource = new BindingSource();
                bsource.DataSource = filterBooksType;
                dgvRemoveBook.DataSource = bsource;
            }
            catch (Exception ex) 
            { MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        private void LoadBooksBookCode() 
        {
            string connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;
            string sqlQuery = "SELECT Book_Details.Book_Code AS [BOOK CODE], Book_Details.Book_Type AS [TYPE], Book_Details.Book_Title AS [TITLE], Book_Details.Book_Condition "+
	            "AS [CONDITION] FROM [dbo].[Book_Details] WHERE [Book_Code] = @Bookcode";

            SqlConnection conn = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(sqlQuery, conn);
            try             
            {
                SqlDataAdapter dAdapter = new SqlDataAdapter(cmd);
                DataTable filterBooks = new DataTable();
                //fill
                dAdapter.Fill(filterBooks);
                //sync
                BindingSource bsource = new BindingSource();
                bsource.DataSource = filterBooks;
                dgvRemoveBook.DataSource = bsource;
            }
            catch (Exception ex) { MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        void filltxts() 
        {
            //FILL TEXTBOXES
            string connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;
            string sqlQuery = "SELECT [Book_Title], [Book_Condition] FROM [dbo].[Book_Details] WHERE [Book_Code] = @Bookcode";

            SqlConnection conn = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(sqlQuery, conn);
            cmd.Parameters.AddWithValue("@Bookcode", cboRmvBkCode.Text);
            try
            {
                conn.Open();
                //conection open
                SqlDataReader reader = cmd.ExecuteReader();

                int ordinalTitle = reader.GetOrdinal("Book_Title");
                int ordinalcondition = reader.GetOrdinal("Book_Condition");
                while (reader.Read())
                {
                    string tempTitle = reader.GetString(ordinalTitle);
                    string tempCondition = reader.GetString(ordinalcondition);

                    txtRmvBkTitle.Text = tempTitle;
                    txtRmvBkCondition.Text = tempCondition;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }  
        }

        void fillcboType()
        {
            //TYPE
            string connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;
            string sqlQuery = "SELECT DISTINCT [Book_Type] FROM [dbo].[Book_Details]";

            SqlConnection conn = new SqlConnection(connectionString);

            try
            {

                SqlCommand cmd = new SqlCommand(sqlQuery, conn);

                SqlDataReader reader;
                //open connection
                conn.Open();
                reader = cmd.ExecuteReader();//executing command

                int ordinal = reader.GetOrdinal("Book_Type");
                while (reader.Read())
                {
                    string temp = reader.GetString(ordinal);
                    cboRmvBkType.Items.Add(temp);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            finally
            {
                conn.Close();//closing connection
            }
        }
        void fillcbo_Code() 
        {
            //TYPE >> CODE
            string connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;
            string sqlQuery = "SELECT [Book_Code] FROM [dbo].[Book_Details] WHERE [Book_Type] = @BookType";

            SqlConnection conn = new SqlConnection(connectionString);

            try
            {

                SqlCommand cmd = new SqlCommand(sqlQuery, conn);
                cmd.Parameters.AddWithValue("@BookType", cboRmvBkType.Text);
                SqlDataReader reader;
                //open connection
                conn.Open();
                reader = cmd.ExecuteReader();//executing command

                int ordinal = reader.GetOrdinal("Book_Code");
                while (reader.Read())
                {
                    string temp = reader.GetString(ordinal);
                    cboRmvBkCode.Items.Add(temp);                    
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            finally
            {
                conn.Close();//closing connection
            }
        }

        private void frmRemoveBook_Load(object sender, EventArgs e)
        {

        }

        private void btnRmvBkDone_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cboRmvBkType_SelectedIndexChanged(object sender, EventArgs e)
        {
            cboRmvBkCode.Items.Clear();
            cboRmvBkCode.Text = null;
            if (cboRmvBkType.SelectedItem != null)
            {
                fillcbo_Code();
                LoadBooksBookType();
                cboRmvBkCode.Enabled = true;
            }
        }

        private void btnRmvBkClear_Click(object sender, EventArgs e)
        {
            txtRmvBkCondition.Clear();
            txtRmvBkTitle.Clear();

            LoadBooks();

            cboRmvBkType.SelectedItem = null;
            cboRmvBkCode.SelectedItem = null;
            cboRmvBkCode.Enabled = false;
            btnRmvBook.Enabled = false;
        }

        private void btnRmvBook_Click(object sender, EventArgs e)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;
            string sqlQueryDelete = "DELETE  FROM [Book_Details] WHERE [Book_Code] = @Book_Code";

            SqlConnection conn = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(sqlQueryDelete, conn);
            cmd.Parameters.AddWithValue("@Book_Code", cboRmvBkCode.Text);
            try
            {
                conn.Open();
                int TotalRowsAffected = cmd.ExecuteNonQuery();

                if (TotalRowsAffected == 1)
                {
                    MessageBox.Show(cboRmvBkCode.Text + " has been deleted from the database.", "Record Deleted", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    btnRmvBook.Enabled = false;
                }
                else
                {
                    MessageBox.Show("Delete unsuccessful, please try again.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();//closing connecion
            }
        }

        private void cboRmvBkCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboRmvBkCode.Text != null)
            {
                btnRmvBook.Enabled = true;
                filltxts();
            }
        }
    }
}
