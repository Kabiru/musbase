﻿namespace MusBase
{
    partial class frmStudentViewProfile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.cboAsgAccAdminNo = new System.Windows.Forms.ComboBox();
            this.lblAsgAccAdminNo = new System.Windows.Forms.Label();
            this.btnSearchStudent = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblBandMemebership = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblChoirMembership = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.StudentAcademicProgresschart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label4 = new System.Windows.Forms.Label();
            this.lstbxAssignedBooks = new System.Windows.Forms.ListBox();
            this.label7 = new System.Windows.Forms.Label();
            this.lstbxAssignedAccessories = new System.Windows.Forms.ListBox();
            this.label6 = new System.Windows.Forms.Label();
            this.lstbxAssignedInstruments = new System.Windows.Forms.ListBox();
            this.label5 = new System.Windows.Forms.Label();
            this.lblHouse = new System.Windows.Forms.Label();
            this.lblForm = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StudentAcademicProgresschart)).BeginInit();
            this.SuspendLayout();
            // 
            // cboAsgAccAdminNo
            // 
            this.cboAsgAccAdminNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboAsgAccAdminNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboAsgAccAdminNo.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cboAsgAccAdminNo.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboAsgAccAdminNo.FormattingEnabled = true;
            this.cboAsgAccAdminNo.Location = new System.Drawing.Point(109, 31);
            this.cboAsgAccAdminNo.Name = "cboAsgAccAdminNo";
            this.cboAsgAccAdminNo.Size = new System.Drawing.Size(121, 25);
            this.cboAsgAccAdminNo.TabIndex = 162;
            this.cboAsgAccAdminNo.SelectedIndexChanged += new System.EventHandler(this.cboAsgAccAdminNo_SelectedIndexChanged);
            // 
            // lblAsgAccAdminNo
            // 
            this.lblAsgAccAdminNo.AutoSize = true;
            this.lblAsgAccAdminNo.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAsgAccAdminNo.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lblAsgAccAdminNo.Location = new System.Drawing.Point(9, 31);
            this.lblAsgAccAdminNo.Name = "lblAsgAccAdminNo";
            this.lblAsgAccAdminNo.Size = new System.Drawing.Size(76, 17);
            this.lblAsgAccAdminNo.TabIndex = 163;
            this.lblAsgAccAdminNo.Text = "Admin No";
            // 
            // btnSearchStudent
            // 
            this.btnSearchStudent.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearchStudent.Image = global::MusBase.Properties.Resources.search;
            this.btnSearchStudent.Location = new System.Drawing.Point(259, 31);
            this.btnSearchStudent.Name = "btnSearchStudent";
            this.btnSearchStudent.Size = new System.Drawing.Size(102, 40);
            this.btnSearchStudent.TabIndex = 13;
            this.btnSearchStudent.Text = "&Search Student";
            this.btnSearchStudent.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnSearchStudent.UseVisualStyleBackColor = true;
            this.btnSearchStudent.Click += new System.EventHandler(this.btnSearchStudent_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.lblBandMemebership);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.lblChoirMembership);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.StudentAcademicProgresschart);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.lstbxAssignedBooks);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.lstbxAssignedAccessories);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.lstbxAssignedInstruments);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.lblHouse);
            this.groupBox1.Controls.Add(this.lblForm);
            this.groupBox1.Controls.Add(this.lblName);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Times New Roman", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 90);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(677, 388);
            this.groupBox1.TabIndex = 164;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Results";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // lblBandMemebership
            // 
            this.lblBandMemebership.AutoSize = true;
            this.lblBandMemebership.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBandMemebership.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lblBandMemebership.Location = new System.Drawing.Point(154, 87);
            this.lblBandMemebership.Name = "lblBandMemebership";
            this.lblBandMemebership.Size = new System.Drawing.Size(50, 19);
            this.lblBandMemebership.TabIndex = 44;
            this.lblBandMemebership.Text = "label4";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Times New Roman", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(19, 88);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(129, 18);
            this.label11.TabIndex = 43;
            this.label11.Text = "Band Membership:";
            // 
            // lblChoirMembership
            // 
            this.lblChoirMembership.AutoSize = true;
            this.lblChoirMembership.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChoirMembership.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lblChoirMembership.Location = new System.Drawing.Point(154, 107);
            this.lblChoirMembership.Name = "lblChoirMembership";
            this.lblChoirMembership.Size = new System.Drawing.Size(50, 19);
            this.lblChoirMembership.TabIndex = 42;
            this.lblChoirMembership.Text = "label4";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Times New Roman", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(19, 107);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(130, 18);
            this.label9.TabIndex = 41;
            this.label9.Text = "Choir Membership:";
            // 
            // StudentAcademicProgresschart
            // 
            this.StudentAcademicProgresschart.BackColor = System.Drawing.SystemColors.ControlLight;
            this.StudentAcademicProgresschart.BorderlineColor = System.Drawing.Color.RoyalBlue;
            chartArea1.Name = "ChartArea1";
            this.StudentAcademicProgresschart.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.StudentAcademicProgresschart.Legends.Add(legend1);
            this.StudentAcademicProgresschart.Location = new System.Drawing.Point(22, 157);
            this.StudentAcademicProgresschart.Name = "StudentAcademicProgresschart";
            series1.ChartArea = "ChartArea1";
            series1.IsValueShownAsLabel = true;
            series1.Legend = "Legend1";
            series1.Name = "Test";
            this.StudentAcademicProgresschart.Series.Add(series1);
            this.StudentAcademicProgresschart.Size = new System.Drawing.Size(402, 213);
            this.StudentAcademicProgresschart.TabIndex = 40;
            this.StudentAcademicProgresschart.Text = "chart1";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(19, 136);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(130, 18);
            this.label4.TabIndex = 39;
            this.label4.Text = "Academic Progress";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // lstbxAssignedBooks
            // 
            this.lstbxAssignedBooks.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lstbxAssignedBooks.FormattingEnabled = true;
            this.lstbxAssignedBooks.ItemHeight = 18;
            this.lstbxAssignedBooks.Location = new System.Drawing.Point(433, 289);
            this.lstbxAssignedBooks.Name = "lstbxAssignedBooks";
            this.lstbxAssignedBooks.Size = new System.Drawing.Size(225, 76);
            this.lstbxAssignedBooks.TabIndex = 38;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Times New Roman", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(430, 259);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(107, 18);
            this.label7.TabIndex = 37;
            this.label7.Text = "Assigned Books";
            // 
            // lstbxAssignedAccessories
            // 
            this.lstbxAssignedAccessories.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lstbxAssignedAccessories.FormattingEnabled = true;
            this.lstbxAssignedAccessories.ItemHeight = 18;
            this.lstbxAssignedAccessories.Location = new System.Drawing.Point(433, 167);
            this.lstbxAssignedAccessories.Name = "lstbxAssignedAccessories";
            this.lstbxAssignedAccessories.Size = new System.Drawing.Size(225, 76);
            this.lstbxAssignedAccessories.TabIndex = 36;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(433, 136);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(141, 18);
            this.label6.TabIndex = 35;
            this.label6.Text = "Assigned Accessories";
            // 
            // lstbxAssignedInstruments
            // 
            this.lstbxAssignedInstruments.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lstbxAssignedInstruments.FormattingEnabled = true;
            this.lstbxAssignedInstruments.ItemHeight = 18;
            this.lstbxAssignedInstruments.Location = new System.Drawing.Point(433, 45);
            this.lstbxAssignedInstruments.Name = "lstbxAssignedInstruments";
            this.lstbxAssignedInstruments.Size = new System.Drawing.Size(225, 76);
            this.lstbxAssignedInstruments.TabIndex = 34;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(430, 14);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(144, 18);
            this.label5.TabIndex = 33;
            this.label5.Text = "Assigned Instruments";
            // 
            // lblHouse
            // 
            this.lblHouse.AutoSize = true;
            this.lblHouse.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHouse.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lblHouse.Location = new System.Drawing.Point(103, 40);
            this.lblHouse.Name = "lblHouse";
            this.lblHouse.Size = new System.Drawing.Size(50, 19);
            this.lblHouse.TabIndex = 32;
            this.lblHouse.Text = "label4";
            // 
            // lblForm
            // 
            this.lblForm.AutoSize = true;
            this.lblForm.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblForm.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lblForm.Location = new System.Drawing.Point(103, 59);
            this.lblForm.Name = "lblForm";
            this.lblForm.Size = new System.Drawing.Size(50, 19);
            this.lblForm.TabIndex = 31;
            this.lblForm.Text = "label4";
            this.lblForm.Click += new System.EventHandler(this.lblForm_Click);
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lblName.Location = new System.Drawing.Point(103, 21);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(50, 19);
            this.lblName.TabIndex = 30;
            this.lblName.Text = "label4";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(18, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 19);
            this.label3.TabIndex = 29;
            this.label3.Text = "Form:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(18, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 19);
            this.label2.TabIndex = 28;
            this.label2.Text = "House:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(18, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 19);
            this.label1.TabIndex = 27;
            this.label1.Text = "Name:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // frmStudentViewProfile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(701, 490);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.cboAsgAccAdminNo);
            this.Controls.Add(this.lblAsgAccAdminNo);
            this.Controls.Add(this.btnSearchStudent);
            this.Name = "frmStudentViewProfile";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Student Profile";
            this.Load += new System.EventHandler(this.frmStudentViewProfile_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StudentAcademicProgresschart)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSearchStudent;
        private System.Windows.Forms.ComboBox cboAsgAccAdminNo;
        private System.Windows.Forms.Label lblAsgAccAdminNo;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblBandMemebership;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lblChoirMembership;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataVisualization.Charting.Chart StudentAcademicProgresschart;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListBox lstbxAssignedBooks;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ListBox lstbxAssignedAccessories;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ListBox lstbxAssignedInstruments;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblHouse;
        private System.Windows.Forms.Label lblForm;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}