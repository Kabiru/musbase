﻿namespace MusBase
{
    partial class frmAddType
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageAdd = new System.Windows.Forms.TabPage();
            this.gbxRdos = new System.Windows.Forms.GroupBox();
            this.rdoPercussion = new System.Windows.Forms.RadioButton();
            this.rdoStrings = new System.Windows.Forms.RadioButton();
            this.rdoWoodwinds = new System.Windows.Forms.RadioButton();
            this.rdoBrass = new System.Windows.Forms.RadioButton();
            this.btnReset = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.txtType = new System.Windows.Forms.TextBox();
            this.rdoBookType = new System.Windows.Forms.RadioButton();
            this.rdoAccessoryType = new System.Windows.Forms.RadioButton();
            this.rdoInstrumentType = new System.Windows.Forms.RadioButton();
            this.tabPageView = new System.Windows.Forms.TabPage();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.lblTypeTitle = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rdoBkTypedgv = new System.Windows.Forms.RadioButton();
            this.rdoAccTypedgv = new System.Windows.Forms.RadioButton();
            this.rdoInstTypedgv = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dgvType = new System.Windows.Forms.DataGridView();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.btnPrevious = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPageAdd.SuspendLayout();
            this.gbxRdos.SuspendLayout();
            this.tabPageView.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvType)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPageAdd);
            this.tabControl1.Controls.Add(this.tabPageView);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(611, 302);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPageAdd
            // 
            this.tabPageAdd.BackColor = System.Drawing.SystemColors.Control;
            this.tabPageAdd.Controls.Add(this.gbxRdos);
            this.tabPageAdd.Controls.Add(this.btnReset);
            this.tabPageAdd.Controls.Add(this.label1);
            this.tabPageAdd.Controls.Add(this.btnSubmit);
            this.tabPageAdd.Controls.Add(this.txtType);
            this.tabPageAdd.Controls.Add(this.rdoBookType);
            this.tabPageAdd.Controls.Add(this.rdoAccessoryType);
            this.tabPageAdd.Controls.Add(this.rdoInstrumentType);
            this.tabPageAdd.Location = new System.Drawing.Point(4, 28);
            this.tabPageAdd.Name = "tabPageAdd";
            this.tabPageAdd.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageAdd.Size = new System.Drawing.Size(603, 270);
            this.tabPageAdd.TabIndex = 0;
            this.tabPageAdd.Text = "Add";
            // 
            // gbxRdos
            // 
            this.gbxRdos.Controls.Add(this.rdoPercussion);
            this.gbxRdos.Controls.Add(this.rdoStrings);
            this.gbxRdos.Controls.Add(this.rdoWoodwinds);
            this.gbxRdos.Controls.Add(this.rdoBrass);
            this.gbxRdos.Location = new System.Drawing.Point(10, 86);
            this.gbxRdos.Name = "gbxRdos";
            this.gbxRdos.Size = new System.Drawing.Size(220, 90);
            this.gbxRdos.TabIndex = 223;
            this.gbxRdos.TabStop = false;
            // 
            // rdoPercussion
            // 
            this.rdoPercussion.AutoSize = true;
            this.rdoPercussion.Location = new System.Drawing.Point(5, 51);
            this.rdoPercussion.Name = "rdoPercussion";
            this.rdoPercussion.Size = new System.Drawing.Size(99, 23);
            this.rdoPercussion.TabIndex = 220;
            this.rdoPercussion.TabStop = true;
            this.rdoPercussion.Text = "Percussion";
            this.rdoPercussion.UseVisualStyleBackColor = true;
            // 
            // rdoStrings
            // 
            this.rdoStrings.AutoSize = true;
            this.rdoStrings.Location = new System.Drawing.Point(110, 25);
            this.rdoStrings.Name = "rdoStrings";
            this.rdoStrings.Size = new System.Drawing.Size(74, 23);
            this.rdoStrings.TabIndex = 219;
            this.rdoStrings.TabStop = true;
            this.rdoStrings.Text = "Strings";
            this.rdoStrings.UseVisualStyleBackColor = true;
            // 
            // rdoWoodwinds
            // 
            this.rdoWoodwinds.AutoSize = true;
            this.rdoWoodwinds.Location = new System.Drawing.Point(110, 51);
            this.rdoWoodwinds.Name = "rdoWoodwinds";
            this.rdoWoodwinds.Size = new System.Drawing.Size(102, 23);
            this.rdoWoodwinds.TabIndex = 218;
            this.rdoWoodwinds.TabStop = true;
            this.rdoWoodwinds.Text = "Woodwinds";
            this.rdoWoodwinds.UseVisualStyleBackColor = true;
            // 
            // rdoBrass
            // 
            this.rdoBrass.AutoSize = true;
            this.rdoBrass.Location = new System.Drawing.Point(6, 22);
            this.rdoBrass.Name = "rdoBrass";
            this.rdoBrass.Size = new System.Drawing.Size(66, 23);
            this.rdoBrass.TabIndex = 217;
            this.rdoBrass.TabStop = true;
            this.rdoBrass.Text = "Brass";
            this.rdoBrass.UseVisualStyleBackColor = true;
            // 
            // btnReset
            // 
            this.btnReset.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.Image = global::MusBase.Properties.Resources._1429511309_131707;
            this.btnReset.Location = new System.Drawing.Point(475, 130);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(87, 47);
            this.btnReset.TabIndex = 221;
            this.btnReset.Text = "&Reset";
            this.btnReset.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click_1);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(6, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(271, 19);
            this.label1.TabIndex = 220;
            this.label1.Text = "Select one of the following to continue.";
            // 
            // btnSubmit
            // 
            this.btnSubmit.Location = new System.Drawing.Point(361, 130);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(87, 47);
            this.btnSubmit.TabIndex = 219;
            this.btnSubmit.Text = "&Submit";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // txtType
            // 
            this.txtType.Location = new System.Drawing.Point(361, 57);
            this.txtType.Name = "txtType";
            this.txtType.Size = new System.Drawing.Size(201, 26);
            this.txtType.TabIndex = 218;
            this.txtType.TextChanged += new System.EventHandler(this.txtType_TextChanged);
            // 
            // rdoBookType
            // 
            this.rdoBookType.AutoSize = true;
            this.rdoBookType.Location = new System.Drawing.Point(6, 230);
            this.rdoBookType.Name = "rdoBookType";
            this.rdoBookType.Size = new System.Drawing.Size(100, 23);
            this.rdoBookType.TabIndex = 217;
            this.rdoBookType.Text = "Book Type";
            this.rdoBookType.UseVisualStyleBackColor = true;
            this.rdoBookType.CheckedChanged += new System.EventHandler(this.rdoBookType_CheckedChanged_1);
            // 
            // rdoAccessoryType
            // 
            this.rdoAccessoryType.AutoSize = true;
            this.rdoAccessoryType.Location = new System.Drawing.Point(6, 192);
            this.rdoAccessoryType.Name = "rdoAccessoryType";
            this.rdoAccessoryType.Size = new System.Drawing.Size(133, 23);
            this.rdoAccessoryType.TabIndex = 216;
            this.rdoAccessoryType.Text = "Accessory Type";
            this.rdoAccessoryType.UseVisualStyleBackColor = true;
            this.rdoAccessoryType.CheckedChanged += new System.EventHandler(this.rdoAccessoryType_CheckedChanged_1);
            // 
            // rdoInstrumentType
            // 
            this.rdoInstrumentType.AutoSize = true;
            this.rdoInstrumentType.Checked = true;
            this.rdoInstrumentType.Location = new System.Drawing.Point(6, 57);
            this.rdoInstrumentType.Name = "rdoInstrumentType";
            this.rdoInstrumentType.Size = new System.Drawing.Size(137, 23);
            this.rdoInstrumentType.TabIndex = 215;
            this.rdoInstrumentType.TabStop = true;
            this.rdoInstrumentType.Text = "Instrument Type";
            this.rdoInstrumentType.UseVisualStyleBackColor = true;
            this.rdoInstrumentType.CheckedChanged += new System.EventHandler(this.rdoInstrumentType_CheckedChanged_1);
            // 
            // tabPageView
            // 
            this.tabPageView.BackColor = System.Drawing.SystemColors.Control;
            this.tabPageView.Controls.Add(this.btnUpdate);
            this.tabPageView.Controls.Add(this.lblTypeTitle);
            this.tabPageView.Controls.Add(this.groupBox1);
            this.tabPageView.Controls.Add(this.panel1);
            this.tabPageView.Location = new System.Drawing.Point(4, 28);
            this.tabPageView.Name = "tabPageView";
            this.tabPageView.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageView.Size = new System.Drawing.Size(603, 270);
            this.tabPageView.TabIndex = 1;
            this.tabPageView.Text = "View / Edit";
            this.tabPageView.Click += new System.EventHandler(this.tabPage2_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(69, 201);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(80, 38);
            this.btnUpdate.TabIndex = 3;
            this.btnUpdate.Text = "&Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // lblTypeTitle
            // 
            this.lblTypeTitle.AutoSize = true;
            this.lblTypeTitle.Font = new System.Drawing.Font("Tempus Sans ITC", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTypeTitle.ForeColor = System.Drawing.Color.Purple;
            this.lblTypeTitle.Location = new System.Drawing.Point(34, 146);
            this.lblTypeTitle.Name = "lblTypeTitle";
            this.lblTypeTitle.Size = new System.Drawing.Size(160, 24);
            this.lblTypeTitle.TabIndex = 2;
            this.lblTypeTitle.Text = "Instrument Type";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rdoBkTypedgv);
            this.groupBox1.Controls.Add(this.rdoAccTypedgv);
            this.groupBox1.Controls.Add(this.rdoInstTypedgv);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(215, 117);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // rdoBkTypedgv
            // 
            this.rdoBkTypedgv.AutoSize = true;
            this.rdoBkTypedgv.Location = new System.Drawing.Point(5, 83);
            this.rdoBkTypedgv.Name = "rdoBkTypedgv";
            this.rdoBkTypedgv.Size = new System.Drawing.Size(100, 23);
            this.rdoBkTypedgv.TabIndex = 6;
            this.rdoBkTypedgv.TabStop = true;
            this.rdoBkTypedgv.Text = "Book Type";
            this.rdoBkTypedgv.UseVisualStyleBackColor = true;
            this.rdoBkTypedgv.CheckedChanged += new System.EventHandler(this.rdoBkTypedgv_CheckedChanged);
            // 
            // rdoAccTypedgv
            // 
            this.rdoAccTypedgv.AutoSize = true;
            this.rdoAccTypedgv.Location = new System.Drawing.Point(5, 54);
            this.rdoAccTypedgv.Name = "rdoAccTypedgv";
            this.rdoAccTypedgv.Size = new System.Drawing.Size(133, 23);
            this.rdoAccTypedgv.TabIndex = 5;
            this.rdoAccTypedgv.TabStop = true;
            this.rdoAccTypedgv.Text = "Accessory Type";
            this.rdoAccTypedgv.UseVisualStyleBackColor = true;
            this.rdoAccTypedgv.CheckedChanged += new System.EventHandler(this.rdoAccTypedgv_CheckedChanged);
            // 
            // rdoInstTypedgv
            // 
            this.rdoInstTypedgv.AutoSize = true;
            this.rdoInstTypedgv.Location = new System.Drawing.Point(6, 25);
            this.rdoInstTypedgv.Name = "rdoInstTypedgv";
            this.rdoInstTypedgv.Size = new System.Drawing.Size(137, 23);
            this.rdoInstTypedgv.TabIndex = 4;
            this.rdoInstTypedgv.TabStop = true;
            this.rdoInstTypedgv.Text = "Instrument Type";
            this.rdoInstTypedgv.UseVisualStyleBackColor = true;
            this.rdoInstTypedgv.CheckedChanged += new System.EventHandler(this.rdoInstTypedgv_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dgvType);
            this.panel1.Location = new System.Drawing.Point(261, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(336, 253);
            this.panel1.TabIndex = 0;
            // 
            // dgvType
            // 
            this.dgvType.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvType.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvType.Location = new System.Drawing.Point(0, 0);
            this.dgvType.Name = "dgvType";
            this.dgvType.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvType.Size = new System.Drawing.Size(336, 253);
            this.dgvType.TabIndex = 0;
            this.dgvType.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvType_CellContentClick);
            this.dgvType.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvType_CellContentDoubleClick);
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.btnClose.Image = global::MusBase.Properties.Resources._1427990866_Done;
            this.btnClose.Location = new System.Drawing.Point(491, 327);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(95, 44);
            this.btnClose.TabIndex = 222;
            this.btnClose.Text = "&Quit";
            this.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnBkClose_Click_1);
            // 
            // btnNext
            // 
            this.btnNext.Font = new System.Drawing.Font("Wide Latin", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNext.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnNext.Location = new System.Drawing.Point(387, 337);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(75, 34);
            this.btnNext.TabIndex = 1;
            this.btnNext.Text = ">>>";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnPrevious
            // 
            this.btnPrevious.Font = new System.Drawing.Font("Wide Latin", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrevious.Location = new System.Drawing.Point(242, 337);
            this.btnPrevious.Name = "btnPrevious";
            this.btnPrevious.Size = new System.Drawing.Size(75, 34);
            this.btnPrevious.TabIndex = 2;
            this.btnPrevious.Text = "<<<";
            this.btnPrevious.UseVisualStyleBackColor = true;
            this.btnPrevious.Click += new System.EventHandler(this.btnPrevious_Click);
            // 
            // frmAddType
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(635, 388);
            this.Controls.Add(this.btnPrevious);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.RoyalBlue;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "frmAddType";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ADD TYPE";
            this.Load += new System.EventHandler(this.frmAddType_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPageAdd.ResumeLayout(false);
            this.tabPageAdd.PerformLayout();
            this.gbxRdos.ResumeLayout(false);
            this.gbxRdos.PerformLayout();
            this.tabPageView.ResumeLayout(false);
            this.tabPageView.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvType)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageAdd;
        private System.Windows.Forms.GroupBox gbxRdos;
        private System.Windows.Forms.RadioButton rdoPercussion;
        private System.Windows.Forms.RadioButton rdoStrings;
        private System.Windows.Forms.RadioButton rdoWoodwinds;
        private System.Windows.Forms.RadioButton rdoBrass;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.TextBox txtType;
        private System.Windows.Forms.RadioButton rdoBookType;
        private System.Windows.Forms.RadioButton rdoAccessoryType;
        private System.Windows.Forms.RadioButton rdoInstrumentType;
        private System.Windows.Forms.TabPage tabPageView;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnPrevious;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dgvType;
        private System.Windows.Forms.Label lblTypeTitle;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rdoBkTypedgv;
        private System.Windows.Forms.RadioButton rdoAccTypedgv;
        private System.Windows.Forms.RadioButton rdoInstTypedgv;
        private System.Windows.Forms.Button btnUpdate;


    }
}