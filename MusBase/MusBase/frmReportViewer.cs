﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MusBase
{
    public partial class frmrptStudentDetails : Form
    {
        public frmrptStudentDetails()
        {
            InitializeComponent();
        }

        private void frmReportViewer_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'DataSet1.Students_View' table. You can move, or remove it, as needed.
            this.Students_ViewTableAdapter.Fill(this.DataSet1.Students_View);

            this.reportViewer1.RefreshReport();
        }
    }
}
