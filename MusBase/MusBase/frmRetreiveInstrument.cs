﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace MusBase
{
    public partial class frmRetrieveInstrument : Form
    {
        string connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;
        SqlConnection conn;
        SqlCommand cmd;
        public frmRetrieveInstrument()
        {
            InitializeComponent();
            fillcboInstType();
            //deactivate constrols
            btnRtvInstrument.Enabled = false;
            btnRtvInstSubmitInst.Enabled = false;
            cboRtvInstSerialNo.Enabled = false;
            cboRtvInstAdminNo.Enabled = false;
            lblLink.Text = "Waiting ...";
        }

        private void LoadAssigneInstAdmin()
        {
            try
            {
                string query = "SELECT Student_Instrument.Admin_No AS [ADMIN NO], Student_Profile.LName AS [LAST NAME], Student_Profile.FName AS [FIRST NAME]," +
                    " Instrument_Details.Inst_Make As [MAKE],Instrument_Details.Inst_Family As [FAMILY], Student_Instrument.Assign_Date [ISSUE DATE] FROM Student_Instrument INNER JOIN Student_Profile ON " +
                    "Student_Instrument.Admin_No = Student_Profile.Admin_No " +
                    "INNER JOIN Instrument_Details ON Student_Instrument.Inst_Serial_No = Instrument_Details.Inst_Serial_No " +
                    "WHERE Student_Instrument.Admin_No = @AdminNo AND Student_Instrument.Inst_Serial_No = @SerialNo";
                conn = new SqlConnection(connectionString);
                SqlDataAdapter dAdapter;
                cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@AdminNo", Convert.ToInt32(cboRtvInstAdminNo.Text));
                cmd.Parameters.AddWithValue("@SerialNo", cboRtvInstSerialNo.Text);
                dAdapter = new SqlDataAdapter(cmd);
                DataTable AsgInstTable = new DataTable();
                //fill data table with data
                dAdapter.Fill(AsgInstTable);
                //sync data with dgv 
                BindingSource bSource = new BindingSource();
                bSource.DataSource = AsgInstTable;
                dgvRtvInstBearerDetails.DataSource = bSource;
                btnRtvInstSubmitInst.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n"+ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void LoadAssignedInst()
        {
            try
            {
                lblLink.Text = "Registration link:";
                string query = "SELECT Student_Instrument.Admin_No AS [ADMIN NO], Student_Profile.LName AS [LAST NAME], Student_Profile.FName AS [FIRST NAME]," +
                    " Instrument_Details.Inst_Make As [MAKE],Instrument_Details.Inst_Family As [FAMILY], Student_Instrument.Assign_Date [ISSUE DATE] FROM Student_Instrument INNER JOIN Student_Profile ON " +
                    "Student_Instrument.Admin_No = Student_Profile.Admin_No " +
                    "INNER JOIN Instrument_Details ON Student_Instrument.Inst_Serial_No = Instrument_Details.Inst_Serial_No " +
                    "WHERE Student_Instrument.Inst_Serial_No = @SerialNo";
                conn = new SqlConnection(connectionString);
                SqlDataAdapter dAdapter;
                cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@SerialNo", cboRtvInstSerialNo.Text);
                dAdapter = new SqlDataAdapter(cmd);
                DataTable AsgInstTable = new DataTable();
                //fill data table with data
                dAdapter.Fill(AsgInstTable);
                //sync data with dgv 
                BindingSource bSource = new BindingSource();
                bSource.DataSource = AsgInstTable;
                dgvRtvInstBearerDetails.DataSource = bSource;
                btnRtvInstSubmitInst.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void fillcboInstSerialNo() 
        {
            cboRtvInstSerialNo.Items.Clear();
            cboRtvInstSerialNo.Text = null;
            string query = "SELECT DISTINCT Student_Instrument.Inst_Serial_No FROM Student_Instrument "+
                "INNER JOIN Instrument_Details ON Student_Instrument.Inst_Serial_No = Instrument_Details.Inst_Serial_No "+
                "WHERE Instrument_Details.Instrument_Type = @InstType";
            conn = new SqlConnection(connectionString);
            cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@InstType", cboRtvInstType.Text);
            try 
            {
                conn.Open();
                 SqlDataReader reader =  cmd.ExecuteReader();
                 int ordinalSerial = reader.GetOrdinal("Inst_Serial_No");
                 while (reader.Read())
                 {
                     string temp = reader.GetString(ordinalSerial);
                     cboRtvInstSerialNo.Items.Add(temp);
                 }
                 reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            { conn.Close(); }
        }

        private void fillcboInstType() 
        {
            cboRtvInstType.Items.Clear();
            cboRtvInstType.Text = null;
            string query = "SELECT DISTINCT Instrument_Details.Instrument_Type FROM Student_Instrument INNER JOIN "+
                "Instrument_Details ON Student_Instrument.Inst_Serial_No = Instrument_Details.Inst_Serial_No";
            conn = new SqlConnection(connectionString);
            cmd = new SqlCommand(query, conn);
            try
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                int ordinal = reader.GetOrdinal("Instrument_Type");
                while (reader.Read())
                {
                    string temp = reader.GetString(ordinal);
                    cboRtvInstType.Items.Add(temp);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            { conn.Close(); }
        }

        private void frmRetreiveInstrument_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnRtvInstClearStd_Click(object sender, EventArgs e)
        {

        }

        private void btnAsgBkDone_Click(object sender, EventArgs e)
        {
            this.Close();//closing form
        }

        private void btnRtvInstSubmitInst_Click(object sender, EventArgs e)
        {
            //activate constrols
            LoadAssigneInstAdmin();
            lblLink.Text = "Unique Check";
            cboRtvInstType.Enabled = false;
            cboRtvInstSerialNo.Enabled = false;
            cboRtvInstAdminNo.Enabled = false;
            btnRtvInstrument.Enabled = true;
        }

        private void cboRtvInstType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboRtvInstType.Text != null)
            {
                cboRtvInstSerialNo.Enabled = true;
                fillcboInstSerialNo();
            }
        }

        private void fillcboAdmin() 
        {
            cboRtvInstAdminNo.Items.Clear();
            cboRtvInstAdminNo.Text = null;
            string query = "SELECT Student_Instrument.Admin_No FROM Student_Instrument " +
                "INNER JOIN Instrument_Details ON Student_Instrument.Inst_Serial_No = Instrument_Details.Inst_Serial_No " +
                "WHERE Instrument_Details.Inst_Serial_No = @SerialNo";
            conn = new SqlConnection(connectionString);
            cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@SerialNo", cboRtvInstSerialNo.Text);
            try
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                int ordinal = reader.GetOrdinal("Admin_No");
                while (reader.Read())
                {
                    int temp = reader.GetInt32(ordinal);
                    cboRtvInstAdminNo.Items.Add(temp);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            { conn.Close(); }
        }

        private void cboRtvInstSerialNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboRtvInstSerialNo != null)
            {
                cboRtvInstAdminNo.Enabled = true;
                LoadAssignedInst();
                fillcboAdmin();
            }
        }

        private void RetrieveInstrument() 
        {
            string query = "DELETE FROM Student_Instrument WHERE Inst_Serial_No = @SerialNo AND Admin_No = @AdminNo ";
            conn = new SqlConnection(connectionString);
            cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@SerialNo", cboRtvInstSerialNo.Text);
            cmd.Parameters.AddWithValue("@AdminNo", Convert.ToInt32(cboRtvInstAdminNo.Text));
            try 
            {
                conn.Open();
                int TotalRowsAffected = cmd.ExecuteNonQuery();
                if (TotalRowsAffected == 1)
                {
                    MessageBox.Show("INSTRUMENT SUCCESSFULLY RETRIEVED.", "RETRIEVAL STATUS", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    gbxRtvInstrument.Enabled = true;
                    cboRtvInstType.Enabled = true;
                }
                else 
                {
                    MessageBox.Show("ERROR,\nINSTRUMENT NOT RETRIEVED,\nPLEASE TRY AGAIN.", "RETRIEVAL STATUS", MessageBoxButtons.OK, MessageBoxIcon.Error); 
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n"+ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            { conn.Close(); }
        }


        private void btnRtvInstrument_Click(object sender, EventArgs e)
        {
            RetrieveInstrument();
            cboRtvInstType.Enabled = true;
            dgvRtvInstBearerDetails.DataSource = null;
            reset();
        }

        private void cboRtvInstAdminNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboRtvInstAdminNo != null)
            { 
                btnRtvInstSubmitInst.Enabled = true; 
            }
        }

        private void reset() 
        {
            lblLink.Text = "Waiting ...";
            //enable status
            gbxRtvInstrument.Enabled = true;
            cboRtvInstType.Enabled = true;
            cboRtvInstSerialNo.Enabled = false;
            cboRtvInstAdminNo.Enabled = false;
            btnRtvInstrument.Enabled = false;
            btnRtvInstSubmitInst.Enabled = false;
            //clear
            dgvRtvInstBearerDetails.DataSource = null;
            cboRtvInstAdminNo.Items.Clear();
            cboRtvInstAdminNo.Text = null;
            cboRtvInstSerialNo.Items.Clear();
            cboRtvInstSerialNo.Text = null;
            fillcboInstType();   
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            reset();
        }
    }
}
