﻿namespace MusBase
{
    partial class frmRemoveBook
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblFName = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cboRmvBkCode = new System.Windows.Forms.ComboBox();
            this.btnRmvBkDone = new System.Windows.Forms.Button();
            this.btnRmvBook = new System.Windows.Forms.Button();
            this.btnRmvBkClear = new System.Windows.Forms.Button();
            this.txtRmvBkCondition = new System.Windows.Forms.TextBox();
            this.cboRmvBkType = new System.Windows.Forms.ComboBox();
            this.lblRmvBkType = new System.Windows.Forms.Label();
            this.lblRmvBkCondition = new System.Windows.Forms.Label();
            this.lblRmvBkCode = new System.Windows.Forms.Label();
            this.txtRmvBkTitle = new System.Windows.Forms.TextBox();
            this.lblRmvBkTitle = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dgvRemoveBook = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRemoveBook)).BeginInit();
            this.SuspendLayout();
            // 
            // lblFName
            // 
            this.lblFName.AutoSize = true;
            this.lblFName.Location = new System.Drawing.Point(39, 56);
            this.lblFName.Name = "lblFName";
            this.lblFName.Size = new System.Drawing.Size(0, 13);
            this.lblFName.TabIndex = 137;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cboRmvBkCode);
            this.groupBox1.Controls.Add(this.btnRmvBkDone);
            this.groupBox1.Controls.Add(this.btnRmvBook);
            this.groupBox1.Controls.Add(this.btnRmvBkClear);
            this.groupBox1.Controls.Add(this.txtRmvBkCondition);
            this.groupBox1.Controls.Add(this.cboRmvBkType);
            this.groupBox1.Controls.Add(this.lblRmvBkType);
            this.groupBox1.Controls.Add(this.lblRmvBkCondition);
            this.groupBox1.Controls.Add(this.lblRmvBkCode);
            this.groupBox1.Controls.Add(this.txtRmvBkTitle);
            this.groupBox1.Controls.Add(this.lblRmvBkTitle);
            this.groupBox1.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(588, 194);
            this.groupBox1.TabIndex = 138;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Select a book";
            // 
            // cboRmvBkCode
            // 
            this.cboRmvBkCode.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboRmvBkCode.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboRmvBkCode.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboRmvBkCode.FormattingEnabled = true;
            this.cboRmvBkCode.Location = new System.Drawing.Point(95, 79);
            this.cboRmvBkCode.Name = "cboRmvBkCode";
            this.cboRmvBkCode.Size = new System.Drawing.Size(121, 25);
            this.cboRmvBkCode.TabIndex = 193;
            this.cboRmvBkCode.SelectedIndexChanged += new System.EventHandler(this.cboRmvBkCode_SelectedIndexChanged);
            // 
            // btnRmvBkDone
            // 
            this.btnRmvBkDone.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRmvBkDone.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.btnRmvBkDone.Image = global::MusBase.Properties.Resources._1427990866_Done;
            this.btnRmvBkDone.Location = new System.Drawing.Point(475, 135);
            this.btnRmvBkDone.Name = "btnRmvBkDone";
            this.btnRmvBkDone.Size = new System.Drawing.Size(95, 44);
            this.btnRmvBkDone.TabIndex = 5;
            this.btnRmvBkDone.Text = "&Quit";
            this.btnRmvBkDone.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnRmvBkDone.UseVisualStyleBackColor = true;
            this.btnRmvBkDone.Click += new System.EventHandler(this.btnRmvBkDone_Click);
            // 
            // btnRmvBook
            // 
            this.btnRmvBook.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRmvBook.Image = global::MusBase.Properties.Resources.book_delete;
            this.btnRmvBook.Location = new System.Drawing.Point(95, 135);
            this.btnRmvBook.Name = "btnRmvBook";
            this.btnRmvBook.Size = new System.Drawing.Size(104, 42);
            this.btnRmvBook.TabIndex = 3;
            this.btnRmvBook.Text = "&Remove";
            this.btnRmvBook.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnRmvBook.UseVisualStyleBackColor = true;
            this.btnRmvBook.Click += new System.EventHandler(this.btnRmvBook_Click);
            // 
            // btnRmvBkClear
            // 
            this.btnRmvBkClear.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRmvBkClear.Image = global::MusBase.Properties.Resources.Oboe_write_32;
            this.btnRmvBkClear.Location = new System.Drawing.Point(319, 135);
            this.btnRmvBkClear.Name = "btnRmvBkClear";
            this.btnRmvBkClear.Size = new System.Drawing.Size(104, 42);
            this.btnRmvBkClear.TabIndex = 4;
            this.btnRmvBkClear.Text = "&Clear";
            this.btnRmvBkClear.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnRmvBkClear.UseVisualStyleBackColor = true;
            this.btnRmvBkClear.Click += new System.EventHandler(this.btnRmvBkClear_Click);
            // 
            // txtRmvBkCondition
            // 
            this.txtRmvBkCondition.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRmvBkCondition.Location = new System.Drawing.Point(319, 80);
            this.txtRmvBkCondition.Name = "txtRmvBkCondition";
            this.txtRmvBkCondition.ReadOnly = true;
            this.txtRmvBkCondition.Size = new System.Drawing.Size(160, 24);
            this.txtRmvBkCondition.TabIndex = 192;
            // 
            // cboRmvBkType
            // 
            this.cboRmvBkType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboRmvBkType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboRmvBkType.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboRmvBkType.FormattingEnabled = true;
            this.cboRmvBkType.Location = new System.Drawing.Point(94, 36);
            this.cboRmvBkType.Name = "cboRmvBkType";
            this.cboRmvBkType.Size = new System.Drawing.Size(121, 25);
            this.cboRmvBkType.TabIndex = 1;
            this.cboRmvBkType.SelectedIndexChanged += new System.EventHandler(this.cboRmvBkType_SelectedIndexChanged);
            // 
            // lblRmvBkType
            // 
            this.lblRmvBkType.AutoSize = true;
            this.lblRmvBkType.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRmvBkType.Location = new System.Drawing.Point(15, 36);
            this.lblRmvBkType.Name = "lblRmvBkType";
            this.lblRmvBkType.Size = new System.Drawing.Size(77, 17);
            this.lblRmvBkType.TabIndex = 189;
            this.lblRmvBkType.Text = "Book Type";
            // 
            // lblRmvBkCondition
            // 
            this.lblRmvBkCondition.AutoSize = true;
            this.lblRmvBkCondition.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRmvBkCondition.Location = new System.Drawing.Point(240, 80);
            this.lblRmvBkCondition.Name = "lblRmvBkCondition";
            this.lblRmvBkCondition.Size = new System.Drawing.Size(73, 17);
            this.lblRmvBkCondition.TabIndex = 188;
            this.lblRmvBkCondition.Text = "Condition";
            // 
            // lblRmvBkCode
            // 
            this.lblRmvBkCode.AutoSize = true;
            this.lblRmvBkCode.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRmvBkCode.Location = new System.Drawing.Point(15, 81);
            this.lblRmvBkCode.Name = "lblRmvBkCode";
            this.lblRmvBkCode.Size = new System.Drawing.Size(79, 17);
            this.lblRmvBkCode.TabIndex = 187;
            this.lblRmvBkCode.Text = "Book Code";
            // 
            // txtRmvBkTitle
            // 
            this.txtRmvBkTitle.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRmvBkTitle.Location = new System.Drawing.Point(319, 37);
            this.txtRmvBkTitle.Name = "txtRmvBkTitle";
            this.txtRmvBkTitle.ReadOnly = true;
            this.txtRmvBkTitle.Size = new System.Drawing.Size(260, 24);
            this.txtRmvBkTitle.TabIndex = 186;
            // 
            // lblRmvBkTitle
            // 
            this.lblRmvBkTitle.AutoSize = true;
            this.lblRmvBkTitle.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRmvBkTitle.Location = new System.Drawing.Point(276, 36);
            this.lblRmvBkTitle.Name = "lblRmvBkTitle";
            this.lblRmvBkTitle.Size = new System.Drawing.Size(37, 17);
            this.lblRmvBkTitle.TabIndex = 185;
            this.lblRmvBkTitle.Text = "Title";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dgvRemoveBook);
            this.panel1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(12, 252);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(588, 158);
            this.panel1.TabIndex = 139;
            // 
            // dgvRemoveBook
            // 
            this.dgvRemoveBook.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvRemoveBook.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRemoveBook.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvRemoveBook.Location = new System.Drawing.Point(0, 0);
            this.dgvRemoveBook.Name = "dgvRemoveBook";
            this.dgvRemoveBook.ReadOnly = true;
            this.dgvRemoveBook.Size = new System.Drawing.Size(588, 158);
            this.dgvRemoveBook.TabIndex = 0;
            // 
            // frmRemoveBook
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(619, 422);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lblFName);
            this.ForeColor = System.Drawing.Color.RoyalBlue;
            this.MaximizeBox = false;
            this.Name = "frmRemoveBook";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Remove Book";
            this.Load += new System.EventHandler(this.frmRemoveBook_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRemoveBook)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblFName;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnRmvBook;
        private System.Windows.Forms.Button btnRmvBkClear;
        private System.Windows.Forms.TextBox txtRmvBkCondition;
        private System.Windows.Forms.ComboBox cboRmvBkType;
        private System.Windows.Forms.Label lblRmvBkType;
        private System.Windows.Forms.Label lblRmvBkCondition;
        private System.Windows.Forms.Label lblRmvBkCode;
        private System.Windows.Forms.TextBox txtRmvBkTitle;
        private System.Windows.Forms.Label lblRmvBkTitle;
        private System.Windows.Forms.Button btnRmvBkDone;
        private System.Windows.Forms.ComboBox cboRmvBkCode;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dgvRemoveBook;
    }
}