﻿namespace MusBase
{
    partial class frmAddBook
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvBookDetails = new System.Windows.Forms.DataGridView();
            this.lblFName = new System.Windows.Forms.Label();
            this.gbxBookDetails = new System.Windows.Forms.GroupBox();
            this.btnBkSave = new System.Windows.Forms.Button();
            this.txtBookCondition = new System.Windows.Forms.TextBox();
            this.txtBkCode = new System.Windows.Forms.TextBox();
            this.cboBkType = new System.Windows.Forms.ComboBox();
            this.lblBkType = new System.Windows.Forms.Label();
            this.btnBkClear = new System.Windows.Forms.Button();
            this.lblInstCondition = new System.Windows.Forms.Label();
            this.lblBkCode = new System.Windows.Forms.Label();
            this.txtBkTitle = new System.Windows.Forms.TextBox();
            this.lblBkTitle = new System.Windows.Forms.Label();
            this.btnAddNewBook = new System.Windows.Forms.Button();
            this.btnBkClose = new System.Windows.Forms.Button();
            this.lblInsertStatus = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnUpdate = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBookDetails)).BeginInit();
            this.gbxBookDetails.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvBookDetails
            // 
            this.dgvBookDetails.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvBookDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBookDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvBookDetails.Location = new System.Drawing.Point(0, 0);
            this.dgvBookDetails.Name = "dgvBookDetails";
            this.dgvBookDetails.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvBookDetails.Size = new System.Drawing.Size(740, 208);
            this.dgvBookDetails.TabIndex = 68;
            this.dgvBookDetails.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvBookDetails_CellContentClick);
            this.dgvBookDetails.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvBookDetails_CellContentDoubleClick);
            // 
            // lblFName
            // 
            this.lblFName.AutoSize = true;
            this.lblFName.Location = new System.Drawing.Point(44, 65);
            this.lblFName.Name = "lblFName";
            this.lblFName.Size = new System.Drawing.Size(0, 13);
            this.lblFName.TabIndex = 60;
            // 
            // gbxBookDetails
            // 
            this.gbxBookDetails.Controls.Add(this.btnBkSave);
            this.gbxBookDetails.Controls.Add(this.txtBookCondition);
            this.gbxBookDetails.Controls.Add(this.txtBkCode);
            this.gbxBookDetails.Controls.Add(this.cboBkType);
            this.gbxBookDetails.Controls.Add(this.lblBkType);
            this.gbxBookDetails.Controls.Add(this.btnBkClear);
            this.gbxBookDetails.Controls.Add(this.lblInstCondition);
            this.gbxBookDetails.Controls.Add(this.lblBkCode);
            this.gbxBookDetails.Controls.Add(this.txtBkTitle);
            this.gbxBookDetails.Controls.Add(this.lblBkTitle);
            this.gbxBookDetails.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxBookDetails.Location = new System.Drawing.Point(12, 21);
            this.gbxBookDetails.Name = "gbxBookDetails";
            this.gbxBookDetails.Size = new System.Drawing.Size(740, 167);
            this.gbxBookDetails.TabIndex = 73;
            this.gbxBookDetails.TabStop = false;
            this.gbxBookDetails.Text = "Book Details";
            // 
            // btnBkSave
            // 
            this.btnBkSave.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBkSave.Image = global::MusBase.Properties.Resources.Book_Blank_Book_icon;
            this.btnBkSave.Location = new System.Drawing.Point(102, 110);
            this.btnBkSave.Name = "btnBkSave";
            this.btnBkSave.Size = new System.Drawing.Size(104, 42);
            this.btnBkSave.TabIndex = 5;
            this.btnBkSave.Text = "&Save";
            this.btnBkSave.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnBkSave.UseVisualStyleBackColor = true;
            this.btnBkSave.Click += new System.EventHandler(this.btnBkSave_Click_1);
            // 
            // txtBookCondition
            // 
            this.txtBookCondition.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBookCondition.Location = new System.Drawing.Point(323, 70);
            this.txtBookCondition.Name = "txtBookCondition";
            this.txtBookCondition.Size = new System.Drawing.Size(120, 24);
            this.txtBookCondition.TabIndex = 4;
            this.txtBookCondition.TextChanged += new System.EventHandler(this.txtBookCondition_TextChanged);
            // 
            // txtBkCode
            // 
            this.txtBkCode.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBkCode.Location = new System.Drawing.Point(102, 70);
            this.txtBkCode.Name = "txtBkCode";
            this.txtBkCode.Size = new System.Drawing.Size(120, 24);
            this.txtBkCode.TabIndex = 3;
            this.txtBkCode.TextChanged += new System.EventHandler(this.txtBkCode_TextChanged);
            // 
            // cboBkType
            // 
            this.cboBkType.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboBkType.FormattingEnabled = true;
            this.cboBkType.Location = new System.Drawing.Point(101, 26);
            this.cboBkType.Name = "cboBkType";
            this.cboBkType.Size = new System.Drawing.Size(121, 25);
            this.cboBkType.TabIndex = 1;
            this.cboBkType.SelectedIndexChanged += new System.EventHandler(this.cboBkType_SelectedIndexChanged);
            // 
            // lblBkType
            // 
            this.lblBkType.AutoSize = true;
            this.lblBkType.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBkType.Location = new System.Drawing.Point(6, 26);
            this.lblBkType.Name = "lblBkType";
            this.lblBkType.Size = new System.Drawing.Size(77, 17);
            this.lblBkType.TabIndex = 86;
            this.lblBkType.Text = "Book Type";
            // 
            // btnBkClear
            // 
            this.btnBkClear.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBkClear.Image = global::MusBase.Properties.Resources.Flute_write_32;
            this.btnBkClear.Location = new System.Drawing.Point(323, 110);
            this.btnBkClear.Name = "btnBkClear";
            this.btnBkClear.Size = new System.Drawing.Size(104, 42);
            this.btnBkClear.TabIndex = 7;
            this.btnBkClear.Text = "&Clear";
            this.btnBkClear.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnBkClear.UseVisualStyleBackColor = true;
            this.btnBkClear.Click += new System.EventHandler(this.btnBkClear_Click_1);
            // 
            // lblInstCondition
            // 
            this.lblInstCondition.AutoSize = true;
            this.lblInstCondition.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInstCondition.Location = new System.Drawing.Point(247, 70);
            this.lblInstCondition.Name = "lblInstCondition";
            this.lblInstCondition.Size = new System.Drawing.Size(73, 17);
            this.lblInstCondition.TabIndex = 81;
            this.lblInstCondition.Text = "Condition";
            // 
            // lblBkCode
            // 
            this.lblBkCode.AutoSize = true;
            this.lblBkCode.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBkCode.Location = new System.Drawing.Point(6, 70);
            this.lblBkCode.Name = "lblBkCode";
            this.lblBkCode.Size = new System.Drawing.Size(79, 17);
            this.lblBkCode.TabIndex = 80;
            this.lblBkCode.Text = "Book Code";
            // 
            // txtBkTitle
            // 
            this.txtBkTitle.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBkTitle.Location = new System.Drawing.Point(323, 27);
            this.txtBkTitle.Name = "txtBkTitle";
            this.txtBkTitle.Size = new System.Drawing.Size(353, 24);
            this.txtBkTitle.TabIndex = 2;
            this.txtBkTitle.TextChanged += new System.EventHandler(this.txtBkTitle_TextChanged);
            // 
            // lblBkTitle
            // 
            this.lblBkTitle.AutoSize = true;
            this.lblBkTitle.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBkTitle.Location = new System.Drawing.Point(260, 28);
            this.lblBkTitle.Name = "lblBkTitle";
            this.lblBkTitle.Size = new System.Drawing.Size(37, 17);
            this.lblBkTitle.TabIndex = 78;
            this.lblBkTitle.Text = "Title";
            // 
            // btnAddNewBook
            // 
            this.btnAddNewBook.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddNewBook.Image = global::MusBase.Properties.Resources.book_add_icon;
            this.btnAddNewBook.Location = new System.Drawing.Point(114, 198);
            this.btnAddNewBook.Name = "btnAddNewBook";
            this.btnAddNewBook.Size = new System.Drawing.Size(104, 42);
            this.btnAddNewBook.TabIndex = 6;
            this.btnAddNewBook.Text = "&Add New";
            this.btnAddNewBook.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnAddNewBook.UseVisualStyleBackColor = true;
            this.btnAddNewBook.Click += new System.EventHandler(this.btnAddNewBook_Click_1);
            // 
            // btnBkClose
            // 
            this.btnBkClose.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBkClose.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.btnBkClose.Image = global::MusBase.Properties.Resources._1427990866_Done;
            this.btnBkClose.Location = new System.Drawing.Point(335, 198);
            this.btnBkClose.Name = "btnBkClose";
            this.btnBkClose.Size = new System.Drawing.Size(104, 44);
            this.btnBkClose.TabIndex = 9;
            this.btnBkClose.Text = "&Quit";
            this.btnBkClose.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnBkClose.UseVisualStyleBackColor = true;
            this.btnBkClose.Click += new System.EventHandler(this.btnBkClose_Click);
            // 
            // lblInsertStatus
            // 
            this.lblInsertStatus.AutoSize = true;
            this.lblInsertStatus.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInsertStatus.ForeColor = System.Drawing.Color.Red;
            this.lblInsertStatus.Location = new System.Drawing.Point(13, 223);
            this.lblInsertStatus.Name = "lblInsertStatus";
            this.lblInsertStatus.Size = new System.Drawing.Size(44, 17);
            this.lblInsertStatus.TabIndex = 204;
            this.lblInsertStatus.Text = "label1";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.dgvBookDetails);
            this.panel1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(12, 253);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(740, 208);
            this.panel1.TabIndex = 205;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Location = new System.Drawing.Point(672, 204);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(80, 38);
            this.btnUpdate.TabIndex = 206;
            this.btnUpdate.Text = "&Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // frmAddBook
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(764, 478);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblInsertStatus);
            this.Controls.Add(this.btnBkClose);
            this.Controls.Add(this.gbxBookDetails);
            this.Controls.Add(this.btnAddNewBook);
            this.Controls.Add(this.lblFName);
            this.ForeColor = System.Drawing.Color.RoyalBlue;
            this.Name = "frmAddBook";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Book Details";
            this.Load += new System.EventHandler(this.frmAddBook_Load_1);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBookDetails)).EndInit();
            this.gbxBookDetails.ResumeLayout(false);
            this.gbxBookDetails.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvBookDetails;
        private System.Windows.Forms.Label lblFName;
        private System.Windows.Forms.GroupBox gbxBookDetails;
        private System.Windows.Forms.TextBox txtBkCode;
        private System.Windows.Forms.ComboBox cboBkType;
        private System.Windows.Forms.Label lblBkType;
        private System.Windows.Forms.Button btnAddNewBook;
        private System.Windows.Forms.Button btnBkClear;
        private System.Windows.Forms.Label lblInstCondition;
        private System.Windows.Forms.Label lblBkCode;
        private System.Windows.Forms.TextBox txtBkTitle;
        private System.Windows.Forms.Label lblBkTitle;
        private System.Windows.Forms.Button btnBkClose;
        private System.Windows.Forms.Label lblInsertStatus;
        private System.Windows.Forms.TextBox txtBookCondition;
        private System.Windows.Forms.Button btnBkSave;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnUpdate;
    }
}