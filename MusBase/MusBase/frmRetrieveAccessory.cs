﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace MusBase
{
    public partial class frmRetrieveAccessory : Form
    {
        string connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;
        SqlConnection conn;
        SqlCommand cmd;
        public frmRetrieveAccessory()
        {
            InitializeComponent();
            fillcboAccType();
            //deactivate constrols
            btnRtvAccessory.Enabled = false;
            btnRtvAccSubmitAcc.Enabled = false;
            cboRtvAccAccessoryCode.Enabled = false;
            cboRtvAccAdminNo.Enabled = false;
            lblLink.Text = "Waiting ...";
            
        }

        private void LoadAssigneAccAdmin()
        {
            try
            {
                string query = "SELECT Student_Accessory.Admin_No AS [ADMIN NO], Student_Profile.LName AS [LAST NAME], Student_Profile.FName AS [FIRST NAME]," +
                    " Accessory_Details.Name As [NAME], Student_Accessory.Assign_Date AS [ISSUE DATE] FROM Student_Accessory INNER JOIN Student_Profile ON " +
                    "Student_Accessory.Admin_No = Student_Profile.Admin_No " +
                    "INNER JOIN Accessory_Details ON Student_Accessory.Accessory_Code = Accessory_Details.Accessory_Code " +
                    "WHERE Student_Accessory.Admin_No = @AdminNo AND Student_Accessory.Accessory_Code = @AccessoryCode";
                conn = new SqlConnection(connectionString);
                SqlDataAdapter dAdapter;
                cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@AdminNo", Convert.ToInt32(cboRtvAccAdminNo.Text));
                cmd.Parameters.AddWithValue("@AccessoryCode", cboRtvAccAccessoryCode.Text);
                dAdapter = new SqlDataAdapter(cmd);
                DataTable AsgAccTable = new DataTable();
                //fill data table with data
                dAdapter.Fill(AsgAccTable);
                //sync data with dgv 
                BindingSource bSource = new BindingSource();
                bSource.DataSource = AsgAccTable;
                dgvRtvAccBearerDetails.DataSource = bSource;
                btnRtvAccSubmitAcc.Enabled = false;
            }
            catch (SqlException sex)
            {
                MessageBox.Show("Error\n"+sex.Message,"Error" ,MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void LoadAssignedAcc()
        {
            try
            {
                lblLink.Text = "Registration link:";
                string query = "SELECT Student_Accessory.Admin_No AS [ADMIN NO], Student_Profile.LName AS [LAST NAME], Student_Profile.FName AS [FIRST NAME]," +
                    " Accessory_Details.Name As [NAME], Student_Accessory.Assign_Date AS [ISSUE DATE] FROM Student_Accessory INNER JOIN Student_Profile ON " +
                    "Student_Accessory.Admin_No = Student_Profile.Admin_No " +
                    "INNER JOIN Accessory_Details ON Student_Accessory.Accessory_Code = Accessory_Details.Accessory_Code " +
                    "WHERE Student_Accessory.Accessory_Code = @AccessoryCode";
                conn = new SqlConnection(connectionString);
                SqlDataAdapter dAdapter;
                cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@AccessoryCode", cboRtvAccAccessoryCode.Text);
                dAdapter = new SqlDataAdapter(cmd);
                DataTable AsgAccTable = new DataTable();
                //fill data table with data
                dAdapter.Fill(AsgAccTable);
                //sync data with dgv 
                BindingSource bSource = new BindingSource();
                bSource.DataSource = AsgAccTable;
                dgvRtvAccBearerDetails.DataSource = bSource;
                btnRtvAccSubmitAcc.Enabled = false;
            }
            catch (Exception ex) 
            {
                MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void fillcboAccType()
        {
            cboRtvAccAccessoryType.Items.Clear();
            cboRtvAccAccessoryType.Text = null;
            string query = "SELECT DISTINCT Accessory_Details.Accessory_Type FROM Student_Accessory INNER JOIN " +
                "Accessory_Details ON Student_Accessory.Accessory_Code = Accessory_Details.Accessory_Code";
            conn = new SqlConnection(connectionString);
            cmd = new SqlCommand(query, conn);
            try
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                int ordinal = reader.GetOrdinal("Accessory_Type");
                while (reader.Read())
                {
                    string temp = reader.GetString(ordinal);
                    cboRtvAccAccessoryType.Items.Add(temp);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            { conn.Close(); }
        }

        private void fillcboAccAccessoryCode()
        {
            cboRtvAccAccessoryCode.Items.Clear();
            cboRtvAccAccessoryCode.Text = null;
            string query = "SELECT DISTINCT Student_Accessory.Accessory_Code FROM Student_Accessory " +
                "INNER JOIN Accessory_Details ON Student_Accessory.Accessory_Code = Accessory_Details.Accessory_Code " +
                "WHERE Accessory_Details.Accessory_Type = @AccessoryType";
            conn = new SqlConnection(connectionString);
            cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@AccessoryType", cboRtvAccAccessoryType.Text);
            try
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                int ordinalSerial = reader.GetOrdinal("Accessory_Code");
                while (reader.Read())
                {
                    string temp = reader.GetString(ordinalSerial);
                    cboRtvAccAccessoryCode.Items.Add(temp);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            { conn.Close(); }
        }

        private void fillcboAdmin()
        {
            cboRtvAccAdminNo.Items.Clear();
            cboRtvAccAdminNo.Text = null;
            string query = "SELECT Student_Accessory.Admin_No FROM Student_Accessory " +
                "INNER JOIN Accessory_Details ON Student_Accessory.Accessory_Code = Accessory_Details.Accessory_Code " +
                "WHERE Accessory_Details.Accessory_Code = @AccessoryCode";
            conn = new SqlConnection(connectionString);
            cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@AccessoryCode", cboRtvAccAccessoryCode.Text);
            try
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                int ordinal = reader.GetOrdinal("Admin_No");
                while (reader.Read())
                {
                    int temp = reader.GetInt32(ordinal);
                    cboRtvAccAdminNo.Items.Add(temp);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            { conn.Close(); }
        }

        private void frmRetrieveAccessory_Load(object sender, EventArgs e)
        {

        }

        private void btnAsgBkDone_Click(object sender, EventArgs e)
        {
            this.Close();//closing form
        }

        private void btnAsgBkSubmitBk_Click(object sender, EventArgs e)
        {
            
        }

        private void cboRtvAccAccessoryType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboRtvAccAccessoryType.Text != null)
            {
                cboRtvAccAccessoryCode.Enabled = true;
                fillcboAccAccessoryCode();
            }
        }

        private void cboRtvAccAccessoryCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboRtvAccAccessoryCode.Text != null)
            {
                cboRtvAccAdminNo.Enabled = true;
                LoadAssignedAcc();
                fillcboAdmin();
            }
        }

        private void reset()
        {
            lblLink.Text = "Waiting ...";
            //enable status
            gbxRtvAccessory.Enabled = true;
            cboRtvAccAccessoryType.Enabled = true;
            cboRtvAccAccessoryCode.Enabled = false;
            cboRtvAccAdminNo.Enabled = false;
            btnRtvAccessory.Enabled = false;
            btnRtvAccSubmitAcc.Enabled = false;
            //clear
            dgvRtvAccBearerDetails.DataSource = null;
            cboRtvAccAdminNo.Items.Clear();
            cboRtvAccAdminNo.Text = null;
            cboRtvAccAccessoryCode.Items.Clear();
            cboRtvAccAccessoryCode.Text = null;
            fillcboAccType();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            reset();
        }

        private void RetrieveAccessory()
        {
            string query = "DELETE FROM Student_Accessory WHERE Accessory_Code = @AccessoryCode AND Admin_No = @AdminNo ";
            conn = new SqlConnection(connectionString);
            cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@AccessoryCode", cboRtvAccAccessoryCode.Text);
            cmd.Parameters.AddWithValue("@AdminNo", Convert.ToInt32(cboRtvAccAdminNo.Text));
            try
            {
                conn.Open();
                int TotalRowsAffected = cmd.ExecuteNonQuery();
                if (TotalRowsAffected == 1)
                {
                    MessageBox.Show("ACCESSORY SUCCESSFULLY RETRIEVED.", "RETRIEVAL STATUS", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    gbxRtvAccessory.Enabled = true;
                    cboRtvAccAccessoryType.Enabled = true;
                }
                else
                {
                    MessageBox.Show("ERROR,\nACCESSORY NOT RETRIEVED,\nPLEASE TRY AGAIN.", "RETRIEVAL STATUS", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            { conn.Close(); }
        }

        private void btnRtvAccessory_Click(object sender, EventArgs e)
        {
            RetrieveAccessory();
            cboRtvAccAccessoryType.Enabled = true;
            dgvRtvAccBearerDetails.DataSource = null;
            reset();
        }

        private void cboRtvAccAdminNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboRtvAccAdminNo != null)
            {
                btnRtvAccSubmitAcc.Enabled = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //activate constrols
            LoadAssigneAccAdmin();
            lblLink.Text = "Unique Check";
            cboRtvAccAccessoryType.Enabled = false;
            cboRtvAccAccessoryCode.Enabled = false;
            cboRtvAccAdminNo.Enabled = false;
            btnRtvAccessory.Enabled = true;
        }
    }
}
