﻿namespace MusBase
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.UsertoolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.StudentMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.AddStudentMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RemoveStudentMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.Form4MenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Form3MenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Form2MenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Form1MenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.AllFormsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.InstrumentMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.AssignInstrumentMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RetrieveInstrumentMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this.AddInstrumentMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RemoveInstrumentMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.BrassMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.StringsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.WoodwindsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PercussionMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator16 = new System.Windows.Forms.ToolStripSeparator();
            this.AllInstrumentsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BooksMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AssignBookMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RetrieveBookMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator17 = new System.Windows.Forms.ToolStripSeparator();
            this.AddBookMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RemoveBookMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator18 = new System.Windows.Forms.ToolStripSeparator();
            this.AllBooksMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AccessoriesMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.AssignAccessoryMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RetrieveAccessoryMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator13 = new System.Windows.Forms.ToolStripSeparator();
            this.AddAccessoryMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RemoveAccessoryMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator15 = new System.Windows.Forms.ToolStripSeparator();
            this.AllAccessoriesMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.contentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.indexToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeBackgroundImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.BaseMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.gradeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.musBandToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.musChoirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.addTypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manageUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.assignedInstrumentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.instrumentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.booksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.assignedInstrumentsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.assignedBooksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.assignedAccessoriesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gbxManageAccessories = new System.Windows.Forms.GroupBox();
            this.btnRetriveInstrument = new System.Windows.Forms.Button();
            this.btnAssignAccessories = new System.Windows.Forms.Button();
            this.bntRemoveAccessory = new System.Windows.Forms.Button();
            this.btnAddAccessory = new System.Windows.Forms.Button();
            this.gbxManageBooks = new System.Windows.Forms.GroupBox();
            this.btnRetrieveBook = new System.Windows.Forms.Button();
            this.btnAssignBook = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.btnAddBook = new System.Windows.Forms.Button();
            this.gboxManageStudents = new System.Windows.Forms.GroupBox();
            this.btnRemoveStudent = new System.Windows.Forms.Button();
            this.btnAddStudent = new System.Windows.Forms.Button();
            this.gbxManageInstruments = new System.Windows.Forms.GroupBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnAddInstrument = new System.Windows.Forms.Button();
            this.btnRemoveInstrument = new System.Windows.Forms.Button();
            this.btnExitMain = new System.Windows.Forms.Button();
            this.searchToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip.SuspendLayout();
            this.menuStrip.SuspendLayout();
            this.gbxManageAccessories.SuspendLayout();
            this.gbxManageBooks.SuspendLayout();
            this.gboxManageStudents.SuspendLayout();
            this.gbxManageInstruments.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip
            // 
            this.statusStrip.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel,
            this.UsertoolStripStatusLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 460);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
            this.statusStrip.Size = new System.Drawing.Size(761, 22);
            this.statusStrip.TabIndex = 2;
            this.statusStrip.Text = "StatusStrip";
            this.statusStrip.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.statusStrip_ItemClicked);
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(39, 17);
            this.toolStripStatusLabel.Text = "Status";
            // 
            // UsertoolStripStatusLabel
            // 
            this.UsertoolStripStatusLabel.Name = "UsertoolStripStatusLabel";
            this.UsertoolStripStatusLabel.Size = new System.Drawing.Size(135, 17);
            this.UsertoolStripStatusLabel.Text = "UsertoolStripStatusLabel";
            // 
            // StudentMenu
            // 
            this.StudentMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AddStudentMenuItem,
            this.RemoveStudentMenuItem,
            this.toolStripSeparator9,
            this.Form4MenuItem,
            this.Form3MenuItem,
            this.Form2MenuItem,
            this.Form1MenuItem,
            this.toolStripSeparator10,
            this.AllFormsMenuItem,
            this.searchToolStripMenuItem1});
            this.StudentMenu.Name = "StudentMenu";
            this.StudentMenu.Size = new System.Drawing.Size(64, 19);
            this.StudentMenu.Text = "&Student";
            this.StudentMenu.Click += new System.EventHandler(this.StudentMenu_Click);
            // 
            // AddStudentMenuItem
            // 
            this.AddStudentMenuItem.Image = global::MusBase.Properties.Resources.mypc_add_24;
            this.AddStudentMenuItem.Name = "AddStudentMenuItem";
            this.AddStudentMenuItem.Size = new System.Drawing.Size(169, 22);
            this.AddStudentMenuItem.Text = "&Add Student";
            this.AddStudentMenuItem.Click += new System.EventHandler(this.AddStudentMenuItem_Click);
            // 
            // RemoveStudentMenuItem
            // 
            this.RemoveStudentMenuItem.Image = global::MusBase.Properties.Resources.mypc_delete_24;
            this.RemoveStudentMenuItem.Name = "RemoveStudentMenuItem";
            this.RemoveStudentMenuItem.Size = new System.Drawing.Size(169, 22);
            this.RemoveStudentMenuItem.Text = "&Remove Student";
            this.RemoveStudentMenuItem.Click += new System.EventHandler(this.RemoveStudentMenuItem_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(166, 6);
            // 
            // Form4MenuItem
            // 
            this.Form4MenuItem.Name = "Form4MenuItem";
            this.Form4MenuItem.Size = new System.Drawing.Size(169, 22);
            this.Form4MenuItem.Text = "Form &4";
            // 
            // Form3MenuItem
            // 
            this.Form3MenuItem.Name = "Form3MenuItem";
            this.Form3MenuItem.Size = new System.Drawing.Size(169, 22);
            this.Form3MenuItem.Text = "Form &3";
            // 
            // Form2MenuItem
            // 
            this.Form2MenuItem.Name = "Form2MenuItem";
            this.Form2MenuItem.Size = new System.Drawing.Size(169, 22);
            this.Form2MenuItem.Text = "Form &2";
            // 
            // Form1MenuItem
            // 
            this.Form1MenuItem.Name = "Form1MenuItem";
            this.Form1MenuItem.Size = new System.Drawing.Size(169, 22);
            this.Form1MenuItem.Text = "Form &1";
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(166, 6);
            // 
            // AllFormsMenuItem
            // 
            this.AllFormsMenuItem.Image = global::MusBase.Properties.Resources.network_32;
            this.AllFormsMenuItem.Name = "AllFormsMenuItem";
            this.AllFormsMenuItem.Size = new System.Drawing.Size(169, 22);
            this.AllFormsMenuItem.Text = "&All Forms";
            // 
            // InstrumentMenu
            // 
            this.InstrumentMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AssignInstrumentMenuItem,
            this.RetrieveInstrumentMenuItem,
            this.toolStripSeparator12,
            this.AddInstrumentMenuItem,
            this.RemoveInstrumentMenuItem,
            this.toolStripSeparator11,
            this.BrassMenuItem,
            this.StringsMenuItem,
            this.WoodwindsMenuItem,
            this.PercussionMenuItem,
            this.toolStripSeparator16,
            this.AllInstrumentsMenuItem});
            this.InstrumentMenu.Name = "InstrumentMenu";
            this.InstrumentMenu.Size = new System.Drawing.Size(82, 19);
            this.InstrumentMenu.Text = "&Instrument";
            this.InstrumentMenu.Click += new System.EventHandler(this.InstrumentMenu_Click);
            // 
            // AssignInstrumentMenuItem
            // 
            this.AssignInstrumentMenuItem.Image = global::MusBase.Properties.Resources.folder_ok_24;
            this.AssignInstrumentMenuItem.Name = "AssignInstrumentMenuItem";
            this.AssignInstrumentMenuItem.Size = new System.Drawing.Size(189, 22);
            this.AssignInstrumentMenuItem.Text = "Assi&gn Instrument";
            this.AssignInstrumentMenuItem.Click += new System.EventHandler(this.AssignInstrumentMenuItem_Click);
            // 
            // RetrieveInstrumentMenuItem
            // 
            this.RetrieveInstrumentMenuItem.Image = global::MusBase.Properties.Resources.folder_delete_24;
            this.RetrieveInstrumentMenuItem.Name = "RetrieveInstrumentMenuItem";
            this.RetrieveInstrumentMenuItem.Size = new System.Drawing.Size(189, 22);
            this.RetrieveInstrumentMenuItem.Text = "Re&trieve Instrument";
            this.RetrieveInstrumentMenuItem.Click += new System.EventHandler(this.RetrieveInstrumentMenuItem_Click);
            // 
            // toolStripSeparator12
            // 
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            this.toolStripSeparator12.Size = new System.Drawing.Size(186, 6);
            // 
            // AddInstrumentMenuItem
            // 
            this.AddInstrumentMenuItem.Image = global::MusBase.Properties.Resources.recyclebin_add_24;
            this.AddInstrumentMenuItem.Name = "AddInstrumentMenuItem";
            this.AddInstrumentMenuItem.Size = new System.Drawing.Size(189, 22);
            this.AddInstrumentMenuItem.Text = "&Add Instrument";
            this.AddInstrumentMenuItem.Click += new System.EventHandler(this.AddInstrumentMenuItem_Click);
            // 
            // RemoveInstrumentMenuItem
            // 
            this.RemoveInstrumentMenuItem.Image = global::MusBase.Properties.Resources.recyclebin_delete_24;
            this.RemoveInstrumentMenuItem.Name = "RemoveInstrumentMenuItem";
            this.RemoveInstrumentMenuItem.Size = new System.Drawing.Size(189, 22);
            this.RemoveInstrumentMenuItem.Text = "&Remove Instrument";
            this.RemoveInstrumentMenuItem.Click += new System.EventHandler(this.RemoveInstrumentMenuItem_Click);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(186, 6);
            // 
            // BrassMenuItem
            // 
            this.BrassMenuItem.Image = global::MusBase.Properties.Resources.Trombone_32;
            this.BrassMenuItem.Name = "BrassMenuItem";
            this.BrassMenuItem.Size = new System.Drawing.Size(189, 22);
            this.BrassMenuItem.Text = "&Brass";
            // 
            // StringsMenuItem
            // 
            this.StringsMenuItem.Image = global::MusBase.Properties.Resources.Violin_32;
            this.StringsMenuItem.Name = "StringsMenuItem";
            this.StringsMenuItem.Size = new System.Drawing.Size(189, 22);
            this.StringsMenuItem.Text = "&Strings";
            // 
            // WoodwindsMenuItem
            // 
            this.WoodwindsMenuItem.Image = global::MusBase.Properties.Resources.Oboe_32;
            this.WoodwindsMenuItem.Name = "WoodwindsMenuItem";
            this.WoodwindsMenuItem.Size = new System.Drawing.Size(189, 22);
            this.WoodwindsMenuItem.Text = "&Woodwinds";
            // 
            // PercussionMenuItem
            // 
            this.PercussionMenuItem.Image = global::MusBase.Properties.Resources.drum;
            this.PercussionMenuItem.Name = "PercussionMenuItem";
            this.PercussionMenuItem.Size = new System.Drawing.Size(189, 22);
            this.PercussionMenuItem.Text = "&Percussion";
            // 
            // toolStripSeparator16
            // 
            this.toolStripSeparator16.Name = "toolStripSeparator16";
            this.toolStripSeparator16.Size = new System.Drawing.Size(186, 6);
            // 
            // AllInstrumentsMenuItem
            // 
            this.AllInstrumentsMenuItem.Image = global::MusBase.Properties.Resources.folder_32;
            this.AllInstrumentsMenuItem.Name = "AllInstrumentsMenuItem";
            this.AllInstrumentsMenuItem.Size = new System.Drawing.Size(189, 22);
            this.AllInstrumentsMenuItem.Text = "A&ll Instruments";
            // 
            // BooksMenuItem
            // 
            this.BooksMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AssignBookMenuItem,
            this.RetrieveBookMenuItem,
            this.toolStripSeparator17,
            this.AddBookMenuItem,
            this.RemoveBookMenuItem,
            this.toolStripSeparator18,
            this.AllBooksMenuItem});
            this.BooksMenuItem.Name = "BooksMenuItem";
            this.BooksMenuItem.Size = new System.Drawing.Size(53, 19);
            this.BooksMenuItem.Text = "&Books";
            this.BooksMenuItem.Click += new System.EventHandler(this.BooksMenuItem_Click);
            // 
            // AssignBookMenuItem
            // 
            this.AssignBookMenuItem.Image = global::MusBase.Properties.Resources.arpa_ok_48;
            this.AssignBookMenuItem.Name = "AssignBookMenuItem";
            this.AssignBookMenuItem.Size = new System.Drawing.Size(155, 22);
            this.AssignBookMenuItem.Text = "Assi&gn Book";
            this.AssignBookMenuItem.Click += new System.EventHandler(this.AssignBookMenuItem_Click);
            // 
            // RetrieveBookMenuItem
            // 
            this.RetrieveBookMenuItem.Image = global::MusBase.Properties.Resources.arpa_cancel_48;
            this.RetrieveBookMenuItem.Name = "RetrieveBookMenuItem";
            this.RetrieveBookMenuItem.Size = new System.Drawing.Size(155, 22);
            this.RetrieveBookMenuItem.Text = "Re&trieve Book";
            this.RetrieveBookMenuItem.Click += new System.EventHandler(this.RetrieveBookMenuItem_Click);
            // 
            // toolStripSeparator17
            // 
            this.toolStripSeparator17.Name = "toolStripSeparator17";
            this.toolStripSeparator17.Size = new System.Drawing.Size(152, 6);
            // 
            // AddBookMenuItem
            // 
            this.AddBookMenuItem.Image = global::MusBase.Properties.Resources.book_add_icon;
            this.AddBookMenuItem.Name = "AddBookMenuItem";
            this.AddBookMenuItem.Size = new System.Drawing.Size(155, 22);
            this.AddBookMenuItem.Text = "&Add Book";
            this.AddBookMenuItem.Click += new System.EventHandler(this.AddBookMenuItem_Click);
            // 
            // RemoveBookMenuItem
            // 
            this.RemoveBookMenuItem.Image = global::MusBase.Properties.Resources.book_delete1;
            this.RemoveBookMenuItem.Name = "RemoveBookMenuItem";
            this.RemoveBookMenuItem.Size = new System.Drawing.Size(155, 22);
            this.RemoveBookMenuItem.Text = "&Remove Book";
            this.RemoveBookMenuItem.Click += new System.EventHandler(this.RemoveBookMenuItem_Click);
            // 
            // toolStripSeparator18
            // 
            this.toolStripSeparator18.Name = "toolStripSeparator18";
            this.toolStripSeparator18.Size = new System.Drawing.Size(152, 6);
            // 
            // AllBooksMenuItem
            // 
            this.AllBooksMenuItem.Image = global::MusBase.Properties.Resources.Book_Blank_Book_icon;
            this.AllBooksMenuItem.Name = "AllBooksMenuItem";
            this.AllBooksMenuItem.Size = new System.Drawing.Size(155, 22);
            this.AllBooksMenuItem.Text = "A&ll Books";
            // 
            // AccessoriesMenu
            // 
            this.AccessoriesMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AssignAccessoryMenuItem,
            this.RetrieveAccessoryMenuItem,
            this.toolStripSeparator13,
            this.AddAccessoryMenuItem,
            this.RemoveAccessoryMenuItem,
            this.toolStripSeparator15,
            this.AllAccessoriesMenuItem});
            this.AccessoriesMenu.Name = "AccessoriesMenu";
            this.AccessoriesMenu.Size = new System.Drawing.Size(83, 19);
            this.AccessoriesMenu.Text = "&Accessories";
            // 
            // AssignAccessoryMenuItem
            // 
            this.AssignAccessoryMenuItem.Image = global::MusBase.Properties.Resources.mydocuments_ok_32;
            this.AssignAccessoryMenuItem.Name = "AssignAccessoryMenuItem";
            this.AssignAccessoryMenuItem.Size = new System.Drawing.Size(181, 22);
            this.AssignAccessoryMenuItem.Text = "Assi&gn Accessory";
            this.AssignAccessoryMenuItem.Click += new System.EventHandler(this.AssignAccessoryMenuItem_Click);
            // 
            // RetrieveAccessoryMenuItem
            // 
            this.RetrieveAccessoryMenuItem.Image = global::MusBase.Properties.Resources.mydocuments_delete_32;
            this.RetrieveAccessoryMenuItem.Name = "RetrieveAccessoryMenuItem";
            this.RetrieveAccessoryMenuItem.Size = new System.Drawing.Size(181, 22);
            this.RetrieveAccessoryMenuItem.Text = "Re&trieve Accessory";
            this.RetrieveAccessoryMenuItem.Click += new System.EventHandler(this.RetrieveAccessoryMenuItem_Click);
            // 
            // toolStripSeparator13
            // 
            this.toolStripSeparator13.Name = "toolStripSeparator13";
            this.toolStripSeparator13.Size = new System.Drawing.Size(178, 6);
            // 
            // AddAccessoryMenuItem
            // 
            this.AddAccessoryMenuItem.Image = global::MusBase.Properties.Resources.mydocuments_add_32;
            this.AddAccessoryMenuItem.Name = "AddAccessoryMenuItem";
            this.AddAccessoryMenuItem.Size = new System.Drawing.Size(181, 22);
            this.AddAccessoryMenuItem.Text = "&Add Accessory";
            this.AddAccessoryMenuItem.Click += new System.EventHandler(this.AddAccessoryMenuItem_Click);
            // 
            // RemoveAccessoryMenuItem
            // 
            this.RemoveAccessoryMenuItem.Image = global::MusBase.Properties.Resources.mydocuments_delete_32;
            this.RemoveAccessoryMenuItem.Name = "RemoveAccessoryMenuItem";
            this.RemoveAccessoryMenuItem.Size = new System.Drawing.Size(181, 22);
            this.RemoveAccessoryMenuItem.Text = "&Remove Accessory";
            this.RemoveAccessoryMenuItem.Click += new System.EventHandler(this.RemoveAccessoryMenuItem_Click);
            // 
            // toolStripSeparator15
            // 
            this.toolStripSeparator15.Name = "toolStripSeparator15";
            this.toolStripSeparator15.Size = new System.Drawing.Size(178, 6);
            // 
            // AllAccessoriesMenuItem
            // 
            this.AllAccessoriesMenuItem.Image = global::MusBase.Properties.Resources.mydocuments_32;
            this.AllAccessoriesMenuItem.Name = "AllAccessoriesMenuItem";
            this.AllAccessoriesMenuItem.Size = new System.Drawing.Size(181, 22);
            this.AllAccessoriesMenuItem.Text = "A&ll Accessories";
            // 
            // helpMenu
            // 
            this.helpMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contentsToolStripMenuItem,
            this.indexToolStripMenuItem,
            this.searchToolStripMenuItem,
            this.toolStripSeparator8,
            this.aboutToolStripMenuItem,
            this.changeBackgroundImageToolStripMenuItem});
            this.helpMenu.Name = "helpMenu";
            this.helpMenu.Size = new System.Drawing.Size(45, 19);
            this.helpMenu.Text = "&Help";
            // 
            // contentsToolStripMenuItem
            // 
            this.contentsToolStripMenuItem.Name = "contentsToolStripMenuItem";
            this.contentsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F1)));
            this.contentsToolStripMenuItem.Size = new System.Drawing.Size(223, 22);
            this.contentsToolStripMenuItem.Text = "&Contents";
            // 
            // indexToolStripMenuItem
            // 
            this.indexToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("indexToolStripMenuItem.Image")));
            this.indexToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black;
            this.indexToolStripMenuItem.Name = "indexToolStripMenuItem";
            this.indexToolStripMenuItem.Size = new System.Drawing.Size(223, 22);
            this.indexToolStripMenuItem.Text = "&Index";
            // 
            // searchToolStripMenuItem
            // 
            this.searchToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("searchToolStripMenuItem.Image")));
            this.searchToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black;
            this.searchToolStripMenuItem.Name = "searchToolStripMenuItem";
            this.searchToolStripMenuItem.Size = new System.Drawing.Size(223, 22);
            this.searchToolStripMenuItem.Text = "&Search";
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(220, 6);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(223, 22);
            this.aboutToolStripMenuItem.Text = "&About ... ...";
            // 
            // changeBackgroundImageToolStripMenuItem
            // 
            this.changeBackgroundImageToolStripMenuItem.Name = "changeBackgroundImageToolStripMenuItem";
            this.changeBackgroundImageToolStripMenuItem.Size = new System.Drawing.Size(223, 22);
            this.changeBackgroundImageToolStripMenuItem.Text = "Change Background Image";
            // 
            // menuStrip
            // 
            this.menuStrip.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.menuStrip.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BaseMenu,
            this.StudentMenu,
            this.InstrumentMenu,
            this.BooksMenuItem,
            this.AccessoriesMenu,
            this.reportsToolStripMenuItem,
            this.helpMenu});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Padding = new System.Windows.Forms.Padding(8, 3, 0, 3);
            this.menuStrip.Size = new System.Drawing.Size(761, 25);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "MenuStrip";
            this.menuStrip.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip_ItemClicked);
            // 
            // BaseMenu
            // 
            this.BaseMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gradeToolStripMenuItem,
            this.toolStripSeparator1,
            this.musBandToolStripMenuItem,
            this.musChoirToolStripMenuItem,
            this.toolStripSeparator2,
            this.addTypeToolStripMenuItem,
            this.manageUserToolStripMenuItem});
            this.BaseMenu.Name = "BaseMenu";
            this.BaseMenu.Size = new System.Drawing.Size(45, 19);
            this.BaseMenu.Text = "&Base";
            // 
            // gradeToolStripMenuItem
            // 
            this.gradeToolStripMenuItem.Name = "gradeToolStripMenuItem";
            this.gradeToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.gradeToolStripMenuItem.Text = "MusGrade";
            this.gradeToolStripMenuItem.Click += new System.EventHandler(this.gradeToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(144, 6);
            // 
            // musBandToolStripMenuItem
            // 
            this.musBandToolStripMenuItem.Name = "musBandToolStripMenuItem";
            this.musBandToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.musBandToolStripMenuItem.Text = "MusBand";
            this.musBandToolStripMenuItem.Click += new System.EventHandler(this.musBandToolStripMenuItem_Click);
            // 
            // musChoirToolStripMenuItem
            // 
            this.musChoirToolStripMenuItem.Name = "musChoirToolStripMenuItem";
            this.musChoirToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.musChoirToolStripMenuItem.Text = "MusChoir";
            this.musChoirToolStripMenuItem.Click += new System.EventHandler(this.musChoirToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(144, 6);
            // 
            // addTypeToolStripMenuItem
            // 
            this.addTypeToolStripMenuItem.Name = "addTypeToolStripMenuItem";
            this.addTypeToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.addTypeToolStripMenuItem.Text = "Manage Type";
            this.addTypeToolStripMenuItem.Click += new System.EventHandler(this.addTypeToolStripMenuItem_Click);
            // 
            // manageUserToolStripMenuItem
            // 
            this.manageUserToolStripMenuItem.Name = "manageUserToolStripMenuItem";
            this.manageUserToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.manageUserToolStripMenuItem.Text = "Manage User";
            // 
            // reportsToolStripMenuItem
            // 
            this.reportsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.assignedInstrumentsToolStripMenuItem,
            this.toolStripSeparator4,
            this.instrumentsToolStripMenuItem,
            this.toolStripMenuItem1,
            this.booksToolStripMenuItem,
            this.toolStripSeparator3,
            this.assignedInstrumentsToolStripMenuItem1,
            this.assignedBooksToolStripMenuItem,
            this.assignedAccessoriesToolStripMenuItem});
            this.reportsToolStripMenuItem.Name = "reportsToolStripMenuItem";
            this.reportsToolStripMenuItem.Size = new System.Drawing.Size(63, 19);
            this.reportsToolStripMenuItem.Text = "&Reports";
            this.reportsToolStripMenuItem.Click += new System.EventHandler(this.reportsToolStripMenuItem_Click);
            // 
            // assignedInstrumentsToolStripMenuItem
            // 
            this.assignedInstrumentsToolStripMenuItem.Image = global::MusBase.Properties.Resources.network_32;
            this.assignedInstrumentsToolStripMenuItem.Name = "assignedInstrumentsToolStripMenuItem";
            this.assignedInstrumentsToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.assignedInstrumentsToolStripMenuItem.Text = "Student Profile";
            this.assignedInstrumentsToolStripMenuItem.Click += new System.EventHandler(this.assignedInstrumentsToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(191, 6);
            // 
            // instrumentsToolStripMenuItem
            // 
            this.instrumentsToolStripMenuItem.Image = global::MusBase.Properties.Resources.folder_32;
            this.instrumentsToolStripMenuItem.Name = "instrumentsToolStripMenuItem";
            this.instrumentsToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.instrumentsToolStripMenuItem.Text = "Instruments";
            this.instrumentsToolStripMenuItem.Click += new System.EventHandler(this.instrumentsToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Image = global::MusBase.Properties.Resources.mydocuments_32;
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(194, 22);
            this.toolStripMenuItem1.Text = "Accessories";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // booksToolStripMenuItem
            // 
            this.booksToolStripMenuItem.Image = global::MusBase.Properties.Resources.Book_Blank_Book_icon;
            this.booksToolStripMenuItem.Name = "booksToolStripMenuItem";
            this.booksToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.booksToolStripMenuItem.Text = "Books";
            this.booksToolStripMenuItem.Click += new System.EventHandler(this.booksToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(191, 6);
            this.toolStripSeparator3.Click += new System.EventHandler(this.toolStripSeparator3_Click);
            // 
            // assignedInstrumentsToolStripMenuItem1
            // 
            this.assignedInstrumentsToolStripMenuItem1.Image = global::MusBase.Properties.Resources.folder_ok_48;
            this.assignedInstrumentsToolStripMenuItem1.Name = "assignedInstrumentsToolStripMenuItem1";
            this.assignedInstrumentsToolStripMenuItem1.Size = new System.Drawing.Size(194, 22);
            this.assignedInstrumentsToolStripMenuItem1.Text = "Assigned Instruments";
            this.assignedInstrumentsToolStripMenuItem1.Click += new System.EventHandler(this.assignedInstrumentsToolStripMenuItem1_Click);
            // 
            // assignedBooksToolStripMenuItem
            // 
            this.assignedBooksToolStripMenuItem.Image = global::MusBase.Properties.Resources.Book_Blank_Book_icon;
            this.assignedBooksToolStripMenuItem.Name = "assignedBooksToolStripMenuItem";
            this.assignedBooksToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.assignedBooksToolStripMenuItem.Text = "Assigned Books";
            this.assignedBooksToolStripMenuItem.Click += new System.EventHandler(this.assignedBooksToolStripMenuItem_Click);
            // 
            // assignedAccessoriesToolStripMenuItem
            // 
            this.assignedAccessoriesToolStripMenuItem.Image = global::MusBase.Properties.Resources.mydocuments_ok_32;
            this.assignedAccessoriesToolStripMenuItem.Name = "assignedAccessoriesToolStripMenuItem";
            this.assignedAccessoriesToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.assignedAccessoriesToolStripMenuItem.Text = "Assigned Accessories";
            this.assignedAccessoriesToolStripMenuItem.Click += new System.EventHandler(this.assignedAccessoriesToolStripMenuItem_Click);
            // 
            // gbxManageAccessories
            // 
            this.gbxManageAccessories.BackColor = System.Drawing.Color.Transparent;
            this.gbxManageAccessories.Controls.Add(this.btnRetriveInstrument);
            this.gbxManageAccessories.Controls.Add(this.btnAssignAccessories);
            this.gbxManageAccessories.Controls.Add(this.bntRemoveAccessory);
            this.gbxManageAccessories.Controls.Add(this.btnAddAccessory);
            this.gbxManageAccessories.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxManageAccessories.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.gbxManageAccessories.Location = new System.Drawing.Point(12, 247);
            this.gbxManageAccessories.Name = "gbxManageAccessories";
            this.gbxManageAccessories.Size = new System.Drawing.Size(322, 189);
            this.gbxManageAccessories.TabIndex = 9;
            this.gbxManageAccessories.TabStop = false;
            this.gbxManageAccessories.Text = "Manage Accessories";
            this.gbxManageAccessories.Enter += new System.EventHandler(this.gbxManageAccessories_Enter);
            // 
            // btnRetriveInstrument
            // 
            this.btnRetriveInstrument.BackColor = System.Drawing.Color.Purple;
            this.btnRetriveInstrument.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnRetriveInstrument.Font = new System.Drawing.Font("Tempus Sans ITC", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRetriveInstrument.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.btnRetriveInstrument.Image = global::MusBase.Properties.Resources.mydocuments_delete_48;
            this.btnRetriveInstrument.Location = new System.Drawing.Point(167, 113);
            this.btnRetriveInstrument.Name = "btnRetriveInstrument";
            this.btnRetriveInstrument.Size = new System.Drawing.Size(139, 57);
            this.btnRetriveInstrument.TabIndex = 3;
            this.btnRetriveInstrument.Text = "Retrieve Accessory";
            this.btnRetriveInstrument.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnRetriveInstrument.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnRetriveInstrument.UseVisualStyleBackColor = false;
            this.btnRetriveInstrument.Click += new System.EventHandler(this.btnRetriveInstrument_Click);
            // 
            // btnAssignAccessories
            // 
            this.btnAssignAccessories.BackColor = System.Drawing.Color.Purple;
            this.btnAssignAccessories.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAssignAccessories.Font = new System.Drawing.Font("Tempus Sans ITC", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAssignAccessories.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.btnAssignAccessories.Image = global::MusBase.Properties.Resources.mydocuments_ok_48;
            this.btnAssignAccessories.Location = new System.Drawing.Point(6, 113);
            this.btnAssignAccessories.Name = "btnAssignAccessories";
            this.btnAssignAccessories.Size = new System.Drawing.Size(139, 57);
            this.btnAssignAccessories.TabIndex = 2;
            this.btnAssignAccessories.Text = "Assign Accessory";
            this.btnAssignAccessories.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnAssignAccessories.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnAssignAccessories.UseVisualStyleBackColor = false;
            this.btnAssignAccessories.Click += new System.EventHandler(this.button4_Click);
            // 
            // bntRemoveAccessory
            // 
            this.bntRemoveAccessory.BackColor = System.Drawing.Color.Purple;
            this.bntRemoveAccessory.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.bntRemoveAccessory.Font = new System.Drawing.Font("Tempus Sans ITC", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bntRemoveAccessory.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.bntRemoveAccessory.Image = global::MusBase.Properties.Resources.mydocuments_delete_48;
            this.bntRemoveAccessory.Location = new System.Drawing.Point(167, 27);
            this.bntRemoveAccessory.Name = "bntRemoveAccessory";
            this.bntRemoveAccessory.Size = new System.Drawing.Size(139, 57);
            this.bntRemoveAccessory.TabIndex = 1;
            this.bntRemoveAccessory.Text = "Remove Accessory";
            this.bntRemoveAccessory.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.bntRemoveAccessory.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.bntRemoveAccessory.UseVisualStyleBackColor = false;
            this.bntRemoveAccessory.Click += new System.EventHandler(this.bntRemoveAccessory_Click);
            // 
            // btnAddAccessory
            // 
            this.btnAddAccessory.BackColor = System.Drawing.Color.Purple;
            this.btnAddAccessory.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAddAccessory.Font = new System.Drawing.Font("Tempus Sans ITC", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddAccessory.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.btnAddAccessory.Image = global::MusBase.Properties.Resources.mydocuments_add_48;
            this.btnAddAccessory.Location = new System.Drawing.Point(6, 27);
            this.btnAddAccessory.Name = "btnAddAccessory";
            this.btnAddAccessory.Size = new System.Drawing.Size(139, 57);
            this.btnAddAccessory.TabIndex = 0;
            this.btnAddAccessory.Text = "Add Accessory";
            this.btnAddAccessory.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnAddAccessory.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnAddAccessory.UseVisualStyleBackColor = false;
            this.btnAddAccessory.Click += new System.EventHandler(this.btnAddAccessory_Click);
            // 
            // gbxManageBooks
            // 
            this.gbxManageBooks.BackColor = System.Drawing.Color.Transparent;
            this.gbxManageBooks.Controls.Add(this.btnRetrieveBook);
            this.gbxManageBooks.Controls.Add(this.btnAssignBook);
            this.gbxManageBooks.Controls.Add(this.button2);
            this.gbxManageBooks.Controls.Add(this.btnAddBook);
            this.gbxManageBooks.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxManageBooks.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.gbxManageBooks.Location = new System.Drawing.Point(408, 40);
            this.gbxManageBooks.Name = "gbxManageBooks";
            this.gbxManageBooks.Size = new System.Drawing.Size(322, 189);
            this.gbxManageBooks.TabIndex = 8;
            this.gbxManageBooks.TabStop = false;
            this.gbxManageBooks.Text = "Manage Books";
            // 
            // btnRetrieveBook
            // 
            this.btnRetrieveBook.BackColor = System.Drawing.Color.Purple;
            this.btnRetrieveBook.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnRetrieveBook.Font = new System.Drawing.Font("Tempus Sans ITC", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRetrieveBook.Image = global::MusBase.Properties.Resources.arpa_cancel_48;
            this.btnRetrieveBook.Location = new System.Drawing.Point(163, 107);
            this.btnRetrieveBook.Name = "btnRetrieveBook";
            this.btnRetrieveBook.Size = new System.Drawing.Size(139, 57);
            this.btnRetrieveBook.TabIndex = 3;
            this.btnRetrieveBook.Text = "Retrieve Book";
            this.btnRetrieveBook.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnRetrieveBook.UseVisualStyleBackColor = false;
            this.btnRetrieveBook.Click += new System.EventHandler(this.btnRetrieveBook_Click);
            // 
            // btnAssignBook
            // 
            this.btnAssignBook.BackColor = System.Drawing.Color.Purple;
            this.btnAssignBook.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAssignBook.Font = new System.Drawing.Font("Tempus Sans ITC", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAssignBook.Image = global::MusBase.Properties.Resources.arpa_ok_48;
            this.btnAssignBook.Location = new System.Drawing.Point(6, 107);
            this.btnAssignBook.Name = "btnAssignBook";
            this.btnAssignBook.Size = new System.Drawing.Size(139, 57);
            this.btnAssignBook.TabIndex = 2;
            this.btnAssignBook.Text = "Assign Book";
            this.btnAssignBook.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnAssignBook.UseVisualStyleBackColor = false;
            this.btnAssignBook.Click += new System.EventHandler(this.btnAssignBook_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Purple;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button2.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Image = global::MusBase.Properties.Resources.book_delete1;
            this.button2.Location = new System.Drawing.Point(163, 27);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(139, 57);
            this.button2.TabIndex = 1;
            this.button2.Text = "Remove Book";
            this.button2.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnAddBook
            // 
            this.btnAddBook.BackColor = System.Drawing.Color.Purple;
            this.btnAddBook.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAddBook.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddBook.Image = global::MusBase.Properties.Resources.book_add_icon;
            this.btnAddBook.Location = new System.Drawing.Point(6, 27);
            this.btnAddBook.Name = "btnAddBook";
            this.btnAddBook.Size = new System.Drawing.Size(139, 57);
            this.btnAddBook.TabIndex = 0;
            this.btnAddBook.Text = "Add Book";
            this.btnAddBook.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnAddBook.UseVisualStyleBackColor = false;
            this.btnAddBook.Click += new System.EventHandler(this.btnAddBook_Click);
            // 
            // gboxManageStudents
            // 
            this.gboxManageStudents.BackColor = System.Drawing.Color.Transparent;
            this.gboxManageStudents.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.gboxManageStudents.Controls.Add(this.btnRemoveStudent);
            this.gboxManageStudents.Controls.Add(this.btnAddStudent);
            this.gboxManageStudents.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gboxManageStudents.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.gboxManageStudents.Location = new System.Drawing.Point(408, 247);
            this.gboxManageStudents.Name = "gboxManageStudents";
            this.gboxManageStudents.Size = new System.Drawing.Size(322, 111);
            this.gboxManageStudents.TabIndex = 7;
            this.gboxManageStudents.TabStop = false;
            this.gboxManageStudents.Text = "Manage Students";
            this.gboxManageStudents.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // btnRemoveStudent
            // 
            this.btnRemoveStudent.BackColor = System.Drawing.Color.Purple;
            this.btnRemoveStudent.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnRemoveStudent.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemoveStudent.Image = global::MusBase.Properties.Resources.mypc_delete_48;
            this.btnRemoveStudent.Location = new System.Drawing.Point(167, 27);
            this.btnRemoveStudent.Name = "btnRemoveStudent";
            this.btnRemoveStudent.Size = new System.Drawing.Size(139, 57);
            this.btnRemoveStudent.TabIndex = 1;
            this.btnRemoveStudent.Text = "Remove Student";
            this.btnRemoveStudent.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnRemoveStudent.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnRemoveStudent.UseVisualStyleBackColor = false;
            this.btnRemoveStudent.Click += new System.EventHandler(this.btnRemoveStudent_Click);
            // 
            // btnAddStudent
            // 
            this.btnAddStudent.BackColor = System.Drawing.Color.Purple;
            this.btnAddStudent.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAddStudent.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddStudent.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.btnAddStudent.Image = global::MusBase.Properties.Resources.mypc_add_48;
            this.btnAddStudent.Location = new System.Drawing.Point(6, 27);
            this.btnAddStudent.Name = "btnAddStudent";
            this.btnAddStudent.Size = new System.Drawing.Size(139, 57);
            this.btnAddStudent.TabIndex = 0;
            this.btnAddStudent.Text = "Add Student";
            this.btnAddStudent.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnAddStudent.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnAddStudent.UseVisualStyleBackColor = false;
            this.btnAddStudent.Click += new System.EventHandler(this.btnAddStudent_Click);
            // 
            // gbxManageInstruments
            // 
            this.gbxManageInstruments.BackColor = System.Drawing.Color.Transparent;
            this.gbxManageInstruments.Controls.Add(this.button3);
            this.gbxManageInstruments.Controls.Add(this.button1);
            this.gbxManageInstruments.Controls.Add(this.btnAddInstrument);
            this.gbxManageInstruments.Controls.Add(this.btnRemoveInstrument);
            this.gbxManageInstruments.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxManageInstruments.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.gbxManageInstruments.Location = new System.Drawing.Point(12, 40);
            this.gbxManageInstruments.Name = "gbxManageInstruments";
            this.gbxManageInstruments.Size = new System.Drawing.Size(322, 189);
            this.gbxManageInstruments.TabIndex = 6;
            this.gbxManageInstruments.TabStop = false;
            this.gbxManageInstruments.Text = "Manage Instruments";
            this.gbxManageInstruments.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Purple;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button3.Font = new System.Drawing.Font("Tempus Sans ITC", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Image = global::MusBase.Properties.Resources.violin_cancel_48;
            this.button3.Location = new System.Drawing.Point(167, 107);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(139, 57);
            this.button3.TabIndex = 3;
            this.button3.Text = "Retrieve Instrument";
            this.button3.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button3.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Purple;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Font = new System.Drawing.Font("Tempus Sans ITC", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Image = global::MusBase.Properties.Resources.violin_ok_48;
            this.button1.Location = new System.Drawing.Point(6, 107);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(139, 57);
            this.button1.TabIndex = 2;
            this.button1.Text = "Assign Instrument";
            this.button1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnAddInstrument
            // 
            this.btnAddInstrument.BackColor = System.Drawing.Color.Purple;
            this.btnAddInstrument.FlatAppearance.BorderSize = 0;
            this.btnAddInstrument.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAddInstrument.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddInstrument.Image = global::MusBase.Properties.Resources.Trombone_add_48;
            this.btnAddInstrument.Location = new System.Drawing.Point(6, 27);
            this.btnAddInstrument.Name = "btnAddInstrument";
            this.btnAddInstrument.Size = new System.Drawing.Size(139, 57);
            this.btnAddInstrument.TabIndex = 0;
            this.btnAddInstrument.Text = "Add Instrument";
            this.btnAddInstrument.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnAddInstrument.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnAddInstrument.UseVisualStyleBackColor = false;
            this.btnAddInstrument.Click += new System.EventHandler(this.btnAddInstrument_Click);
            // 
            // btnRemoveInstrument
            // 
            this.btnRemoveInstrument.BackColor = System.Drawing.Color.Purple;
            this.btnRemoveInstrument.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnRemoveInstrument.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemoveInstrument.Image = global::MusBase.Properties.Resources.Trombone_delete_48;
            this.btnRemoveInstrument.Location = new System.Drawing.Point(167, 27);
            this.btnRemoveInstrument.Name = "btnRemoveInstrument";
            this.btnRemoveInstrument.Size = new System.Drawing.Size(139, 57);
            this.btnRemoveInstrument.TabIndex = 1;
            this.btnRemoveInstrument.Text = "Remove Instrument";
            this.btnRemoveInstrument.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnRemoveInstrument.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnRemoveInstrument.UseVisualStyleBackColor = false;
            this.btnRemoveInstrument.Click += new System.EventHandler(this.btnRemoveInstrument_Click);
            // 
            // btnExitMain
            // 
            this.btnExitMain.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnExitMain.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnExitMain.Font = new System.Drawing.Font("Monotype Corsiva", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExitMain.ForeColor = System.Drawing.Color.DarkRed;
            this.btnExitMain.Image = global::MusBase.Properties.Resources.cross_icon__1_;
            this.btnExitMain.Location = new System.Drawing.Point(575, 377);
            this.btnExitMain.Name = "btnExitMain";
            this.btnExitMain.Size = new System.Drawing.Size(105, 55);
            this.btnExitMain.TabIndex = 10;
            this.btnExitMain.Text = "E&xit";
            this.btnExitMain.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnExitMain.UseVisualStyleBackColor = false;
            this.btnExitMain.Click += new System.EventHandler(this.btnExitMain_Click);
            // 
            // searchToolStripMenuItem1
            // 
            this.searchToolStripMenuItem1.Name = "searchToolStripMenuItem1";
            this.searchToolStripMenuItem1.Size = new System.Drawing.Size(169, 22);
            this.searchToolStripMenuItem1.Text = "Search";
            this.searchToolStripMenuItem1.Click += new System.EventHandler(this.searchToolStripMenuItem1_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.BackgroundImage = global::MusBase.Properties.Resources.equalizer_2_wallpaper_1366x768;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(761, 482);
            this.Controls.Add(this.btnExitMain);
            this.Controls.Add(this.gbxManageAccessories);
            this.Controls.Add(this.gbxManageBooks);
            this.Controls.Add(this.gboxManageStudents);
            this.Controls.Add(this.gbxManageInstruments);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.menuStrip);
            this.Font = new System.Drawing.Font("Monotype Corsiva", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.RoyalBlue;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MusBase";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.gbxManageAccessories.ResumeLayout(false);
            this.gbxManageBooks.ResumeLayout(false);
            this.gboxManageStudents.ResumeLayout(false);
            this.gbxManageInstruments.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.ToolStripMenuItem StudentMenu;
        private System.Windows.Forms.ToolStripMenuItem AddStudentMenuItem;
        private System.Windows.Forms.ToolStripMenuItem RemoveStudentMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripMenuItem Form4MenuItem;
        private System.Windows.Forms.ToolStripMenuItem Form3MenuItem;
        private System.Windows.Forms.ToolStripMenuItem Form2MenuItem;
        private System.Windows.Forms.ToolStripMenuItem Form1MenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripMenuItem AllFormsMenuItem;
        private System.Windows.Forms.ToolStripMenuItem InstrumentMenu;
        private System.Windows.Forms.ToolStripMenuItem AssignInstrumentMenuItem;
        private System.Windows.Forms.ToolStripMenuItem RetrieveInstrumentMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
        private System.Windows.Forms.ToolStripMenuItem AddInstrumentMenuItem;
        private System.Windows.Forms.ToolStripMenuItem RemoveInstrumentMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripMenuItem BrassMenuItem;
        private System.Windows.Forms.ToolStripMenuItem StringsMenuItem;
        private System.Windows.Forms.ToolStripMenuItem WoodwindsMenuItem;
        private System.Windows.Forms.ToolStripMenuItem PercussionMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator16;
        private System.Windows.Forms.ToolStripMenuItem AllInstrumentsMenuItem;
        private System.Windows.Forms.ToolStripMenuItem BooksMenuItem;
        private System.Windows.Forms.ToolStripMenuItem AssignBookMenuItem;
        private System.Windows.Forms.ToolStripMenuItem RetrieveBookMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator17;
        private System.Windows.Forms.ToolStripMenuItem AddBookMenuItem;
        private System.Windows.Forms.ToolStripMenuItem RemoveBookMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator18;
        private System.Windows.Forms.ToolStripMenuItem AllBooksMenuItem;
        private System.Windows.Forms.ToolStripMenuItem AccessoriesMenu;
        private System.Windows.Forms.ToolStripMenuItem AssignAccessoryMenuItem;
        private System.Windows.Forms.ToolStripMenuItem RetrieveAccessoryMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator13;
        private System.Windows.Forms.ToolStripMenuItem AddAccessoryMenuItem;
        private System.Windows.Forms.ToolStripMenuItem RemoveAccessoryMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator15;
        private System.Windows.Forms.ToolStripMenuItem AllAccessoriesMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpMenu;
        private System.Windows.Forms.ToolStripMenuItem contentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem indexToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changeBackgroundImageToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.GroupBox gbxManageAccessories;
        private System.Windows.Forms.Button bntRemoveAccessory;
        private System.Windows.Forms.Button btnAddAccessory;
        private System.Windows.Forms.GroupBox gbxManageBooks;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnAddBook;
        private System.Windows.Forms.GroupBox gboxManageStudents;
        private System.Windows.Forms.Button btnRemoveStudent;
        private System.Windows.Forms.Button btnAddStudent;
        private System.Windows.Forms.GroupBox gbxManageInstruments;
        private System.Windows.Forms.Button btnAddInstrument;
        private System.Windows.Forms.Button btnRemoveInstrument;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnRetrieveBook;
        private System.Windows.Forms.Button btnAssignBook;
        private System.Windows.Forms.Button btnRetriveInstrument;
        private System.Windows.Forms.Button btnAssignAccessories;
        private System.Windows.Forms.Button btnExitMain;
        private System.Windows.Forms.ToolStripMenuItem BaseMenu;
        private System.Windows.Forms.ToolStripMenuItem reportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem assignedInstrumentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem instrumentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem booksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gradeToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem musBandToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem musChoirToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem addTypeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manageUserToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel UsertoolStripStatusLabel;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem assignedInstrumentsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem assignedBooksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem assignedAccessoriesToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem searchToolStripMenuItem1;
    }
}



