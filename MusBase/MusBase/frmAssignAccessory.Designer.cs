﻿namespace MusBase
{
    partial class frmAssignAccessory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAssignAccessory));
            this.dgvAssignAccessory = new System.Windows.Forms.DataGridView();
            this.lblFName = new System.Windows.Forms.Label();
            this.gbxAsgAccSelectAcc = new System.Windows.Forms.GroupBox();
            this.cboAsgAccCode = new System.Windows.Forms.ComboBox();
            this.txtAsgAccType = new System.Windows.Forms.TextBox();
            this.txtAsgAccSize = new System.Windows.Forms.TextBox();
            this.txtAsgAccCondition = new System.Windows.Forms.TextBox();
            this.lblAsgAccCode = new System.Windows.Forms.Label();
            this.lblAsgAccSize = new System.Windows.Forms.Label();
            this.lblAsgAccType = new System.Windows.Forms.Label();
            this.lblAsgAccCondition = new System.Windows.Forms.Label();
            this.txtAsgAccName = new System.Windows.Forms.TextBox();
            this.lblAsgAccName = new System.Windows.Forms.Label();
            this.btnAsgAccSubmitAcc = new System.Windows.Forms.Button();
            this.gbxAsgAccSelectstd = new System.Windows.Forms.GroupBox();
            this.txtAsgAccForm = new System.Windows.Forms.TextBox();
            this.txtAsgAccHouse = new System.Windows.Forms.TextBox();
            this.cboAsgAccAdminNo = new System.Windows.Forms.ComboBox();
            this.lblAsgAccHouse = new System.Windows.Forms.Label();
            this.txtAsgAccMName = new System.Windows.Forms.TextBox();
            this.lblAsgAccAdminNo = new System.Windows.Forms.Label();
            this.lblAsgAccMName = new System.Windows.Forms.Label();
            this.txtAsgAccLName = new System.Windows.Forms.TextBox();
            this.lblAsgAccForm = new System.Windows.Forms.Label();
            this.txtAsgAccFName = new System.Windows.Forms.TextBox();
            this.lblAsgAccLName = new System.Windows.Forms.Label();
            this.lblAsgAccFName = new System.Windows.Forms.Label();
            this.btnAsgAccNext = new System.Windows.Forms.Button();
            this.btnNewAsgAcc = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnAsgAccDone = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAssignAccessory)).BeginInit();
            this.gbxAsgAccSelectAcc.SuspendLayout();
            this.gbxAsgAccSelectstd.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvAssignAccessory
            // 
            this.dgvAssignAccessory.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvAssignAccessory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAssignAccessory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAssignAccessory.Location = new System.Drawing.Point(0, 0);
            this.dgvAssignAccessory.Name = "dgvAssignAccessory";
            this.dgvAssignAccessory.ReadOnly = true;
            this.dgvAssignAccessory.Size = new System.Drawing.Size(821, 225);
            this.dgvAssignAccessory.TabIndex = 153;
            // 
            // lblFName
            // 
            this.lblFName.AutoSize = true;
            this.lblFName.Location = new System.Drawing.Point(32, 59);
            this.lblFName.Name = "lblFName";
            this.lblFName.Size = new System.Drawing.Size(0, 13);
            this.lblFName.TabIndex = 157;
            // 
            // gbxAsgAccSelectAcc
            // 
            this.gbxAsgAccSelectAcc.Controls.Add(this.cboAsgAccCode);
            this.gbxAsgAccSelectAcc.Controls.Add(this.txtAsgAccType);
            this.gbxAsgAccSelectAcc.Controls.Add(this.txtAsgAccSize);
            this.gbxAsgAccSelectAcc.Controls.Add(this.txtAsgAccCondition);
            this.gbxAsgAccSelectAcc.Controls.Add(this.lblAsgAccCode);
            this.gbxAsgAccSelectAcc.Controls.Add(this.lblAsgAccSize);
            this.gbxAsgAccSelectAcc.Controls.Add(this.lblAsgAccType);
            this.gbxAsgAccSelectAcc.Controls.Add(this.lblAsgAccCondition);
            this.gbxAsgAccSelectAcc.Controls.Add(this.txtAsgAccName);
            this.gbxAsgAccSelectAcc.Controls.Add(this.lblAsgAccName);
            this.gbxAsgAccSelectAcc.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxAsgAccSelectAcc.Location = new System.Drawing.Point(535, 12);
            this.gbxAsgAccSelectAcc.Name = "gbxAsgAccSelectAcc";
            this.gbxAsgAccSelectAcc.Size = new System.Drawing.Size(291, 312);
            this.gbxAsgAccSelectAcc.TabIndex = 158;
            this.gbxAsgAccSelectAcc.TabStop = false;
            this.gbxAsgAccSelectAcc.Text = "Select an Accessory";
            this.gbxAsgAccSelectAcc.Enter += new System.EventHandler(this.gbxAsgAccSelectAcc_Enter_1);
            // 
            // cboAsgAccCode
            // 
            this.cboAsgAccCode.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboAsgAccCode.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboAsgAccCode.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cboAsgAccCode.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboAsgAccCode.FormattingEnabled = true;
            this.cboAsgAccCode.Location = new System.Drawing.Point(121, 20);
            this.cboAsgAccCode.Name = "cboAsgAccCode";
            this.cboAsgAccCode.Size = new System.Drawing.Size(121, 25);
            this.cboAsgAccCode.TabIndex = 3;
            this.cboAsgAccCode.SelectedIndexChanged += new System.EventHandler(this.cboAsgAccCode_SelectedIndexChanged);
            // 
            // txtAsgAccType
            // 
            this.txtAsgAccType.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAsgAccType.Location = new System.Drawing.Point(121, 58);
            this.txtAsgAccType.Name = "txtAsgAccType";
            this.txtAsgAccType.ReadOnly = true;
            this.txtAsgAccType.Size = new System.Drawing.Size(154, 24);
            this.txtAsgAccType.TabIndex = 185;
            // 
            // txtAsgAccSize
            // 
            this.txtAsgAccSize.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAsgAccSize.Location = new System.Drawing.Point(121, 172);
            this.txtAsgAccSize.Name = "txtAsgAccSize";
            this.txtAsgAccSize.ReadOnly = true;
            this.txtAsgAccSize.Size = new System.Drawing.Size(56, 24);
            this.txtAsgAccSize.TabIndex = 181;
            // 
            // txtAsgAccCondition
            // 
            this.txtAsgAccCondition.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAsgAccCondition.Location = new System.Drawing.Point(121, 135);
            this.txtAsgAccCondition.Name = "txtAsgAccCondition";
            this.txtAsgAccCondition.ReadOnly = true;
            this.txtAsgAccCondition.Size = new System.Drawing.Size(154, 24);
            this.txtAsgAccCondition.TabIndex = 180;
            // 
            // lblAsgAccCode
            // 
            this.lblAsgAccCode.AutoSize = true;
            this.lblAsgAccCode.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAsgAccCode.Location = new System.Drawing.Point(6, 21);
            this.lblAsgAccCode.Name = "lblAsgAccCode";
            this.lblAsgAccCode.Size = new System.Drawing.Size(109, 17);
            this.lblAsgAccCode.TabIndex = 174;
            this.lblAsgAccCode.Text = "Accessory Code";
            // 
            // lblAsgAccSize
            // 
            this.lblAsgAccSize.AutoSize = true;
            this.lblAsgAccSize.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAsgAccSize.Location = new System.Drawing.Point(81, 175);
            this.lblAsgAccSize.Name = "lblAsgAccSize";
            this.lblAsgAccSize.Size = new System.Drawing.Size(32, 17);
            this.lblAsgAccSize.TabIndex = 179;
            this.lblAsgAccSize.Text = "Size";
            // 
            // lblAsgAccType
            // 
            this.lblAsgAccType.AutoSize = true;
            this.lblAsgAccType.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAsgAccType.Location = new System.Drawing.Point(6, 57);
            this.lblAsgAccType.Name = "lblAsgAccType";
            this.lblAsgAccType.Size = new System.Drawing.Size(107, 17);
            this.lblAsgAccType.TabIndex = 176;
            this.lblAsgAccType.Text = "Accessory Type";
            // 
            // lblAsgAccCondition
            // 
            this.lblAsgAccCondition.AutoSize = true;
            this.lblAsgAccCondition.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAsgAccCondition.Location = new System.Drawing.Point(40, 135);
            this.lblAsgAccCondition.Name = "lblAsgAccCondition";
            this.lblAsgAccCondition.Size = new System.Drawing.Size(73, 17);
            this.lblAsgAccCondition.TabIndex = 175;
            this.lblAsgAccCondition.Text = "Condition";
            // 
            // txtAsgAccName
            // 
            this.txtAsgAccName.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAsgAccName.Location = new System.Drawing.Point(121, 96);
            this.txtAsgAccName.Name = "txtAsgAccName";
            this.txtAsgAccName.ReadOnly = true;
            this.txtAsgAccName.Size = new System.Drawing.Size(154, 24);
            this.txtAsgAccName.TabIndex = 173;
            // 
            // lblAsgAccName
            // 
            this.lblAsgAccName.AutoSize = true;
            this.lblAsgAccName.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAsgAccName.Location = new System.Drawing.Point(67, 96);
            this.lblAsgAccName.Name = "lblAsgAccName";
            this.lblAsgAccName.Size = new System.Drawing.Size(46, 17);
            this.lblAsgAccName.TabIndex = 172;
            this.lblAsgAccName.Text = "Name";
            // 
            // btnAsgAccSubmitAcc
            // 
            this.btnAsgAccSubmitAcc.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAsgAccSubmitAcc.Image = global::MusBase.Properties.Resources.Ocarina_save_32;
            this.btnAsgAccSubmitAcc.Location = new System.Drawing.Point(387, 90);
            this.btnAsgAccSubmitAcc.Name = "btnAsgAccSubmitAcc";
            this.btnAsgAccSubmitAcc.Size = new System.Drawing.Size(104, 42);
            this.btnAsgAccSubmitAcc.TabIndex = 4;
            this.btnAsgAccSubmitAcc.Text = "&Submit";
            this.btnAsgAccSubmitAcc.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnAsgAccSubmitAcc.UseVisualStyleBackColor = true;
            this.btnAsgAccSubmitAcc.Click += new System.EventHandler(this.btnAsgAccSubmitAcc_Click_1);
            // 
            // gbxAsgAccSelectstd
            // 
            this.gbxAsgAccSelectstd.Controls.Add(this.txtAsgAccForm);
            this.gbxAsgAccSelectstd.Controls.Add(this.txtAsgAccHouse);
            this.gbxAsgAccSelectstd.Controls.Add(this.cboAsgAccAdminNo);
            this.gbxAsgAccSelectstd.Controls.Add(this.lblAsgAccHouse);
            this.gbxAsgAccSelectstd.Controls.Add(this.txtAsgAccMName);
            this.gbxAsgAccSelectstd.Controls.Add(this.lblAsgAccAdminNo);
            this.gbxAsgAccSelectstd.Controls.Add(this.lblAsgAccMName);
            this.gbxAsgAccSelectstd.Controls.Add(this.txtAsgAccLName);
            this.gbxAsgAccSelectstd.Controls.Add(this.lblAsgAccForm);
            this.gbxAsgAccSelectstd.Controls.Add(this.txtAsgAccFName);
            this.gbxAsgAccSelectstd.Controls.Add(this.lblAsgAccLName);
            this.gbxAsgAccSelectstd.Controls.Add(this.lblAsgAccFName);
            this.gbxAsgAccSelectstd.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxAsgAccSelectstd.Location = new System.Drawing.Point(64, 12);
            this.gbxAsgAccSelectstd.Name = "gbxAsgAccSelectstd";
            this.gbxAsgAccSelectstd.Size = new System.Drawing.Size(273, 312);
            this.gbxAsgAccSelectstd.TabIndex = 0;
            this.gbxAsgAccSelectstd.TabStop = false;
            this.gbxAsgAccSelectstd.Text = "Select a Student";
            // 
            // txtAsgAccForm
            // 
            this.txtAsgAccForm.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAsgAccForm.Location = new System.Drawing.Point(106, 215);
            this.txtAsgAccForm.Name = "txtAsgAccForm";
            this.txtAsgAccForm.ReadOnly = true;
            this.txtAsgAccForm.Size = new System.Drawing.Size(60, 24);
            this.txtAsgAccForm.TabIndex = 188;
            // 
            // txtAsgAccHouse
            // 
            this.txtAsgAccHouse.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAsgAccHouse.Location = new System.Drawing.Point(106, 175);
            this.txtAsgAccHouse.Name = "txtAsgAccHouse";
            this.txtAsgAccHouse.ReadOnly = true;
            this.txtAsgAccHouse.Size = new System.Drawing.Size(121, 24);
            this.txtAsgAccHouse.TabIndex = 166;
            // 
            // cboAsgAccAdminNo
            // 
            this.cboAsgAccAdminNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboAsgAccAdminNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboAsgAccAdminNo.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cboAsgAccAdminNo.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboAsgAccAdminNo.FormattingEnabled = true;
            this.cboAsgAccAdminNo.Location = new System.Drawing.Point(106, 20);
            this.cboAsgAccAdminNo.Name = "cboAsgAccAdminNo";
            this.cboAsgAccAdminNo.Size = new System.Drawing.Size(121, 25);
            this.cboAsgAccAdminNo.TabIndex = 1;
            this.cboAsgAccAdminNo.SelectedIndexChanged += new System.EventHandler(this.cboAsgAccAdminNo_SelectedIndexChanged);
            // 
            // lblAsgAccHouse
            // 
            this.lblAsgAccHouse.AutoSize = true;
            this.lblAsgAccHouse.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAsgAccHouse.Location = new System.Drawing.Point(34, 175);
            this.lblAsgAccHouse.Name = "lblAsgAccHouse";
            this.lblAsgAccHouse.Size = new System.Drawing.Size(48, 17);
            this.lblAsgAccHouse.TabIndex = 162;
            this.lblAsgAccHouse.Text = "House";
            // 
            // txtAsgAccMName
            // 
            this.txtAsgAccMName.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAsgAccMName.Location = new System.Drawing.Point(106, 96);
            this.txtAsgAccMName.Name = "txtAsgAccMName";
            this.txtAsgAccMName.ReadOnly = true;
            this.txtAsgAccMName.Size = new System.Drawing.Size(121, 24);
            this.txtAsgAccMName.TabIndex = 160;
            // 
            // lblAsgAccAdminNo
            // 
            this.lblAsgAccAdminNo.AutoSize = true;
            this.lblAsgAccAdminNo.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAsgAccAdminNo.Location = new System.Drawing.Point(6, 20);
            this.lblAsgAccAdminNo.Name = "lblAsgAccAdminNo";
            this.lblAsgAccAdminNo.Size = new System.Drawing.Size(76, 17);
            this.lblAsgAccAdminNo.TabIndex = 161;
            this.lblAsgAccAdminNo.Text = "Admin No";
            // 
            // lblAsgAccMName
            // 
            this.lblAsgAccMName.AutoSize = true;
            this.lblAsgAccMName.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAsgAccMName.Location = new System.Drawing.Point(4, 96);
            this.lblAsgAccMName.Name = "lblAsgAccMName";
            this.lblAsgAccMName.Size = new System.Drawing.Size(94, 17);
            this.lblAsgAccMName.TabIndex = 159;
            this.lblAsgAccMName.Text = "Middle Name";
            // 
            // txtAsgAccLName
            // 
            this.txtAsgAccLName.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAsgAccLName.Location = new System.Drawing.Point(106, 135);
            this.txtAsgAccLName.Name = "txtAsgAccLName";
            this.txtAsgAccLName.ReadOnly = true;
            this.txtAsgAccLName.Size = new System.Drawing.Size(121, 24);
            this.txtAsgAccLName.TabIndex = 158;
            // 
            // lblAsgAccForm
            // 
            this.lblAsgAccForm.AutoSize = true;
            this.lblAsgAccForm.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAsgAccForm.Location = new System.Drawing.Point(40, 215);
            this.lblAsgAccForm.Name = "lblAsgAccForm";
            this.lblAsgAccForm.Size = new System.Drawing.Size(42, 17);
            this.lblAsgAccForm.TabIndex = 163;
            this.lblAsgAccForm.Text = "Form";
            // 
            // txtAsgAccFName
            // 
            this.txtAsgAccFName.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAsgAccFName.Location = new System.Drawing.Point(106, 57);
            this.txtAsgAccFName.Name = "txtAsgAccFName";
            this.txtAsgAccFName.ReadOnly = true;
            this.txtAsgAccFName.Size = new System.Drawing.Size(121, 24);
            this.txtAsgAccFName.TabIndex = 157;
            // 
            // lblAsgAccLName
            // 
            this.lblAsgAccLName.AutoSize = true;
            this.lblAsgAccLName.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAsgAccLName.Location = new System.Drawing.Point(7, 135);
            this.lblAsgAccLName.Name = "lblAsgAccLName";
            this.lblAsgAccLName.Size = new System.Drawing.Size(75, 17);
            this.lblAsgAccLName.TabIndex = 156;
            this.lblAsgAccLName.Text = "Last Name";
            // 
            // lblAsgAccFName
            // 
            this.lblAsgAccFName.AutoSize = true;
            this.lblAsgAccFName.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAsgAccFName.Location = new System.Drawing.Point(4, 58);
            this.lblAsgAccFName.Name = "lblAsgAccFName";
            this.lblAsgAccFName.Size = new System.Drawing.Size(78, 17);
            this.lblAsgAccFName.TabIndex = 155;
            this.lblAsgAccFName.Text = "First Name";
            // 
            // btnAsgAccNext
            // 
            this.btnAsgAccNext.BackgroundImage = global::MusBase.Properties.Resources.next;
            this.btnAsgAccNext.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnAsgAccNext.Font = new System.Drawing.Font("Tempus Sans ITC", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAsgAccNext.Location = new System.Drawing.Point(401, 32);
            this.btnAsgAccNext.Name = "btnAsgAccNext";
            this.btnAsgAccNext.Size = new System.Drawing.Size(75, 36);
            this.btnAsgAccNext.TabIndex = 2;
            this.btnAsgAccNext.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnAsgAccNext.UseVisualStyleBackColor = true;
            this.btnAsgAccNext.Click += new System.EventHandler(this.btnAsgAccNext_Click);
            // 
            // btnNewAsgAcc
            // 
            this.btnNewAsgAcc.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNewAsgAcc.Image = global::MusBase.Properties.Resources.Ocarina_add_32;
            this.btnNewAsgAcc.Location = new System.Drawing.Point(387, 249);
            this.btnNewAsgAcc.Name = "btnNewAsgAcc";
            this.btnNewAsgAcc.Size = new System.Drawing.Size(104, 42);
            this.btnNewAsgAcc.TabIndex = 5;
            this.btnNewAsgAcc.Text = "&NEW";
            this.btnNewAsgAcc.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnNewAsgAcc.UseVisualStyleBackColor = true;
            this.btnNewAsgAcc.Click += new System.EventHandler(this.btnNewAsgAcc_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dgvAssignAccessory);
            this.panel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(35, 353);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(821, 225);
            this.panel1.TabIndex = 193;
            // 
            // btnAsgAccDone
            // 
            this.btnAsgAccDone.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAsgAccDone.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.btnAsgAccDone.Image = global::MusBase.Properties.Resources._1427990866_Done;
            this.btnAsgAccDone.Location = new System.Drawing.Point(387, 187);
            this.btnAsgAccDone.Name = "btnAsgAccDone";
            this.btnAsgAccDone.Size = new System.Drawing.Size(104, 44);
            this.btnAsgAccDone.TabIndex = 194;
            this.btnAsgAccDone.Text = "&Quit";
            this.btnAsgAccDone.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnAsgAccDone.UseVisualStyleBackColor = true;
            this.btnAsgAccDone.Click += new System.EventHandler(this.btnAsgAccDone_Click);
            // 
            // frmAssignAccessory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(893, 602);
            this.Controls.Add(this.btnAsgAccDone);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnNewAsgAcc);
            this.Controls.Add(this.btnAsgAccSubmitAcc);
            this.Controls.Add(this.btnAsgAccNext);
            this.Controls.Add(this.gbxAsgAccSelectstd);
            this.Controls.Add(this.gbxAsgAccSelectAcc);
            this.Controls.Add(this.lblFName);
            this.ForeColor = System.Drawing.Color.RoyalBlue;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmAssignAccessory";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Assign Accessory";
            this.Load += new System.EventHandler(this.frmAssignAccessory_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAssignAccessory)).EndInit();
            this.gbxAsgAccSelectAcc.ResumeLayout(false);
            this.gbxAsgAccSelectAcc.PerformLayout();
            this.gbxAsgAccSelectstd.ResumeLayout(false);
            this.gbxAsgAccSelectstd.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvAssignAccessory;
        private System.Windows.Forms.Label lblFName;
        private System.Windows.Forms.GroupBox gbxAsgAccSelectAcc;
        private System.Windows.Forms.TextBox txtAsgAccSize;
        private System.Windows.Forms.TextBox txtAsgAccCondition;
        private System.Windows.Forms.Label lblAsgAccSize;
        private System.Windows.Forms.Label lblAsgAccType;
        private System.Windows.Forms.Label lblAsgAccCondition;
        private System.Windows.Forms.Label lblAsgAccCode;
        private System.Windows.Forms.TextBox txtAsgAccName;
        private System.Windows.Forms.Label lblAsgAccName;
        private System.Windows.Forms.Button btnAsgAccSubmitAcc;
        private System.Windows.Forms.GroupBox gbxAsgAccSelectstd;
        private System.Windows.Forms.TextBox txtAsgAccHouse;
        private System.Windows.Forms.ComboBox cboAsgAccAdminNo;
        private System.Windows.Forms.Label lblAsgAccForm;
        private System.Windows.Forms.Label lblAsgAccHouse;
        private System.Windows.Forms.Label lblAsgAccAdminNo;
        private System.Windows.Forms.TextBox txtAsgAccMName;
        private System.Windows.Forms.Label lblAsgAccMName;
        private System.Windows.Forms.TextBox txtAsgAccLName;
        private System.Windows.Forms.TextBox txtAsgAccFName;
        private System.Windows.Forms.Label lblAsgAccLName;
        private System.Windows.Forms.Label lblAsgAccFName;
        private System.Windows.Forms.TextBox txtAsgAccForm;
        private System.Windows.Forms.TextBox txtAsgAccType;
        private System.Windows.Forms.ComboBox cboAsgAccCode;
        private System.Windows.Forms.Button btnAsgAccNext;
        private System.Windows.Forms.Button btnNewAsgAcc;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnAsgAccDone;
    }
}