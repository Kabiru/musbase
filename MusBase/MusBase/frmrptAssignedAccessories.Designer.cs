﻿namespace MusBase
{
    partial class frmrptAssignedAccessories
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.DataSet1 = new MusBase.DataSet1();
            this.Assigned_Accessories_ViewBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.Assigned_Accessories_ViewTableAdapter = new MusBase.DataSet1TableAdapters.Assigned_Accessories_ViewTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.DataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Assigned_Accessories_ViewBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // reportViewer1
            // 
            this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "DataSet1";
            reportDataSource1.Value = this.Assigned_Accessories_ViewBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "MusBase.Assigned_Accessories.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(0, 0);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(709, 441);
            this.reportViewer1.TabIndex = 0;
            // 
            // DataSet1
            // 
            this.DataSet1.DataSetName = "DataSet1";
            this.DataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // Assigned_Accessories_ViewBindingSource
            // 
            this.Assigned_Accessories_ViewBindingSource.DataMember = "Assigned_Accessories_View";
            this.Assigned_Accessories_ViewBindingSource.DataSource = this.DataSet1;
            // 
            // Assigned_Accessories_ViewTableAdapter
            // 
            this.Assigned_Accessories_ViewTableAdapter.ClearBeforeFill = true;
            // 
            // frmrptAssignedAccessories
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(709, 441);
            this.Controls.Add(this.reportViewer1);
            this.Name = "frmrptAssignedAccessories";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Assigned Accessories";
            this.Load += new System.EventHandler(this.frmrptAssignedAccessories_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Assigned_Accessories_ViewBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource Assigned_Accessories_ViewBindingSource;
        private DataSet1 DataSet1;
        private DataSet1TableAdapters.Assigned_Accessories_ViewTableAdapter Assigned_Accessories_ViewTableAdapter;
    }
}