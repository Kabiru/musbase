﻿namespace MusBase
{
    partial class frmRetrieveBook
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxRtvBook = new System.Windows.Forms.GroupBox();
            this.cboRtvBkBookType = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cboRtvBkBookCode = new System.Windows.Forms.ComboBox();
            this.lblRtvBkCode = new System.Windows.Forms.Label();
            this.lblLink = new System.Windows.Forms.Label();
            this.btnRtvBook = new System.Windows.Forms.Button();
            this.btnRtvBkSubmitBk = new System.Windows.Forms.Button();
            this.btnAsgBkDone = new System.Windows.Forms.Button();
            this.cboRtvBkAdminNo = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnReset = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dgvRtvBkBearerDetails = new System.Windows.Forms.DataGridView();
            this.gbxRtvBook.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRtvBkBearerDetails)).BeginInit();
            this.SuspendLayout();
            // 
            // gbxRtvBook
            // 
            this.gbxRtvBook.Controls.Add(this.cboRtvBkAdminNo);
            this.gbxRtvBook.Controls.Add(this.label3);
            this.gbxRtvBook.Controls.Add(this.cboRtvBkBookType);
            this.gbxRtvBook.Controls.Add(this.label1);
            this.gbxRtvBook.Controls.Add(this.cboRtvBkBookCode);
            this.gbxRtvBook.Controls.Add(this.lblRtvBkCode);
            this.gbxRtvBook.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxRtvBook.Location = new System.Drawing.Point(53, 21);
            this.gbxRtvBook.Name = "gbxRtvBook";
            this.gbxRtvBook.Size = new System.Drawing.Size(307, 149);
            this.gbxRtvBook.TabIndex = 0;
            this.gbxRtvBook.TabStop = false;
            this.gbxRtvBook.Text = "Enter the following details";
            this.gbxRtvBook.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // cboRtvBkBookType
            // 
            this.cboRtvBkBookType.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cboRtvBkBookType.FormattingEnabled = true;
            this.cboRtvBkBookType.Location = new System.Drawing.Point(119, 27);
            this.cboRtvBkBookType.Name = "cboRtvBkBookType";
            this.cboRtvBkBookType.Size = new System.Drawing.Size(121, 28);
            this.cboRtvBkBookType.TabIndex = 1;
            this.cboRtvBkBookType.SelectedIndexChanged += new System.EventHandler(this.cboRtvBkBookType_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(36, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 17);
            this.label1.TabIndex = 191;
            this.label1.Text = "Book Type";
            // 
            // cboRtvBkBookCode
            // 
            this.cboRtvBkBookCode.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cboRtvBkBookCode.FormattingEnabled = true;
            this.cboRtvBkBookCode.Location = new System.Drawing.Point(119, 63);
            this.cboRtvBkBookCode.Name = "cboRtvBkBookCode";
            this.cboRtvBkBookCode.Size = new System.Drawing.Size(121, 28);
            this.cboRtvBkBookCode.TabIndex = 2;
            this.cboRtvBkBookCode.SelectedIndexChanged += new System.EventHandler(this.cboRtvBkBookCode_SelectedIndexChanged);
            // 
            // lblRtvBkCode
            // 
            this.lblRtvBkCode.AutoSize = true;
            this.lblRtvBkCode.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRtvBkCode.Location = new System.Drawing.Point(34, 63);
            this.lblRtvBkCode.Name = "lblRtvBkCode";
            this.lblRtvBkCode.Size = new System.Drawing.Size(79, 17);
            this.lblRtvBkCode.TabIndex = 144;
            this.lblRtvBkCode.Text = "Book Code";
            // 
            // lblLink
            // 
            this.lblLink.AutoSize = true;
            this.lblLink.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLink.ForeColor = System.Drawing.Color.Red;
            this.lblLink.Location = new System.Drawing.Point(12, 245);
            this.lblLink.Name = "lblLink";
            this.lblLink.Size = new System.Drawing.Size(145, 20);
            this.lblLink.TabIndex = 192;
            this.lblLink.Text = "Registration link: ";
            // 
            // btnRtvBook
            // 
            this.btnRtvBook.Font = new System.Drawing.Font("Tempus Sans ITC", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRtvBook.Image = global::MusBase.Properties.Resources.arpa_cancel_48;
            this.btnRtvBook.Location = new System.Drawing.Point(345, 186);
            this.btnRtvBook.Name = "btnRtvBook";
            this.btnRtvBook.Size = new System.Drawing.Size(139, 57);
            this.btnRtvBook.TabIndex = 4;
            this.btnRtvBook.Text = "Retrieve Book";
            this.btnRtvBook.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnRtvBook.UseVisualStyleBackColor = true;
            this.btnRtvBook.Click += new System.EventHandler(this.btnRtvBook_Click);
            // 
            // btnRtvBkSubmitBk
            // 
            this.btnRtvBkSubmitBk.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRtvBkSubmitBk.Image = global::MusBase.Properties.Resources.Book_Blank_Book_icon;
            this.btnRtvBkSubmitBk.Location = new System.Drawing.Point(380, 128);
            this.btnRtvBkSubmitBk.Name = "btnRtvBkSubmitBk";
            this.btnRtvBkSubmitBk.Size = new System.Drawing.Size(104, 42);
            this.btnRtvBkSubmitBk.TabIndex = 3;
            this.btnRtvBkSubmitBk.Text = "&Submit";
            this.btnRtvBkSubmitBk.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnRtvBkSubmitBk.UseVisualStyleBackColor = true;
            this.btnRtvBkSubmitBk.Click += new System.EventHandler(this.btnAsgBkSubmitBk_Click);
            // 
            // btnAsgBkDone
            // 
            this.btnAsgBkDone.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAsgBkDone.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.btnAsgBkDone.Image = global::MusBase.Properties.Resources._1427990866_Done;
            this.btnAsgBkDone.Location = new System.Drawing.Point(198, 186);
            this.btnAsgBkDone.Name = "btnAsgBkDone";
            this.btnAsgBkDone.Size = new System.Drawing.Size(95, 44);
            this.btnAsgBkDone.TabIndex = 5;
            this.btnAsgBkDone.Text = "&Quit";
            this.btnAsgBkDone.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnAsgBkDone.UseVisualStyleBackColor = true;
            this.btnAsgBkDone.Click += new System.EventHandler(this.btnAsgBkDone_Click);
            // 
            // cboRtvBkAdminNo
            // 
            this.cboRtvBkAdminNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboRtvBkAdminNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboRtvBkAdminNo.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cboRtvBkAdminNo.FormattingEnabled = true;
            this.cboRtvBkAdminNo.Location = new System.Drawing.Point(119, 98);
            this.cboRtvBkAdminNo.Name = "cboRtvBkAdminNo";
            this.cboRtvBkAdminNo.Size = new System.Drawing.Size(121, 28);
            this.cboRtvBkAdminNo.TabIndex = 197;
            this.cboRtvBkAdminNo.SelectedIndexChanged += new System.EventHandler(this.cboRtvBkAdminNo_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 98);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 17);
            this.label3.TabIndex = 196;
            this.label3.Text = "Admin No";
            // 
            // btnReset
            // 
            this.btnReset.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.Image = global::MusBase.Properties.Resources._1429511309_131707;
            this.btnReset.Location = new System.Drawing.Point(53, 186);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(95, 44);
            this.btnReset.TabIndex = 207;
            this.btnReset.Text = "Reset";
            this.btnReset.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dgvRtvBkBearerDetails);
            this.panel1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(22, 283);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(660, 197);
            this.panel1.TabIndex = 208;
            // 
            // dgvRtvBkBearerDetails
            // 
            this.dgvRtvBkBearerDetails.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvRtvBkBearerDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRtvBkBearerDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvRtvBkBearerDetails.Location = new System.Drawing.Point(0, 0);
            this.dgvRtvBkBearerDetails.Name = "dgvRtvBkBearerDetails";
            this.dgvRtvBkBearerDetails.Size = new System.Drawing.Size(660, 197);
            this.dgvRtvBkBearerDetails.TabIndex = 137;
            // 
            // frmRetrieveBook
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(694, 492);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnAsgBkDone);
            this.Controls.Add(this.lblLink);
            this.Controls.Add(this.btnRtvBook);
            this.Controls.Add(this.btnRtvBkSubmitBk);
            this.Controls.Add(this.gbxRtvBook);
            this.ForeColor = System.Drawing.Color.RoyalBlue;
            this.MaximizeBox = false;
            this.Name = "frmRetrieveBook";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Retrieve Book";
            this.Load += new System.EventHandler(this.frmRetrieveBook_Load);
            this.gbxRtvBook.ResumeLayout(false);
            this.gbxRtvBook.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRtvBkBearerDetails)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxRtvBook;
        private System.Windows.Forms.ComboBox cboRtvBkBookCode;
        private System.Windows.Forms.Label lblRtvBkCode;
        private System.Windows.Forms.Button btnRtvBkSubmitBk;
        private System.Windows.Forms.ComboBox cboRtvBkBookType;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnRtvBook;
        private System.Windows.Forms.Label lblLink;
        private System.Windows.Forms.Button btnAsgBkDone;
        private System.Windows.Forms.ComboBox cboRtvBkAdminNo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dgvRtvBkBearerDetails;
    }
}