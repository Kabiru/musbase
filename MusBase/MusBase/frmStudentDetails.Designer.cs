﻿namespace MusBase
{
    partial class frmAddStudent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dgvStudentProfile = new System.Windows.Forms.DataGridView();
            this.gbxStudentDetails = new System.Windows.Forms.GroupBox();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.lblPhone = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.lblEmail = new System.Windows.Forms.Label();
            this.btnSavestd = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.cboForm = new System.Windows.Forms.ComboBox();
            this.lblClass = new System.Windows.Forms.Label();
            this.cboHouse = new System.Windows.Forms.ComboBox();
            this.lblHouse = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtAdminNo = new System.Windows.Forms.TextBox();
            this.txtMName = new System.Windows.Forms.TextBox();
            this.lblMName = new System.Windows.Forms.Label();
            this.txtLName = new System.Windows.Forms.TextBox();
            this.txtFName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblFName = new System.Windows.Forms.Label();
            this.btnAddNewStudent = new System.Windows.Forms.Button();
            this.btnDone = new System.Windows.Forms.Button();
            this.lblInsertStatus = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.EmailerrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.adminerrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.PhoneerrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.btnUpdate = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStudentProfile)).BeginInit();
            this.gbxStudentDetails.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EmailerrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.adminerrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PhoneerrorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvStudentProfile
            // 
            this.dgvStudentProfile.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvStudentProfile.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvStudentProfile.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvStudentProfile.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvStudentProfile.GridColor = System.Drawing.SystemColors.ActiveCaption;
            this.dgvStudentProfile.Location = new System.Drawing.Point(0, 0);
            this.dgvStudentProfile.Name = "dgvStudentProfile";
            this.dgvStudentProfile.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvStudentProfile.Size = new System.Drawing.Size(804, 197);
            this.dgvStudentProfile.TabIndex = 19;
            this.dgvStudentProfile.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvStudentProfile_CellContentClick);
            this.dgvStudentProfile.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvStudentProfile_CellContentDoubleClick);
            // 
            // gbxStudentDetails
            // 
            this.gbxStudentDetails.Controls.Add(this.txtPhone);
            this.gbxStudentDetails.Controls.Add(this.lblPhone);
            this.gbxStudentDetails.Controls.Add(this.txtEmail);
            this.gbxStudentDetails.Controls.Add(this.lblEmail);
            this.gbxStudentDetails.Controls.Add(this.btnSavestd);
            this.gbxStudentDetails.Controls.Add(this.btnClear);
            this.gbxStudentDetails.Controls.Add(this.cboForm);
            this.gbxStudentDetails.Controls.Add(this.lblClass);
            this.gbxStudentDetails.Controls.Add(this.cboHouse);
            this.gbxStudentDetails.Controls.Add(this.lblHouse);
            this.gbxStudentDetails.Controls.Add(this.label1);
            this.gbxStudentDetails.Controls.Add(this.txtAdminNo);
            this.gbxStudentDetails.Controls.Add(this.txtMName);
            this.gbxStudentDetails.Controls.Add(this.lblMName);
            this.gbxStudentDetails.Controls.Add(this.txtLName);
            this.gbxStudentDetails.Controls.Add(this.txtFName);
            this.gbxStudentDetails.Controls.Add(this.label2);
            this.gbxStudentDetails.Controls.Add(this.lblFName);
            this.gbxStudentDetails.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxStudentDetails.Location = new System.Drawing.Point(12, 12);
            this.gbxStudentDetails.Name = "gbxStudentDetails";
            this.gbxStudentDetails.Size = new System.Drawing.Size(810, 240);
            this.gbxStudentDetails.TabIndex = 26;
            this.gbxStudentDetails.TabStop = false;
            this.gbxStudentDetails.Text = "Student details";
            this.gbxStudentDetails.Enter += new System.EventHandler(this.gbxStudentDetails_Enter);
            // 
            // txtPhone
            // 
            this.txtPhone.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhone.Location = new System.Drawing.Point(384, 24);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(157, 24);
            this.txtPhone.TabIndex = 5;
            this.txtPhone.TextChanged += new System.EventHandler(this.txtPhone_TextChanged);
            // 
            // lblPhone
            // 
            this.lblPhone.AutoSize = true;
            this.lblPhone.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhone.Location = new System.Drawing.Point(314, 24);
            this.lblPhone.Name = "lblPhone";
            this.lblPhone.Size = new System.Drawing.Size(48, 17);
            this.lblPhone.TabIndex = 48;
            this.lblPhone.Text = "Phone";
            // 
            // txtEmail
            // 
            this.txtEmail.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail.Location = new System.Drawing.Point(384, 54);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(157, 24);
            this.txtEmail.TabIndex = 6;
            this.txtEmail.Validating += new System.ComponentModel.CancelEventHandler(this.txtEmail_Validating);
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmail.Location = new System.Drawing.Point(313, 54);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(43, 17);
            this.lblEmail.TabIndex = 46;
            this.lblEmail.Text = "Email";
            // 
            // btnSavestd
            // 
            this.btnSavestd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSavestd.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSavestd.Image = global::MusBase.Properties.Resources.mypc_save_32;
            this.btnSavestd.Location = new System.Drawing.Point(104, 156);
            this.btnSavestd.Name = "btnSavestd";
            this.btnSavestd.Size = new System.Drawing.Size(104, 42);
            this.btnSavestd.TabIndex = 9;
            this.btnSavestd.Text = "&Save";
            this.btnSavestd.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnSavestd.UseVisualStyleBackColor = true;
            this.btnSavestd.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClear
            // 
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClear.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.Image = global::MusBase.Properties.Resources.mypc_write_32;
            this.btnClear.Location = new System.Drawing.Point(258, 156);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(104, 42);
            this.btnClear.TabIndex = 11;
            this.btnClear.Text = "&Clear";
            this.btnClear.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click_1);
            // 
            // cboForm
            // 
            this.cboForm.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboForm.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboForm.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboForm.FormattingEnabled = true;
            this.cboForm.Location = new System.Drawing.Point(384, 115);
            this.cboForm.Name = "cboForm";
            this.cboForm.Size = new System.Drawing.Size(66, 25);
            this.cboForm.TabIndex = 8;
            this.cboForm.SelectedIndexChanged += new System.EventHandler(this.cboForm_SelectedIndexChanged);
            // 
            // lblClass
            // 
            this.lblClass.AutoSize = true;
            this.lblClass.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClass.Location = new System.Drawing.Point(314, 116);
            this.lblClass.Name = "lblClass";
            this.lblClass.Size = new System.Drawing.Size(42, 17);
            this.lblClass.TabIndex = 41;
            this.lblClass.Text = "Form";
            // 
            // cboHouse
            // 
            this.cboHouse.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboHouse.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboHouse.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboHouse.FormattingEnabled = true;
            this.cboHouse.Location = new System.Drawing.Point(384, 84);
            this.cboHouse.Name = "cboHouse";
            this.cboHouse.Size = new System.Drawing.Size(157, 25);
            this.cboHouse.TabIndex = 7;
            // 
            // lblHouse
            // 
            this.lblHouse.AutoSize = true;
            this.lblHouse.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHouse.Location = new System.Drawing.Point(314, 85);
            this.lblHouse.Name = "lblHouse";
            this.lblHouse.Size = new System.Drawing.Size(48, 17);
            this.lblHouse.TabIndex = 39;
            this.lblHouse.Text = "House";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 116);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 17);
            this.label1.TabIndex = 38;
            this.label1.Text = "Admin No";
            // 
            // txtAdminNo
            // 
            this.txtAdminNo.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAdminNo.Location = new System.Drawing.Point(104, 116);
            this.txtAdminNo.Name = "txtAdminNo";
            this.txtAdminNo.Size = new System.Drawing.Size(157, 24);
            this.txtAdminNo.TabIndex = 4;
            this.txtAdminNo.Validating += new System.ComponentModel.CancelEventHandler(this.txtAdminNo_Validating);
            // 
            // txtMName
            // 
            this.txtMName.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMName.Location = new System.Drawing.Point(104, 85);
            this.txtMName.Name = "txtMName";
            this.txtMName.Size = new System.Drawing.Size(157, 24);
            this.txtMName.TabIndex = 3;
            // 
            // lblMName
            // 
            this.lblMName.AutoSize = true;
            this.lblMName.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMName.Location = new System.Drawing.Point(6, 85);
            this.lblMName.Name = "lblMName";
            this.lblMName.Size = new System.Drawing.Size(94, 17);
            this.lblMName.TabIndex = 35;
            this.lblMName.Text = "Middle Name";
            // 
            // txtLName
            // 
            this.txtLName.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLName.Location = new System.Drawing.Point(104, 54);
            this.txtLName.Name = "txtLName";
            this.txtLName.Size = new System.Drawing.Size(157, 24);
            this.txtLName.TabIndex = 2;
            // 
            // txtFName
            // 
            this.txtFName.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFName.Location = new System.Drawing.Point(104, 24);
            this.txtFName.Name = "txtFName";
            this.txtFName.Size = new System.Drawing.Size(157, 24);
            this.txtFName.TabIndex = 1;
            this.txtFName.TextChanged += new System.EventHandler(this.txtFName_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 17);
            this.label2.TabIndex = 32;
            this.label2.Text = "Last Name";
            // 
            // lblFName
            // 
            this.lblFName.AutoSize = true;
            this.lblFName.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFName.Location = new System.Drawing.Point(6, 24);
            this.lblFName.Name = "lblFName";
            this.lblFName.Size = new System.Drawing.Size(78, 17);
            this.lblFName.TabIndex = 31;
            this.lblFName.Text = "First Name";
            // 
            // btnAddNewStudent
            // 
            this.btnAddNewStudent.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddNewStudent.Image = global::MusBase.Properties.Resources.mypc_add_32;
            this.btnAddNewStudent.Location = new System.Drawing.Point(116, 257);
            this.btnAddNewStudent.Name = "btnAddNewStudent";
            this.btnAddNewStudent.Size = new System.Drawing.Size(104, 42);
            this.btnAddNewStudent.TabIndex = 10;
            this.btnAddNewStudent.Text = "&Add New";
            this.btnAddNewStudent.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnAddNewStudent.UseVisualStyleBackColor = true;
            this.btnAddNewStudent.Click += new System.EventHandler(this.btnAddNewStudent_Click_1);
            // 
            // btnDone
            // 
            this.btnDone.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDone.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.btnDone.Image = global::MusBase.Properties.Resources._1427990866_Done;
            this.btnDone.Location = new System.Drawing.Point(458, 258);
            this.btnDone.Name = "btnDone";
            this.btnDone.Size = new System.Drawing.Size(95, 44);
            this.btnDone.TabIndex = 12;
            this.btnDone.Text = "&Quit";
            this.btnDone.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnDone.UseVisualStyleBackColor = true;
            this.btnDone.Click += new System.EventHandler(this.btnDone_Click);
            // 
            // lblInsertStatus
            // 
            this.lblInsertStatus.AutoSize = true;
            this.lblInsertStatus.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInsertStatus.ForeColor = System.Drawing.Color.Red;
            this.lblInsertStatus.Location = new System.Drawing.Point(18, 302);
            this.lblInsertStatus.Name = "lblInsertStatus";
            this.lblInsertStatus.Size = new System.Drawing.Size(45, 17);
            this.lblInsertStatus.TabIndex = 162;
            this.lblInsertStatus.Text = "label3";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.dgvStudentProfile);
            this.panel1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(12, 322);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(804, 197);
            this.panel1.TabIndex = 163;
            // 
            // EmailerrorProvider
            // 
            this.EmailerrorProvider.ContainerControl = this;
            // 
            // adminerrorProvider
            // 
            this.adminerrorProvider.ContainerControl = this;
            // 
            // PhoneerrorProvider
            // 
            this.PhoneerrorProvider.ContainerControl = this;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Location = new System.Drawing.Point(270, 257);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(104, 42);
            this.btnUpdate.TabIndex = 207;
            this.btnUpdate.Text = "&Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // frmAddStudent
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(834, 534);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblInsertStatus);
            this.Controls.Add(this.btnAddNewStudent);
            this.Controls.Add(this.gbxStudentDetails);
            this.Controls.Add(this.btnDone);
            this.ForeColor = System.Drawing.Color.RoyalBlue;
            this.Name = "frmAddStudent";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Student Profile";
            this.Load += new System.EventHandler(this.frmAddStudent_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvStudentProfile)).EndInit();
            this.gbxStudentDetails.ResumeLayout(false);
            this.gbxStudentDetails.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.EmailerrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.adminerrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PhoneerrorProvider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvStudentProfile;
        private System.Windows.Forms.Button btnAddNewStudent;
        private System.Windows.Forms.GroupBox gbxStudentDetails;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.Label lblPhone;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Button btnSavestd;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.ComboBox cboForm;
        private System.Windows.Forms.Label lblClass;
        private System.Windows.Forms.ComboBox cboHouse;
        private System.Windows.Forms.Label lblHouse;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtAdminNo;
        private System.Windows.Forms.TextBox txtMName;
        private System.Windows.Forms.Label lblMName;
        private System.Windows.Forms.TextBox txtLName;
        private System.Windows.Forms.TextBox txtFName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblFName;
        private System.Windows.Forms.Button btnDone;
        private System.Windows.Forms.Label lblInsertStatus;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ErrorProvider EmailerrorProvider;
        private System.Windows.Forms.ErrorProvider adminerrorProvider;
        private System.Windows.Forms.ErrorProvider PhoneerrorProvider;
        private System.Windows.Forms.Button btnUpdate;
    }
}