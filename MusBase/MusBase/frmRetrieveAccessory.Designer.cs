﻿namespace MusBase
{
    partial class frmRetrieveAccessory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxRtvAccessory = new System.Windows.Forms.GroupBox();
            this.cboRtvAccAdminNo = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cboRtvAccAccessoryType = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cboRtvAccAccessoryCode = new System.Windows.Forms.ComboBox();
            this.lblRtvBkCode = new System.Windows.Forms.Label();
            this.lblLink = new System.Windows.Forms.Label();
            this.btnRtvAccessory = new System.Windows.Forms.Button();
            this.btnAsgBkDone = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dgvRtvAccBearerDetails = new System.Windows.Forms.DataGridView();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnRtvAccSubmitAcc = new System.Windows.Forms.Button();
            this.gbxRtvAccessory.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRtvAccBearerDetails)).BeginInit();
            this.SuspendLayout();
            // 
            // gbxRtvAccessory
            // 
            this.gbxRtvAccessory.Controls.Add(this.cboRtvAccAdminNo);
            this.gbxRtvAccessory.Controls.Add(this.label3);
            this.gbxRtvAccessory.Controls.Add(this.cboRtvAccAccessoryType);
            this.gbxRtvAccessory.Controls.Add(this.label1);
            this.gbxRtvAccessory.Controls.Add(this.cboRtvAccAccessoryCode);
            this.gbxRtvAccessory.Controls.Add(this.lblRtvBkCode);
            this.gbxRtvAccessory.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxRtvAccessory.Location = new System.Drawing.Point(53, 12);
            this.gbxRtvAccessory.Name = "gbxRtvAccessory";
            this.gbxRtvAccessory.Size = new System.Drawing.Size(307, 149);
            this.gbxRtvAccessory.TabIndex = 0;
            this.gbxRtvAccessory.TabStop = false;
            this.gbxRtvAccessory.Text = "Enter the following details";
            // 
            // cboRtvAccAdminNo
            // 
            this.cboRtvAccAdminNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboRtvAccAdminNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboRtvAccAdminNo.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cboRtvAccAdminNo.FormattingEnabled = true;
            this.cboRtvAccAdminNo.Location = new System.Drawing.Point(149, 104);
            this.cboRtvAccAdminNo.Name = "cboRtvAccAdminNo";
            this.cboRtvAccAdminNo.Size = new System.Drawing.Size(121, 28);
            this.cboRtvAccAdminNo.TabIndex = 195;
            this.cboRtvAccAdminNo.SelectedIndexChanged += new System.EventHandler(this.cboRtvAccAdminNo_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(36, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 17);
            this.label3.TabIndex = 194;
            this.label3.Text = "Admin No";
            // 
            // cboRtvAccAccessoryType
            // 
            this.cboRtvAccAccessoryType.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cboRtvAccAccessoryType.FormattingEnabled = true;
            this.cboRtvAccAccessoryType.Location = new System.Drawing.Point(149, 22);
            this.cboRtvAccAccessoryType.Name = "cboRtvAccAccessoryType";
            this.cboRtvAccAccessoryType.Size = new System.Drawing.Size(121, 28);
            this.cboRtvAccAccessoryType.TabIndex = 1;
            this.cboRtvAccAccessoryType.SelectedIndexChanged += new System.EventHandler(this.cboRtvAccAccessoryType_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(36, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 17);
            this.label1.TabIndex = 191;
            this.label1.Text = "Accessory Type";
            // 
            // cboRtvAccAccessoryCode
            // 
            this.cboRtvAccAccessoryCode.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cboRtvAccAccessoryCode.FormattingEnabled = true;
            this.cboRtvAccAccessoryCode.Location = new System.Drawing.Point(149, 63);
            this.cboRtvAccAccessoryCode.Name = "cboRtvAccAccessoryCode";
            this.cboRtvAccAccessoryCode.Size = new System.Drawing.Size(121, 28);
            this.cboRtvAccAccessoryCode.TabIndex = 2;
            this.cboRtvAccAccessoryCode.SelectedIndexChanged += new System.EventHandler(this.cboRtvAccAccessoryCode_SelectedIndexChanged);
            // 
            // lblRtvBkCode
            // 
            this.lblRtvBkCode.AutoSize = true;
            this.lblRtvBkCode.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRtvBkCode.Location = new System.Drawing.Point(34, 63);
            this.lblRtvBkCode.Name = "lblRtvBkCode";
            this.lblRtvBkCode.Size = new System.Drawing.Size(109, 17);
            this.lblRtvBkCode.TabIndex = 144;
            this.lblRtvBkCode.Text = "Accessory Code";
            // 
            // lblLink
            // 
            this.lblLink.AutoSize = true;
            this.lblLink.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLink.ForeColor = System.Drawing.Color.Red;
            this.lblLink.Location = new System.Drawing.Point(30, 259);
            this.lblLink.Name = "lblLink";
            this.lblLink.Size = new System.Drawing.Size(145, 20);
            this.lblLink.TabIndex = 197;
            this.lblLink.Text = "Registration link: ";
            // 
            // btnRtvAccessory
            // 
            this.btnRtvAccessory.Font = new System.Drawing.Font("Tempus Sans ITC", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRtvAccessory.Image = global::MusBase.Properties.Resources.mydocuments_delete_48;
            this.btnRtvAccessory.Location = new System.Drawing.Point(350, 190);
            this.btnRtvAccessory.Name = "btnRtvAccessory";
            this.btnRtvAccessory.Size = new System.Drawing.Size(139, 57);
            this.btnRtvAccessory.TabIndex = 4;
            this.btnRtvAccessory.Text = "Retrieve Accessory";
            this.btnRtvAccessory.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnRtvAccessory.UseVisualStyleBackColor = true;
            this.btnRtvAccessory.Click += new System.EventHandler(this.btnRtvAccessory_Click);
            // 
            // btnAsgBkDone
            // 
            this.btnAsgBkDone.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAsgBkDone.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.btnAsgBkDone.Image = global::MusBase.Properties.Resources._1427990866_Done;
            this.btnAsgBkDone.Location = new System.Drawing.Point(202, 190);
            this.btnAsgBkDone.Name = "btnAsgBkDone";
            this.btnAsgBkDone.Size = new System.Drawing.Size(95, 44);
            this.btnAsgBkDone.TabIndex = 5;
            this.btnAsgBkDone.Text = "&Quit";
            this.btnAsgBkDone.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnAsgBkDone.UseVisualStyleBackColor = true;
            this.btnAsgBkDone.Click += new System.EventHandler(this.btnAsgBkDone_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dgvRtvAccBearerDetails);
            this.panel1.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(34, 285);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(580, 184);
            this.panel1.TabIndex = 198;
            // 
            // dgvRtvAccBearerDetails
            // 
            this.dgvRtvAccBearerDetails.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvRtvAccBearerDetails.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dgvRtvAccBearerDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRtvAccBearerDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvRtvAccBearerDetails.Location = new System.Drawing.Point(0, 0);
            this.dgvRtvAccBearerDetails.Name = "dgvRtvAccBearerDetails";
            this.dgvRtvAccBearerDetails.Size = new System.Drawing.Size(580, 184);
            this.dgvRtvAccBearerDetails.TabIndex = 195;
            // 
            // btnReset
            // 
            this.btnReset.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.Image = global::MusBase.Properties.Resources._1429511309_131707;
            this.btnReset.Location = new System.Drawing.Point(53, 190);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(95, 44);
            this.btnReset.TabIndex = 206;
            this.btnReset.Text = "Reset";
            this.btnReset.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnRtvAccSubmitAcc
            // 
            this.btnRtvAccSubmitAcc.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRtvAccSubmitAcc.Image = global::MusBase.Properties.Resources.mydocuments_32;
            this.btnRtvAccSubmitAcc.Location = new System.Drawing.Point(385, 119);
            this.btnRtvAccSubmitAcc.Name = "btnRtvAccSubmitAcc";
            this.btnRtvAccSubmitAcc.Size = new System.Drawing.Size(104, 42);
            this.btnRtvAccSubmitAcc.TabIndex = 207;
            this.btnRtvAccSubmitAcc.Text = "&Submit";
            this.btnRtvAccSubmitAcc.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnRtvAccSubmitAcc.UseVisualStyleBackColor = true;
            this.btnRtvAccSubmitAcc.Click += new System.EventHandler(this.button1_Click);
            // 
            // frmRetrieveAccessory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(649, 492);
            this.Controls.Add(this.btnRtvAccSubmitAcc);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnAsgBkDone);
            this.Controls.Add(this.btnRtvAccessory);
            this.Controls.Add(this.gbxRtvAccessory);
            this.Controls.Add(this.lblLink);
            this.ForeColor = System.Drawing.Color.RoyalBlue;
            this.MaximizeBox = false;
            this.Name = "frmRetrieveAccessory";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Retrieve Accessory";
            this.Load += new System.EventHandler(this.frmRetrieveAccessory_Load);
            this.gbxRtvAccessory.ResumeLayout(false);
            this.gbxRtvAccessory.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRtvAccBearerDetails)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxRtvAccessory;
        private System.Windows.Forms.ComboBox cboRtvAccAccessoryType;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboRtvAccAccessoryCode;
        private System.Windows.Forms.Label lblRtvBkCode;
        private System.Windows.Forms.Label lblLink;
        private System.Windows.Forms.Button btnRtvAccessory;
        private System.Windows.Forms.Button btnAsgBkDone;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dgvRtvAccBearerDetails;
        private System.Windows.Forms.ComboBox cboRtvAccAdminNo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnRtvAccSubmitAcc;
    }
}