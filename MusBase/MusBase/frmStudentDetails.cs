﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System.Net.Mail;

namespace MusBase
{
    public partial class frmAddStudent : Form
    {
        string connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;
        SqlCommand cmd;
        SqlConnection conn;
        SqlDataReader reader;
        SqlDataAdapter dAdapter;
        DataTable StudentDetails;
        SqlCommandBuilder cmdbld;


        public frmAddStudent()
        {
            InitializeComponent();
            //fill cbo
            fillcboForm();
            fillcboHouse();
            LoadStudentDetails();
            disableButtons();
        }
        private void disableButtons()
        { 
            //disable controls
            btnSavestd.Enabled = false;
            btnClear.Enabled = false;
            btnAddNewStudent.Enabled = false;
            lblInsertStatus.Text = null;
            btnAddNewStudent.Enabled = false;
        }

        void LoadStudentDetails() 
        {
            string selectQuery = "Load_Student_Details_Procedure";            
            conn = new SqlConnection(connectionString);
            try
            {
                cmd = new SqlCommand(selectQuery, conn);
                cmd.CommandType = CommandType.StoredProcedure;
                dAdapter = new SqlDataAdapter(cmd);
                StudentDetails = new DataTable();
                //fill data table
                dAdapter.Fill(StudentDetails);
                //sync with dgv
                BindingSource bSource = new BindingSource();
                bSource.DataSource = StudentDetails;
                dgvStudentProfile.DataSource = bSource;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void fillcboForm() 
        {
           //fill cbo
            conn = new SqlConnection(connectionString);
            string sqlQuery = "SELECT * FROM [dbo].[Form]";
            cmd = new SqlCommand(sqlQuery, conn);
            conn.Open();
            reader = cmd.ExecuteReader();
            try
            {                
                int ordinalForm = reader.GetOrdinal("Form_Num");
                while (reader.Read())
                {
                    string temp = reader.GetString(ordinalForm);
                    cboForm.Items.Add(temp);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                reader.Close();
                conn.Close();
            }  
        }

        void fillcboHouse() 
        {
            //fill cbo
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString);
            string sqlQuery = "SELECT * FROM [dbo].[House]";
            SqlCommand cmd = new SqlCommand(sqlQuery, conn);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            try
            {                
                while (reader.Read())
                {
                    string temp = reader.GetString(1);
                    cboHouse.Items.Add(temp);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                reader.Close();
                conn.Close();
            }
        }

        private void frmAddStudent_Load(object sender, EventArgs e)
        {
          
        }


        private void btnStdClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            //save
            string sqlQuery = null;
            SqlConnection conn;
            SqlCommand cmd;

            string connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;
            sqlQuery = "INSERT INTO [dbo].[Student_Profile] ([Admin_No], [FName], [LName], [MName], [Email], [Phone], [Form], [House])"+
                " VALUES (@AdminNO, @FName, @LName, @MName, @Email, @Phone, @Form, @House)";

            conn = new SqlConnection(connectionString);
            cmd = new SqlCommand(sqlQuery, conn);
            cmd.Parameters.AddWithValue("@AdminNo", txtAdminNo.Text);
            cmd.Parameters.AddWithValue("@FName", txtFName.Text);
            cmd.Parameters.AddWithValue("@LName", txtLName.Text);
            cmd.Parameters.AddWithValue("@MName", txtMName.Text);
            cmd.Parameters.AddWithValue("@Email", txtEmail.Text);
            cmd.Parameters.AddWithValue("@Phone", txtPhone.Text);
            cmd.Parameters.AddWithValue("@Form", cboForm.Text);
            cmd.Parameters.AddWithValue("@House", cboHouse.Text);
            try
            {
                conn.Open();
                int TotalRowsAffected = cmd.ExecuteNonQuery();

                if (TotalRowsAffected == 1)
                {
                    lblInsertStatus.Text = "Record saved!";
                    LoadStudentDetails();
                    gbxStudentDetails.Enabled = false;
                    btnAddNewStudent.Enabled = true;
                    dgvStudentProfile.Update();
                    dgvStudentProfile.Refresh();
                }
                else if (TotalRowsAffected > 1)
                {
                    lblInsertStatus.Text = "Duplicate record";
                }
                else
                {
                    lblInsertStatus.Text = "Insert incomplete, please try again";
                }
            }
            catch (SqlException sex)
            {
                if (sex.Number == 2627)
                {
                    MessageBox.Show("THE RECORD ALREADY EXISTS IN THE DATABASE.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    addnew();
                }
                else
                {
                    MessageBox.Show(sex.Message);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
    
        }

        private void btnClear_Click_1(object sender, EventArgs e)
        {
            //clearing txts
            txtPhone.Clear();
            txtMName.Clear();
            txtLName.Clear();
            txtFName.Clear();
            txtEmail.Clear();
            txtAdminNo.Clear();

            txtFName.Focus();

            EmailerrorProvider.SetError(txtEmail, null);
            adminerrorProvider.SetError(txtAdminNo, null);
            PhoneerrorProvider.SetError(txtPhone, null);
        }

        private void clearBoxes()
        {
            //clearing txts
            txtPhone.Clear();
            txtMName.Clear();
            txtLName.Clear();
            txtFName.Clear();
            txtEmail.Clear();
            txtAdminNo.Clear();
            cboForm.SelectedItem = null;
            cboForm.Text = null;
            cboHouse.SelectedItem = null;
            cboHouse.Text = null;
        }

        private void addnew()
        {
            clearBoxes();
            //activate gbx
            gbxStudentDetails.Enabled = true;
            disableButtons();
            txtFName.Focus();
        }

        private void btnAddNewStudent_Click_1(object sender, EventArgs e)
        {
            addnew();  
        }

        private void btnDone_Click(object sender, EventArgs e)
        {
            this.Close();//closing the form
        }

        private void gbxStudentDetails_Enter(object sender, EventArgs e)
        {

        }

        private void dgvStudentProfile_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            btnUpdate.Enabled = true;
        }

        private void studentProfileBindingSource_CurrentChanged(object sender, EventArgs e)
        {

        }      

        private void cboForm_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (txtAdminNo.Text != null && txtEmail.Text != null && txtFName.Text != null
                && txtLName.Text != null && cboHouse.Text != null)
            {
                btnSavestd.Enabled = true;   
            }
        }

        private void txtFName_TextChanged(object sender, EventArgs e)
        {
            if (txtFName.Text != null) 
            {
                btnClear.Enabled = true;
            }
        }

        private void txtEmail_Validating(object sender, CancelEventArgs e)
        {
            string mailaddress = null;
            if (txtEmail.Text != null)
            {
                try
                {                  
                    mailaddress = txtEmail.Text;
                    MailAddress IsMailValid = new MailAddress(mailaddress);
                }
                catch (ArgumentNullException nullex)
                {                    
                    EmailerrorProvider.SetError(txtEmail, nullex.Message);                   
                }
                catch (ArgumentException nullstringex)
                {                   
                    EmailerrorProvider.SetError(txtEmail, nullstringex.Message);
                }
                catch (FormatException fex)
                {                  
                    EmailerrorProvider.SetError(txtEmail, fex.Message);                  
                }
            }
            else 
            {
                EmailerrorProvider.SetError(txtEmail, null);
            }
        }

        private void txtAdminNo_Validating(object sender, CancelEventArgs e)
        {
            int adminNo;
            if (txtAdminNo.Text != null)
            {
                if (int.TryParse(txtAdminNo.Text, out adminNo))
                {
                    //adminNo is numeric
                    adminerrorProvider.SetError(txtAdminNo, null);
                }
                else
                {
                    //admin not numeric
                    adminerrorProvider.SetError(txtAdminNo, "Invalid Admission Number");
                }
            }
            else if (txtAdminNo.Text == null)
            {
                adminerrorProvider.SetError(txtAdminNo, "Admission Number Required");
            }
            else
            {
                adminerrorProvider.SetError(txtAdminNo, null);
            }
        }

        private void txtPhone_TextChanged(object sender, EventArgs e)
        {
            int phoneNumber;
            if (txtPhone.Text != null)
            {
                if (int.TryParse(txtPhone.Text, out phoneNumber))
                {
                    PhoneerrorProvider.SetError(txtPhone, null);
                }
                else
                {
                    PhoneerrorProvider.SetError(txtPhone, "Invalid Phone Number");
                }
            }
            else
            {
                PhoneerrorProvider.SetError(txtPhone, "Phone Number is required");
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try 
            {
                cmdbld = new SqlCommandBuilder(dAdapter);
                dAdapter.Update(StudentDetails);
                MessageBox.Show("Information updated successfully into the database.", "Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dgvStudentProfile_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            btnUpdate.Enabled = true;
        }

       
    }
}
