﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
namespace MusBase
{
    public partial class frmLogin : Form
    {
         
        public frmLogin()
        {
            InitializeComponent();
            
            txtPasswd.PasswordChar = '*';
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void frmLogin_Load(object sender, EventArgs e)
        {
           
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Dispose();
            Application.Exit();//exiting application
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
                                        
                string sqlLogin = null;
                SqlConnection conn;
                SqlCommand cmd;
                SqlDataReader reader;//data reader object declaration

                string connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;
                sqlLogin = "SELECT * FROM [dbo].[User] WHERE [UserName] = @user AND [Password] = @pass";

                conn = new SqlConnection(connectionString);//passing the connection string
                cmd = new SqlCommand(sqlLogin, conn);//Query, connection  
                cmd.Parameters.AddWithValue("@user", txtUserName.Text);
                cmd.Parameters.AddWithValue("@pass", txtPasswd.Text);               
            try
            {
                userManagerClass usr = new userManagerClass();
                
                conn.Open();
                //connection open
                reader = cmd.ExecuteReader();//executing the reader
                int count = 0;
                int ordinal = reader.GetOrdinal("uid");
                while (reader.Read())
                {
                    count += 1;
                    int temp = reader.GetInt32(ordinal);
                    usr.checkUser(temp);
                }
                if (count == 1)
                {
                    frmMain main = new frmMain();
                    this.Hide();
                    main.Show();

                }
                else
                {
                    MessageBox.Show("Incorrect username or password", "Access Denied", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtUserName.Clear();
                    txtPasswd.Clear();
                    txtUserName.Focus();
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally 
            {
               
                conn.Close();
            }

        }

        private void txtPasswd_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtPasswd_KeyDown(object sender, KeyEventArgs e)
        {            
            if (e.KeyCode == Keys.Enter)
            {                
                btnLogin.PerformClick();
            }
        }

        private void txtUserName_TextChanged(object sender, EventArgs e)
        {

        }
      
    }
}
