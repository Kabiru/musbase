﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;

namespace MusBase
{
    public partial class frmRemoveStudent : Form
    {
        public frmRemoveStudent()
        {
            InitializeComponent();
            fillcboForm();

            cboRmvStdAdminNo.Enabled = false;
            btnRmvStudent.Enabled = false;
        }

        void filltxt() 
        {
            //FILL TEXTBOXES
            string connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;
            string sqlQuery = "SELECT [House], [FName], [MName], [LName] FROM [dbo].[Student_Profile] WHERE [Admin_No] = @AdminNo";

            SqlConnection conn = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(sqlQuery, conn);
            cmd.Parameters.AddWithValue("@AdminNo", Convert.ToInt32(cboRmvStdAdminNo.Text));
            try
            {
                conn.Open();
                //conection open
                SqlDataReader reader = cmd.ExecuteReader();

                int ordinalHouse = reader.GetOrdinal("House");
                int ordinalFName = reader.GetOrdinal("FName");
                int ordinalMName = reader.GetOrdinal("MName");
                int ordinalLName = reader.GetOrdinal("LName");
                while (reader.Read())
                {
                    string tempHouse = reader.GetString(ordinalHouse);
                    string tempFName = reader.GetString(ordinalFName);
                    string tempMName = reader.GetString(ordinalMName);
                    string tempLName = reader.GetString(ordinalLName);

                    txtRmvStdHouse.Text = tempHouse;
                    txtAsgAccFName.Text = tempFName;
                    txtRmvStdMName.Text = tempMName;
                    txtRmvStdLName.Text = tempLName;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }  
        }

        void fillcboAdmin()
        {
            //FORM >> ADMIN
            string connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;
            string sqlQuery = "SELECT [Admin_No] FROM [dbo].[Student_Profile] WHERE [Form] = @Form";

            SqlConnection conn = new SqlConnection(connectionString);
            try
            {
                SqlCommand cmd = new SqlCommand(sqlQuery, conn);
                cmd.Parameters.AddWithValue("@Form", cboRmvStdForm.Text);
                SqlDataReader reader;
                //open connection
                conn.Open();
                reader = cmd.ExecuteReader();//executing command

                int ordinal = reader.GetOrdinal("Admin_No");
                while (reader.Read())
                {
                    int temp = reader.GetInt32(ordinal);
                    cboRmvStdAdminNo.Items.Add(temp);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            finally
            {
                conn.Close();//closing connection
            }
        }

        void fillcboForm()
        {
            //FORM
            string connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;
            string sqlQuery = "SELECT DISTINCT [Form] FROM [dbo].[Student_Profile]";

            SqlConnection conn = new SqlConnection(connectionString);

            try
            {

                SqlCommand cmd = new SqlCommand(sqlQuery, conn);

                SqlDataReader reader;
                //open connection
                conn.Open();
                reader = cmd.ExecuteReader();//executing command

                int ordinal = reader.GetOrdinal("Form");
                while (reader.Read())
                {
                    string temp = reader.GetString(ordinal);
                    cboRmvStdForm.Items.Add(temp);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            finally
            {
                conn.Close();//closing connection
            }
        }

        private void frmRemoveStudent_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void btnRmvStudent_Click(object sender, EventArgs e)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;
            string sqlQueryDelete = "DELETE  FROM [Student_Profile] WHERE [Admin_No] = @AdminNo";

            SqlConnection conn = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(sqlQueryDelete, conn);
            cmd.Parameters.AddWithValue("@AdminNo", Convert.ToInt32((cboRmvStdAdminNo.Text)));
            try
            {
                conn.Open();
                int TotalRowsAffected = cmd.ExecuteNonQuery();

                if (TotalRowsAffected == 1)
                {
                    MessageBox.Show(cboRmvStdAdminNo.Text +" " +txtRmvStdLName.Text+ " has been deleted from the database.", "Record Deleted", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    btnRmvStudent.Enabled = false;
                }
                else
                {
                    MessageBox.Show("Delete unsuccessful, please try again.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();//closing connecion
            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void btnRmvStdDone_Click(object sender, EventArgs e)
        {
            this.Close();//closing form

        }

        private void cboRmvStdForm_SelectedIndexChanged(object sender, EventArgs e)
        {
            cboRmvStdAdminNo.Items.Clear();
            if(cboRmvStdForm.SelectedItem != null)
            {
                cboRmvStdAdminNo.Enabled = true;
                fillcboAdmin();
            }
        }

        private void cboRmvStdAdminNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboRmvStdAdminNo.SelectedItem != null)
            {
                filltxt();
                btnRmvStudent.Enabled = true;
            }
        }

        private void btnRmvStdClear_Click(object sender, EventArgs e)
        {
            txtRmvStdLName.Clear();
            txtAsgAccFName.Clear();
            txtRmvStdHouse.Clear();
            txtRmvStdMName.Clear();

            cboRmvStdForm.SelectedItem = null;
            cboRmvStdForm.Text = null;
            cboRmvStdAdminNo.SelectedItem = null;
            cboRmvStdAdminNo.Text = null;
            btnRmvStudent.Enabled = false;
        }
    }
}
