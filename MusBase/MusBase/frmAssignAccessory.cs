﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace MusBase
{
    public partial class frmAssignAccessory : Form
    {
        string connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;
        SqlConnection conn;
        SqlCommand cmd;
        public frmAssignAccessory()
        {
            InitializeComponent();
            LoadAssignedAccessories();
            fillcboAdminNo();
            //disabling accessory 

            gbxAsgAccSelectAcc.Enabled = false;
            btnAsgAccSubmitAcc.Enabled = false;
            btnAsgAccNext.Enabled = false;
            btnNewAsgAcc.Enabled = false;
        }

        private void assignAccessory()
        {
            

            string query = "INSERT INTO [dbo].[Student_Accessory] ([Admin_No], [Accessory_Code])" +
                "VALUES(@AdminNo, @AccessoryCode)";

            conn = new SqlConnection(connectionString);
            cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@AdminNo", Convert.ToInt32(cboAsgAccAdminNo.Text));
            cmd.Parameters.AddWithValue("@AccessoryCode", cboAsgAccCode.Text);
            try
            {
                conn.Open();
                int TotalRowsAffected = cmd.ExecuteNonQuery();
                if (TotalRowsAffected == 1)
                {
                    MessageBox.Show("ACCESSORY SUCCESSFULLY ASSIGNED.", "Assign Status", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    LoadAssignedAccessories();//refresh dgv
                    gbxAsgAccSelectstd.Enabled = false;
                    gbxAsgAccSelectAcc.Enabled = false;
                    btnAsgAccNext.Enabled = false;
                    btnNewAsgAcc.Enabled = true;
                    btnAsgAccSubmitAcc.Enabled = false;
                }
                else
                {
                    MessageBox.Show("ACCESSORY NOT ASSIGNED,\nPLEASE TRY AGAIN.", "Assign Status", MessageBoxButtons.RetryCancel, MessageBoxIcon.Exclamation);
                }
            }
            catch (SqlException sex)
            {
                if (sex.Number == 2627)
                {
                    MessageBox.Show("THE RECORD ALREADY EXISTS IN THE DATABASE.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    addnew();
                }
                else
                {
                    MessageBox.Show(sex.Message);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                conn.Close();
            }

            
        }

        private void fillAcctxt()
        {
            string query = "SELECT * FROM [Accessory_Details] WHERE [Accessory_Code] = @AccCode";
            SqlDataReader reader;
            SqlConnection conn = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@AccCode", cboAsgAccCode.Text);
            try
            {
                conn.Open();
                reader = cmd.ExecuteReader();
                int ordinalName = reader.GetOrdinal("Name");
                int ordinalSize = reader.GetOrdinal("Size");
                int ordinalCondition = reader.GetOrdinal("Condition");
                int ordinalType = reader.GetOrdinal("Accessory_Type");          

                while (reader.Read())
                {
                    string Name = reader.GetString(ordinalName);
                    string Size = reader.GetString(ordinalSize);
                    string Condition = reader.GetString(ordinalCondition);
                    string Type = reader.GetString(ordinalType);                   

                    txtAsgAccName.Text = Name;
                    txtAsgAccSize.Text = Size;
                    txtAsgAccCondition.Text = Condition;
                    txtAsgAccType.Text = Type;                    
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Error);
            }
            finally
            {
                conn.Close();
            }
        }

        private void fillStdtxt() 
        {
            string query = "SELECT * FROM [Student_Profile] WHERE [Admin_No] = @AdminNo";
            SqlDataReader reader;
            SqlConnection conn = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@AdminNo", Convert.ToInt32(cboAsgAccAdminNo.Text));
            try
            {
                conn.Open();
                reader = cmd.ExecuteReader();
                int ordinalFName = reader.GetOrdinal("FName");
                int ordinalMName = reader.GetOrdinal("MName");
                int ordinalLName = reader.GetOrdinal("LName");
                int ordinalForm = reader.GetOrdinal("Form");
                int ordinalHouse = reader.GetOrdinal("House");

                while (reader.Read())
                {
                    string FName = reader.GetString(ordinalFName);
                    string MName = reader.GetString(ordinalMName);
                    string LName = reader.GetString(ordinalLName);
                    string Form = reader.GetString(ordinalForm);
                    string House = reader.GetString(ordinalHouse);

                    txtAsgAccFName.Text = FName;
                    txtAsgAccMName.Text = MName;
                    txtAsgAccLName.Text = LName;
                    txtAsgAccForm.Text = Form;
                    txtAsgAccHouse.Text = House;
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Error);
            }
            finally
            {
                conn.Close();
            }
        }

        private void LoadAssignedAccessories() 
        {
            string query = "Load_Student_Accessories_Procedure"; 
            conn = new SqlConnection(connectionString);
            cmd = new SqlCommand(query, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter dAdapter = new SqlDataAdapter(cmd);
            DataTable AssignedAccessories = new DataTable();
            //fill
            dAdapter.Fill(AssignedAccessories);
            //sync
            BindingSource bSource = new BindingSource();
            bSource.DataSource = AssignedAccessories;
            dgvAssignAccessory.DataSource = bSource;
        }

        void fillcboAccCode() 
        {
            cboAsgAccCode.Items.Clear();
            cboAsgAccCode.Text = null;
            conn = new SqlConnection(connectionString);
            string sqlQuery = "SELECT [Accessory_Code] FROM [dbo].[Accessory_Details]";
            cmd = new SqlCommand(sqlQuery, conn);

            try
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    string temp = reader.GetString(0);
                    cboAsgAccCode.Items.Add(temp);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }  
        }

        void fillcboAdminNo()
        {
            conn = new SqlConnection(connectionString);
            string sqlQuery = "SELECT [Admin_No] FROM [dbo].[Student_Profile]";
            SqlCommand cmd = new SqlCommand(sqlQuery, conn);
            try
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    int temp = reader.GetInt32(0);
                    cboAsgAccAdminNo.Items.Add(temp);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }  
        }

        private void lblAccCondition_Click(object sender, EventArgs e)
        {

        }

        private void cboAccSize_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void frmAssignAccessory_Load(object sender, EventArgs e)
        {

        }

        private void cboAsgBkAdminNo_SelectedIndexChanged(object sender, EventArgs e)
        {

        }              

        private void btnAsgAccSubmit_Click(object sender, EventArgs e)
        {
            
        }

        private void txtAsgAccCode_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnAsgAccSubmitAcc_Click(object sender, EventArgs e)
        {
     
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void gbxAsgAccSelectAcc_Enter(object sender, EventArgs e)
        {

        }       

        private void btnAsgAccSubmitAcc_Click_1(object sender, EventArgs e)
        {
            assignAccessory();           
        }

        private void cboAsgAccAdminNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboAsgAccAdminNo.Text != null)
            {
                fillStdtxt();
                btnAsgAccNext.Enabled = true;
            }
        }

        private void btnAsgAccNext_Click(object sender, EventArgs e)
        {
            gbxAsgAccSelectAcc.Enabled = true;
            fillcboAccCode();
        }

        private void cboAsgAccCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboAsgAccCode.Text != null)
            {
                fillAcctxt();
                btnAsgAccSubmitAcc.Enabled = true;
            }
        }

        private void btnAsgAccDone_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void gbxAsgAccSelectAcc_Enter_1(object sender, EventArgs e)
        {

        }

        private void disable() { btnNewAsgAcc.Enabled = false; }

        private void addnew()
        {
            gbxAsgAccSelectstd.Enabled = true;
            //clear student
            txtAsgAccForm.Clear();
            txtAsgAccHouse.Clear();
            txtAsgAccFName.Clear();
            txtAsgAccLName.Clear();
            txtAsgAccMName.Clear();
            //clear inst
            cboAsgAccCode.SelectedItem = null;
            cboAsgAccCode.Text = null;
            txtAsgAccType.Clear();
            txtAsgAccCondition.Clear();
            txtAsgAccCondition.Clear();
            txtAsgAccSize.Clear();
            cboAsgAccAdminNo.Focus();
            //disable            
            disable();
            gbxAsgAccSelectAcc.Enabled = false;
            btnAsgAccSubmitAcc.Enabled = false;
            btnAsgAccNext.Enabled = false;
            btnNewAsgAcc.Enabled = false;
        }

        private void btnNewAsgAcc_Click(object sender, EventArgs e)
        {
            addnew();
        }
    }
}
