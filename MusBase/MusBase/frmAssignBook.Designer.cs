﻿namespace MusBase
{
    partial class frmAssignBook
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvAssignBooks = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnNewAsgBk = new System.Windows.Forms.Button();
            this.btnAsgBkNext = new System.Windows.Forms.Button();
            this.gbxAsgBkSelectBk = new System.Windows.Forms.GroupBox();
            this.txtAsgBkCondition = new System.Windows.Forms.TextBox();
            this.txtAsgBkType = new System.Windows.Forms.TextBox();
            this.cboAsgBkCode = new System.Windows.Forms.ComboBox();
            this.lblAsgBkType = new System.Windows.Forms.Label();
            this.lblAsgBkCondition = new System.Windows.Forms.Label();
            this.lblAsgBkCode = new System.Windows.Forms.Label();
            this.txtAsgBkTitle = new System.Windows.Forms.TextBox();
            this.lblAsgBkTitle = new System.Windows.Forms.Label();
            this.btnAsgBkSubmitBk = new System.Windows.Forms.Button();
            this.gbxAsgBkSelectStd = new System.Windows.Forms.GroupBox();
            this.txtAsgBkForm = new System.Windows.Forms.TextBox();
            this.txtAsgBkHouse = new System.Windows.Forms.TextBox();
            this.cboAsgBkAdminNo = new System.Windows.Forms.ComboBox();
            this.txtAsgBkMName = new System.Windows.Forms.TextBox();
            this.txtAsgBkLName = new System.Windows.Forms.TextBox();
            this.lblAsgAccForm = new System.Windows.Forms.Label();
            this.lblAsgAccHouse = new System.Windows.Forms.Label();
            this.lblAsgAccAdminNo = new System.Windows.Forms.Label();
            this.txtAsgBkFName = new System.Windows.Forms.TextBox();
            this.lblAsgAccMName = new System.Windows.Forms.Label();
            this.lblAsgAccLName = new System.Windows.Forms.Label();
            this.lblAsgAccFName = new System.Windows.Forms.Label();
            this.btnAsgBkDone = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAssignBooks)).BeginInit();
            this.panel1.SuspendLayout();
            this.gbxAsgBkSelectBk.SuspendLayout();
            this.gbxAsgBkSelectStd.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvAssignBooks
            // 
            this.dgvAssignBooks.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvAssignBooks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAssignBooks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAssignBooks.Location = new System.Drawing.Point(0, 0);
            this.dgvAssignBooks.Name = "dgvAssignBooks";
            this.dgvAssignBooks.ReadOnly = true;
            this.dgvAssignBooks.Size = new System.Drawing.Size(844, 198);
            this.dgvAssignBooks.TabIndex = 122;
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel1.Controls.Add(this.dgvAssignBooks);
            this.panel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(21, 346);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(844, 198);
            this.panel1.TabIndex = 188;
            // 
            // btnNewAsgBk
            // 
            this.btnNewAsgBk.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNewAsgBk.Image = global::MusBase.Properties.Resources.book_add_icon;
            this.btnNewAsgBk.Location = new System.Drawing.Point(350, 236);
            this.btnNewAsgBk.Name = "btnNewAsgBk";
            this.btnNewAsgBk.Size = new System.Drawing.Size(104, 42);
            this.btnNewAsgBk.TabIndex = 191;
            this.btnNewAsgBk.Text = "&NEW";
            this.btnNewAsgBk.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnNewAsgBk.UseVisualStyleBackColor = true;
            this.btnNewAsgBk.Click += new System.EventHandler(this.btnNewAsgBk_Click_1);
            // 
            // btnAsgBkNext
            // 
            this.btnAsgBkNext.BackgroundImage = global::MusBase.Properties.Resources.next;
            this.btnAsgBkNext.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnAsgBkNext.Font = new System.Drawing.Font("Tempus Sans ITC", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAsgBkNext.Location = new System.Drawing.Point(362, 34);
            this.btnAsgBkNext.Name = "btnAsgBkNext";
            this.btnAsgBkNext.Size = new System.Drawing.Size(75, 36);
            this.btnAsgBkNext.TabIndex = 190;
            this.btnAsgBkNext.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnAsgBkNext.UseVisualStyleBackColor = true;
            this.btnAsgBkNext.Click += new System.EventHandler(this.btnAsgBkNext_Click);
            // 
            // gbxAsgBkSelectBk
            // 
            this.gbxAsgBkSelectBk.Controls.Add(this.txtAsgBkCondition);
            this.gbxAsgBkSelectBk.Controls.Add(this.txtAsgBkType);
            this.gbxAsgBkSelectBk.Controls.Add(this.cboAsgBkCode);
            this.gbxAsgBkSelectBk.Controls.Add(this.lblAsgBkType);
            this.gbxAsgBkSelectBk.Controls.Add(this.lblAsgBkCondition);
            this.gbxAsgBkSelectBk.Controls.Add(this.lblAsgBkCode);
            this.gbxAsgBkSelectBk.Controls.Add(this.txtAsgBkTitle);
            this.gbxAsgBkSelectBk.Controls.Add(this.lblAsgBkTitle);
            this.gbxAsgBkSelectBk.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxAsgBkSelectBk.Location = new System.Drawing.Point(478, 12);
            this.gbxAsgBkSelectBk.Name = "gbxAsgBkSelectBk";
            this.gbxAsgBkSelectBk.Size = new System.Drawing.Size(358, 286);
            this.gbxAsgBkSelectBk.TabIndex = 192;
            this.gbxAsgBkSelectBk.TabStop = false;
            this.gbxAsgBkSelectBk.Text = "Select a Book";
            // 
            // txtAsgBkCondition
            // 
            this.txtAsgBkCondition.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAsgBkCondition.Location = new System.Drawing.Point(92, 154);
            this.txtAsgBkCondition.Name = "txtAsgBkCondition";
            this.txtAsgBkCondition.ReadOnly = true;
            this.txtAsgBkCondition.Size = new System.Drawing.Size(120, 28);
            this.txtAsgBkCondition.TabIndex = 152;
            // 
            // txtAsgBkType
            // 
            this.txtAsgBkType.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAsgBkType.Location = new System.Drawing.Point(92, 66);
            this.txtAsgBkType.Name = "txtAsgBkType";
            this.txtAsgBkType.ReadOnly = true;
            this.txtAsgBkType.Size = new System.Drawing.Size(120, 28);
            this.txtAsgBkType.TabIndex = 151;
            // 
            // cboAsgBkCode
            // 
            this.cboAsgBkCode.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cboAsgBkCode.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboAsgBkCode.FormattingEnabled = true;
            this.cboAsgBkCode.Location = new System.Drawing.Point(91, 22);
            this.cboAsgBkCode.Name = "cboAsgBkCode";
            this.cboAsgBkCode.Size = new System.Drawing.Size(121, 28);
            this.cboAsgBkCode.TabIndex = 3;
            this.cboAsgBkCode.SelectedIndexChanged += new System.EventHandler(this.cboAsgBkCode_SelectedIndexChanged_1);
            // 
            // lblAsgBkType
            // 
            this.lblAsgBkType.AutoSize = true;
            this.lblAsgBkType.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAsgBkType.Location = new System.Drawing.Point(6, 66);
            this.lblAsgBkType.Name = "lblAsgBkType";
            this.lblAsgBkType.Size = new System.Drawing.Size(77, 17);
            this.lblAsgBkType.TabIndex = 149;
            this.lblAsgBkType.Text = "Book Type";
            // 
            // lblAsgBkCondition
            // 
            this.lblAsgBkCondition.AutoSize = true;
            this.lblAsgBkCondition.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAsgBkCondition.Location = new System.Drawing.Point(12, 154);
            this.lblAsgBkCondition.Name = "lblAsgBkCondition";
            this.lblAsgBkCondition.Size = new System.Drawing.Size(73, 17);
            this.lblAsgBkCondition.TabIndex = 148;
            this.lblAsgBkCondition.Text = "Condition";
            // 
            // lblAsgBkCode
            // 
            this.lblAsgBkCode.AutoSize = true;
            this.lblAsgBkCode.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAsgBkCode.Location = new System.Drawing.Point(6, 22);
            this.lblAsgBkCode.Name = "lblAsgBkCode";
            this.lblAsgBkCode.Size = new System.Drawing.Size(79, 17);
            this.lblAsgBkCode.TabIndex = 147;
            this.lblAsgBkCode.Text = "Book Code";
            // 
            // txtAsgBkTitle
            // 
            this.txtAsgBkTitle.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAsgBkTitle.Location = new System.Drawing.Point(91, 106);
            this.txtAsgBkTitle.Multiline = true;
            this.txtAsgBkTitle.Name = "txtAsgBkTitle";
            this.txtAsgBkTitle.ReadOnly = true;
            this.txtAsgBkTitle.Size = new System.Drawing.Size(260, 37);
            this.txtAsgBkTitle.TabIndex = 146;
            // 
            // lblAsgBkTitle
            // 
            this.lblAsgBkTitle.AutoSize = true;
            this.lblAsgBkTitle.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAsgBkTitle.Location = new System.Drawing.Point(39, 111);
            this.lblAsgBkTitle.Name = "lblAsgBkTitle";
            this.lblAsgBkTitle.Size = new System.Drawing.Size(37, 17);
            this.lblAsgBkTitle.TabIndex = 145;
            this.lblAsgBkTitle.Text = "Title";
            // 
            // btnAsgBkSubmitBk
            // 
            this.btnAsgBkSubmitBk.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAsgBkSubmitBk.Image = global::MusBase.Properties.Resources.Book_Blank_Book_icon;
            this.btnAsgBkSubmitBk.Location = new System.Drawing.Point(350, 93);
            this.btnAsgBkSubmitBk.Name = "btnAsgBkSubmitBk";
            this.btnAsgBkSubmitBk.Size = new System.Drawing.Size(104, 42);
            this.btnAsgBkSubmitBk.TabIndex = 4;
            this.btnAsgBkSubmitBk.Text = "&Submit";
            this.btnAsgBkSubmitBk.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnAsgBkSubmitBk.UseVisualStyleBackColor = true;
            this.btnAsgBkSubmitBk.Click += new System.EventHandler(this.btnAsgBkSubmitBk_Click_1);
            // 
            // gbxAsgBkSelectStd
            // 
            this.gbxAsgBkSelectStd.Controls.Add(this.txtAsgBkForm);
            this.gbxAsgBkSelectStd.Controls.Add(this.txtAsgBkHouse);
            this.gbxAsgBkSelectStd.Controls.Add(this.cboAsgBkAdminNo);
            this.gbxAsgBkSelectStd.Controls.Add(this.txtAsgBkMName);
            this.gbxAsgBkSelectStd.Controls.Add(this.txtAsgBkLName);
            this.gbxAsgBkSelectStd.Controls.Add(this.lblAsgAccForm);
            this.gbxAsgBkSelectStd.Controls.Add(this.lblAsgAccHouse);
            this.gbxAsgBkSelectStd.Controls.Add(this.lblAsgAccAdminNo);
            this.gbxAsgBkSelectStd.Controls.Add(this.txtAsgBkFName);
            this.gbxAsgBkSelectStd.Controls.Add(this.lblAsgAccMName);
            this.gbxAsgBkSelectStd.Controls.Add(this.lblAsgAccLName);
            this.gbxAsgBkSelectStd.Controls.Add(this.lblAsgAccFName);
            this.gbxAsgBkSelectStd.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxAsgBkSelectStd.Location = new System.Drawing.Point(41, 12);
            this.gbxAsgBkSelectStd.Name = "gbxAsgBkSelectStd";
            this.gbxAsgBkSelectStd.Size = new System.Drawing.Size(279, 286);
            this.gbxAsgBkSelectStd.TabIndex = 189;
            this.gbxAsgBkSelectStd.TabStop = false;
            this.gbxAsgBkSelectStd.Text = "Select a Student";
            // 
            // txtAsgBkForm
            // 
            this.txtAsgBkForm.Location = new System.Drawing.Point(106, 238);
            this.txtAsgBkForm.Name = "txtAsgBkForm";
            this.txtAsgBkForm.ReadOnly = true;
            this.txtAsgBkForm.Size = new System.Drawing.Size(57, 28);
            this.txtAsgBkForm.TabIndex = 164;
            // 
            // txtAsgBkHouse
            // 
            this.txtAsgBkHouse.Location = new System.Drawing.Point(106, 197);
            this.txtAsgBkHouse.Name = "txtAsgBkHouse";
            this.txtAsgBkHouse.ReadOnly = true;
            this.txtAsgBkHouse.Size = new System.Drawing.Size(121, 28);
            this.txtAsgBkHouse.TabIndex = 130;
            // 
            // cboAsgBkAdminNo
            // 
            this.cboAsgBkAdminNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboAsgBkAdminNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboAsgBkAdminNo.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cboAsgBkAdminNo.FormattingEnabled = true;
            this.cboAsgBkAdminNo.Location = new System.Drawing.Point(106, 22);
            this.cboAsgBkAdminNo.Name = "cboAsgBkAdminNo";
            this.cboAsgBkAdminNo.Size = new System.Drawing.Size(121, 28);
            this.cboAsgBkAdminNo.TabIndex = 1;
            this.cboAsgBkAdminNo.SelectedIndexChanged += new System.EventHandler(this.cboAsgBkAdminNo_SelectedIndexChanged_1);
            // 
            // txtAsgBkMName
            // 
            this.txtAsgBkMName.Location = new System.Drawing.Point(106, 111);
            this.txtAsgBkMName.Name = "txtAsgBkMName";
            this.txtAsgBkMName.ReadOnly = true;
            this.txtAsgBkMName.Size = new System.Drawing.Size(121, 28);
            this.txtAsgBkMName.TabIndex = 124;
            // 
            // txtAsgBkLName
            // 
            this.txtAsgBkLName.Location = new System.Drawing.Point(106, 154);
            this.txtAsgBkLName.Name = "txtAsgBkLName";
            this.txtAsgBkLName.ReadOnly = true;
            this.txtAsgBkLName.Size = new System.Drawing.Size(121, 28);
            this.txtAsgBkLName.TabIndex = 122;
            // 
            // lblAsgAccForm
            // 
            this.lblAsgAccForm.AutoSize = true;
            this.lblAsgAccForm.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAsgAccForm.Location = new System.Drawing.Point(42, 238);
            this.lblAsgAccForm.Name = "lblAsgAccForm";
            this.lblAsgAccForm.Size = new System.Drawing.Size(42, 17);
            this.lblAsgAccForm.TabIndex = 163;
            this.lblAsgAccForm.Text = "Form";
            // 
            // lblAsgAccHouse
            // 
            this.lblAsgAccHouse.AutoSize = true;
            this.lblAsgAccHouse.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAsgAccHouse.Location = new System.Drawing.Point(36, 197);
            this.lblAsgAccHouse.Name = "lblAsgAccHouse";
            this.lblAsgAccHouse.Size = new System.Drawing.Size(48, 17);
            this.lblAsgAccHouse.TabIndex = 162;
            this.lblAsgAccHouse.Text = "House";
            // 
            // lblAsgAccAdminNo
            // 
            this.lblAsgAccAdminNo.AutoSize = true;
            this.lblAsgAccAdminNo.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAsgAccAdminNo.Location = new System.Drawing.Point(11, 22);
            this.lblAsgAccAdminNo.Name = "lblAsgAccAdminNo";
            this.lblAsgAccAdminNo.Size = new System.Drawing.Size(76, 17);
            this.lblAsgAccAdminNo.TabIndex = 161;
            this.lblAsgAccAdminNo.Text = "Admin No";
            // 
            // txtAsgBkFName
            // 
            this.txtAsgBkFName.Location = new System.Drawing.Point(106, 66);
            this.txtAsgBkFName.Name = "txtAsgBkFName";
            this.txtAsgBkFName.ReadOnly = true;
            this.txtAsgBkFName.Size = new System.Drawing.Size(121, 28);
            this.txtAsgBkFName.TabIndex = 121;
            // 
            // lblAsgAccMName
            // 
            this.lblAsgAccMName.AutoSize = true;
            this.lblAsgAccMName.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAsgAccMName.Location = new System.Drawing.Point(6, 111);
            this.lblAsgAccMName.Name = "lblAsgAccMName";
            this.lblAsgAccMName.Size = new System.Drawing.Size(94, 17);
            this.lblAsgAccMName.TabIndex = 159;
            this.lblAsgAccMName.Text = "Middle Name";
            // 
            // lblAsgAccLName
            // 
            this.lblAsgAccLName.AutoSize = true;
            this.lblAsgAccLName.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAsgAccLName.Location = new System.Drawing.Point(9, 154);
            this.lblAsgAccLName.Name = "lblAsgAccLName";
            this.lblAsgAccLName.Size = new System.Drawing.Size(75, 17);
            this.lblAsgAccLName.TabIndex = 156;
            this.lblAsgAccLName.Text = "Last Name";
            // 
            // lblAsgAccFName
            // 
            this.lblAsgAccFName.AutoSize = true;
            this.lblAsgAccFName.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAsgAccFName.Location = new System.Drawing.Point(9, 66);
            this.lblAsgAccFName.Name = "lblAsgAccFName";
            this.lblAsgAccFName.Size = new System.Drawing.Size(78, 17);
            this.lblAsgAccFName.TabIndex = 155;
            this.lblAsgAccFName.Text = "First Name";
            // 
            // btnAsgBkDone
            // 
            this.btnAsgBkDone.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAsgBkDone.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.btnAsgBkDone.Image = global::MusBase.Properties.Resources._1427990866_Done;
            this.btnAsgBkDone.Location = new System.Drawing.Point(350, 166);
            this.btnAsgBkDone.Name = "btnAsgBkDone";
            this.btnAsgBkDone.Size = new System.Drawing.Size(104, 44);
            this.btnAsgBkDone.TabIndex = 193;
            this.btnAsgBkDone.Text = "&Quit";
            this.btnAsgBkDone.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnAsgBkDone.UseVisualStyleBackColor = true;
            this.btnAsgBkDone.Click += new System.EventHandler(this.btnAsgBkDone_Click_1);
            // 
            // frmAssignBook
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(877, 587);
            this.Controls.Add(this.btnAsgBkDone);
            this.Controls.Add(this.btnNewAsgBk);
            this.Controls.Add(this.btnAsgBkNext);
            this.Controls.Add(this.btnAsgBkSubmitBk);
            this.Controls.Add(this.gbxAsgBkSelectBk);
            this.Controls.Add(this.gbxAsgBkSelectStd);
            this.Controls.Add(this.panel1);
            this.ForeColor = System.Drawing.Color.RoyalBlue;
            this.MaximizeBox = false;
            this.Name = "frmAssignBook";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Assign Book";
            this.Load += new System.EventHandler(this.frmAssignBook_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAssignBooks)).EndInit();
            this.panel1.ResumeLayout(false);
            this.gbxAsgBkSelectBk.ResumeLayout(false);
            this.gbxAsgBkSelectBk.PerformLayout();
            this.gbxAsgBkSelectStd.ResumeLayout(false);
            this.gbxAsgBkSelectStd.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvAssignBooks;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnNewAsgBk;
        private System.Windows.Forms.Button btnAsgBkNext;
        private System.Windows.Forms.GroupBox gbxAsgBkSelectBk;
        private System.Windows.Forms.TextBox txtAsgBkCondition;
        private System.Windows.Forms.TextBox txtAsgBkType;
        private System.Windows.Forms.ComboBox cboAsgBkCode;
        private System.Windows.Forms.Button btnAsgBkSubmitBk;
        private System.Windows.Forms.Label lblAsgBkType;
        private System.Windows.Forms.Label lblAsgBkCondition;
        private System.Windows.Forms.Label lblAsgBkCode;
        private System.Windows.Forms.TextBox txtAsgBkTitle;
        private System.Windows.Forms.Label lblAsgBkTitle;
        private System.Windows.Forms.GroupBox gbxAsgBkSelectStd;
        private System.Windows.Forms.TextBox txtAsgBkForm;
        private System.Windows.Forms.TextBox txtAsgBkHouse;
        private System.Windows.Forms.ComboBox cboAsgBkAdminNo;
        private System.Windows.Forms.TextBox txtAsgBkMName;
        private System.Windows.Forms.TextBox txtAsgBkLName;
        private System.Windows.Forms.Label lblAsgAccForm;
        private System.Windows.Forms.Label lblAsgAccHouse;
        private System.Windows.Forms.Label lblAsgAccAdminNo;
        private System.Windows.Forms.TextBox txtAsgBkFName;
        private System.Windows.Forms.Label lblAsgAccMName;
        private System.Windows.Forms.Label lblAsgAccLName;
        private System.Windows.Forms.Label lblAsgAccFName;
        private System.Windows.Forms.Button btnAsgBkDone;
    }
}