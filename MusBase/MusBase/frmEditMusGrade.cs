﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace MusBase
{
    public partial class frmEditMusGrade : Form
    {        
        private string connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;

        SqlConnection conn;
        SqlCommand cmd;

        public frmEditMusGrade()
        {
            InitializeComponent();
            startState();
            LoadStudentGrades();
            fillAdminNo();
        }

        public void fillAdminNo()
        {
            cboAdmin.Items.Clear();
            conn = new SqlConnection(connectionString);
            string query = "Load_Grade_Admin_Procedure";
            cmd = new SqlCommand(query, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                int ordinal = reader.GetOrdinal("Admin_No");
                while (reader.Read())
                {
                    int temp = reader.GetInt32(ordinal);
                    cboAdmin.Items.Add(temp);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        private void LoadStudentGradesAdmin()
        {
            string query = "SELECT Grade_Table.Admin_No AS [ADMIN NO], Student_Profile.FName AS [FIRST NAME], Student_Profile.LName As [LAST NAME],"+
		"Student_Profile.Form AS [FORM], Grade_Table.Paper1 AS [Pp 1], Grade_Table.Paper2 AS [Pp 2], Grade_Table.Paper3 AS [Pp 3]," +
		"Grade_Table.Average AS [AVG], Grade_Table.GradeAttained AS [GRADE], Grade_Table.Descprition AS [DESC], " +
		"Grade_Table.DateDone AS [DATE], Grade_Table.Term As [TERM] FROM Grade_Table   INNER JOIN Student_Profile ON "+
        "Grade_Table.Admin_No = Student_Profile.Admin_No WHERE Grade_Table.Admin_No = @adm ORDER BY Student_Profile.Form DESC";
            conn = new SqlConnection(connectionString);
            cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@adm", cboAdmin.Text);

            try
            {
                SqlDataAdapter dAdapter = new SqlDataAdapter(cmd);
                DataTable grade = new DataTable();
                //fill
                dAdapter.Fill(grade);
                //sync
                BindingSource bsource = new BindingSource();
                bsource.DataSource = grade;
                dgvGrading.DataSource = bsource;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Error);
            }
        }

        public void LoadStudentGrades()
        {
            string query = "Load_Grades_Procedure";
            conn = new SqlConnection(connectionString);
            cmd = new SqlCommand(query, conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                SqlDataAdapter dAdapter = new SqlDataAdapter(cmd);
                DataTable grade = new DataTable();
                //fill
                dAdapter.Fill(grade);
                //sync
                BindingSource bsource = new BindingSource();
                bsource.DataSource = grade;
                dgvGrading.DataSource = bsource;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Error);
            }
        }

        private void frmEditMusGrade_Load(object sender, EventArgs e)
        {
            
        }    
    
        private void fill()
        {
            string query = "SELECT * FROM Grade_Table WHERE Admin_No = @adm";
            conn = new SqlConnection(connectionString);
            cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@adm", cboAdmin.Text);

            try 
            {
                conn.Open();
                SqlDataReader reader =  cmd.ExecuteReader();
                int pp1ordinal = reader.GetOrdinal("Paper1");
                int pp2ordinal = reader.GetOrdinal("Paper2");
                int pp3ordinal = reader.GetOrdinal("Paper3");
                int ordianlTerm = reader.GetOrdinal("Term");
                int ordinalDate = reader.GetOrdinal("DateDone");
                int ordinalDesc = reader.GetOrdinal("Descprition");

                while (reader.Read())
                {
                    int pp1 = reader.GetInt32(pp1ordinal);
                    int pp2 = reader.GetInt32(pp2ordinal);
                    int pp3 = reader.GetInt32(pp3ordinal);

                    string term = reader.GetString(ordianlTerm);
                    DateTime dat = reader.GetDateTime(ordinalDate);               
                    string desc = reader.GetString(ordinalDesc);

                    txtPaper1.Text = Convert.ToString(pp1);
                    txtPaper2.Text = Convert.ToString(pp2);
                    txtPaper3.Text = Convert.ToString(pp3);
                    txtTerm.Text = term;
                    txtDesc.Text = desc;                    
                    dateTimePicker1.Value = dat;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                conn.Close();
            }
        }

        private void enable()
        {
            btnUpdate.Enabled = true;
            btnDelete.Enabled = true;

            txtDesc.Enabled = true;
            txtPaper1.Enabled = true;
            txtPaper2.Enabled = true;
            txtPaper3.Enabled = true;
            txtTerm.Enabled = true;
            txtDesc.Enabled = true;
            dateTimePicker1.Enabled = true;
        }

        private void startState()
        {            
            btnUpdate.Enabled = false;
            btnDelete.Enabled = false;

            txtDesc.Enabled = false;
            txtPaper1.Enabled = false;
            txtPaper2.Enabled = false;
            txtPaper3.Enabled = false;
            txtTerm.Enabled = false;
            txtDesc.Enabled = false;
            dateTimePicker1.Enabled = false;
        }

        private void cboAdmin_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboAdmin.Text != null)
            {
                fill();
                enable();
                LoadStudentGradesAdmin();
            }
        }

        private void txtPaper1_TextChanged(object sender, EventArgs e)
        {
            if (txtPaper1.Text != null)
            {
                btnUpdate.Enabled = true;
            }
        }

        private void txtPaper2_TextChanged(object sender, EventArgs e)
        {
            if (txtPaper2.Text != null)
            {
                btnUpdate.Enabled = true;
            }
        }

        private void txtPaper3_TextChanged(object sender, EventArgs e)
        {
            if (txtPaper3.Text != null)
            {
                btnUpdate.Enabled = true;
            }
        }

        private void cboTerm_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void txtDesc_TextChanged(object sender, EventArgs e)
        {
            if (txtDesc.Text != null)
            {
                btnUpdate.Enabled = true;
            }
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtPaper1.Clear();
            txtPaper2.Clear();
            txtPaper3.Clear();
            txtTerm.Clear();
            txtDesc.Clear();            

            cboAdmin.Text = null;
            cboAdmin.SelectedItem = null;

            LoadStudentGrades();

            Paper1errorProvider.SetError(txtPaper1, null);
            Paper2errorProvider.SetError(txtPaper2, null);
            Paper3errorProvider.SetError(txtPaper3, null);

            startState();
        }

        private string AssignGrade(double avg, string grade)
        {
            //assign grade
            if (avg >= 80 && avg <= 100)
            {
                grade = "A";
                return grade;
            }
            else if (avg <= 79 && avg >= 75)
            {
                grade = "A-";
                return grade;
            }
            else if (avg <= 74 && avg >=70)
            {
                grade = "B+";
                return grade;
            }
            else if (avg <= 69 && avg >= 65)
            {
                grade = "B";
                return grade;
            }
            else if (avg <= 64 && avg >= 60)
            {
                grade = "B-";
                return grade;
            }
            else if (avg <= 59 && avg >= 55)
            {
                grade = "C+";
                return grade;
            }
            else if (avg <= 54 && avg >= 50)
            {
                grade = "C";
                return grade;
            }
                else if (avg <= 49 && avg >= 45)
            {
                grade = "C-";
                return grade;
                }
            else if (avg <= 44 && avg >= 40) 
            {
                grade = "D+";
                return grade;
            }
                else if (avg <= 39 && avg >= 35)
            {
                grade = "D";
                return grade;
            }
                else if (avg <=34 && avg >= 30)
            {
                grade = "D-";
                return grade;
            }
            else if (avg >= 29 && avg > 0 )
            {
                grade = "E";
                return grade;
            }
            else
            {
                grade = "Incomplete";
                return grade;
            }
            
        }

        private void update() 
        {
            string query = "UPDATE Grade_Table SET Term = @term, Paper1 = @pp1, Paper2 = @pp2, Paper3 = @pp3, " +
                "Average = @avg, GradeAttained = @grd, DateDone = @date, Descprition = @desc " +
                "WHERE Admin_No = @adm ";

            string grade = " ";
            int pp1 = Convert.ToInt32(txtPaper1.Text);
            int pp2 = Convert.ToInt32(txtPaper2.Text);
            int pp3 = Convert.ToInt32(txtPaper3.Text);

            //calculate average
            double avg = (pp1 + pp2 + pp3) / 2;
            string mygrade = AssignGrade(avg, grade);            

            conn = new SqlConnection(connectionString);
            cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@pp1", pp1);
            cmd.Parameters.AddWithValue("@pp2", pp2);
            cmd.Parameters.AddWithValue("@pp3", pp3);
            cmd.Parameters.AddWithValue("@avg", avg);
            cmd.Parameters.AddWithValue("@grd", mygrade);
            cmd.Parameters.AddWithValue("@date", dateTimePicker1.Value.ToShortDateString());
            cmd.Parameters.AddWithValue("@desc", txtDesc.Text);
            cmd.Parameters.AddWithValue("@adm", cboAdmin.Text);
            cmd.Parameters.AddWithValue("@term", txtTerm.Text);

            try
            {

                conn.Open();
                int TotalRowsAffected = cmd.ExecuteNonQuery();
                if (TotalRowsAffected == 1)
                {
                    //lblStatus.Text = "Record Added!";
                    LoadStudentGradesAdmin();
                    startState();
                }
                else
                {
                    //lblStatus.Text = "Insert Unsuccessful.";
                }
            }            
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                conn.Close();
            }

        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            update();
        }

        private void txtPaper1_Validating(object sender, CancelEventArgs e)
        {
            int PaperOneMark;
            if (int.TryParse(txtPaper1.Text, out PaperOneMark))
            {
                //marks are numeric
                if (PaperOneMark < 0 || PaperOneMark > 50)
                {
                    Paper1errorProvider.SetError(txtPaper1, "Paper 1 marks cannot be greater than 50 or less than 0.");
                    txtPaper1.Clear();
                    return;
                }
                else
                {
                    Paper1errorProvider.SetError(txtPaper1, null);
                }
            }
            else
            {
                //marks are not numeric
                Paper1errorProvider.SetError(txtPaper1, "Invalid Marks");
                txtPaper1.Clear();
                return;
            }
        }

        private void txtPaper2_Validating(object sender, CancelEventArgs e)
        {
            int PapertwoMark;
            if (int.TryParse(txtPaper2.Text, out PapertwoMark))
            {
                //marks are numeric
                if (PapertwoMark < 0 || PapertwoMark > 50)
                {
                    Paper2errorProvider.SetError(txtPaper2, "Paper 2 marks cannot be greater than 50 or less than 0.");
                    txtPaper2.Clear();
                    return;
                }
                else
                {
                    Paper2errorProvider.SetError(txtPaper2, null);
                }
            }
            else
            {
                //marks are not numeric
                Paper2errorProvider.SetError(txtPaper2, "Invalid Marks");
                txtPaper2.Clear();
                return;
            }
        }

        private void txtPaper3_Validating(object sender, CancelEventArgs e)
        {
            int PaperThreeMark;
            if (int.TryParse(txtPaper3.Text, out PaperThreeMark))
            {
                //marks are numeric
                if (PaperThreeMark < 0 || PaperThreeMark > 100)
                {
                    Paper3errorProvider.SetError(txtPaper3, "Paper 3 marks cannot be greater than 100 or less than 0.");
                    txtPaper3.Clear();
                    return;
                }
                else
                {
                    Paper3errorProvider.SetError(txtPaper3, null);
                }
            }
            else
            {
                //marks are not numeric
                Paper3errorProvider.SetError(txtPaper3, "Invalid Marks");
                return;
            }
        }
        private void deleteGrade()
        {
            string query = "DELETE  FROM  Grade_Table WHERE Admin_No = @adm AND Term = @term AND DateDone = @dat";
            conn = new SqlConnection(connectionString);
            cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@adm", cboAdmin.Text);
            cmd.Parameters.AddWithValue("@term", txtTerm.Text);
            cmd.Parameters.AddWithValue("@dat", dateTimePicker1.Value.ToShortDateString());
            try
            {

                conn.Open();
                int TotalRowsAffected = cmd.ExecuteNonQuery();
                if (TotalRowsAffected == 1)
                {
                    MessageBox.Show("Record deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    LoadStudentGradesAdmin();
                    startState();
                }
                else
                {
                    DialogResult rst =  MessageBox.Show("Error\nDelete Unsuccessfull.", "Error", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error);
                    if (rst == System.Windows.Forms.DialogResult.Retry)
                        deleteGrade();
                    else if (rst == System.Windows.Forms.DialogResult.Cancel)
                    {
                        return;
                    }                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                conn.Close();
            }
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure you want to delete this record from MusBase?", "Confirm Deletion", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == System.Windows.Forms.DialogResult.Yes)
            { deleteGrade(); }
            else if (result == System.Windows.Forms.DialogResult.No)
                return;
        }

        private void dgvGrading_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {

                DataGridViewRow rw = this.dgvGrading.Rows[e.RowIndex];

                txtPaper1.Text = rw.Cells["Pp 1"].Value.ToString();
                txtPaper2.Text = rw.Cells["Pp 2"].Value.ToString();
                txtPaper3.Text = rw.Cells["Pp 3"].Value.ToString();
                txtDesc.Text = rw.Cells["DESC"].Value.ToString();
                txtTerm.Text = rw.Cells["TERM"].Value.ToString();
                string dt = rw.Cells["DATE"].Value.ToString();
                cboAdmin.Text = rw.Cells["ADMIN NO"].Value.ToString();

                dateTimePicker1.Value = Convert.ToDateTime(dt);
                enable();
            }
        }
    }
}
