﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MusBase
{
    public partial class frmrptAssignedBooks : Form
    {
        public frmrptAssignedBooks()
        {
            InitializeComponent();
        }

        private void frmrptAssignedBooks_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'DataSet1.Assigned_Books_View' table. You can move, or remove it, as needed.
            this.Assigned_Books_ViewTableAdapter.Fill(this.DataSet1.Assigned_Books_View);

            this.reportViewer1.RefreshReport();
        }
    }
}
