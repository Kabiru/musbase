﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace MusBase
{
    public partial class frmAddBook : Form
    {
        private string connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;
        private string SqlQuery;
        private SqlConnection conn;
        private SqlCommand cmd;
        private SqlDataReader reader;
        SqlDataAdapter dAdapter;
        SqlCommandBuilder cmdbld;
        DataTable bookTable;

        public frmAddBook()
        {
            InitializeComponent();
            LoadBooks();
            //fill  book type
            fillcboBkType();
            btnAddNewBook.Enabled = false;
            btnUpdate.Enabled = false;
            lblInsertStatus.Text = null;
            //disable save btn
            btnBkSave.Enabled = false;
        }

        public bool IsTextBoxFull()
        {
            bool state = false;

            if (cboBkType.Text != null && txtBkCode.Text != null && txtBkTitle.Text != null
                && txtBookCondition.Text != null)
            {
                state = true;
                return state;
            }
            else
            {
                state = false;
                return state;
            }
        }

        private void editstate() 
        {
            gbxBookDetails.Enabled = false;
            btnAddNewBook.Enabled = true;
        }

        private void LoadBooks() 
        {
            SqlQuery = "SELECT * FROM [dbo].[Book_Details]";
            conn = new SqlConnection(connectionString);
            bookTable = new DataTable();
            try 
            {
                dAdapter = new SqlDataAdapter(SqlQuery, conn);
                //fill datatable with data from dataadapter
                dAdapter.Fill(bookTable);
                //binding the datatable to the datagrid view
                BindingSource bSource = new BindingSource();
                bSource.DataSource = bookTable;
                dgvBookDetails.DataSource = bSource;               

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }            
        }

        private void fillcboBkType()
        {            
            conn = new SqlConnection(connectionString);
            SqlQuery = "SELECT * FROM [dbo].[Book_Type]";
            cmd = new SqlCommand(SqlQuery, conn);

            try
            {
                conn.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    string BookType = reader.GetString(1);
                    cboBkType.Items.Add(BookType);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n"+ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                reader.Close();
                conn.Close();
            }
        }     

        private void btnBkAssign_Click(object sender, EventArgs e)
        {

        }

        private void btnBkExit_Click(object sender, EventArgs e)
        {

        }

        private void btnBkClose_Click(object sender, EventArgs e)
        {
            this.Close();//closing form
        }

        

        private void frmAddBook_Load(object sender, EventArgs e)
        {

        }

        private void cboBkCondition_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnBkSave_Click(object sender, EventArgs e)
        {         
           
        }

        private void btnBkClear_Click(object sender, EventArgs e)
        {
            txtBookCondition.Clear();
            txtBkTitle.Clear();
            txtBkCode.Clear();
        }

        private void btnAddNewBook_Click(object sender, EventArgs e)
        {
            //reactivate gbx
            gbxBookDetails.Enabled = true;
            //clear
            txtBookCondition.Clear();
            txtBkTitle.Clear();
            txtBkCode.Clear();

            lblInsertStatus.Text = "Waiting ...";
            cboBkType.Focus();
        }

        private void txtBookCondition_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnBkSave.PerformClick();
            }
        }

        private void frmAddBook_Load_1(object sender, EventArgs e)
        {

        }

        private void btnBkSave_Click_1(object sender, EventArgs e)
        {
            //save            
            conn = new SqlConnection(connectionString);

            SqlQuery = "INSERT INTO [dbo].[Book_Details] ([Book_Code], [Book_Title], [Book_Condition], [Book_Type]) VALUES ('"+txtBkCode.Text+"', '"+txtBkTitle.Text+"', '"+txtBookCondition.Text+"', '"+cboBkType.Text+"')";

            cmd = new SqlCommand(SqlQuery, conn);

            try
            {
                conn.Open();
                //db connection open
                int TotalRowsAffected = cmd.ExecuteNonQuery();

                if (TotalRowsAffected == 1)
                {
                    lblInsertStatus.Text = "Record saved";

                    //adding the data to the datagridview                     
                    LoadBooks();
                    gbxBookDetails.Enabled = false;
                    btnAddNewBook.Enabled = true;

                }
                else if (TotalRowsAffected > 1)
                {
                    lblInsertStatus.Text = "Duplicate records added";
                }
                else
                {
                    lblInsertStatus.Text = "Insert incomplete";
                } 
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n"+ex.Message,"Error" ,MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                conn.Close();//closing the connection
            }
        }

        private void btnAddNewBook_Click_1(object sender, EventArgs e)
        {
            //Add new book
            gbxBookDetails.Enabled = true;

            txtBkCode.Clear();
            txtBkTitle.Clear();
            txtBookCondition.Clear();

            cboBkType.Focus();
        }

        private void btnBkClear_Click_1(object sender, EventArgs e)
        {
            //clear txts
            txtBkCode.Clear();
            txtBkTitle.Clear();
            txtBookCondition.Clear();

            cboBkType.Focus();
        }

        private void dgvBookDetails_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            btnUpdate.Enabled = true;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                cmdbld = new SqlCommandBuilder(dAdapter);
                dAdapter.Update(bookTable);
                MessageBox.Show("Information updated successfully into the database.", "Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvBookDetails_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            btnUpdate.Enabled = true;
        }

        private void cboBkType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (IsTextBoxFull())
            {
                btnBkSave.Enabled = true;
            }
            else
            {
                btnBkSave.Enabled = false;
            }
        }

        private void txtBkCode_TextChanged(object sender, EventArgs e)
        {
            if (IsTextBoxFull())
            {
                btnBkSave.Enabled = true;
            }
            else
            {
                btnBkSave.Enabled = false;
            }
        }

        private void txtBkTitle_TextChanged(object sender, EventArgs e)
        {
            if (IsTextBoxFull())
            {
                btnBkSave.Enabled = true;
            }
            else
            {
                btnBkSave.Enabled = false;
            }
        }

        private void txtBookCondition_TextChanged(object sender, EventArgs e)
        {
            if (IsTextBoxFull())
            {
                btnBkSave.Enabled = true;
            }
            else
            {
                btnBkSave.Enabled = false;
            }
        }
    }
}
