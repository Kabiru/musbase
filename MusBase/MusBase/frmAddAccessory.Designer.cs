﻿namespace MusBase
{
    partial class frmAddAccessory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAddAccessory));
            this.dgvAccessoryDetails = new System.Windows.Forms.DataGridView();
            this.gbxAddAccAccessoryDetails = new System.Windows.Forms.GroupBox();
            this.txtAccSize = new System.Windows.Forms.TextBox();
            this.txtAccCondition = new System.Windows.Forms.TextBox();
            this.lblAccSize = new System.Windows.Forms.Label();
            this.txtAccCode = new System.Windows.Forms.TextBox();
            this.cboAccType = new System.Windows.Forms.ComboBox();
            this.lblAccType = new System.Windows.Forms.Label();
            this.btnAccSave = new System.Windows.Forms.Button();
            this.btnAccClear = new System.Windows.Forms.Button();
            this.lblAccCondition = new System.Windows.Forms.Label();
            this.lblAccCode = new System.Windows.Forms.Label();
            this.txtAccName = new System.Windows.Forms.TextBox();
            this.lblAccName = new System.Windows.Forms.Label();
            this.lblFName = new System.Windows.Forms.Label();
            this.btnAddNewAcc = new System.Windows.Forms.Button();
            this.btnAccClose = new System.Windows.Forms.Button();
            this.lblInsertStatus = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnUpdate = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAccessoryDetails)).BeginInit();
            this.gbxAddAccAccessoryDetails.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvAccessoryDetails
            // 
            this.dgvAccessoryDetails.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvAccessoryDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAccessoryDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAccessoryDetails.Location = new System.Drawing.Point(0, 0);
            this.dgvAccessoryDetails.Name = "dgvAccessoryDetails";
            this.dgvAccessoryDetails.Size = new System.Drawing.Size(627, 201);
            this.dgvAccessoryDetails.TabIndex = 85;
            this.dgvAccessoryDetails.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAccessoryDetails_CellContentClick);
            this.dgvAccessoryDetails.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAccessoryDetails_CellContentDoubleClick);
            // 
            // gbxAddAccAccessoryDetails
            // 
            this.gbxAddAccAccessoryDetails.Controls.Add(this.txtAccSize);
            this.gbxAddAccAccessoryDetails.Controls.Add(this.txtAccCondition);
            this.gbxAddAccAccessoryDetails.Controls.Add(this.lblAccSize);
            this.gbxAddAccAccessoryDetails.Controls.Add(this.txtAccCode);
            this.gbxAddAccAccessoryDetails.Controls.Add(this.cboAccType);
            this.gbxAddAccAccessoryDetails.Controls.Add(this.lblAccType);
            this.gbxAddAccAccessoryDetails.Controls.Add(this.btnAccSave);
            this.gbxAddAccAccessoryDetails.Controls.Add(this.btnAccClear);
            this.gbxAddAccAccessoryDetails.Controls.Add(this.lblAccCondition);
            this.gbxAddAccAccessoryDetails.Controls.Add(this.lblAccCode);
            this.gbxAddAccAccessoryDetails.Controls.Add(this.txtAccName);
            this.gbxAddAccAccessoryDetails.Controls.Add(this.lblAccName);
            this.gbxAddAccAccessoryDetails.Controls.Add(this.lblFName);
            this.gbxAddAccAccessoryDetails.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.gbxAddAccAccessoryDetails.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxAddAccAccessoryDetails.Location = new System.Drawing.Point(30, 28);
            this.gbxAddAccAccessoryDetails.Name = "gbxAddAccAccessoryDetails";
            this.gbxAddAccAccessoryDetails.Size = new System.Drawing.Size(627, 161);
            this.gbxAddAccAccessoryDetails.TabIndex = 90;
            this.gbxAddAccAccessoryDetails.TabStop = false;
            this.gbxAddAccAccessoryDetails.Text = "Accessory Details";
            this.gbxAddAccAccessoryDetails.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // txtAccSize
            // 
            this.txtAccSize.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAccSize.Location = new System.Drawing.Point(492, 25);
            this.txtAccSize.Name = "txtAccSize";
            this.txtAccSize.Size = new System.Drawing.Size(69, 24);
            this.txtAccSize.TabIndex = 3;
            // 
            // txtAccCondition
            // 
            this.txtAccCondition.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAccCondition.Location = new System.Drawing.Point(324, 65);
            this.txtAccCondition.Name = "txtAccCondition";
            this.txtAccCondition.Size = new System.Drawing.Size(120, 24);
            this.txtAccCondition.TabIndex = 5;
            // 
            // lblAccSize
            // 
            this.lblAccSize.AutoSize = true;
            this.lblAccSize.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAccSize.Location = new System.Drawing.Point(454, 24);
            this.lblAccSize.Name = "lblAccSize";
            this.lblAccSize.Size = new System.Drawing.Size(32, 17);
            this.lblAccSize.TabIndex = 108;
            this.lblAccSize.Text = "Size";
            // 
            // txtAccCode
            // 
            this.txtAccCode.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAccCode.Location = new System.Drawing.Point(119, 65);
            this.txtAccCode.Name = "txtAccCode";
            this.txtAccCode.Size = new System.Drawing.Size(120, 24);
            this.txtAccCode.TabIndex = 4;
            // 
            // cboAccType
            // 
            this.cboAccType.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboAccType.FormattingEnabled = true;
            this.cboAccType.Location = new System.Drawing.Point(119, 24);
            this.cboAccType.Name = "cboAccType";
            this.cboAccType.Size = new System.Drawing.Size(121, 25);
            this.cboAccType.TabIndex = 1;
            // 
            // lblAccType
            // 
            this.lblAccType.AutoSize = true;
            this.lblAccType.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAccType.Location = new System.Drawing.Point(6, 24);
            this.lblAccType.Name = "lblAccType";
            this.lblAccType.Size = new System.Drawing.Size(107, 17);
            this.lblAccType.TabIndex = 105;
            this.lblAccType.Text = "Accessory Type";
            // 
            // btnAccSave
            // 
            this.btnAccSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAccSave.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAccSave.Image = global::MusBase.Properties.Resources.Ocarina_save_32;
            this.btnAccSave.Location = new System.Drawing.Point(119, 104);
            this.btnAccSave.Name = "btnAccSave";
            this.btnAccSave.Size = new System.Drawing.Size(104, 42);
            this.btnAccSave.TabIndex = 6;
            this.btnAccSave.Text = "Save";
            this.btnAccSave.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnAccSave.UseVisualStyleBackColor = true;
            this.btnAccSave.Click += new System.EventHandler(this.btnAccSave_Click);
            // 
            // btnAccClear
            // 
            this.btnAccClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAccClear.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAccClear.Image = global::MusBase.Properties.Resources.Ocarina_write_32;
            this.btnAccClear.Location = new System.Drawing.Point(324, 104);
            this.btnAccClear.Name = "btnAccClear";
            this.btnAccClear.Size = new System.Drawing.Size(104, 42);
            this.btnAccClear.TabIndex = 8;
            this.btnAccClear.Text = "&Clear";
            this.btnAccClear.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnAccClear.UseVisualStyleBackColor = true;
            this.btnAccClear.Click += new System.EventHandler(this.btnAccClear_Click);
            // 
            // lblAccCondition
            // 
            this.lblAccCondition.AutoSize = true;
            this.lblAccCondition.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAccCondition.Location = new System.Drawing.Point(245, 66);
            this.lblAccCondition.Name = "lblAccCondition";
            this.lblAccCondition.Size = new System.Drawing.Size(73, 17);
            this.lblAccCondition.TabIndex = 100;
            this.lblAccCondition.Text = "Condition";
            // 
            // lblAccCode
            // 
            this.lblAccCode.AutoSize = true;
            this.lblAccCode.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAccCode.Location = new System.Drawing.Point(6, 69);
            this.lblAccCode.Name = "lblAccCode";
            this.lblAccCode.Size = new System.Drawing.Size(109, 17);
            this.lblAccCode.TabIndex = 99;
            this.lblAccCode.Text = "Accessory Code";
            // 
            // txtAccName
            // 
            this.txtAccName.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAccName.Location = new System.Drawing.Point(324, 24);
            this.txtAccName.Name = "txtAccName";
            this.txtAccName.Size = new System.Drawing.Size(118, 24);
            this.txtAccName.TabIndex = 2;
            // 
            // lblAccName
            // 
            this.lblAccName.AutoSize = true;
            this.lblAccName.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAccName.Location = new System.Drawing.Point(272, 24);
            this.lblAccName.Name = "lblAccName";
            this.lblAccName.Size = new System.Drawing.Size(46, 17);
            this.lblAccName.TabIndex = 97;
            this.lblAccName.Text = "Name";
            // 
            // lblFName
            // 
            this.lblFName.AutoSize = true;
            this.lblFName.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFName.Location = new System.Drawing.Point(6, 25);
            this.lblFName.Name = "lblFName";
            this.lblFName.Size = new System.Drawing.Size(0, 17);
            this.lblFName.TabIndex = 96;
            // 
            // btnAddNewAcc
            // 
            this.btnAddNewAcc.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddNewAcc.Image = global::MusBase.Properties.Resources.Ocarina_add_32;
            this.btnAddNewAcc.Location = new System.Drawing.Point(149, 204);
            this.btnAddNewAcc.Name = "btnAddNewAcc";
            this.btnAddNewAcc.Size = new System.Drawing.Size(104, 42);
            this.btnAddNewAcc.TabIndex = 7;
            this.btnAddNewAcc.Text = "Add new";
            this.btnAddNewAcc.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnAddNewAcc.UseVisualStyleBackColor = true;
            this.btnAddNewAcc.Click += new System.EventHandler(this.btnAddNewAcc_Click);
            // 
            // btnAccClose
            // 
            this.btnAccClose.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAccClose.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.btnAccClose.Image = global::MusBase.Properties.Resources._1427990866_Done;
            this.btnAccClose.Location = new System.Drawing.Point(562, 204);
            this.btnAccClose.Name = "btnAccClose";
            this.btnAccClose.Size = new System.Drawing.Size(95, 44);
            this.btnAccClose.TabIndex = 9;
            this.btnAccClose.Text = "&Quit";
            this.btnAccClose.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnAccClose.UseVisualStyleBackColor = true;
            this.btnAccClose.Click += new System.EventHandler(this.btnAccClose_Click);
            // 
            // lblInsertStatus
            // 
            this.lblInsertStatus.AutoSize = true;
            this.lblInsertStatus.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInsertStatus.ForeColor = System.Drawing.Color.Red;
            this.lblInsertStatus.Location = new System.Drawing.Point(30, 232);
            this.lblInsertStatus.Name = "lblInsertStatus";
            this.lblInsertStatus.Size = new System.Drawing.Size(44, 17);
            this.lblInsertStatus.TabIndex = 208;
            this.lblInsertStatus.Text = "label1";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.dgvAccessoryDetails);
            this.panel1.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(30, 268);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(627, 201);
            this.panel1.TabIndex = 209;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Location = new System.Drawing.Point(354, 203);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(104, 42);
            this.btnUpdate.TabIndex = 210;
            this.btnUpdate.Text = "&Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // frmAddAccessory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(686, 481);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblInsertStatus);
            this.Controls.Add(this.btnAddNewAcc);
            this.Controls.Add(this.btnAccClose);
            this.Controls.Add(this.gbxAddAccAccessoryDetails);
            this.ForeColor = System.Drawing.Color.RoyalBlue;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmAddAccessory";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Accessory Details";
            this.Load += new System.EventHandler(this.frmAddAccessory_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAccessoryDetails)).EndInit();
            this.gbxAddAccAccessoryDetails.ResumeLayout(false);
            this.gbxAddAccAccessoryDetails.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvAccessoryDetails;
        private System.Windows.Forms.GroupBox gbxAddAccAccessoryDetails;
        private System.Windows.Forms.Label lblAccSize;
        private System.Windows.Forms.TextBox txtAccCode;
        private System.Windows.Forms.ComboBox cboAccType;
        private System.Windows.Forms.Label lblAccType;
        private System.Windows.Forms.Button btnAccSave;
        private System.Windows.Forms.Button btnAccClear;
        private System.Windows.Forms.Label lblAccCondition;
        private System.Windows.Forms.Label lblAccCode;
        private System.Windows.Forms.TextBox txtAccName;
        private System.Windows.Forms.Label lblAccName;
        private System.Windows.Forms.Label lblFName;
        private System.Windows.Forms.Button btnAccClose;
        private System.Windows.Forms.Button btnAddNewAcc;
        private System.Windows.Forms.TextBox txtAccSize;
        private System.Windows.Forms.TextBox txtAccCondition;
        private System.Windows.Forms.Label lblInsertStatus;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnUpdate;
    }
}