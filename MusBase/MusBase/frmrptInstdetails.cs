﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MusBase
{
    public partial class frmrptInstdetails : Form
    {
        public frmrptInstdetails()
        {
            InitializeComponent();
        }

        private void frmrptInstdetails_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'DataSet1.Instrument_Details' table. You can move, or remove it, as needed.
            this.Instrument_DetailsTableAdapter.Fill(this.DataSet1.Instrument_Details);

            this.reportViewer1.RefreshReport();
        }
    }
}
