﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MusBase
{
    public partial class frmMain : Form
    {
        private int childFormNumber = 0;
        private string userLogged = null;
        public frmMain()
        {
            InitializeComponent();          
        }

        private void ShowNewForm(object sender, EventArgs e)
        {
            Form childForm = new Form();
            childForm.MdiParent = this;
            childForm.Text = "Window " + childFormNumber++;
            childForm.Show();
        }        

        private void OpenFile(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            openFileDialog.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*";
            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = openFileDialog.FileName;
            }
        }

        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
          
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CutToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void CopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void PasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void ToolBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //toolStrip.Visible = toolBarToolStripMenuItem.Checked;
        }

        private void StatusBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //statusStrip.Visible = statusBarToolStripMenuItem.Checked;
        }

        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }

        private void StudentMenu_Click(object sender, EventArgs e)
        {

        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            userManagerClass us = new userManagerClass();
            UsertoolStripStatusLabel.Text = us.getUid();
        }       

        private void InstrumentMenu_Click(object sender, EventArgs e)
        {

        }

        private void fileMenu_Click(object sender, EventArgs e)
        {

        }

        private void BooksMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void toolStripProgressBar1_Click(object sender, EventArgs e)
        {

        }

        private void statusStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void AddStudentMenuItem_Click(object sender, EventArgs e)
        {
            frmAddStudent addstd = new frmAddStudent();
            addstd.ShowDialog();//modal call

        }

        private void RemoveStudentMenuItem_Click(object sender, EventArgs e)
        {
            frmRemoveStudent RmvStd = new frmRemoveStudent();
            RmvStd.ShowDialog();//modal call
        }

        private void AssignInstrumentMenuItem_Click(object sender, EventArgs e)
        {
            frmAssignInstrument AsgInst = new frmAssignInstrument();
            AsgInst.ShowDialog();//modal call
        }

        private void RetrieveInstrumentMenuItem_Click(object sender, EventArgs e)
        {
            frmRetrieveInstrument RtvInst = new frmRetrieveInstrument();
            RtvInst.ShowDialog();//modal call
        }

        private void AddInstrumentMenuItem_Click(object sender, EventArgs e)
        {
            frmAddInstrument AddInst = new frmAddInstrument();
            AddInst.ShowDialog();//modal call
        }

        private void RemoveInstrumentMenuItem_Click(object sender, EventArgs e)
        {
            frmRemoveInstrument RmvInst = new frmRemoveInstrument();
            RmvInst.ShowDialog();//modal call
        }

        private void AssignBookMenuItem_Click(object sender, EventArgs e)
        {
            frmAssignBook AsgBk = new frmAssignBook();
            AsgBk.ShowDialog();//modal call
        }

        private void RetrieveBookMenuItem_Click(object sender, EventArgs e)
        {
            frmRetrieveBook RtvBk = new frmRetrieveBook();
            RtvBk.ShowDialog();//modal call
        }

        private void AddBookMenuItem_Click(object sender, EventArgs e)
        {
            frmAddBook AddBk = new frmAddBook();
            AddBk.ShowDialog();//modal call
        }

        private void RemoveBookMenuItem_Click(object sender, EventArgs e)
        {
            frmRetrieveBook RmvBk = new frmRetrieveBook();
            RmvBk.ShowDialog();//modal call
        }

        private void AssignAccessoryMenuItem_Click(object sender, EventArgs e)
        {
            frmAssignAccessory AsgAcc = new frmAssignAccessory();
            AsgAcc.ShowDialog();//modal call
        }

        private void RetrieveAccessoryMenuItem_Click(object sender, EventArgs e)
        {
            frmRetrieveAccessory RtvAcc = new frmRetrieveAccessory();
            RtvAcc.ShowDialog();//modal call
        }

        private void AddAccessoryMenuItem_Click(object sender, EventArgs e)
        {
            frmAddAccessory AddAcc = new frmAddAccessory();
            AddAcc.ShowDialog();//modal call
        }

        private void RemoveAccessoryMenuItem_Click(object sender, EventArgs e)
        {
            frmRemoveAccessory RmvAcc = new frmRemoveAccessory();
            RmvAcc.ShowDialog();//remove accessory
        }

        private void editMenu_Click(object sender, EventArgs e)
        {

        }

        private void menuStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void btnAddStudent_Click(object sender, EventArgs e)
        {
            //add student
            frmAddStudent addstd = new frmAddStudent();
            addstd.ShowDialog();//modal call
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //retrieve inst
            frmRetrieveInstrument rtvinst = new frmRetrieveInstrument();
            rtvinst.ShowDialog();//modal call
        }

        private void btnAddAccessory_Click(object sender, EventArgs e)
        {
            //add accessory
            frmAddAccessory addacc = new frmAddAccessory();
            addacc.ShowDialog();//modal call
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void btnRemoveInstrument_Click(object sender, EventArgs e)
        {
            //remove instrument
            frmRemoveInstrument RmvInst = new frmRemoveInstrument();
            RmvInst.ShowDialog();//modal call
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //assign inst
            frmAssignInstrument asgInst = new frmAssignInstrument();
            asgInst.ShowDialog();//modal call
        }

        private void btnAddInstrument_Click(object sender, EventArgs e)
        {
            //add instrument
            frmAddInstrument addInst = new frmAddInstrument();
            addInst.ShowDialog();//modal call
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void btnAddBook_Click(object sender, EventArgs e)
        {
            //add book
            frmAddBook addbk = new frmAddBook();
            addbk.ShowDialog();//modal call
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //remove book
            frmRemoveBook rmvbk = new frmRemoveBook();
            rmvbk.ShowDialog();//modal call
        }

        private void gbxManageAccessories_Enter(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            //assign accessory
            frmAssignAccessory asgacc = new frmAssignAccessory();
            asgacc.ShowDialog();//modal call
        }

        private void btnRemoveStudent_Click(object sender, EventArgs e)
        {
            //remove student
            frmRemoveStudent rmvstd = new frmRemoveStudent();
            rmvstd.ShowDialog();//modal call
        }

        private void btnAssignBook_Click(object sender, EventArgs e)
        {
            //assign book
            frmAssignBook asgbk = new frmAssignBook();
            asgbk.ShowDialog();//modal call
        }

        private void btnRetrieveBook_Click(object sender, EventArgs e)
        {
            //retrieve book
            frmRetrieveBook rtvbk = new frmRetrieveBook();
            rtvbk.ShowDialog();//modal call
        }

        private void bntRemoveAccessory_Click(object sender, EventArgs e)
        {
            frmRemoveAccessory rmvacc = new frmRemoveAccessory();
            rmvacc.ShowDialog();//modal call
        }

        private void btnRetriveInstrument_Click(object sender, EventArgs e)
        {
            //retrieve accessory
            frmRetrieveAccessory rtvacc = new frmRetrieveAccessory();
            rtvacc.ShowDialog();//modal call
        }

        private void btnExitMain_Click(object sender, EventArgs e)
        {
            DialogResult result =  MessageBox.Show("Are you sure you want to exit MusBase?", "Exit", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                this.Dispose();
                Application.Exit();//exiting application
            }
            else if (result == System.Windows.Forms.DialogResult.Cancel)
                return;
        }

        private void assignedInstrumentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmrptStudentDetails rpt = new frmrptStudentDetails();
            rpt.Show();
        }

        private void studentGradeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void gradeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMusGrade grd = new frmMusGrade();
            grd.ShowDialog();//modal call
        }

        private void addTypeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAddType typ = new frmAddType();
            typ.ShowDialog();//modal call
        }

        private void musBandToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMusBand band = new frmMusBand();
            band.ShowDialog();//modal call
        }

        private void musChoirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMusChoir choir = new frmMusChoir();
            choir.ShowDialog();
        }

        private void assignedInstrumentsToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmRptAssignedInstruments asgint = new frmRptAssignedInstruments();
            asgint.Show();
        }

        private void instrumentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmrptInstdetails inst = new frmrptInstdetails();
            inst.Show();
        }

        private void booksToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmrptBookDetails bk = new frmrptBookDetails();
            bk.Show();
        }

        private void assignedBooksToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmrptAssignedBooks bk = new frmrptAssignedBooks();
            bk.Show();
        }

        private void assignedAccessoriesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmrptAssignedAccessories acc = new frmrptAssignedAccessories();
            acc.Show();
        }

        private void toolStripSeparator3_Click(object sender, EventArgs e)
        {

        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmrptAccessoryDetails acc = new frmrptAccessoryDetails();
            acc.Show();
        }

        private void reportsToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void searchToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmStudentViewProfile s = new frmStudentViewProfile();
            s.Show();//modal call
        }
    }
}
