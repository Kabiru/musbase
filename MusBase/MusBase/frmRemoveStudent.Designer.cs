﻿namespace MusBase
{
    partial class frmRemoveStudent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRemoveStudent));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnRmvStdDone = new System.Windows.Forms.Button();
            this.btnRmvStudent = new System.Windows.Forms.Button();
            this.btnRmvStdClear = new System.Windows.Forms.Button();
            this.txtRmvStdHouse = new System.Windows.Forms.TextBox();
            this.cboRmvStdAdminNo = new System.Windows.Forms.ComboBox();
            this.cboRmvStdForm = new System.Windows.Forms.ComboBox();
            this.lblRmvStdForm = new System.Windows.Forms.Label();
            this.lblRmvStdHouse = new System.Windows.Forms.Label();
            this.lblRmvStdAdminNo = new System.Windows.Forms.Label();
            this.txtRmvStdMName = new System.Windows.Forms.TextBox();
            this.lblRmvStdMName = new System.Windows.Forms.Label();
            this.txtRmvStdLName = new System.Windows.Forms.TextBox();
            this.txtAsgAccFName = new System.Windows.Forms.TextBox();
            this.lblRmvStdLName = new System.Windows.Forms.Label();
            this.lblRmvStdFName = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnRmvStdDone);
            this.groupBox1.Controls.Add(this.btnRmvStudent);
            this.groupBox1.Controls.Add(this.btnRmvStdClear);
            this.groupBox1.Controls.Add(this.txtRmvStdHouse);
            this.groupBox1.Controls.Add(this.cboRmvStdAdminNo);
            this.groupBox1.Controls.Add(this.cboRmvStdForm);
            this.groupBox1.Controls.Add(this.lblRmvStdForm);
            this.groupBox1.Controls.Add(this.lblRmvStdHouse);
            this.groupBox1.Controls.Add(this.lblRmvStdAdminNo);
            this.groupBox1.Controls.Add(this.txtRmvStdMName);
            this.groupBox1.Controls.Add(this.lblRmvStdMName);
            this.groupBox1.Controls.Add(this.txtRmvStdLName);
            this.groupBox1.Controls.Add(this.txtAsgAccFName);
            this.groupBox1.Controls.Add(this.lblRmvStdLName);
            this.groupBox1.Controls.Add(this.lblRmvStdFName);
            this.groupBox1.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 32);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(604, 210);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Select a student";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // btnRmvStdDone
            // 
            this.btnRmvStdDone.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRmvStdDone.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.btnRmvStdDone.Image = global::MusBase.Properties.Resources._1427990866_Done;
            this.btnRmvStdDone.Location = new System.Drawing.Point(498, 122);
            this.btnRmvStdDone.Name = "btnRmvStdDone";
            this.btnRmvStdDone.Size = new System.Drawing.Size(95, 44);
            this.btnRmvStdDone.TabIndex = 5;
            this.btnRmvStdDone.Text = "&Quit";
            this.btnRmvStdDone.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnRmvStdDone.UseVisualStyleBackColor = true;
            this.btnRmvStdDone.Click += new System.EventHandler(this.btnRmvStdDone_Click);
            // 
            // btnRmvStudent
            // 
            this.btnRmvStudent.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRmvStudent.Image = global::MusBase.Properties.Resources.mypc_delete_32;
            this.btnRmvStudent.Location = new System.Drawing.Point(94, 124);
            this.btnRmvStudent.Name = "btnRmvStudent";
            this.btnRmvStudent.Size = new System.Drawing.Size(104, 42);
            this.btnRmvStudent.TabIndex = 3;
            this.btnRmvStudent.Text = "&Remove";
            this.btnRmvStudent.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnRmvStudent.UseVisualStyleBackColor = true;
            this.btnRmvStudent.Click += new System.EventHandler(this.btnRmvStudent_Click);
            // 
            // btnRmvStdClear
            // 
            this.btnRmvStdClear.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRmvStdClear.Image = global::MusBase.Properties.Resources.mypc_write_32;
            this.btnRmvStdClear.Location = new System.Drawing.Point(310, 124);
            this.btnRmvStdClear.Name = "btnRmvStdClear";
            this.btnRmvStdClear.Size = new System.Drawing.Size(104, 42);
            this.btnRmvStdClear.TabIndex = 4;
            this.btnRmvStdClear.Text = "&Clear";
            this.btnRmvStdClear.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnRmvStdClear.UseVisualStyleBackColor = true;
            this.btnRmvStdClear.Click += new System.EventHandler(this.btnRmvStdClear_Click);
            // 
            // txtRmvStdHouse
            // 
            this.txtRmvStdHouse.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRmvStdHouse.Location = new System.Drawing.Point(498, 29);
            this.txtRmvStdHouse.Name = "txtRmvStdHouse";
            this.txtRmvStdHouse.ReadOnly = true;
            this.txtRmvStdHouse.Size = new System.Drawing.Size(100, 24);
            this.txtRmvStdHouse.TabIndex = 177;
            // 
            // cboRmvStdAdminNo
            // 
            this.cboRmvStdAdminNo.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboRmvStdAdminNo.FormattingEnabled = true;
            this.cboRmvStdAdminNo.Location = new System.Drawing.Point(310, 27);
            this.cboRmvStdAdminNo.Name = "cboRmvStdAdminNo";
            this.cboRmvStdAdminNo.Size = new System.Drawing.Size(121, 25);
            this.cboRmvStdAdminNo.TabIndex = 2;
            this.cboRmvStdAdminNo.SelectedIndexChanged += new System.EventHandler(this.cboRmvStdAdminNo_SelectedIndexChanged);
            // 
            // cboRmvStdForm
            // 
            this.cboRmvStdForm.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboRmvStdForm.FormattingEnabled = true;
            this.cboRmvStdForm.Location = new System.Drawing.Point(94, 28);
            this.cboRmvStdForm.Name = "cboRmvStdForm";
            this.cboRmvStdForm.Size = new System.Drawing.Size(71, 25);
            this.cboRmvStdForm.TabIndex = 1;
            this.cboRmvStdForm.SelectedIndexChanged += new System.EventHandler(this.cboRmvStdForm_SelectedIndexChanged);
            // 
            // lblRmvStdForm
            // 
            this.lblRmvStdForm.AutoSize = true;
            this.lblRmvStdForm.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRmvStdForm.Location = new System.Drawing.Point(36, 32);
            this.lblRmvStdForm.Name = "lblRmvStdForm";
            this.lblRmvStdForm.Size = new System.Drawing.Size(42, 17);
            this.lblRmvStdForm.TabIndex = 174;
            this.lblRmvStdForm.Text = "Form";
            // 
            // lblRmvStdHouse
            // 
            this.lblRmvStdHouse.AutoSize = true;
            this.lblRmvStdHouse.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRmvStdHouse.Location = new System.Drawing.Point(444, 32);
            this.lblRmvStdHouse.Name = "lblRmvStdHouse";
            this.lblRmvStdHouse.Size = new System.Drawing.Size(48, 17);
            this.lblRmvStdHouse.TabIndex = 173;
            this.lblRmvStdHouse.Text = "House";
            // 
            // lblRmvStdAdminNo
            // 
            this.lblRmvStdAdminNo.AutoSize = true;
            this.lblRmvStdAdminNo.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRmvStdAdminNo.Location = new System.Drawing.Point(205, 30);
            this.lblRmvStdAdminNo.Name = "lblRmvStdAdminNo";
            this.lblRmvStdAdminNo.Size = new System.Drawing.Size(76, 17);
            this.lblRmvStdAdminNo.TabIndex = 172;
            this.lblRmvStdAdminNo.Text = "Admin No";
            // 
            // txtRmvStdMName
            // 
            this.txtRmvStdMName.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRmvStdMName.Location = new System.Drawing.Point(310, 70);
            this.txtRmvStdMName.Name = "txtRmvStdMName";
            this.txtRmvStdMName.ReadOnly = true;
            this.txtRmvStdMName.Size = new System.Drawing.Size(100, 24);
            this.txtRmvStdMName.TabIndex = 171;
            // 
            // lblRmvStdMName
            // 
            this.lblRmvStdMName.AutoSize = true;
            this.lblRmvStdMName.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRmvStdMName.Location = new System.Drawing.Point(205, 73);
            this.lblRmvStdMName.Name = "lblRmvStdMName";
            this.lblRmvStdMName.Size = new System.Drawing.Size(94, 17);
            this.lblRmvStdMName.TabIndex = 170;
            this.lblRmvStdMName.Text = "Middle Name";
            // 
            // txtRmvStdLName
            // 
            this.txtRmvStdLName.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRmvStdLName.Location = new System.Drawing.Point(498, 73);
            this.txtRmvStdLName.Name = "txtRmvStdLName";
            this.txtRmvStdLName.ReadOnly = true;
            this.txtRmvStdLName.Size = new System.Drawing.Size(100, 24);
            this.txtRmvStdLName.TabIndex = 169;
            // 
            // txtAsgAccFName
            // 
            this.txtAsgAccFName.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAsgAccFName.Location = new System.Drawing.Point(94, 73);
            this.txtAsgAccFName.Name = "txtAsgAccFName";
            this.txtAsgAccFName.ReadOnly = true;
            this.txtAsgAccFName.Size = new System.Drawing.Size(100, 24);
            this.txtAsgAccFName.TabIndex = 168;
            // 
            // lblRmvStdLName
            // 
            this.lblRmvStdLName.AutoSize = true;
            this.lblRmvStdLName.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRmvStdLName.Location = new System.Drawing.Point(424, 73);
            this.lblRmvStdLName.Name = "lblRmvStdLName";
            this.lblRmvStdLName.Size = new System.Drawing.Size(75, 17);
            this.lblRmvStdLName.TabIndex = 167;
            this.lblRmvStdLName.Text = "Last Name";
            // 
            // lblRmvStdFName
            // 
            this.lblRmvStdFName.AutoSize = true;
            this.lblRmvStdFName.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRmvStdFName.Location = new System.Drawing.Point(10, 73);
            this.lblRmvStdFName.Name = "lblRmvStdFName";
            this.lblRmvStdFName.Size = new System.Drawing.Size(78, 17);
            this.lblRmvStdFName.TabIndex = 166;
            this.lblRmvStdFName.Text = "First Name";
            // 
            // frmRemoveStudent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(636, 267);
            this.Controls.Add(this.groupBox1);
            this.ForeColor = System.Drawing.Color.RoyalBlue;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmRemoveStudent";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Remove Student";
            this.Load += new System.EventHandler(this.frmRemoveStudent_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnRmvStudent;
        private System.Windows.Forms.Button btnRmvStdClear;
        private System.Windows.Forms.TextBox txtRmvStdHouse;
        private System.Windows.Forms.ComboBox cboRmvStdAdminNo;
        private System.Windows.Forms.ComboBox cboRmvStdForm;
        private System.Windows.Forms.Label lblRmvStdForm;
        private System.Windows.Forms.Label lblRmvStdHouse;
        private System.Windows.Forms.Label lblRmvStdAdminNo;
        private System.Windows.Forms.TextBox txtRmvStdMName;
        private System.Windows.Forms.Label lblRmvStdMName;
        private System.Windows.Forms.TextBox txtRmvStdLName;
        private System.Windows.Forms.TextBox txtAsgAccFName;
        private System.Windows.Forms.Label lblRmvStdLName;
        private System.Windows.Forms.Label lblRmvStdFName;
        private System.Windows.Forms.Button btnRmvStdDone;

    }
}