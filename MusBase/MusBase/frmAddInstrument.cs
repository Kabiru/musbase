﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace MusBase
{
    public partial class frmAddInstrument : Form
    {
        private string connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;
        private string SqlQuery;
        private SqlConnection conn;
        private SqlCommand cmd;
        private SqlDataReader reader;
        SqlDataAdapter dAdapter;
        DataTable InstTable;
        SqlCommandBuilder cmdbld;

        public frmAddInstrument()
        {
            InitializeComponent();
            LoadInstruments();
            fillcboFamily();
            startDisable();

            //label color
            lblInsertStatus.ForeColor = System.Drawing.Color.Red;
        }

        private void startDisable() 
        {
            //control 
            btnInstSave.Enabled = false;
            btnInstClear.Enabled = false;
            btnAddNew.Enabled = false;
            cboInstType.Enabled = false;

            //box
            txtInstSerialNo.Enabled = false;
            txtInstCondition.Enabled = false;
            txtInstMake.Enabled = false;
            
        }

        void fillcboType_Strings()
        {       
            connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;
            SqlQuery = "SELECT * FROM [dbo].[Instrument_Type]  WHERE [Instrument_Type_Id] LIKE 'S%'";

            conn = new SqlConnection(connectionString);

            try
            {
                cmd = new SqlCommand(SqlQuery, conn);

                //open connection
                conn.Open();
                reader = cmd.ExecuteReader();//executing command

                while (reader.Read())
                {
                    string instType = reader.GetString(1);
                    cboInstType.Items.Add(instType);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            finally
            {
                reader.Close();
                conn.Close();//closing connection
            }
        }

        void fillcboType_Percussion()
        {          
            SqlQuery = "SELECT * FROM [dbo].[Instrument_Type]  WHERE [Instrument_Type_Id] LIKE 'P%'";
            conn = new SqlConnection(connectionString);
            try
            {
                cmd = new SqlCommand(SqlQuery, conn);                              
                //open connection
                conn.Open();
                reader = cmd.ExecuteReader();//executing command
                while (reader.Read())
                {
                    string instType = reader.GetString(1);
                    cboInstType.Items.Add(instType);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            finally
            {
                reader.Close();
                conn.Close();//closing connection
            }
        }

        void fillcboType_Woodwinds()
        {
            connectionString = ConfigurationManager.ConnectionStrings["MusBase.Properties.Settings.BaseConnectionString"].ConnectionString;
            SqlQuery = "SELECT * FROM [dbo].[Instrument_Type]  WHERE [Instrument_Type_Id] LIKE 'W%'";
            conn = new SqlConnection(connectionString);
            try
            {
                cmd = new SqlCommand(SqlQuery, conn);
                //open connection
                conn.Open();
                reader = cmd.ExecuteReader();//executing command

                while (reader.Read())
                {
                    string instType = reader.GetString(1);
                    cboInstType.Items.Add(instType);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            finally
            {
                conn.Close();//closing connection
            }
        }

        void fillcboType_Brass()
        {           
                SqlQuery = "SELECT * FROM [dbo].[Instrument_Type]  WHERE [Instrument_Type_Id] LIKE 'B%'";          
           
                conn = new SqlConnection(connectionString);                
            try
            {              
                cmd = new SqlCommand(SqlQuery, conn);                
                //open connection
                conn.Open();
                reader = cmd.ExecuteReader();//executing command

                while (reader.Read())
                {
                    string instType = reader.GetString(1);
                    cboInstType.Items.Add(instType);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            finally 
            {
                conn.Close();//closing connection
            }
        }

        void fillcboFamily() 
        {            
            SqlQuery = "SELECT * FROM [dbo].[Family]";
            conn = new SqlConnection(connectionString);//passing connection string

                try
                {
                    SqlCommand  cmd = new SqlCommand(SqlQuery, conn);

                    SqlDataReader reader;
                    conn.Open();//opening connection
                    //execute
                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        string familyType = reader.GetString(1);
                        cboInstrumentFamily.Items.Add(familyType);//populating combo box
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally 
                {
                    conn.Close();//closing connection
                }

        }

       

        private void LoadInstruments()
        {
            conn = new SqlConnection(connectionString);
            SqlQuery = "Load_Instruments_Procedure";
            cmd = new SqlCommand(SqlQuery, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            try 
            {
                dAdapter = new SqlDataAdapter(cmd);
                InstTable = new DataTable();
                //fill datatable with data
                dAdapter.Fill(InstTable);
                //binding to datagridview
                BindingSource bSource = new BindingSource();
                bSource.DataSource = InstTable;
                dgvInstrumentDetails.DataSource = bSource;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                conn.Close();//closing connection
            }
        }

        private void btnInstAssign_Click(object sender, EventArgs e)
        {
            frmAssignInstrument AsgInst = new frmAssignInstrument();
            AsgInst.ShowDialog();//modal form
        }

        private void frmAddInstrument_Load(object sender, EventArgs e)
        {
            
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtInstMake_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnInstClose_Click(object sender, EventArgs e)
        {
            this.Close();//closing form
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //assign instrument
            frmAssignInstrument asginst = new frmAssignInstrument();
            asginst.ShowDialog();//modal call
        }

        private void btnInstClose_Click_1(object sender, EventArgs e)
        {
            //close form
            this.Close();            
        }

        private void btnInstAdd_Click(object sender, EventArgs e)
        {
            //save record         
            SqlQuery = "INSERT INTO [dbo].[Instrument_Details] ([Inst_Serial_No], [Inst_Make], [Instrument_Type], [Inst_Family], [Inst_Condition])"+
                " VALUES(@InstSerialNo, @InstMake, @InstType, @InstrumentFamily, @InstCondition)";
            conn = new SqlConnection(connectionString);                
            cmd = new SqlCommand(SqlQuery, conn);
            cmd.Parameters.AddWithValue("@InstSerialNo", txtInstSerialNo.Text);
            cmd.Parameters.AddWithValue("@InstMake", txtInstMake.Text);
            cmd.Parameters.AddWithValue("@InstType", cboInstType.Text);
            cmd.Parameters.AddWithValue("@InstrumentFamily", cboInstrumentFamily.Text);
            cmd.Parameters.AddWithValue("@InstCondition", txtInstCondition.Text);
            int TotalRowsAffected = 0;
            try
            {
                conn.Open();

                TotalRowsAffected = cmd.ExecuteNonQuery();

                if (TotalRowsAffected == 1)
                {
                    lblInsertStatus.Text = "Record saved!";
                        
                    //adding the data to the datagridview                     
                    LoadInstruments();
                    btnAddNew.Enabled = true;
                    gbxAddInstInstrumentDetails.Enabled = false;
                        
                }
                else if (TotalRowsAffected > 1)
                {
                    lblInsertStatus.Text = "Duplicate records added";
                }
                else
                {
                    lblInsertStatus.Text = "Insert incomplete";
                }                  
                    
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                conn.Close();//closing connection
            }               

                
        }

        private void reset()
        {    //clear
            txtInstMake.Clear();
            txtInstSerialNo.Clear();
            txtInstCondition.Clear();
            cboInstType.Text = null;
            cboInstType.SelectedItem = null;
            cboInstrumentFamily.Text = null;
            cboInstrumentFamily.SelectedItem = null;

            startDisable();
            cboInstrumentFamily.Focus();
        }

        private void btnInstClear_Click(object sender, EventArgs e)
        {
            reset();
        }

        private void cboInstrumentFamily_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboInstrumentFamily.Text != null)
            {
                cboInstType.Enabled = true;
                cboInstType.Items.Clear();
                btnInstClear.Enabled = true;

                if ((string)cboInstrumentFamily.SelectedItem == "Brass")
                {
                    fillcboType_Brass();
                }
                else if ((string)cboInstrumentFamily.SelectedItem == "Percussion")
                {
                    fillcboType_Percussion();
                }
                else if ((string)cboInstrumentFamily.SelectedItem == "Strings")
                {
                    fillcboType_Strings();
                }
                else if ((string)cboInstrumentFamily.SelectedItem == "Woodwinds")
                {
                    fillcboType_Woodwinds();
                }
            }
        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            
            //reactivate gbx
            gbxAddInstInstrumentDetails.Enabled = true;
            //clear
            txtInstMake.Clear();
            txtInstSerialNo.Clear();
            txtInstCondition.Clear();

            lblInsertStatus.Text = "Waiting ...";
            cboInstrumentFamily.Focus();
            startDisable();
        }

        private void gbxAddInstInstrumentDetails_Enter(object sender, EventArgs e)
        {

        }

        private void dgvInstrumentDetails_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            btnUpdate.Enabled = true;
        }

        private void button1_MouseHover(object sender, EventArgs e)
        {

        }

        private void chart1_Click(object sender, EventArgs e)
        {

        }

        private void btnAddInstrumentType_Click(object sender, EventArgs e)
        {

        }

        private void cboInstType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboInstType.Text != null)
            {
                txtInstSerialNo.Enabled = true;
            }
        }

        private void txtInstSerialNo_TextChanged(object sender, EventArgs e)
        {
            if (txtInstSerialNo.Text != null)
            {
                txtInstMake.Enabled = true;
            }
        }

        private void txtInstCondition_TextChanged(object sender, EventArgs e)
        {
            if (txtInstCondition.Text != null)
            {
                btnInstSave.Enabled = true;
            }
        }

        private void txtInstMake_TextChanged_1(object sender, EventArgs e)
        {
            if (txtInstMake.Text != null)
            {
                txtInstCondition.Enabled = true;
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                cmdbld = new SqlCommandBuilder(dAdapter);
                dAdapter.Update(InstTable);
                MessageBox.Show("Information successfully updated into the database.", "Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
