﻿namespace MusBase
{
    partial class frmRemoveAccessory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblFName = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnRmvAccDone = new System.Windows.Forms.Button();
            this.btnRmvAccessory = new System.Windows.Forms.Button();
            this.btnRmvAccClear = new System.Windows.Forms.Button();
            this.txtRmvAccSize = new System.Windows.Forms.TextBox();
            this.txtRmvAccCondition = new System.Windows.Forms.TextBox();
            this.lblRmvAccSize = new System.Windows.Forms.Label();
            this.txtRmvAccType = new System.Windows.Forms.TextBox();
            this.cboRmvAccCode = new System.Windows.Forms.ComboBox();
            this.lblRmvAccType = new System.Windows.Forms.Label();
            this.lblRmvAccCondition = new System.Windows.Forms.Label();
            this.lblRmvAccCode = new System.Windows.Forms.Label();
            this.txtRmvAccName = new System.Windows.Forms.TextBox();
            this.lblRmvAccName = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dgvRemoveAccessory = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRemoveAccessory)).BeginInit();
            this.SuspendLayout();
            // 
            // lblFName
            // 
            this.lblFName.AutoSize = true;
            this.lblFName.Location = new System.Drawing.Point(34, 68);
            this.lblFName.Name = "lblFName";
            this.lblFName.Size = new System.Drawing.Size(0, 13);
            this.lblFName.TabIndex = 171;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnRmvAccDone);
            this.groupBox1.Controls.Add(this.btnRmvAccessory);
            this.groupBox1.Controls.Add(this.btnRmvAccClear);
            this.groupBox1.Controls.Add(this.txtRmvAccSize);
            this.groupBox1.Controls.Add(this.txtRmvAccCondition);
            this.groupBox1.Controls.Add(this.lblRmvAccSize);
            this.groupBox1.Controls.Add(this.txtRmvAccType);
            this.groupBox1.Controls.Add(this.cboRmvAccCode);
            this.groupBox1.Controls.Add(this.lblRmvAccType);
            this.groupBox1.Controls.Add(this.lblRmvAccCondition);
            this.groupBox1.Controls.Add(this.lblRmvAccCode);
            this.groupBox1.Controls.Add(this.txtRmvAccName);
            this.groupBox1.Controls.Add(this.lblRmvAccName);
            this.groupBox1.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 22);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(583, 177);
            this.groupBox1.TabIndex = 172;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Select and Accessory";
            // 
            // btnRmvAccDone
            // 
            this.btnRmvAccDone.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRmvAccDone.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.btnRmvAccDone.Image = global::MusBase.Properties.Resources._1427990866_Done;
            this.btnRmvAccDone.Location = new System.Drawing.Point(465, 118);
            this.btnRmvAccDone.Name = "btnRmvAccDone";
            this.btnRmvAccDone.Size = new System.Drawing.Size(95, 44);
            this.btnRmvAccDone.TabIndex = 4;
            this.btnRmvAccDone.Text = "&Quit";
            this.btnRmvAccDone.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnRmvAccDone.UseVisualStyleBackColor = true;
            this.btnRmvAccDone.Click += new System.EventHandler(this.btnRmvAccDone_Click);
            // 
            // btnRmvAccessory
            // 
            this.btnRmvAccessory.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRmvAccessory.Image = global::MusBase.Properties.Resources.Ocarina_delete_32;
            this.btnRmvAccessory.Location = new System.Drawing.Point(118, 120);
            this.btnRmvAccessory.Name = "btnRmvAccessory";
            this.btnRmvAccessory.Size = new System.Drawing.Size(104, 42);
            this.btnRmvAccessory.TabIndex = 2;
            this.btnRmvAccessory.Text = "&Remove";
            this.btnRmvAccessory.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnRmvAccessory.UseVisualStyleBackColor = true;
            this.btnRmvAccessory.Click += new System.EventHandler(this.btnRmvAccessory_Click_1);
            // 
            // btnRmvAccClear
            // 
            this.btnRmvAccClear.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRmvAccClear.Image = global::MusBase.Properties.Resources.Ocarina_write_32;
            this.btnRmvAccClear.Location = new System.Drawing.Point(322, 120);
            this.btnRmvAccClear.Name = "btnRmvAccClear";
            this.btnRmvAccClear.Size = new System.Drawing.Size(104, 42);
            this.btnRmvAccClear.TabIndex = 3;
            this.btnRmvAccClear.Text = "&Clear";
            this.btnRmvAccClear.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnRmvAccClear.UseVisualStyleBackColor = true;
            this.btnRmvAccClear.Click += new System.EventHandler(this.btnRmvAccClear_Click);
            // 
            // txtRmvAccSize
            // 
            this.txtRmvAccSize.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRmvAccSize.Location = new System.Drawing.Point(504, 30);
            this.txtRmvAccSize.Name = "txtRmvAccSize";
            this.txtRmvAccSize.ReadOnly = true;
            this.txtRmvAccSize.Size = new System.Drawing.Size(56, 24);
            this.txtRmvAccSize.TabIndex = 197;
            // 
            // txtRmvAccCondition
            // 
            this.txtRmvAccCondition.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRmvAccCondition.Location = new System.Drawing.Point(323, 74);
            this.txtRmvAccCondition.Name = "txtRmvAccCondition";
            this.txtRmvAccCondition.ReadOnly = true;
            this.txtRmvAccCondition.Size = new System.Drawing.Size(117, 24);
            this.txtRmvAccCondition.TabIndex = 196;
            // 
            // lblRmvAccSize
            // 
            this.lblRmvAccSize.AutoSize = true;
            this.lblRmvAccSize.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRmvAccSize.Location = new System.Drawing.Point(466, 32);
            this.lblRmvAccSize.Name = "lblRmvAccSize";
            this.lblRmvAccSize.Size = new System.Drawing.Size(32, 17);
            this.lblRmvAccSize.TabIndex = 195;
            this.lblRmvAccSize.Text = "Size";
            // 
            // txtRmvAccType
            // 
            this.txtRmvAccType.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRmvAccType.Location = new System.Drawing.Point(119, 76);
            this.txtRmvAccType.Name = "txtRmvAccType";
            this.txtRmvAccType.ReadOnly = true;
            this.txtRmvAccType.Size = new System.Drawing.Size(120, 24);
            this.txtRmvAccType.TabIndex = 194;
            // 
            // cboRmvAccCode
            // 
            this.cboRmvAccCode.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboRmvAccCode.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboRmvAccCode.FormattingEnabled = true;
            this.cboRmvAccCode.Location = new System.Drawing.Point(118, 29);
            this.cboRmvAccCode.Name = "cboRmvAccCode";
            this.cboRmvAccCode.Size = new System.Drawing.Size(121, 25);
            this.cboRmvAccCode.TabIndex = 1;
            this.cboRmvAccCode.SelectedIndexChanged += new System.EventHandler(this.cboRmvAccCode_SelectedIndexChanged);
            // 
            // lblRmvAccType
            // 
            this.lblRmvAccType.AutoSize = true;
            this.lblRmvAccType.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRmvAccType.Location = new System.Drawing.Point(6, 74);
            this.lblRmvAccType.Name = "lblRmvAccType";
            this.lblRmvAccType.Size = new System.Drawing.Size(107, 17);
            this.lblRmvAccType.TabIndex = 192;
            this.lblRmvAccType.Text = "Accessory Type";
            // 
            // lblRmvAccCondition
            // 
            this.lblRmvAccCondition.AutoSize = true;
            this.lblRmvAccCondition.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRmvAccCondition.Location = new System.Drawing.Point(244, 76);
            this.lblRmvAccCondition.Name = "lblRmvAccCondition";
            this.lblRmvAccCondition.Size = new System.Drawing.Size(73, 17);
            this.lblRmvAccCondition.TabIndex = 191;
            this.lblRmvAccCondition.Text = "Condition";
            // 
            // lblRmvAccCode
            // 
            this.lblRmvAccCode.AutoSize = true;
            this.lblRmvAccCode.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRmvAccCode.Location = new System.Drawing.Point(6, 29);
            this.lblRmvAccCode.Name = "lblRmvAccCode";
            this.lblRmvAccCode.Size = new System.Drawing.Size(109, 17);
            this.lblRmvAccCode.TabIndex = 190;
            this.lblRmvAccCode.Text = "Accessory Code";
            // 
            // txtRmvAccName
            // 
            this.txtRmvAccName.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRmvAccName.Location = new System.Drawing.Point(322, 29);
            this.txtRmvAccName.Name = "txtRmvAccName";
            this.txtRmvAccName.ReadOnly = true;
            this.txtRmvAccName.Size = new System.Drawing.Size(118, 24);
            this.txtRmvAccName.TabIndex = 189;
            // 
            // lblRmvAccName
            // 
            this.lblRmvAccName.AutoSize = true;
            this.lblRmvAccName.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRmvAccName.Location = new System.Drawing.Point(259, 32);
            this.lblRmvAccName.Name = "lblRmvAccName";
            this.lblRmvAccName.Size = new System.Drawing.Size(46, 17);
            this.lblRmvAccName.TabIndex = 188;
            this.lblRmvAccName.Text = "Name";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dgvRemoveAccessory);
            this.panel1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(12, 234);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(583, 174);
            this.panel1.TabIndex = 173;
            // 
            // dgvRemoveAccessory
            // 
            this.dgvRemoveAccessory.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvRemoveAccessory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRemoveAccessory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvRemoveAccessory.Location = new System.Drawing.Point(0, 0);
            this.dgvRemoveAccessory.Name = "dgvRemoveAccessory";
            this.dgvRemoveAccessory.ReadOnly = true;
            this.dgvRemoveAccessory.Size = new System.Drawing.Size(583, 174);
            this.dgvRemoveAccessory.TabIndex = 0;
            // 
            // frmRemoveAccessory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(607, 420);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lblFName);
            this.ForeColor = System.Drawing.Color.RoyalBlue;
            this.MaximizeBox = false;
            this.Name = "frmRemoveAccessory";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Remove Accessory";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRemoveAccessory)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblFName;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnRmvAccessory;
        private System.Windows.Forms.Button btnRmvAccClear;
        private System.Windows.Forms.TextBox txtRmvAccSize;
        private System.Windows.Forms.TextBox txtRmvAccCondition;
        private System.Windows.Forms.Label lblRmvAccSize;
        private System.Windows.Forms.TextBox txtRmvAccType;
        private System.Windows.Forms.ComboBox cboRmvAccCode;
        private System.Windows.Forms.Label lblRmvAccType;
        private System.Windows.Forms.Label lblRmvAccCondition;
        private System.Windows.Forms.Label lblRmvAccCode;
        private System.Windows.Forms.TextBox txtRmvAccName;
        private System.Windows.Forms.Label lblRmvAccName;
        private System.Windows.Forms.Button btnRmvAccDone;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dgvRemoveAccessory;
    }
}