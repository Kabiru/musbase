﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MusBase
{
    public partial class frmrptBookDetails : Form
    {
        public frmrptBookDetails()
        {
            InitializeComponent();
        }

        private void frmrptBookDetails_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'DataSet1.Book_Details' table. You can move, or remove it, as needed.
            this.Book_DetailsTableAdapter.Fill(this.DataSet1.Book_Details);

            this.reportViewer1.RefreshReport();
        }
    }
}
