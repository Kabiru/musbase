﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MusBase
{
    public partial class frmSplash : Form
    {
        public frmSplash()
        {
            InitializeComponent();
            timer1.Enabled = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Start();
            pgbStart.Increment(+3);
            if (pgbStart.Value < 100)
            {

            }
            else {
                timer1.Stop();
                this.Hide();
                frmLogin login = new frmLogin();
                login.Show();
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void frmSplash_Load(object sender, EventArgs e)
        {

        }

        private void pgbStart_Click(object sender, EventArgs e)
        {

        }
    }
}
