﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MusBase
{
    public partial class frmEventsList : Form
    {
        public frmEventsList()
        {
            InitializeComponent();
        }

        private void frmEventsList_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'DataSet1.Assigned_Inst_View' table. You can move, or remove it, as needed.
            this.Assigned_Inst_ViewTableAdapter.Fill(this.DataSet1.Assigned_Inst_View);

            this.reportViewer1.RefreshReport();
            this.reportViewer1.RefreshReport();
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {

        }
    }
}
