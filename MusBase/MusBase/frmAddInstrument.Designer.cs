﻿namespace MusBase
{
    partial class frmAddInstrument
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAddInstrument));
            this.dgvInstrumentDetails = new System.Windows.Forms.DataGridView();
            this.lblFName = new System.Windows.Forms.Label();
            this.gbxAddInstInstrumentDetails = new System.Windows.Forms.GroupBox();
            this.txtInstCondition = new System.Windows.Forms.TextBox();
            this.txtInstSerialNo = new System.Windows.Forms.TextBox();
            this.cboInstType = new System.Windows.Forms.ComboBox();
            this.cboInstrumentFamily = new System.Windows.Forms.ComboBox();
            this.lblFamily = new System.Windows.Forms.Label();
            this.btnInstSave = new System.Windows.Forms.Button();
            this.btnInstClear = new System.Windows.Forms.Button();
            this.lblInstCondition = new System.Windows.Forms.Label();
            this.lblHouse = new System.Windows.Forms.Label();
            this.txtInstMake = new System.Windows.Forms.TextBox();
            this.lblInstMake = new System.Windows.Forms.Label();
            this.lblInstType = new System.Windows.Forms.Label();
            this.lblInsertStatus = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnAddNew = new System.Windows.Forms.Button();
            this.btnInstClose = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInstrumentDetails)).BeginInit();
            this.gbxAddInstInstrumentDetails.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvInstrumentDetails
            // 
            this.dgvInstrumentDetails.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvInstrumentDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvInstrumentDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvInstrumentDetails.Location = new System.Drawing.Point(0, 0);
            this.dgvInstrumentDetails.Name = "dgvInstrumentDetails";
            this.dgvInstrumentDetails.Size = new System.Drawing.Size(669, 180);
            this.dgvInstrumentDetails.TabIndex = 65;
            this.dgvInstrumentDetails.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvInstrumentDetails_CellContentClick);
            // 
            // lblFName
            // 
            this.lblFName.AutoSize = true;
            this.lblFName.Location = new System.Drawing.Point(37, 77);
            this.lblFName.Name = "lblFName";
            this.lblFName.Size = new System.Drawing.Size(0, 13);
            this.lblFName.TabIndex = 31;
            // 
            // gbxAddInstInstrumentDetails
            // 
            this.gbxAddInstInstrumentDetails.Controls.Add(this.txtInstCondition);
            this.gbxAddInstInstrumentDetails.Controls.Add(this.txtInstSerialNo);
            this.gbxAddInstInstrumentDetails.Controls.Add(this.cboInstType);
            this.gbxAddInstInstrumentDetails.Controls.Add(this.cboInstrumentFamily);
            this.gbxAddInstInstrumentDetails.Controls.Add(this.lblFamily);
            this.gbxAddInstInstrumentDetails.Controls.Add(this.btnInstSave);
            this.gbxAddInstInstrumentDetails.Controls.Add(this.btnInstClear);
            this.gbxAddInstInstrumentDetails.Controls.Add(this.lblInstCondition);
            this.gbxAddInstInstrumentDetails.Controls.Add(this.lblHouse);
            this.gbxAddInstInstrumentDetails.Controls.Add(this.txtInstMake);
            this.gbxAddInstInstrumentDetails.Controls.Add(this.lblInstMake);
            this.gbxAddInstInstrumentDetails.Controls.Add(this.lblInstType);
            this.gbxAddInstInstrumentDetails.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxAddInstInstrumentDetails.Location = new System.Drawing.Point(12, 36);
            this.gbxAddInstInstrumentDetails.Name = "gbxAddInstInstrumentDetails";
            this.gbxAddInstInstrumentDetails.Size = new System.Drawing.Size(463, 197);
            this.gbxAddInstInstrumentDetails.TabIndex = 66;
            this.gbxAddInstInstrumentDetails.TabStop = false;
            this.gbxAddInstInstrumentDetails.Text = "Instrument Details";
            this.gbxAddInstInstrumentDetails.Enter += new System.EventHandler(this.gbxAddInstInstrumentDetails_Enter);
            // 
            // txtInstCondition
            // 
            this.txtInstCondition.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInstCondition.Location = new System.Drawing.Point(104, 104);
            this.txtInstCondition.Name = "txtInstCondition";
            this.txtInstCondition.Size = new System.Drawing.Size(120, 24);
            this.txtInstCondition.TabIndex = 5;
            this.txtInstCondition.TextChanged += new System.EventHandler(this.txtInstCondition_TextChanged);
            // 
            // txtInstSerialNo
            // 
            this.txtInstSerialNo.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInstSerialNo.Location = new System.Drawing.Point(105, 68);
            this.txtInstSerialNo.Name = "txtInstSerialNo";
            this.txtInstSerialNo.Size = new System.Drawing.Size(120, 24);
            this.txtInstSerialNo.TabIndex = 3;
            this.txtInstSerialNo.TextChanged += new System.EventHandler(this.txtInstSerialNo_TextChanged);
            // 
            // cboInstType
            // 
            this.cboInstType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboInstType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboInstType.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cboInstType.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboInstType.FormattingEnabled = true;
            this.cboInstType.Location = new System.Drawing.Point(310, 24);
            this.cboInstType.Name = "cboInstType";
            this.cboInstType.Size = new System.Drawing.Size(117, 25);
            this.cboInstType.TabIndex = 2;
            this.cboInstType.SelectedIndexChanged += new System.EventHandler(this.cboInstType_SelectedIndexChanged);
            // 
            // cboInstrumentFamily
            // 
            this.cboInstrumentFamily.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboInstrumentFamily.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboInstrumentFamily.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cboInstrumentFamily.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboInstrumentFamily.FormattingEnabled = true;
            this.cboInstrumentFamily.Location = new System.Drawing.Point(104, 25);
            this.cboInstrumentFamily.Name = "cboInstrumentFamily";
            this.cboInstrumentFamily.Size = new System.Drawing.Size(121, 25);
            this.cboInstrumentFamily.TabIndex = 1;
            this.cboInstrumentFamily.SelectedIndexChanged += new System.EventHandler(this.cboInstrumentFamily_SelectedIndexChanged);
            // 
            // lblFamily
            // 
            this.lblFamily.AutoSize = true;
            this.lblFamily.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFamily.Location = new System.Drawing.Point(25, 24);
            this.lblFamily.Name = "lblFamily";
            this.lblFamily.Size = new System.Drawing.Size(51, 17);
            this.lblFamily.TabIndex = 69;
            this.lblFamily.Text = "Family";
            // 
            // btnInstSave
            // 
            this.btnInstSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInstSave.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInstSave.Image = global::MusBase.Properties.Resources.folder_save_32;
            this.btnInstSave.Location = new System.Drawing.Point(105, 140);
            this.btnInstSave.Name = "btnInstSave";
            this.btnInstSave.Size = new System.Drawing.Size(104, 42);
            this.btnInstSave.TabIndex = 6;
            this.btnInstSave.Text = "Save";
            this.btnInstSave.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnInstSave.UseVisualStyleBackColor = true;
            this.btnInstSave.Click += new System.EventHandler(this.btnInstAdd_Click);
            // 
            // btnInstClear
            // 
            this.btnInstClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInstClear.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInstClear.Image = global::MusBase.Properties.Resources.folder_write_32;
            this.btnInstClear.Location = new System.Drawing.Point(310, 140);
            this.btnInstClear.Name = "btnInstClear";
            this.btnInstClear.Size = new System.Drawing.Size(104, 42);
            this.btnInstClear.TabIndex = 7;
            this.btnInstClear.Text = "&Clear";
            this.btnInstClear.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnInstClear.UseVisualStyleBackColor = true;
            this.btnInstClear.Click += new System.EventHandler(this.btnInstClear_Click);
            // 
            // lblInstCondition
            // 
            this.lblInstCondition.AutoSize = true;
            this.lblInstCondition.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInstCondition.Location = new System.Drawing.Point(25, 106);
            this.lblInstCondition.Name = "lblInstCondition";
            this.lblInstCondition.Size = new System.Drawing.Size(73, 17);
            this.lblInstCondition.TabIndex = 68;
            this.lblInstCondition.Text = "Condition";
            // 
            // lblHouse
            // 
            this.lblHouse.AutoSize = true;
            this.lblHouse.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHouse.Location = new System.Drawing.Point(6, 69);
            this.lblHouse.Name = "lblHouse";
            this.lblHouse.Size = new System.Drawing.Size(97, 17);
            this.lblHouse.TabIndex = 67;
            this.lblHouse.Text = "Serial Number";
            // 
            // txtInstMake
            // 
            this.txtInstMake.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInstMake.Location = new System.Drawing.Point(310, 67);
            this.txtInstMake.Name = "txtInstMake";
            this.txtInstMake.Size = new System.Drawing.Size(117, 24);
            this.txtInstMake.TabIndex = 4;
            this.txtInstMake.TextChanged += new System.EventHandler(this.txtInstMake_TextChanged_1);
            // 
            // lblInstMake
            // 
            this.lblInstMake.AutoSize = true;
            this.lblInstMake.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInstMake.Location = new System.Drawing.Point(250, 69);
            this.lblInstMake.Name = "lblInstMake";
            this.lblInstMake.Size = new System.Drawing.Size(43, 17);
            this.lblInstMake.TabIndex = 66;
            this.lblInstMake.Text = "Make";
            // 
            // lblInstType
            // 
            this.lblInstType.AutoSize = true;
            this.lblInstType.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInstType.Location = new System.Drawing.Point(250, 25);
            this.lblInstType.Name = "lblInstType";
            this.lblInstType.Size = new System.Drawing.Size(39, 17);
            this.lblInstType.TabIndex = 65;
            this.lblInstType.Text = "Type";
            // 
            // lblInsertStatus
            // 
            this.lblInsertStatus.AutoSize = true;
            this.lblInsertStatus.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInsertStatus.Location = new System.Drawing.Point(12, 241);
            this.lblInsertStatus.Name = "lblInsertStatus";
            this.lblInsertStatus.Size = new System.Drawing.Size(0, 17);
            this.lblInsertStatus.TabIndex = 205;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.dgvInstrumentDetails);
            this.panel1.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(12, 307);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(669, 180);
            this.panel1.TabIndex = 207;
            // 
            // btnAddNew
            // 
            this.btnAddNew.Font = new System.Drawing.Font("Tempus Sans ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddNew.Image = global::MusBase.Properties.Resources.folder_add_32;
            this.btnAddNew.Location = new System.Drawing.Point(117, 241);
            this.btnAddNew.Name = "btnAddNew";
            this.btnAddNew.Size = new System.Drawing.Size(104, 42);
            this.btnAddNew.TabIndex = 8;
            this.btnAddNew.Text = "Add new";
            this.btnAddNew.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnAddNew.UseVisualStyleBackColor = true;
            this.btnAddNew.Click += new System.EventHandler(this.btnAddNew_Click);
            // 
            // btnInstClose
            // 
            this.btnInstClose.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInstClose.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.btnInstClose.Image = global::MusBase.Properties.Resources._1427990866_Done;
            this.btnInstClose.Location = new System.Drawing.Point(586, 241);
            this.btnInstClose.Name = "btnInstClose";
            this.btnInstClose.Size = new System.Drawing.Size(95, 44);
            this.btnInstClose.TabIndex = 9;
            this.btnInstClose.Text = "&Quit";
            this.btnInstClose.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnInstClose.UseVisualStyleBackColor = true;
            this.btnInstClose.Click += new System.EventHandler(this.btnInstClose_Click_1);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Location = new System.Drawing.Point(322, 241);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(104, 42);
            this.btnUpdate.TabIndex = 208;
            this.btnUpdate.Text = "&Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // frmAddInstrument
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.Menu;
            this.ClientSize = new System.Drawing.Size(702, 501);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnAddNew);
            this.Controls.Add(this.lblInsertStatus);
            this.Controls.Add(this.btnInstClose);
            this.Controls.Add(this.gbxAddInstInstrumentDetails);
            this.Controls.Add(this.lblFName);
            this.ForeColor = System.Drawing.Color.RoyalBlue;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmAddInstrument";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Instrument Details";
            this.Load += new System.EventHandler(this.frmAddInstrument_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvInstrumentDetails)).EndInit();
            this.gbxAddInstInstrumentDetails.ResumeLayout(false);
            this.gbxAddInstInstrumentDetails.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvInstrumentDetails;
        private System.Windows.Forms.Label lblFName;
        private System.Windows.Forms.GroupBox gbxAddInstInstrumentDetails;
        private System.Windows.Forms.TextBox txtInstSerialNo;
        private System.Windows.Forms.ComboBox cboInstType;
        private System.Windows.Forms.ComboBox cboInstrumentFamily;
        private System.Windows.Forms.Label lblFamily;
        private System.Windows.Forms.Button btnInstSave;
        private System.Windows.Forms.Button btnInstClear;
        private System.Windows.Forms.Label lblInstCondition;
        private System.Windows.Forms.Label lblHouse;
        private System.Windows.Forms.TextBox txtInstMake;
        private System.Windows.Forms.Label lblInstMake;
        private System.Windows.Forms.Label lblInstType;
        private System.Windows.Forms.Button btnInstClose;
        private System.Windows.Forms.TextBox txtInstCondition;
        private System.Windows.Forms.Label lblInsertStatus;
        private System.Windows.Forms.Button btnAddNew;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnUpdate;
    }
}